Documentation
...     something here

*** Settings ***
Resource          resource/resources.robot
Suite Teardown    Close Browser
Test Setup    Open Leo Site
Test Teardown    Close Browser

*** Test Cases ***
#####Validate Leo Desktop
Validate Exercise - Text - Freehand Drawing
    [Tags]    leo_monitor    exercise
    Validate Exercise - Text - Freehand Drawing

Validate Exercise - Text - Exercise Instruction
    [Tags]    leo_monitor    exercise
    Validate Exercise - Text - Exercise Instruction

Validate Exercise - Text - Mind Map
    [Tags]    leo_monitor    exercise
    Validate Exercise - Text - Mind Map

Validate Exercise - Arrange
    [Tags]    leo_monitor    exercise
    Validate Exercise - Arrange

Validate Exercise - Arrange (Shuffled)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Arrange (Shuffled)

Validate Exercise - Arrange (Assets)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Arrange (Assets)

Validate Exercise - Choice (Single)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Choice (Single)

Validate Exercise - Choice (Multiple)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Choice (Multiple)

Validate Exercise - Choice (Math)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Choice (Math)

Validate Exercise - Choice (Multiple - Any Answer Accepted)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Choice (Multiple - Any Answer Accepted)

Validate Exercise - Fill In - Drop (Deprecated)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Fill In - Drop (Deprecated)

Validate Exercise - Fill In - Open Question Long Response
    [Tags]    leo_monitor    exercise
    Validate Exercise - Fill In - Open Question Long Response

Validate Exercise - Match - Cloze Combi (Hidegaps) ===> NOT finished!!!
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Cloze Combi (Hidegaps)

Validate Exercise - Match - Cloze Combi - Unscored (Without Gap Match)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Cloze Combi - Unscored (Without Gap Match)

Validate Exercise - Match - Cloze Combi (Mixed Score Answers) (Without Gap Match)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Cloze Combi (Mixed Score Answers) (Without Gap Match)

Validate Exercise - Match - Cloze Combi (Math Rewrite)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Cloze Combi (Math Rewrite)

Validate Exercise - Match - Cloze Combi (Math With Fractions)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Cloze Combi (Math With Fractions)

Validate Exercise - Match - Gap Match (Deprecated)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Gap Match (Deprecated)

Validate Exercise - Match - Single Response
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Single Response

Validate Exercise - Match - Multiple Response
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Multiple Response

Validate Exercise - Match - Matrix
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Matrix

Validate Exercise - Match - Matrix (Multiple Columns, Assets) ====> CLICKING A VIDEO ELEMENT NOT WORKING
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Matrix (Multiple Columns, Assets)

Validate Exercise - Match - Table (Many Gaps)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Table (Many Gaps)

Validate Exercise - Match - Drag Image (Hidegaps)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Drag Image (Hidegaps)

Validate Exercise - Match - Drag Image (Hidegaps) (Unscored)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Drag Image (Hidegaps) (Unscored)

Validate Exercise - Match - Drag Image (Fillin)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Drag Image (Fillin)

Validate Exercise - Play - Dialogue (aka. Discussion)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Play - Dialogue (aka. Discussion)

Validate Exercise - Play - Dialogue (Person B)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Play - Dialogue (Person B)

Validate Exercise - Play - Wordsearch
    [Tags]    leo_monitor    exercise
    Validate Exercise - Play - Wordsearch

Validate Exercise - Play - Wordsearch (Assets)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Play - Wordsearch (Assets)

Validate Exercise - Right Option (Deprecated)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Right Option (Deprecated)

Validate Exercise - Select Words
    [Tags]    leo_monitor    exercise
    Validate Exercise - Select Words

Validate Exercise - Select Words Multiple
    [Tags]    leo_monitor    exercise
    Validate Exercise - Select Words Multiple

# #Validate Exercise - Select Words Multiple DD-1248 ===> NOT finished!!!
Validate Exercise - Select Words Multiple DD-1248
    [Tags]    leo_monitor    exercise
    Validate Exercise - Select Words Multiple DD-1248

Validate Exercise - Match - Drag Image (Unscored)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Drag Image (Unscored)

Validate Exercise - Match - Drag Image (Fillin) (Unscored)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Drag Image (Fillin) (Unscored)

Validate Exercise - Match - Table Match (Unscored)
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Table Match (Unscored)

# #Validate Exercise - Match - Table Match ===> NOT finished!!!
Validate Exercise - Match - Table Match
    [Tags]    leo_monitor    exercise
    Validate Exercise - Match - Table Match

Validate Exercise - Fill In - Open Question Short Response =====> NOTE: an additional step added to be able to test the extra panel in the end
    [Tags]    leo_monitor    exercise
    Validate Exercise - Fill In - Open Question Short Response

Validate Exercise - Select - Math
    [Tags]    leo_monitor    exercise
    Validate Exercise - Select - Math

Validate Exercise - Select - Math 2
    [Tags]    leo_monitor    exercise
    Validate Exercise - Select - Math 2


# Chapters
Validate Chapter - Text A + Notes + Popup Image
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text A + Notes + Popup Image

Validate Chapter - Text A Extra
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text A Extra

Validate Chapter - Text B
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text B

Validate Chapter - Text B Extra
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text B Extra

Validate Chapter - Text C
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text C

Validate Chapter - Text C Extra
    [Tags]    leo_monitor    chapter
    Validate Chapter - Text C Extra

Validate Chapter - Validate Story
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Story

Validate Chapter - Validate Story Extra
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Story Extra

Validate Chapter - Validate Article
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Article

Validate Chapter - Validate Article Extra
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Article Extra

Validate Chapter - Validate Song
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Song

Validate Chapter - Validate Dialogue
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Dialogue

Validate Chapter - Validate Grammar And Grammar Annotation
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Grammar And Grammar Annotation

Validate Chapter - Validate Phonetics
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Phonetics

Validate Chapter - Validate Image Carousel And Flashcard
    [Tags]    leo_monitor    chapter
    Validate Chapter - Validate Image Carousel And Flashcard


# desktop specifics (Validate LEO desktop specifics)
Navigate To First Chapter In Test Outline
    [Tags]    leo_monitor    desktop_specifics
    Navigate To First Chapter In Test Outline

Validate Arkku Link In Text A
    [Tags]    leo_monitor    desktop_specifics
    Validate Arkku Link In Text A

Navigate To Second Chapter In Test Outline (desktop)
    [Tags]    leo_monitor    desktop_specifics
    Navigate To Second Chapter In Test Outline

Validate First Image In Story
    [Tags]    leo_monitor    desktop_specifics
    Validate First Image In Story

Validate Exercise - Play Crossword (Desktop)
    [Tags]    leo_monitor    desktop_specifics
    Validate Exercise - Play Crossword (Desktop)

# desktop specifics (Validate LEO desktop specifics)
Navigate To Second Chapter In Test Outline (mobile)
    [Tags]    leo_monitor    mobile_specifics
    Navigate To Second Chapter In Test Outline Mobile

Validate Hotspot Startpage (mobile)
    [Tags]    leo_monitor    mobile_specifics
    Validate Hotspot Startpage Mobile

Validate Story
    [Tags]    leo_monitor    mobile_specifics
    Validate Story

Validate Exercise - Match - Multiple Response (Assets) ===> NOTE: video playing causes problems
    [Tags]    leo_monitor    mobile_specifics
    Validate Exercise - Match - Multiple Response (Assets)

Validate Exercise - Play Crossword (Mobile)
    [Tags]    leo_monitor    desktop_specifics
    Validate Exercise - Play Crossword (Mobile)
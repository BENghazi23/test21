*** Settings ***
Library     SeleniumLibrary    timeout=20.0    run_on_failure=seleniumlibrary common failure
Library     Screenshot
Library     DateTime
Library     String
Library     Collections


*** Variables ***
&{screen_size_desktop}              width=1280    height=720
&{screen_size_mobile}               width=320    height=480
&{screen_size_tablet_landscape}     width=924    height=630
&{screen_size_tablet_portrait}      width=630    height=924

${question_shown}

${leo_base}                         https://leo-backend-stg.herokuapp.com/
${leo_site}                         https://leo-backend-stg.herokuapp.com/api/login

${leo_site_user}                    UseTraceTesterOM
${leo_site_password}                2W8BSRcze3755Kq
${leo_exercise_1}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/59a6c9efca8eb256cacd2636
${leo_exercise_1_sectionId}         AD6B8B96-91B7-4D95-8F8F-55CC30F6E7B1
${leo_exercise_1_type}              drawing

${leo_exercise_2}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/5845828b23b1bc0ad82671eb
${leo_exercise_2_sectionId}         DEDD4389-531C-4A52-96AB-3871FD94E372
${leo_exercise_2_type}              exerciseInstruction

${leo_exercise_3}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/584537af23b1bc0ad8267161
${leo_exercise_3_sectionId}         4AE8DE0D-C7E5-454A-AA74-F22F4108C9B2
${leo_exercise_3_type}              mind-map

${leo_exercise_4}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d93669b8b24476a8ff88cd
${leo_exercise_4_sectionId}         251EF89A-7F54-45D6-8DF9-90247342C941
${leo_exercise_4_type}              exerciseInstruction

${leo_exercise_5}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58e4c0d14e256e286e1c6868
${leo_exercise_5_sectionId}         F2BAAC8B-92BC-450D-B29D-8B5C82977E97
${leo_exercise_5_type}              exerciseInstruction

${leo_exercise_6}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/59391905ca8eb22a64d0b56d
${leo_exercise_6_sectionId}         16233731-10BB-431E-91A1-29215C4857F6
${leo_exercise_6_type}              arrange

${leo_exercise_7}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d94071fbf9be5f044c9794
${leo_exercise_7_sectionId}         D3FC2985-BB8C-4351-8140-92FBA7557A52
${leo_exercise_7_type}              exerciseInstruction

${leo_exercise_8}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d93d42fbf9be5f044c978c
${leo_exercise_8_sectionId}         0F366BE4-46AA-449F-8E42-89639714E6B1
${leo_exercise_8_type}              exerciseInstruction

${leo_exercise_9}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d940e7b8b24476a8ff88e8
${leo_exercise_9_sectionId}         A603B617-9EA6-4A87-ABFE-28DCCEA7F5A2
${leo_exercise_9_type}              exerciseInstruction

${leo_exercise_10}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58171f5658d2dc2172485cd9
${leo_exercise_10_sectionId}         1564B16B-67B9-49A9-B440-4688C6DF364A
${leo_exercise_10_type}              exerciseInstruction

${leo_exercise_11}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d93789b8b24476a8ff88d0
${leo_exercise_11_sectionId}         25E9C112-7F4B-4895-96B4-E532140AD82A
${leo_exercise_11_type}              exerciseInstruction

${leo_exercise_12}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f74889b8b2441f5e21c599
${leo_exercise_12_sectionId}         A82C5E09-4BE2-4832-B91A-74A862E8D88F
${leo_exercise_12_type}              exerciseInstruction

${leo_exercise_13}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58903d3e23b1bc53c4bd5ff3
${leo_exercise_13_sectionId}         4123C27B-A480-4E4B-9909-B89D1BC13C5E
${leo_exercise_13_type}              cloze-combi-hide-gaps

${leo_exercise_14}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58186404e7216521cc18a126
${leo_exercise_14_sectionId}         68AEE908-B0C1-4740-ABE6-226FFBAF5AF3
${leo_exercise_14_type}              cloze-combi

${leo_exercise_15}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/5821c62be7216562b6abe322
${leo_exercise_15_sectionId}         BAC5F7DF-3C8C-4BE8-8540-E3B3EAF71B03
${leo_exercise_15_type}              cloze-combi

${leo_exercise_16}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58c014a653bc3c1212fdde9d
${leo_exercise_16_sectionId}         7D135413-52FB-4ACE-9C2C-4F703A0F364B
${leo_exercise_16_type}              cloze-combi

${leo_exercise_17}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58d10fd253bc3c64ecf215cf
${leo_exercise_17_sectionId}         E434B428-6B64-42B8-B9F0-78C7F34BD408
${leo_exercise_17_type}              cloze-combi

${leo_exercise_18}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d93b175147455eb9c8a55e
${leo_exercise_18_sectionId}         D04AA0D2-CE6C-41BC-8CC9-F8F6ED85F4FB
${leo_exercise_18_type}              exerciseInstruction

${leo_exercise_19}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d9618cfbf9be5f044c97a6
${leo_exercise_19_sectionId}         EE1F8A17-44A0-4598-A6F0-3ED49BED0291
${leo_exercise_19_type}              exerciseInstruction

${leo_exercise_20}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d937086334735df4f36a3c
${leo_exercise_20_sectionId}         D9999F53-2B2A-4454-92EB-F5F5A79083AD
${leo_exercise_20_type}              exerciseInstruction

${leo_exercise_21}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d924536334735df4f36a34
${leo_exercise_21_sectionId}         23ADCD35-2112-4780-865D-B9D0FA6CD9A0
${leo_exercise_21_type}              exerciseInstruction

${leo_exercise_22}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/5937aa3fca8eb22a64d0ad79
${leo_exercise_22_sectionId}         59B50D0A-97BB-46A5-91F1-5934952E08A4
${leo_exercise_22_type}              match-matrix

${leo_exercise_23}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/589d8163e721651b276a094c
${leo_exercise_23_sectionId}         AD1DA8B6-C524-47B8-AE7B-0DA642596F9C
${leo_exercise_23_type}              drop-table

${leo_exercise_24}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f7484c63347308ab311c18?cache_buster=1481631230072
${leo_exercise_24_sectionId}         FD9FB8B6-A0F7-4699-BE1E-8AA890BD427A
${leo_exercise_24_type}              exerciseInstruction

${leo_exercise_25}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58d0f2cc4e256e710f1601b0
${leo_exercise_25_sectionId}         E925B2FC-EE8F-4D05-A287-1C2BB908F03D
${leo_exercise_25_type}              exerciseInstruction

${leo_exercise_26}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/583801fa23b1bc096766e5a0?cache_buster=1481625898346
${leo_exercise_26_sectionId}         4C3664DB-C031-4397-BA82-D830D79EA044
${leo_exercise_26_type}              exerciseInstruction

${leo_exercise_27}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f749b3b8b2441f5e21c59b
${leo_exercise_27_sectionId}         B333D772-1956-4242-8006-9B7986364DF2
${leo_exercise_27_type}              exerciseInstruction

${leo_exercise_28}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f7499c63347308ab311c1c
${leo_exercise_28_sectionId}         1A09AAE0-CFBC-4C7E-914F-DA4938E9082C
${leo_exercise_28_type}              exerciseInstruction

${leo_exercise_29}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/595e154f0ac8ab6cf3373f23
${leo_exercise_29_sectionId}         5A339110-05FB-486E-A2F6-F4943E1D00F9
${leo_exercise_29_type}              exerciseInstruction

${leo_exercise_30}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d93af4fbf9be5f044c9786
${leo_exercise_30_sectionId}         A0B8E205-0A95-4838-AA43-EF3B1A48D866
${leo_exercise_30_type}              exerciseInstruction

${leo_exercise_31}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f7496a51474549d984bd2b
${leo_exercise_31_sectionId}         D5F3F191-FB42-4325-B79F-38C1007E6848
${leo_exercise_31_type}              exerciseInstruction

${leo_exercise_32}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/587744fbe721657944e186d3
${leo_exercise_32_sectionId}         A5BE6197-ECF9-4948-BBB0-1DCE0417A15E
${leo_exercise_32_type}              exerciseInstruction

${leo_exercise_33}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58de56064e256e29788cd5d0
${leo_exercise_33_sectionId}         22680174-21F6-4D8B-A7F5-A0FBDD9841EA
${leo_exercise_33_type}              exerciseInstruction

${leo_exercise_34}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58d0ef7c4e256e710f1601a7
${leo_exercise_34_sectionId}         725B5CC3-D556-4D75-9770-983999E6BE0D
${leo_exercise_34_type}              exerciseInstruction

${leo_exercise_35}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58d0f04cca8eb262d1a23415
${leo_exercise_35_sectionId}         ACAE1C75-10D4-4F41-BA2F-8BEF0042C3C2
${leo_exercise_35_type}              exerciseInstruction

${leo_exercise_36}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/58d122ccca8eb262d1a234f5
${leo_exercise_36_sectionId}         EBA745BB-452A-49C9-A8BF-DFE66B1F9A5F
${leo_exercise_36_type}              exerciseInstruction

${leo_exercise_37}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d9467afbf9be5f044c979d
${leo_exercise_37_sectionId}         EAC83AFA-E132-40EF-BDE1-D6249E7CBE04
${leo_exercise_37_type}              exerciseInstruction

${leo_exercise_38}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57d7adf46334735df4f31530
${leo_exercise_38_sectionId}         D0A846B0-8B29-4FC4-9BB3-7D72D8F84F5A
${leo_exercise_38_type}              exerciseInstruction

${leo_exercise_39}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/57f749c351474549d984bd34
${leo_exercise_39_sectionId}         0A462A72-928B-48F7-AD96-C65EE2AB0439
${leo_exercise_39_type}              exerciseInstruction

${leo_exercise_40}                   outlines/57cd600cb8b2442b1eed29c9/contentitems/599bb8400ac8ab5adc47f6b4
${leo_exercise_40_sectionId}         D31FA18E-0701-409E-ADE8-A1826F8F0DF4
${leo_exercise_40_type}              exerciseInstruction

# chapters
${leo_chapter_1}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_1_sectionId}         0C85BAB7-F328-462A-97B3-E5AA27EADDCB
${leo_chapter_1_type}              text

${leo_chapter_2}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_2_sectionId}         487EB64A-560D-465E-912A-304C0A3DD5FE
${leo_chapter_2_type}              extra

${leo_chapter_3}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_3_sectionId}         4AF511B6-9768-4F35-BDF4-4B9A270FA472
${leo_chapter_3_type}              text

${leo_chapter_4}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_4_sectionId}         C041F5A2-A919-4B63-A61D-08AAC9F6CB41
${leo_chapter_4_type}              extra

${leo_chapter_5}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_5_sectionId}         36D8D22D-8700-47F3-8B6B-419CCEDCE7F6
${leo_chapter_5_type}              text

${leo_chapter_6}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd60cbfbf9be17e0afe476
${leo_chapter_6_sectionId}         FA26812D-CFFD-4C91-9A6D-5ADC1EE851D6
${leo_chapter_6_type}              extra

${leo_chapter_7}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_7_sectionId}         97FE1260-1948-4D8C-9BC4-5BE83DD87F41
${leo_chapter_7_type}              text

${leo_chapter_8}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_8_sectionId}         97FE1260-1948-4D8C-9BC4-5BE83DD87F41
${leo_chapter_8_type}              extra

${leo_chapter_9}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_9_sectionId}         E0C2E8D8-50DA-4A4D-B7A5-9B14831C8DC4
${leo_chapter_9_type}              text

${leo_chapter_10}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_10_sectionId}         8B4F8FB4-2E11-4172-B615-8CDF1AB1EF10
${leo_chapter_10_type}              extra

${leo_chapter_11}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_11_sectionId}         993847B0-FDE5-4C42-A6E0-4FC336B15977
${leo_chapter_11_type}              text

${leo_chapter_12}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_12_sectionId}         CC2F548C-24E2-4AE6-88CB-91C7380E14F5
${leo_chapter_12_type}              text

${leo_chapter_13}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_13_sectionId}         A5817B7B-945A-475F-BB1D-853777562CE1
${leo_chapter_13_type}              text

${leo_chapter_14}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_14_sectionId}         212F240D-E12B-4FFC-A2AB-689A1309920E
${leo_chapter_14_type}              text
${leo_chapter_14_phonetics}         ${True}

${leo_chapter_15}                   outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c
${leo_chapter_15_sectionId}         4C89C9AF-1D4A-4457-8892-AF117EA02D48
${leo_chapter_15_type}              text
${leo_chapter_15_flashcard}         ${True}

${leo_chapter_desktop_2}            outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c

${leo_chapter_desktop_3}            outlines/57cd600cb8b2442b1eed29c9/contentitems/57f7498e63347308ab311c1a

${leo_chapter_mobile_1}             outlines/57cd600cb8b2442b1eed29c9/chapters/57cd62a35147451730cc893c

${leo_chapter_mobile_2}             outlines/57cd600cb8b2442b1eed29c9/contentitems/595d01123f929063e9e04521



*** Keywords ***
Open Leo Site
    Open Site    ${leo_site}    ${browser}    ${device}
    Login Leo


Open Site
    [Arguments]   ${site}    ${browser}    ${device}=desktop
    Open Browser    ${site}    ${browser}    alias=MAIN_WINDOW    desired_capabilities=devtools.jsonview.enabled:${False}
    ${width}    Get From Dictionary    ${screen_size_${device}}    width
    ${height}    Get From Dictionary    ${screen_size_${device}}    height
    Set Window Size    ${width}    ${height}


Login Leo
    Wait Until Element Is Enabled    xpath=.//input[@name="username"]
    Input Text    xpath=.//input[@name="username"]    ${leo_site_user}
    Input Text    xpath=.//input[@name="password"]    ${leo_site_password}
    Click Element On Page    xpath=.//input[@value='Log In']
    Wait Until Page Contains Element    xpath=.//pre
    Wait Until Page Contains    "userid":"${leo_site_user}"


Set Section Elements
    ${section_element}    Get Webelement    xpath=.//section
    Set Test Variable    ${section_element}
    ${section_id}    Get Element Attribute    ${section_element}    id
    Set Test Variable    ${section_id}
    ${section_data_type}    Get Element Attribute    ${section_element}    data-type
    Set Test Variable    ${section_data_type}


Get SessionStorage
    [Arguments]    ${section_id}
    ${sessionStorage}    Execute JavaScript    var answers = sessionStorage.getItem("scorm-results:${section_id}");  return answers;
    ${sessionStorage_string}    Convert To String    ${sessionStorage}
    Should Be True    '${sessionStorage_string}'!='None'
    [Return]    ${sessionStorage_string}

Check Value In SessionStorage
    [Arguments]    ${section_id}    ${string_to_search}
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Contain    ${sessionStorage_string}    ${string_to_search}

Click Element On Page
    [Arguments]    ${element}
    Wait Until Element Is Enabled    ${element}
    Click Element    ${element}


Validate Exercise - Text - Freehand Drawing
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_1}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//span[@id="es:F2A5C3CA-856B-46A8-9459-99A1967616A7"]/i
    Wait Until Element Is Enabled    xpath=.//p[@id="418EA14D-E066-49E2-922B-138E475035B1"]/a
    Wait Until Element Is Enabled    xpath=.//p[@id="1D7A4291-86AC-4ED2-A408-063A23712158"]/a
    Wait Until Element Is Enabled    xpath=.//div[@class="control-button-container"]//button[text()="Tallenna"]
    Wait Until Element Is Enabled    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//form[@class="toolbar"]//button[@id="diagram-draw"]
    Wait Until Element Is Enabled    xpath=.//form[@class="toolbar"]//button[@id="diagram-select"]
    Wait Until Element Is Enabled    xpath=.//div[@id="drawingDiagramDiv"]/canvas
    Wait Until Element Is Enabled    xpath=.//form[@class="toolbar"]//input[@id="diagram-thickness" and @placeholder="5"]
    Input Text    xpath=.//form[@class="toolbar"]//input[@id="diagram-thickness"]    2
    Click Element On Page    xpath=.//form[@class="toolbar"]//button[@id="diagram-color"]
    Click Element On Page    xpath=.//div[@id="diagram-colorpicker"]//div[@class="Scp-hue"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//button[@id="diagram-select"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Tallenna"]
    Sleep    1
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    Sleep    1
    ${s_someValue}    Execute JavaScript    sessionStorage.getItem("scorm-results: AD6B8B96-91B7-4D95-8F8F-55CC30F6E7B1");
    ${s_someValue}    Execute JavaScript    sessionStorage.getItem("scorm-results:AD6B8B96-91B7-4D95-8F8F-55CC30F6E7B1");
    Execute JavaScript    for(var i in sessionStorage) {console.log(i + ' = ' + localStorage[i]);}
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Korjaa"]
    Check Extra Panel


Validate Exercise - Text - Exercise Instruction
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_2}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Text - Exercise Instruction"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//span[@id="es:CD5B44B4-5809-48ED-A415-1B5FB745303F"]/i
    Wait Until Element Is Enabled    xpath=.//span[@id="es:83B45A4D-1762-42AD-92AE-376C1C72A4A2"]//i
    Wait Until Element Is Enabled    xpath=.//span[@id="es:F0CFCF40-D2BC-437B-A8A2-940668946C07"]//i
    Wait Until Element Is Enabled    xpath=.//blockquote//p[text()="Like blockquotes!"]
    ${innerHTML}    Get Element Attribute    xpath=.//div[@data-content-type="exercise-instruction"]//blockquote    innerHTML
    Should Contain    ${innerHTML}    <svg
    ${sessionStorage}    Execute JavaScript    var answers = sessionStorage.getItem("scorm-results:AD6B8B96-91B7-4D95-8F8F-55CC30F6E7B1");  return answers;
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    ${sessionStorage}    Execute JavaScript    var answers = sessionStorage.getItem("scorm-results:AD6B8B96-91B7-4D95-8F8F-55CC30F6E7B1");  return answers;
    Sleep    2
    ${sessionStorage}    Execute JavaScript    var answers = sessionStorage.getItem("scorm-results:DEDD4389-531C-4A52-96AB-3871FD94E372");  return answers;
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="infotext" and contains(text(), "Tehtävä on suoritettu")]
    Check Extra Panel
    Click Element On Page    xpath=.//*[@id="es:219C56E4-7F5F-42AB-8A14-5E0E468CC013"]/i
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-inner"]//img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-inner"]//img    naturalWidth
    Should Be True    ${width} > 0


Validate Exercise - Text - Mind Map
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_3}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Text - Mind map"]
    Set Section Elements
    ${section_inner_html}    Get Element Attribute    ${section_element}    innerHTML
    Wait Until Element Is Enabled    xpath=.//a[contains(text(), "arkku-hs-link")]
    Wait Until Element Is Enabled    xpath=.//div[@class="control-button-container"]//button[text()="Tallenna"]
    Wait Until Element Is Enabled    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    Click Element On Page    xpath=.//*[@id="diagram-zout"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Tallenna"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Tallenna"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//div[@class="control-button-container"]//button[text()="Valmis"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Contain    ${sessionStorage_string}    "completed":true
    Check Extra Panel


Validate Exercise - Arrange
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_4}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="0"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//div[@class="feedback incorrect"]/p[text()="Bad job ._."]
    Wait Until Element Is Enabled    xpath=.//div[@id="items"]//div//i[@class="icon-wrong"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@id="items"]//div//i[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Check Extra Panel


Validate Exercise - Arrange (Shuffled)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_5}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Contain    ${sessionStorage_string}    "raw":0
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="0"]
    Wait Until Element Is Enabled    xpath=.//div[@id="items"]//div//i[@class="icon-wrong"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@id="items"]//div//i[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]


Validate Exercise - Arrange (Assets)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_6}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@id="items"]//*[@id="es:8F3D6BA4-3172-4FFF-8C6B-DB69EA3BC8CC"]//img
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@id="items"]//*[@id="es:75184E52-076F-4250-9F49-DA6008E4F3E8"]//img
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@id="items"]//*[@id="es:F0B24169-D2E7-44AA-92EA-65A039D3212F"]/span[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@id="items"]//*[@id="es:7440550A-A65A-4CB9-9042-39DA9A7BAAF6"]/span[@class="video-container"]/video
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()[not(.="")]]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="alternativeHeader" and text()="Vaihtoehto 1:"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="alternativeHeader" and text()="Vaihtoehto 2:"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@id="BDC80647-D622-42B2-BC64-D56C5407C22E"]//i[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="image-container"]//img
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//span[@class="image-container"]//i[@class="icon-zoom-plus"]    innerHTML
    Mouse Over    xpath=.//*[@id="${section_id}"]//span[@class="image-container"]//img
    Sleep    2
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//span[@class="image-container"]//i[@class="icon-zoom-plus"]    innerHTML
    Click Element On Page    xpath=.//*[@id="${section_id}"]//span[@class="image-container"]//i[@class="icon-zoom-plus"]
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-container image is-visible"]/img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-container image is-visible"]/img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'


Validate Exercise - Choice (Single)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_7}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Choice - Single"]
    Set Section Elements
    ${section_element_selected}    Execute JavaScript    var sectionElementx = document.getElementById("${section_id}").querySelector(".instructions-container"); return sectionElementx;
    Click Element On Page    xpath=.//*[@id="opt1"]/div
    Wait Until Element Is Enabled    xpath=.//*[@id="es:7F1D290B-8C51-4831-882C-C8030DF5E474"]/*[@class="figcaption" and text()="Caption: Leivänpaahtimen murukolo on lähes tyhjä."]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:7F1D290B-8C51-4831-882C-C8030DF5E474"]/span[@class="image-container"]/img
    Wait Until Element Is Enabled    xpath=.//*[@id="opt1"]/div/div/p
    Wait Until Element Is Enabled    xpath=.//*[@id="es:5136D8D1-D66C-49A6-8044-215DAAA61274"]/span[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="opt3"]/div/div/p[text()="Lattialla ei ole tavaroita eikä rutussa olevia mattoja."]
    Wait Until Element Is Enabled    xpath=.//*[@id="opt4"]/div/div/p[text()="Terävät veitset ovat omilla paikoillaan."]
    Wait Until Element Is Enabled    xpath=.//*[@id="opt1"]/div/div/p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="opt1"]/div/div/p    innerHTML
    Should Contain    ${innerHTML}    <svg
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="opt3"]/div
    Click Element On Page    xpath=.//*[@id="opt2"]
    Click Element On Page    xpath=.//*[@id="opt2"]/div
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt4"]/div
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="es:7F1D290B-8C51-4831-882C-C8030DF5E474"]/span[@class="image-container"]/img
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Check Common Exercise Instruction Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@class="icon-correct"]
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    Close Extra Panel


Validate Exercise - Choice (Multiple)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_8}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Choice - Multiple"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="opt2"]//*[text()="Leivänpaahtimen murukolo on lähes tyhjä."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="1"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="Only incorrect feedback for student"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="opt2"]//*[text()="Leivänpaahtimen murukolo on lähes tyhjä."]
    Click Element On Page    xpath=.//*[@id="opt4"]//*[text()="Terävät veitset ovat omilla paikoillaan."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt2"]//*[text()="Leivänpaahtimen murukolo on lähes tyhjä."]
    Click Element On Page    xpath=.//*[@id="opt4"]//*[text()="Terävät veitset ovat omilla paikoillaan."]
    Click Element On Page    xpath=.//*[@id="opt1"]/div/div/p
    Click Element On Page    xpath=.//*[@id="opt4"]//*[text()="Terävät veitset ovat omilla paikoillaan."]
    Click Element On Page    xpath=.//*[@id="opt3"]//*[text()="Lattialla ei ole tavaroita eikä rutussa olevia mattoja."]
    Click Element On Page    xpath=.//*[@id="opt3"]//*[text()="Lattialla ei ole tavaroita eikä rutussa olevia mattoja."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":2
    Click Element On Page    xpath=.//*[@id="opt1"]//*[@class="answer-level-feedback-icon"]/i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="tooltip"]
    ${innerHTML}    Get Element Attribute    xpath=.//div[@class="tooltip"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Should Contain    ${innerHTML}    This is a
    Should Contain    ${innerHTML}    feedback
    Should Contain    ${innerHTML}    for incorrect answer.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt1"]/div/div/p
    Click Element On Page    xpath=.//*[@id="opt2"]/div/div/p
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Check Common Exercise Instruction Elements
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@id="opt2"]//i[@class="icon-correct"]
    Check Extra Panel
    Wait Until Element Is Enabled    xpath=.//*[@class="icon-bulb"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//*[text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//div[@id="opt2"]//i[@class="icon-correct"]
    Click Element On Page    xpath=.//*[@id="es:6AF3CCB4-119A-41E3-AD29-FB8037B304AD"]/i
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-container image is-visible"]/img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-container image is-visible"]/img    naturalWidth
    Should Be True    ${width} > 0
    Click Element On Page    xpath=.//div[@class="modal is-visible"]


Validate Exercise - Choice (Math)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_9}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Choice - Math"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="opt1"]//p[text()="1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="opt5"]//p[text()="5"]
    Click Element On Page    xpath=.//*[@id="opt1"]//p[text()="1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="opt3"]//p[text()="3"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt3"]//p[text()="3"]
    Click Element On Page    xpath=.//*[@id="opt5"]//p[text()="5"]
    Click Element On Page    xpath=.//*[@id="opt4"]//p[text()="4"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt4"]//p[text()="4"]
    Click Element On Page    xpath=.//*[@id="opt5"]//p[text()="5"]
    Click Element On Page    xpath=.//*[@id="opt3"]//p[text()="3"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    ${section_element_selected}    Execute JavaScript    var sectionElementx = document.getElementById("${section_id}").querySelector(".instructions-container"); return sectionElementx;
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="opt3"]//p[text()="3"]
    Check Extra Panel


Validate Exercise - Choice (Multiple - Any Answer Accepted)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_10}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Choice - Multiple (any answer accepted)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="opt2"]//*[text()="Leivänpaahtimen murukolo on lähes tyhjä."]
    Click Element On Page    xpath=.//*[@id="opt3"]//*[text()="Lattialla ei ole tavaroita eikä rutussa olevia mattoja."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "data":"opt2,opt3"
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="opt3"]//*[text()="Lattialla ei ole tavaroita eikä rutussa olevia mattoja."]
    Click Element On Page    xpath=.//*[@id="opt1"]//*[text()="Patalaput ja patakintaat ovat helposti saatavilla.Simple math 3x"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "data":"opt1,opt2"
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":0
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    ${section_element}    Get Webelement    xpath=.//aside
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    ${section_inner_html}    Get Element Attribute    ${section_element}    innerHTML
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//header[@class="extra-heading"]//h2[text()="Chapter Startpage and heading without number"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="song"]//h2[text()="Song"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="grammar-special-content"]//p[@class="grammar-text" and contains(text(),"Grammar body with math. Simple math 3x4-ye/6. Chemistry H20. Erityisesti elämä kaupungeissa oli vaarallista ja vaivalloista. Kaupunkien aarteet houkuttelivat ryösteleviä armeijoita. Vesijohdot katkesivat ja sillat sortuivat, kun ihmiset eivät pitäneet niistä huolta.")]


Validate Exercise - Fill In - Drop (Deprecated)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_11}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Fill in - Drop (deprecated)"]
    Set Section Elements
    @{answers_given}    Create List    ${2}    ${1}    ${4}    ${2}    ${2}    ${2}    ${1}    ${1}    ${1}    ${2}
    ${question_elements}    Get Element Count    xpath=.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"]
    FOR    ${index}    IN RANGE     0       ${question_elements}
        ${list_index}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[${index}]//span[text()="Valitse:"]
        ${chosen_answer_index}    Get From List    ${answers_given}    ${list_index}
        Click Element On Page    xpath=.//*[@id="${section_id}"]//ul[@class="ddl is-visible"]//li[${chosen_answer_index}]/div
    END
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="10"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p[text()="Mieti vielä."]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[1]//span[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[4]//span[@class="icon-wrong"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    @{answers_given}    Create List    ${1}    ${1}    ${1}    ${1}    ${1}    ${1}    ${1}    ${1}    ${1}    ${1}
    ${question_elements}    Get Element Count    xpath=.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"]
    FOR    ${index}    IN RANGE     0       ${question_elements}
        ${list_index}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[${index}]//span[text()="Valitse:"]
        ${chosen_answer_index}    Get From List    ${answers_given}    ${list_index}
        Click Element On Page    xpath=.//*[@id="${section_id}"]//ul[@class="ddl is-visible"]//li[${chosen_answer_index}]/div
    END
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="10"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p[text()="Mieti vielä."]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//*[text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[1]//span[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[1]//span[@class="placeholder-text" and text()="Pohjois-Italiasta."]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="task-wrapper"])[2]//span[@class="placeholder-text" and text()="keksi painokoneen."]
    Check Extra Panel


Validate Exercise - Fill In - Open Question Long Response
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_12}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Fill in - Open question long response"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//textarea[@name="response"]
    Input Text    xpath=.//*[@id="${section_id}"]//textarea[@name="response"]    Answer 1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="char-count" and text()="Vastauksen pituus: 8 / 2500"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tallenna"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="infotext"]
    ${info_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="infotext"]
    Should Match Regexp    ${info_text}    Vastaus on tallennettu arvioitavaksi \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback correct"]/p[text()="Great answer!"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "data":"Answer 1"
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//textarea[@name="response"]
    Input Text    xpath=.//*[@id="${section_id}"]//textarea[@name="response"]    Answer Test
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "data":"Answer Test"
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="infotext" and text()="Mallivastaus on näkyvillä."]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="answer-content"]/p[text()="Oppilaan oma ratkaisu."]
    Check Extra Panel


Validate Exercise - Match - Cloze Combi (Hidegaps)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_13}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Cloze Combi (hideGaps)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2]    lineeeeerrrrr
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2][@class="edited" and text()="lineeeeerrrrr"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="compound-button"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="undo-button"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2][@class="edited" and text()="lineeeeerrrrr"]
    ${default_text}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2]    data-initial
    ${actual_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2]
    Should Be Equal As Strings    ${actual_text}    ${default_text}
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="compound-button"]
    ${default_text_1}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[1]    data-initial
    ${default_text_2}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[2]    data-initial
    ${compound_word}    Catenate    SEPARATOR=    ${default_text_1}    ${default_text_2}
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[1]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[1][@class="edited" and text()="${compound_word}"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="undo-button"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[1][@class="edited" and text()="${compound_word}"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[1][not(@class="edited") and text()="${default_text_1}"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="edit-button"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[20]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[20]    color.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[32]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[32]    mark!
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="misspeled"]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="misspeled"]    misspelled
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]    colour.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[text()="Therefore"]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[text()="Therefore"]    Therefore,
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//form[@class="toolbar"]//label[@for="edit-button"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="misspeled"]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="misspeled"]    misspelled
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]    color.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[@data-initial="Color."]    colour.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[text()="Therefore"]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]//span[text()="Therefore"]    Therefore,
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[32]
    Input Text    xpath=.//*[@id="${section_id}"]//div[@class="editor"]/p[1]/span[32]    mark!
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    Close Extra Panel


Validate Exercise - Match - Cloze Combi - Unscored (Without Gap Match)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_14}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Cloze Combi - Unscored (without gap match)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:622335E0-0B6E-4CD2-8FA6-12BCF01B1DE7"]/i[@class="icon-video modal-trigger"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="9E81AD57-A9A5-4FB6-84C4-5562A54F4DFF"]//sup[text()="e/6"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[contains(@class, "gap-match-options")]//span[contains(@class, "gap-match-option") and contains(text(), "Whatever")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:3CD88F15-6BB7-4027-B94F-6431EB2812D4"]//i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="70E00106-F82E-4850-AD3F-515199B93212"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="70E00106-F82E-4850-AD3F-515199B93212"]    ANY VALUE
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="AFDF19C4-8EDD-40AF-9832-E04DF2DDBACF"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="AFDF19C4-8EDD-40AF-9832-E04DF2DDBACF"]    whatever
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="CD081902-8BD0-4E9C-8713-6E8D9529ECEC"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="CD081902-8BD0-4E9C-8713-6E8D9529ECEC"]//span[@class="inline-options"]//span[text()="One"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    ANY VALUE
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    One
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":0
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="70E00106-F82E-4850-AD3F-515199B93212"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="70E00106-F82E-4850-AD3F-515199B93212"]    ANYTHING
    Click Element On Page    xpath=.//*[@id="68AEE908-B0C1-4740-ABE6-226FFBAF5AF3"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="CD081902-8BD0-4E9C-8713-6E8D9529ECEC"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="CD081902-8BD0-4E9C-8713-6E8D9529ECEC"]//span[@class="inline-options"]//span[text()="Two"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    ANYTHING
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    Two
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Not Contain    ${sessionStorage_string}    ANY VALUE
    Should Not Contain    ${sessionStorage_string}    One
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":0
    Click Element On Page    xpath=.//*[@id="es:EC78B0A4-5D9F-4674-BB6E-C6BCD3C3B911"]//img
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-inner"]//img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-inner"]//img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'


Validate Exercise - Match - Cloze Combi (Mixed Score Answers) (Without Gap Match)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_15}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Cloze Combi (Mixed Score Answers)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:97BE7C36-7E0C-4E21-B4DD-386F2AF500C2"]//img[@class="fig-image"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="D46573B4-F615-48C4-A25F-DD9FB6CB3E8F"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="D46573B4-F615-48C4-A25F-DD9FB6CB3E8F"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]//span[@class="inline-options"]//span[text()="Answer 2"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="4686D492-555E-4E74-8A06-11985D212BAE"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="4686D492-555E-4E74-8A06-11985D212BAE"]    test
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="07F052BB-DF42-4AE4-A3BC-C428C54E54E1"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="07F052BB-DF42-4AE4-A3BC-C428C54E54E1"]//span[@class="inline-options"]//span[text()="Answer 3"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="35867382-7F40-4B41-8A51-8B1D2DBCFD3A"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="35867382-7F40-4B41-8A51-8B1D2DBCFD3A"]//span[@class="inline-options"]//span[text()="2"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="D46573B4-F615-48C4-A25F-DD9FB6CB3E8F"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="D46573B4-F615-48C4-A25F-DD9FB6CB3E8F"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="5E2CCDD4-C696-4EAF-8250-5FA45C4E42D2"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="4686D492-555E-4E74-8A06-11985D212BAE"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="4686D492-555E-4E74-8A06-11985D212BAE"]    jaaa
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="07F052BB-DF42-4AE4-A3BC-C428C54E54E1"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="07F052BB-DF42-4AE4-A3BC-C428C54E54E1"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="35867382-7F40-4B41-8A51-8B1D2DBCFD3A"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="35867382-7F40-4B41-8A51-8B1D2DBCFD3A"]//span[@class="inline-options"]//span[text()="1"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="35867382-7F40-4B41-8A51-8B1D2DBCFD3A"]//*[@class="icon-correct"]
    Click Element On Page    xpath=.//*[@id="es:6BE95F39-7CAD-44E2-99D1-0B1109E29BCB"]//img
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-inner"]//img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-inner"]//img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'


Validate Exercise - Match - Cloze Combi (Math Rewrite)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_16}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Cloze Combi (Math Rewrite)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="7DE280B3-E853-48E1-B97D-9281F8167D67"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="7DE280B3-E853-48E1-B97D-9281F8167D67"]    2
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="E497BF2E-7B94-490E-8FC6-C795A5BE213E"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="E497BF2E-7B94-490E-8FC6-C795A5BE213E"]    8
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="5088E672-4405-40AA-AD7C-33FE0CC73B6E"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="5088E672-4405-40AA-AD7C-33FE0CC73B6E"]    2
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="FCFD42CB-DCA6-49E4-9E7C-600992C8E2AE"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="FCFD42CB-DCA6-49E4-9E7C-600992C8E2AE"]    4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="8F19F8D9-4A28-441E-A5C9-505716CF7A0D"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="8F19F8D9-4A28-441E-A5C9-505716CF7A0D"]//span[@class="inline-options"]//span[text()="Answer 1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]    puuhöylä
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="888178D4-C591-4D22-AC20-A517E86FB15F"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="888178D4-C591-4D22-AC20-A517E86FB15F"]//span[@class="inline-options"]//span[text()="Drop down field"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "completed":true
    Click Element On Page    xpath=.//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]//i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//*[text()="Ei ollu puuhöylä"]
    Click Element On Page    xpath=.//*[@id="8F19F8D9-4A28-441E-A5C9-505716CF7A0D"]//i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//*[text()="Juu ei"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]    answer
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="8F19F8D9-4A28-441E-A5C9-505716CF7A0D"]/span[@class="label" and @placeholder="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="8F19F8D9-4A28-441E-A5C9-505716CF7A0D"]//span[@class="inline-options"]//span[text()="Correct Answer"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "completed":true
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="B05C337E-EDBD-412A-946A-6D250A6D4F5F"]//*[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="E497BF2E-7B94-490E-8FC6-C795A5BE213E"]//*[@class="icon-correct"]


Validate Exercise - Match - Cloze Combi (Math With Fractions)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_17}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Cloze Combi (math with fractions)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="1044F8AB-CA89-4DDC-B5D8-6FA2436F23F4"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="1044F8AB-CA89-4DDC-B5D8-6FA2436F23F4"]    1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="2DA0235D-F162-4D1E-ADAB-07468692A2E8"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="2DA0235D-F162-4D1E-ADAB-07468692A2E8"]    2
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="CFD151AC-D04B-4E8C-9342-61AF4F607347"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="CFD151AC-D04B-4E8C-9342-61AF4F607347"]    0,5
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="0FBA8486-C4BD-45D5-84BC-9836FCD04E2A"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="0FBA8486-C4BD-45D5-84BC-9836FCD04E2A"]    4
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="87E72560-DD23-4FD6-AF8C-093AEC3EB65E"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="87E72560-DD23-4FD6-AF8C-093AEC3EB65E"]    1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="5CFB0D13-3F0A-4A05-B874-1F7AA8B381C5"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="5CFB0D13-3F0A-4A05-B874-1F7AA8B381C5"]    1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="D4CA402D-75B7-4668-B828-6B38AEAB9DB8"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="D4CA402D-75B7-4668-B828-6B38AEAB9DB8"]    6
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="CFD151AC-D04B-4E8C-9342-61AF4F607347"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="CFD151AC-D04B-4E8C-9342-61AF4F607347"]    0,7
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "completed":true
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="CFD151AC-D04B-4E8C-9342-61AF4F607347"]//*[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="87E72560-DD23-4FD6-AF8C-093AEC3EB65E"]//*[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="D4CA402D-75B7-4668-B828-6B38AEAB9DB8"]//*[@class="icon-correct"]


Validate Exercise - Match - Gap Match (Deprecated)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_18}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Gap Match (deprecated)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="texts-container"]//*[@class="plain-text" and contains(text(), "Aamulla herätyskello")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="positioning-container"]//div/span[text()="Nousen"]
    Mouse Over    xpath=.//*[@id="${section_id}"]//*[@class="positioning-container"]//div/span[text()="Nousen"]
    Click Element On Page    ${section_element}
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Validate Exercise - Match - Single Response
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_19}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Single response"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="B9824A8C-90F9-431C-BFA2-EC3132FF9C01"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="77FCE2C7-3DFD-472E-991C-39D825781D77"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="E5838794-C69E-46B2-AA19-5856A5144C72"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="13D00DBF-94E7-4F23-8A31-BA40EFB1DD44"]//p
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="388AE0AA-F35D-4570-BA27-12D8AABB17E5"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="DC14EC9A-983B-4E34-A21C-94987DC87852"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="B9824A8C-90F9-431C-BFA2-EC3132FF9C01"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="388AE0AA-F35D-4570-BA27-12D8AABB17E5"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="388AE0AA-F35D-4570-BA27-12D8AABB17E5"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="77FCE2C7-3DFD-472E-991C-39D825781D77"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="DC14EC9A-983B-4E34-A21C-94987DC87852"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="B9824A8C-90F9-431C-BFA2-EC3132FF9C01"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="388AE0AA-F35D-4570-BA27-12D8AABB17E5"]//i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="tooltip"]
    ${innerHTML}    Get Element Attribute    xpath=.//div[@class="tooltip"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Should Contain    ${innerHTML}    This is
    Should Contain    ${innerHTML}    feedback
    Should Contain    ${innerHTML}    1.
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Validate Exercise - Match - Multiple Response
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_20}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Multiple Response"]
    Set Section Elements
    Click Element On Page    xpath=.//*[text()="PUU"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="saunan lauteet"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="PUU"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="leikkimökki"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="PAPERI"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="sanomalehti"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="METALLI"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="ruuvi"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="METALLI"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="pyörän runko"]/parent::div/parent::div
    ${points_text}    Get Text    xpath=.//span[@class="show-points"]
    Should Be Equal As Strings    ${points_text}    Sait 4/4 oikein!
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[text()="METALLI"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="pyörän runko"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="leikkimökki"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="PUU"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="PUU"]/parent::div/parent::div
    Click Element On Page    xpath=.//*[text()="saunan lauteet"]/parent::div/parent::div
    ${points_text}    Get Text    xpath=.//span[@class="show-points"]
    Should Be Equal As Strings    ${points_text}    Sait 3/4 oikein!
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Validate Exercise - Match - Matrix
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_21}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Matrix"]
    Set Section Elements
    Click Element On Page    xpath=.//*[text()="suolakalaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="turkiksia"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="hienoja koruja"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="juomalaseja"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="hopeaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="mursunluisia esineitä"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    ${points_text}    Get Text    xpath=.//span[@class="show-points"]
    Should Be Equal As Strings    ${points_text}    Sait 4/6 oikein!
    ${feedback}    Get Text    xpath=.//div[@class="feedback incorrect"]
    Should Be Equal As Strings    ${feedback}    Yritä uudelleen.
    Click Element On Page    xpath=.//*[text()="juomalaseja"]/parent::span[@class="option-text"]/parent::div//i[@class="icon-bubble2"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[text()="suolakalaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="turkiksia"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="mursunluisia esineitä"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="suolakalaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="hienoja koruja"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="hopeaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="juomalaseja"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="ostivat"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    ${points_text}    Get Text    xpath=.//span[@class="show-points"]
    Should Be Equal As Strings    ${points_text}    Sait 6/6 oikein!
    ${feedback}    Get Text    xpath=.//div[@class="feedback correct"]
    Should Be Equal As Strings    ${feedback}    Hienoa!
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":6
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":6
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[text()="hopeaa"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="myivät"]/parent::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":5
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//*[text()="suolakalaa"]/parent::span[@class="option-text"]/parent::div[contains(@class, "is-correct")]
    Wait Until Element Is Enabled    xpath=.//*[text()="suolakalaa"]/parent::span[@class="option-text"]/parent::div[contains(@class, "is-correct")]//*[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[text()="hopeaa"]/parent::span[@class="option-text"]/parent::div[contains(@class, "is-correct")]
    Wait Until Element Is Enabled    xpath=.//*[text()="hopeaa"]/parent::span[@class="option-text"]/parent::div[contains(@class, "is-correct")]//*[@class="icon-correct"]
    Check Extra Panel


###
###
###
# Note: clicking video element not working currently
###
###
###
Validate Exercise - Match - Matrix (Multiple Columns, Assets)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_22}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Matrix (multiple columns, assets)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:3BB748F5-AF83-4F58-B0DA-893D963F3B5A"]//img[@class="fig-image"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:80F08E1B-C27C-4280-B3F0-9FC3475A790F"]//i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:6C00D69C-47A1-4438-9CA7-D3214C9E2E9C"]//span[@class="video-container"]/video
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="options"]//*[@class="option-text"]//*[contains(text(), "lisää")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:B773C12B-3D57-4596-9972-2075E7E17053"]//i[@class="icon-video modal-trigger"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:B773C12B-3D57-4596-9972-2075E7E17053"]//*[@class="figcaption" and text()="Videon Caption-kentän teksti"]
    Click Element On Page    xpath=.//*[text()="rakennustaito"]/parent::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="etruskeilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="es:3BB748F5-AF83-4F58-B0DA-893D963F3B5A"]//img[@class="fig-image"]
    Click Element On Page    xpath=.//*[text()="etruskeilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[contains(text(),"ennustaminen äänet")]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="kreikkalaisilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[contains(text(),"Ei mitään")]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[contains(text(),"suomalaisilta pitkä lorem ipsum")]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[contains(text(),"jawohl")]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="kreikkalaisilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="es:6C00D69C-47A1-4438-9CA7-D3214C9E2E9C"]//span[@class="video-container"]/video
    Click Element On Page    xpath=.//*[text()="kreikkalaisilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="Ingenting"]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="etruskeilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="67769633-8A79-42DB-BB48-56E4C093C259"]
    Reload Page
    Sleep    2
    Click Element On Page    xpath=.//*[contains(text(),"ruotsalaisilta video")]/ancestor::div[@class="title modalDisabled"]/p[contains(text(), "ruotsalaisilta video")]
    Sleep    2
    Click Element On Page    xpath=.//*[contains(text(),"ruotsalaisilta video")]/ancestor::div[@class="title modalDisabled"]/p[contains(text(), "ruotsalaisilta video")]
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[contains(text(),"ennustaminen äänet")]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[text()="etruskeilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[text()="Ingenting"]/ancestor::span[@class="option-text"]
    Click Element On Page    xpath=.//*[contains(text(),"ruotsalaisilta video")]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@data-id="67769633-8A79-42DB-BB48-56E4C093C259"]
    Click Element On Page    xpath=.//*[text()="kreikkalaisilta"]/ancestor::div[@class="title modalDisabled"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":7
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//*[contains(text(),"jawohl")]/ancestor::span[@class="option-text"]/parent::div[contains(@class, "is-correct")]//*[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:7DA42632-B4A7-48B5-99C7-7ED015D45C30"]//img[@class="fig-image"]
    Mouse Over    xpath=.//*[@id="es:7DA42632-B4A7-48B5-99C7-7ED015D45C30"]//img[@class="fig-image"]
    Click Element On Page    xpath=.//*[@id="es:7DA42632-B4A7-48B5-99C7-7ED015D45C30"]//span[@class="zoom-icon"]/i[@class="icon-zoom-plus"]
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-container image is-visible"]/img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-container image is-visible"]/img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'


Validate Exercise - Match - Table (Many Gaps)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_23}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Table Match (many gaps)"]
    Set Section Elements
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option00"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option12"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option20"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option30"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option41"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option50"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option60"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option70"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="6"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="12"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="mööö"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":6
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":12
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Sleep    5
    # SOMETHING WEIRD HAPPENS HERE, THE SCREEN IS MAXIMIZED (or later, check it)
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="content-container"]//div[@class="content"][8]//td[@class="nav-button next"]/i[@class="nav-icon icon-right"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="content-container"]//div[@class="content"][1]//td[@class="nav-button next"]/i[@class="nav-icon icon-right"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="content-container"]//div[@class="content"][2]//td[@class="nav-button next"]/i[@class="nav-icon icon-right"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option22"]
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select2"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select2"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select2"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option71"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select7"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select7"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select7"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option40"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select4"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button previous"]/i[@class="nav-icon icon-left"]//ancestor::div[@class="content"]//div[@id="select4"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option22"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//td[@class="nav-button next"]/i[@class="nav-icon icon-right"]//ancestor::div[@class="content"]//div[@id="select2"]
    Sleep    1
    Click Element On Page    xpath=.//*[@id="option31"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="12"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="12"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":12
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option00"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option12"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option22"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option31"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option40"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option50"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option61"]
    Sleep    1
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//*[@id="option70"]
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="10"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="12"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="mööö"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":10
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Wait Until Element Is Enabled    xpath=.//div[@id="option40"]//div[@class="option-content"]
    Wait Until Element Is Enabled    xpath=.//div[@id="option40"]//i[@class="icon-correct"][1]
    Wait Until Element Is Enabled    xpath=.//div[@id="option41"]//div[@class="option-content" and contains(text(),"Mutta tästä vain 2")]
    Wait Until Element Is Enabled    xpath=.//div[@id="option41"]//i[@class="icon-correct"][1]


Check Which Question Is Shown
    ${questions_in_total}    Get Element Count    xpath=.//div[@class="content-container"]//div[@class="content"]//div[starts-with(@id, "select")]
    FOR    ${index}    IN RANGE     0       ${questions_in_total}
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Element Should Be Visible    xpath=.//div[@class="content-container"]//div[@class="content"]//div[@id="select${index}"]
        ${question_shown}    Set Variable If    '${status}'=='PASS'    ${index}
    END
    [Return]    ${question_shown}


Validate Exercise - Match - Drag Image (Hidegaps)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_24}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Drag Image (hideGaps)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//div[@class="actual-content"]//div[starts-with(@class,"options-container")]/div[1]
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Validate Exercise - Match - Drag Image (Hidegaps) (Unscored)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_25}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Drag Image (hideGaps) (Unscored)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//div[@class="actual-content"]//div[starts-with(@class,"options-container")]//div[@class="option-content-wrapper" and contains(text(), "lamppu")]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Not Be True    '${sessionStorage_string}'=='${EMPTY}'
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="infotext" and contains(text(), "Tehtävä on suoritettu")]
    ${infotext}    Get Text    xpath=.//div[@class="results"]/span[@class="infotext"]
    ${response}    Should Match Regexp    ${infotext}    Tehtävä on suoritettu \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Check Extra Panel


Validate Exercise - Match - Drag Image (Fillin)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_26}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Drag Image (fillIn)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     nuuuuu
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     jaaaaa
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     jjjuu
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="1"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="incorrect"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2" and @data-word="lamppu" and starts-with(@class, "answer-target")]//div[@class="answer-level-feedback-icon"]/i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//*[text()="Feedback (lamppu)."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     satula
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     rengas
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     lamppu
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback correct"]/*[text()="correct"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Sleep    5
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     satukka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     rengas
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     lamppu
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     satula
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Check Extra Panel


Validate Exercise - Play - Dialogue (aka. Discussion)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_27}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Play - Dialogue (aka. Discussion)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="player-select-button"]/*[text()="Person A"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-sentence"]//*[@class="sentence" and text()="Hi."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-button-answers" and text()="Näytä oikea vastaus"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-item visible"]//div[@class="discussion-translation"]/*[text()="Oikea vastaus:"]/following-sibling::p[text()="Hei."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-button" and text()=">"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-button" and text()=">"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-item visible"]//div[@class="discussion-button-answers" and text()="Näytä oikea vastaus"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-item visible"]//div[@class="discussion-translation"]/*[text()="Oikea vastaus:"]/following-sibling::p[text()="Kiitos."]


Validate Exercise - Play - Dialogue (Person B)
    # NOTE: the same URL as in the previous exercise
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_27}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Play - Dialogue (aka. Discussion)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="player-select-button"]/*[text()="Person B"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-sentence"]//*[@class="sentence-player-b" and text()="Hei."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-button" and text()=">"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-item visible"]//div[@class="discussion-button-answers" and text()="Näytä oikea vastaus"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-item visible"]//div[@class="discussion-translation"]/*[text()="Oikea vastaus:"]/following-sibling::p[text()="Good morning."]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="discussion-button" and text()=">"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="discussion-sentence"]//*[@class="sentence-player-b" and text()="Kiitos."]


Validate Exercise - Play - Wordsearch
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_28}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Play - Wordsearch"]
    Set Section Elements
    Click Element In Wordsearch Table    ${section_id}    2    2
    Click Element In Wordsearch Table    ${section_id}    3    2
    Click Element In Wordsearch Table    ${section_id}    4    2
    Click Element In Wordsearch Table    ${section_id}    5    2
    Click Element In Wordsearch Table    ${section_id}    6    2
    Click Element In Wordsearch Table    ${section_id}    7    2
    Click Element In Wordsearch Table    ${section_id}    8    2
    Click Element In Wordsearch Table    ${section_id}    9    2
    Click Element In Wordsearch Table    ${section_id}    10    4
    Click Element In Wordsearch Table    ${section_id}    10    5
    Click Element In Wordsearch Table    ${section_id}    10    6
    Click Element In Wordsearch Table    ${section_id}    10    7
    Click Element In Wordsearch Table    ${section_id}    10    8
    Click Element In Wordsearch Table    ${section_id}    10    8
    Click Element In Wordsearch Table    ${section_id}    8    7
    Click Element In Wordsearch Table    ${section_id}    9    7
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="7"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="hint found-hint"]//i[@class="icon-checkmark"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element In Wordsearch Table    ${section_id}    10    4
    Click Element In Wordsearch Table    ${section_id}    10    5
    Click Element In Wordsearch Table    ${section_id}    10    6
    Click Element In Wordsearch Table    ${section_id}    11    1
    Click Element In Wordsearch Table    ${section_id}    11    2
    Click Element In Wordsearch Table    ${section_id}    11    3
    Click Element In Wordsearch Table    ${section_id}    11    4
    Click Element In Wordsearch Table    ${section_id}    11    5
    Click Element In Wordsearch Table    ${section_id}    11    6
    Click Element In Wordsearch Table    ${section_id}    11    7
    Click Element In Wordsearch Table    ${section_id}    11    8
    Click Element In Wordsearch Table    ${section_id}    9    2
    Click Element In Wordsearch Table    ${section_id}    8    2
    Click Element In Wordsearch Table    ${section_id}    7    2
    Click Element In Wordsearch Table    ${section_id}    6    2
    Click Element In Wordsearch Table    ${section_id}    5    2
    Click Element In Wordsearch Table    ${section_id}    4    2
    Click Element In Wordsearch Table    ${section_id}    3    2
    Click Element In Wordsearch Table    ${section_id}    2    2
    Click Element In Wordsearch Table    ${section_id}    3    4
    Click Element In Wordsearch Table    ${section_id}    4    4
    Click Element In Wordsearch Table    ${section_id}    5    4
    Click Element In Wordsearch Table    ${section_id}    6    4
    Click Element In Wordsearch Table    ${section_id}    7    4
    Click Element In Wordsearch Table    ${section_id}    8    4
    Click Element In Wordsearch Table    ${section_id}    9    4
    Click Element In Wordsearch Table    ${section_id}    9    5
    Click Element In Wordsearch Table    ${section_id}    9    6
    Click Element In Wordsearch Table    ${section_id}    9    8
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="6"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="7"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":6
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element In Wordsearch Table    ${section_id}    3    4
    Click Element In Wordsearch Table    ${section_id}    9    8
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="7"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="7"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "completed":true
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element In Wordsearch Table    ${section_id}    4    4
    Click Element In Wordsearch Table    ${section_id}    5    4
    Click Element In Wordsearch Table    ${section_id}    6    4
    Click Element In Wordsearch Table    ${section_id}    7    4
    Click Element In Wordsearch Table    ${section_id}    8    4
    Click Element In Wordsearch Table    ${section_id}    9    4
    Click Element In Wordsearch Table    ${section_id}    7    7
    Click Element In Wordsearch Table    ${section_id}    7    8
    Click Element In Wordsearch Table    ${section_id}    9    5
    Click Element In Wordsearch Table    ${section_id}    9    6
    Click Element In Wordsearch Table    ${section_id}    9    7
    Click Element In Wordsearch Table    ${section_id}    9    8
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":5
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":7
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    ${section_element}    Get Webelement    xpath=.//aside
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//header[@class="extra-heading"]//h2[text()="Image carousel and Flashcards"]
    Close Extra Panel


Click Element In Wordsearch Table
    [Arguments]    ${section_id}    ${row}    ${column}
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@class="word-search-table"]//tr[${row}]//td[${column}]


Validate Exercise - Play - Wordsearch (Assets)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_29}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Play - Wordsearch (assets)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:684CC3CA-C0AB-4886-9763-41E1507B5D93"]//img[@class="fig-image"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="video-container"]/video
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:E30E613B-3D09-44DE-9DE5-A72CCB5BADE5"]//img[@class="fig-image"]
    Click Element In Wordsearch Table    ${section_id}    1    1
    Click Element In Wordsearch Table    ${section_id}    1    2
    Click Element In Wordsearch Table    ${section_id}    1    3
    Click Element In Wordsearch Table    ${section_id}    1    4
    Click Element In Wordsearch Table    ${section_id}    1    6
    Click Element In Wordsearch Table    ${section_id}    1    7
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="word-search-hints"]//span[@class="hint found-hint" and @data-word="hevo"]//i[@class="icon-checkmark"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element In Wordsearch Table    ${section_id}    1    8
    Click Element In Wordsearch Table    ${section_id}    2    8
    Click Element In Wordsearch Table    ${section_id}    3    8
    Click Element In Wordsearch Table    ${section_id}    4    8
    Click Element In Wordsearch Table    ${section_id}    5    8
    Click Element In Wordsearch Table    ${section_id}    6    8
    Click Element In Wordsearch Table    ${section_id}    7    8
    Click Element In Wordsearch Table    ${section_id}    2    10
    Click Element In Wordsearch Table    ${section_id}    3    10
    Click Element In Wordsearch Table    ${section_id}    4    10
    Click Element In Wordsearch Table    ${section_id}    2    7
    Click Element In Wordsearch Table    ${section_id}    3    7
    Click Element In Wordsearch Table    ${section_id}    4    7
    Click Element In Wordsearch Table    ${section_id}    5    7
    Click Element In Wordsearch Table    ${section_id}    6    7
    Click Element In Wordsearch Table    ${section_id}    2    2
    Click Element In Wordsearch Table    ${section_id}    2    3
    Click Element In Wordsearch Table    ${section_id}    2    4
    Click Element In Wordsearch Table    ${section_id}    2    5
    Click Element In Wordsearch Table    ${section_id}    2    6
    Click Element In Wordsearch Table    ${section_id}    6    1
    Click Element In Wordsearch Table    ${section_id}    6    2
    Click Element In Wordsearch Table    ${section_id}    6    3
    Click Element In Wordsearch Table    ${section_id}    6    4
    Click Element In Wordsearch Table    ${section_id}    6    5
    Click Element In Wordsearch Table    ${section_id}    4    2
    Click Element In Wordsearch Table    ${section_id}    5    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="6"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="7"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":6
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element In Wordsearch Table    ${section_id}    1    6
    Click Element In Wordsearch Table    ${section_id}    5    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="7"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="7"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "completed":true
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]


Validate Exercise - Right Option (Deprecated)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_30}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Right Option (deprecated)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="task-wrapper"][1]//div[@class="word" and normalize-space(text())="Nettietiketti"]
    Choose Option For Right Option Exercise    ${section_id}    1    Oikein
    Choose Option For Right Option Exercise    ${section_id}    2    Väärin
    Choose Option For Right Option Exercise    ${section_id}    3    Oikein
    Choose Option For Right Option Exercise    ${section_id}    4    Väärin
    Choose Option For Right Option Exercise    ${section_id}    5    Oikein
    Choose Option For Right Option Exercise    ${section_id}    6    Väärin
    Choose Option For Right Option Exercise    ${section_id}    7    Oikein
    Choose Option For Right Option Exercise    ${section_id}    8    Väärin
    Choose Option For Right Option Exercise    ${section_id}    9    Oikein
    Choose Option For Right Option Exercise    ${section_id}    10    Väärin
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="7"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="10"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="task-wrapper"][1]//span[@class="icon-correct"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="task-wrapper"][4]//span[@class="icon-wrong"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Choose Option For Right Option Exercise    ${section_id}    1    Oikein
    Choose Option For Right Option Exercise    ${section_id}    2    Väärin
    Choose Option For Right Option Exercise    ${section_id}    3    Oikein
    Choose Option For Right Option Exercise    ${section_id}    4    Oikein
    Choose Option For Right Option Exercise    ${section_id}    5    Väärin
    Choose Option For Right Option Exercise    ${section_id}    6    Väärin
    Choose Option For Right Option Exercise    ${section_id}    7    Oikein
    Choose Option For Right Option Exercise    ${section_id}    8    Väärin
    Choose Option For Right Option Exercise    ${section_id}    9    Oikein
    Choose Option For Right Option Exercise    ${section_id}    10    Oikein
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":10
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Choose Option For Right Option Exercise
    [Arguments]    ${section_id}    ${question_number}    ${answer_to_choose}
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="task-wrapper"][${question_number}]//span[@class="placeholder-text" and text()="Valitse:"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="task-wrapper"][${question_number}]//div[@class="ddl-wrapper is-expanded"]//li[@class="ddl-item"]//div[text()="${answer_to_choose}"]


Validate Exercise - Select Words
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_31}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Select - Words"]
    Set Section Elements
    Select Words In Select Words Exercise    ${section_id}    haukkuu
    Select Words In Select Words Exercise    ${section_id}    naukaisee
    Select Words In Select Words Exercise    ${section_id}    pihamaalle. Kuu
    Select Words In Select Words Exercise    ${section_id}    nukkuu
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Select Words In Select Words Exercise    ${section_id}    naukaisee
    Select Words In Select Words Exercise    ${section_id}    valoja kattolamppuun
    Select Words In Select Words Exercise    ${section_id}    eivät jätä
    Select Words In Select Words Exercise    ${section_id}    Liikkuvatko
    Select Words In Select Words Exercise    ${section_id}    naukaisee
    Select Words In Select Words Exercise    ${section_id}    a
    Select Words In Select Words Exercise    ${section_id}    phone
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Select Words In Select Words Exercise    ${section_id}    haukkuu
    Select Words In Select Words Exercise    ${section_id}    naukaisee
    Select Words In Select Words Exercise    ${section_id}    astuu
    Select Words In Select Words Exercise    ${section_id}    paistaa
    Select Words In Select Words Exercise    ${section_id}    Liikkuvatko
    Select Words In Select Words Exercise    ${section_id}    nukkuu
    Select Words In Select Words Exercise    ${section_id}    eivät jätä
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="7"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="exercise-content"]//*[@class="texts-container"]//div[@class="text correct"]//i[@class="icon-correct"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":7
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":9
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Check Extra Panel


Select Words In Select Words Exercise
    [Arguments]    ${section_id}    ${word_to_select}
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@class="exercise-content"]//*[@class="texts-container"]//span[normalize-space(text())="${word_to_select}"]


Validate Exercise - Select Words Multiple
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_32}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Select - Words multiple"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color3"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color4"]
    Select Words In Select Words Multiple Exercise    ${section_id}    DonecITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    DonecITALIC
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Select Words In Select Words Multiple Exercise    ${section_id}    consectetuerITALICUNDERSTIKE.
    Select Words In Select Words Multiple Exercise    ${section_id}    SedITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    DonecITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color3"]
    Select Words In Select Words Multiple Exercise    ${section_id}    ametUNDER
    Select Words In Select Words Multiple Exercise    ${section_id}    consectetuerITALICUNDERSTIKE.
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Select Words In Select Words Multiple Exercise    ${section_id}    sitUNDER
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color4"]
    Select Words In Select Words Multiple Exercise    ${section_id}    consectetuerITALICUNDERSTIKE.
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback correct"]/p[text()="Great, your answer is correct!"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":9
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Select Words In Select Words Multiple Exercise    ${section_id}    consectetuerITALICUNDERSTIKE.
    Select Words In Select Words Multiple Exercise    ${section_id}    SedITALIC
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color3"]
    Select Words In Select Words Multiple Exercise    ${section_id}    SedITALIC
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range correct"]//span[@class="word" and text()="consectetuerITALICUNDERSTIKE."]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="word incorrect" and text()="SedITALIC"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="1"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p[text()="Sorry, your answer is incorrect and here is a long lorem ipsum. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. "]
    ${bold_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p/b
    Should Be Equal As Strings    ${bold_text}    With bold and
    ${bold_italic_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p/b/i
    Should Be Equal As Strings    ${bold_italic_text}    and
    ${italic_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback incorrect"]/p/i
    Should Be Equal As Strings    ${italic_text}    italic text
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="word incorrect"]/i[@class="icon-wrong"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range correct"]/i[@class="icon-correct"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Select Words In Select Words Multiple Exercise    ${section_id}    DonecITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Select Words In Select Words Multiple Exercise    ${section_id}    leoBOLDUNDERITALIC
    Select Words In Select Words Multiple Exercise    ${section_id}    massa.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color4"]
    Select Words In Select Words Multiple Exercise    ${section_id}    consectetuerITALICUNDERSTIKE.
    Select Words In Select Words Multiple Exercise    ${section_id}    placeratALL
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="9"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="word incorrect" and text()="massa."]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range correct"]/span[@class="word" and text()="placeratALL"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range correct"]/span[@class="word" and text()="consectetuerITALICUNDERSTIKE."]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":9
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range color3"]/span[@class="word" and text()="ametUNDER"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//span[@class="range color2"]/span[@class="word" and text()="SedITALIC"]
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    Close Extra Panel


Select Words In Select Words Multiple Exercise
    [Arguments]    ${section_id}    ${word_to_select}    ${order_number}=${1}
    Sleep    1
    Click Element On Page    xpath=(.//*[@id="${section_id}"]//div[@class="contents"]//span[starts-with(@class,"word") and text()="${word_to_select}"])[${order_number}]


Validate Exercise - Select Words Multiple DD-1248
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_33}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Bug: Select - Words DD-1248"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color1"]/i[@class="icon-checkmark"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    1
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    3
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    all
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Select Words In Select Words Multiple Exercise    ${section_id}    different
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color1"]
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":6
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color2"]
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    1
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    3
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    BLUE    4
    Select Words In Select Words Multiple Exercise    ${section_id}    same    1
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    3
    Select Words In Select Words Multiple Exercise    ${section_id}    blub    1
    Select Words In Select Words Multiple Exercise    ${section_id}    blob    1
    Select Words In Select Words Multiple Exercise    ${section_id}    blub    2
    Select Words In Select Words Multiple Exercise    ${section_id}    test    1
    Select Words In Select Words Multiple Exercise    ${section_id}    test    2
    Select Words In Select Words Multiple Exercise    ${section_id}    testing    1
    Select Words In Select Words Multiple Exercise    ${section_id}    all
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Select Words In Select Words Multiple Exercise    ${section_id}    different
    Click Element On Page    xpath=.//*[@id="${section_id}"]//form[@class="toolbar"]//label[@class="color1"]
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE
    Select Words In Select Words Multiple Exercise    ${section_id}    MAGENTABLUE    2
    Select Words In Select Words Multiple Exercise    ${section_id}    same    2
    Select Words In Select Words Multiple Exercise    ${section_id}    blob    1
    Select Words In Select Words Multiple Exercise    ${section_id}    test    2
    Select Words In Select Words Multiple Exercise    ${section_id}    are
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":12
    Click Element On Page    xpath=.//div[@class="correct-answers"]
    Page Should Contain Element    xpath=(.//*[@id="${section_id}"]//div[@class="contents"]//span[starts-with(@class,"word") and text()="MAGENTABLUE"])[1]/parent::span[@class="range color1"]
    Page Should Contain Element    xpath=(.//*[@id="${section_id}"]//div[@class="contents"]//span[starts-with(@class,"word") and text()="blub"])[2]/parent::span[@class="range color2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//span[@class="correct-answer-infotext" and text()="Tässä näet oikeat vastaukset!"]
    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    Close Extra Panel


Validate Exercise - Match - Drag Image (Unscored)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_34}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Drag Image (Unscored)"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="option-content-wrapper" and normalize-space(text())="(-3, 0)"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="option-content-wrapper" and normalize-space(text())="Ympyrä"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    ${sessionStorage_string}    Wait Until Keyword Succeeds    10 sec    1 sec    Get SessionStorage    ${section_id}
    Should Not Be True    '${sessionStorage_string}'=='${EMPTY}'
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="infotext" and contains(text(), "Tehtävä on suoritettu")]
    ${infotext}    Get Text    xpath=.//div[@class="results"]/span[@class="infotext"]
    ${response}    Should Match Regexp    ${infotext}    Tehtävä on suoritettu \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Check Extra Panel


Validate Exercise - Match - Drag Image (Fillin) (Unscored)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_35}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Drag Image (fillIn) (Unscored)"]
    Set Section Elements
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     nuuuuu
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     jaaaaa
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     jjjuu
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="infotext" and contains(text(), "Tehtävä on suoritettu")]
    ${infotext}    Get Text    xpath=.//div[@class="results"]/span[@class="infotext"]
    ${response}    Should Match Regexp    ${infotext}    Tehtävä on suoritettu \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback correct"]/p[text()="correct"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     satula
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     rengas
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     lamppu
    Sleep    1
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Click Element On Page    xpath=.//*[@data-id="choiceObject0" and @data-word="satula" and starts-with(@class, "answer-target")]
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject0"]//input
    Input Text    xpath=.//*[@data-id="choiceObject0"]//input     satukka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject1"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject1"]//input
    Input Text    xpath=.//*[@data-id="choiceObject1"]//input     tarakka
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject3"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject3"]//input
    Input Text    xpath=.//*[@data-id="choiceObject3"]//input     rengas
    Sleep    1
    Click Element On Page    xpath=.//*[@data-id="choiceObject2"]
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@data-id="choiceObject2"]//input
    Input Text    xpath=.//*[@data-id="choiceObject2"]//input     lamppu
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Check Extra Panel


Validate Exercise - Match - Table Match (Unscored)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_36}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Table match (Unscored)"]
    Set Section Elements
    Select Option in Table Match    1    Suurmogulien valtakunta
    Select Option in Table Match    2    Inkojen valtakunta
    Select Option in Table Match    3    Kiinan keisarikunta
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="infotext" and contains(text(), "Tehtävä on suoritettu")]
    ${infotext}    Get Text    xpath=.//div[@class="results"]/span[@class="infotext"]
    ${response}    Should Match Regexp    ${infotext}    Tehtävä on suoritettu \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Select Option in Table Match    4    Länsi-Rooman valtakunta
    Click Element On Page     xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Select Option in Table Match    1    First image option
    Select Option in Table Match    2    Mayojen valtakunta
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Check Extra Panel


Select Option in Table Match
    [Arguments]    ${question_number}    ${option_text}
    Click Element On Page    xpath=(.//div[@class="content-container"]//div[@class="content"])[${question_number}]/div[starts-with(@class,"select-option")]//div[@class="option-content" and normalize-space(text())="${option_text}"]


Validate Exercise - Match - Table Match
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_37}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Table match"]
    Set Section Elements
    ${screen_width}    Execute JavaScript    var screenWidth = screen.width; return screenWidth;
    Run Keyword If    ${screen_width} <= 700    Element Should Not Be Visible    xpath=.//span[@id="es:396B90E2-D4C0-4D0C-8472-636A318669DD"]/span[@class="image-container"]//img[@class="fig-image"]
    Run Keyword If    ${screen_width} <= 700    Element Should Not Be Visible    xpath=.//span[@id="es:4FBE9B61-F490-4C17-82FA-B8AAB553BF4D"]/span[@class="audio-element"]//i[@class="icon-audio"]
    Run Keyword If    ${screen_width} <= 700    Element Should Not Be Visible    xpath=.//span[@id="es:819FE77E-7A29-4C79-83F0-7A17860FECA2"]/span[@class="video-container"]/video
    Run Keyword If    ${screen_width} <= 700    Element Should Not Be Visible    xpath=.//div[@class="content-container"]//div[@class="content"]//td[@class="hint" and normalize-space(text())="Text and video"]
    Run Keyword If    ${screen_width} <= 700    Element Should Not Be Visible    xpath=.//div[@class="content-container"]//div[@class="content"]//td[@class="hint" and normalize-space(text())="Math ML:"]/*[name()="svg"]
    Select Option in Table Match    1    Suurmogulien valtakunta
    Select Option in Table Match    2    Inkojen valtakunta
    Select Option in Table Match    3    Asteekkien valtakunta
    Select Option in Table Match    4    MathML
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="4"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="4"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":4
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Sleep    2
    Select Option in Table Match    4    Länsi-Rooman valtakunta
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Select Option in Table Match    2    Inkojen valtakunta
    Click Element On Page    xpath=.//*[@id="${section_id}"]//*[@id="option30"]//i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="tooltip"]
    ${tooltip_text}    Get Text    xpath=.//div[@class="tooltip"]
    Should Be Equal As Strings    ${tooltip_text}    This a feedback.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Check Extra Panel


Validate Exercise - Fill In - Open Question Short Response
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_38}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Fill in - Open question short response"]
    Set Section Elements
    # math
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    Should Contain    ${innerHTML}    <sup>4</sup>-y<sup>e/6</sup>. Chemistry H<sub>2</sub>
    Should Contain Any    ${innerHTML}    Advanced MathML: <svg    Advanced MathML:<svg
    # styling
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${bold_exists}    Should Match Regexp    ${innerHTML}    <b>(.|\n)+?<\/b>
    ${italics_exists}    Should Match Regexp    ${innerHTML}    <i>(.|\n)+?<\/i>
    ${undeline_exists}    Should Match Regexp    ${innerHTML}    <u>(.|\n)+?<\/u>
    ${strikethrough_exists}    Should Match Regexp    ${innerHTML}    <s>(.|\n)+?<\/s>
    ${links_exists}    Should Match Regexp    ${innerHTML}    <a(.|\n)+?<\/a>
    # hidden text
    ${hidden_text_exists}    Should Not Match Regexp    ${innerHTML}    Should not be visible
    # image and sound
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img
    ${width}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img    naturalWidth
    Should Be True    ${width} > 0
    Sleep    2
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="open-short-answer"]//*[contains(text(), "Pohjolan väkiluku k")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="open-short-answer"]//*[contains(text(), "Alternative answer.")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="questions-container"]//*[@class="block-content"]//*[contains(text(), "Millaisia viikinkien laivat olivat?")]
    Sleep    2
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Sleep    2
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[1]//button[@title="Liitä tiedosto"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[1]//button[@title="Äänitä tiedosto"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[1]//button[@title="Lisätietoa tiedostojen latauksesta"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[2]//button[@title="Liitä tiedosto"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[2]//button[@title="Äänitä tiedosto"]
    Wait Until Element Is Enabled    xpath=(.//*[@id="${section_id}"]//*[@class="fileupload-container"])[2]//button[@title="Lisätietoa tiedostojen latauksesta"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:34F35100-0F13-4E87-BD47-74A687AC6BBE"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:34F35100-0F13-4E87-BD47-74A687AC6BBE"]    Answer 1
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="E5FD1B3B-0A56-4CD1-B387-E33C3304363C"]//*[text()="Text content with image"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="es:88C840FA-6BEA-4FF8-8040-8BDC31EB89E2"]//*[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]    Answer 2
    Sleep    2
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]/parent::div[@class="text-area-container"]//div[@class="char-count"]
    ${charcount_text}    Get Text    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]/parent::div[@class="text-area-container"]//div[@class="char-count"]
    ${response}    Should Match Regexp    ${charcount_text}    Vastauksen pituus: \\d+ \\/ \\d+
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Sleep    2
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="infotext"]
    ${info_text}    Get Text    xpath=.//*[@id="${section_id}"]//div[@class="results"]//span[@class="infotext"]
    Should Match Regexp    ${info_text}    Vastaus on tallennettu arvioitavaksi \\d+\\.\\d+\\.\\d+ \\d+:\\d+\\.
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]//div[@class="feedback correct"]/p[text()="Erinomaista!"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    Answer 1
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    Answer 2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:34F35100-0F13-4E87-BD47-74A687AC6BBE"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:34F35100-0F13-4E87-BD47-74A687AC6BBE"]    Answer Fixored
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]
    Input Text    xpath=.//*[@id="${section_id}"]//*[@id="textForIdes:9A0C666D-6D66-4430-90E8-2C6548BC49F9"]    Answer Whatever
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Valmis"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="questions-container"]//div[@class="text-area-container"]//div[@class="answer-level-feedback-icon"]/i[@class="icon-bubble2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="tooltip"]
    ${innerHTML}    Get Element Attribute    xpath=.//div[@class="tooltip"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Should Contain    ${innerHTML}    This is a
    Should Contain    ${innerHTML}    feedback
    Should Contain    ${innerHTML}    test.
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="exercise-content"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    Answer Fixored
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    Answer Whatever
    ###
    # NOTE: the following command is added to enable clicking on the open extra panel button
    ###
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Check Extra Panel
    Wait Until Element Is Enabled    xpath=.//*[@id="es:7E1F2F70-BF30-43CC-BEAE-1B8ECE608178"]/i[@class="icon-video modal-trigger"]
    Click Element On Page    xpath=.//*[@id="es:CF810372-891C-42D8-938D-C2FCDB502DAF"]/i[@class="icon-picture modal-trigger"]
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-inner"]//img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-inner"]//img    naturalWidth
    Should Be True    ${width} > 0
    Click Element On Page    xpath=.//div[@class="modal is-visible"]


Validate Exercise - Select - Math
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_39}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Select - Math"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="banner-image-wrapper"]/span[@id="es:D9808FDA-128B-4D63-8BC5-B30303DC9BA5"]/span[@class="image-container"]//img[@class="fig-image"]
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]//img[4]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]//img[5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][2]//img[6]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[2]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[3]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[8]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][5]//img[3]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][6]//img[6]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][4]//img[5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[10]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Element Is Enabled    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]/i[@class="icon-correct marked-correct is-correct"]
    Check Extra Panel


Validate Exercise - Select - Math 2
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_exercise_40}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title"]
    ${innerHTML}    Get Element Attribute    xpath=.//h1[@class="exercise-title"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Select - Math type 2"]
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="sample-image-wrapper"]//img
    Sleep    2
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]//img[not(contains(@class, "sample-image"))][9]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]//img[not(contains(@class, "sample-image"))][10]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//button[@class="phase-button next"]/*[text()="Seuraava"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][2]//img[not(contains(@class, "sample-image"))][6]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][2]//img[not(contains(@class, "sample-image"))][5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][2]//img[not(contains(@class, "sample-image"))][4]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//button[@class="phase-button next"]/*[text()="Seuraava"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[not(contains(@class, "sample-image"))][7]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[not(contains(@class, "sample-image"))][8]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[not(contains(@class, "sample-image"))][1]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    ${points_text}    Get Text    xpath=.//span[@class="show-points"]
    Should Be Equal As Strings    ${points_text}    Sait 0/3 oikein!
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="0"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":0
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]//img[not(contains(@class, "sample-image"))][5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//button[@class="phase-button next"]/*[text()="Seuraava"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//button[@class="phase-button next"]/*[text()="Seuraava"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][3]//img[not(contains(@class, "sample-image"))][5]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Wait Until Element Is Enabled    xpath=.//div[@class="correct-answers"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="texts-container clearfix"]//div[starts-with(@class, "select-image-wrapper")][1]/i[@class="icon-correct marked-correct is-correct"]
    Check Extra Panel


Check Extra Panel
    [Arguments]    ${chapter_section}=${EMPTY}
    Run Keyword If    '${chapter_section}'=='${EMPTY}'    Click Element On Page    xpath=.//span[@class="toggle-panel"]
    ...    ELSE  Click Element On Page    xpath=.//section[@data-content-type="${chapter_section}"]//div[@class="toggle-panel"]
    ${section_element}    Run Keyword If    '${chapter_section}'=='${EMPTY}'    Get Webelement    xpath=.//aside
    ...    ELSE  Get Webelement    xpath=.//section[@data-content-type="${chapter_section}"]//aside
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    ${section_inner_html}    Get Element Attribute    ${section_element}    innerHTML
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img
    ${width}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img    naturalWidth
    Should Be True    ${width} > 0
    # Math exists #
    # here a check for the math formula
    #
    #
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    Should Contain    ${innerHTML}    <sup>4</sup>-y<sup>e/6</sup>. Chemistry H<sub>2</sub>
    Should Contain Any    ${innerHTML}    Advanced MathML: <svg    Advanced MathML:<svg
    ###
    # Side bar
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${bold_exists}    Should Match Regexp    ${innerHTML}    <b>(.|\n)+?<\/b>
    ${italics_exists}    Should Match Regexp    ${innerHTML}    <i>(.|\n)+?<\/i>
    ${undeline_exists}    Should Match Regexp    ${innerHTML}    <u>(.|\n)+?<\/u>
    ${strikethrough_exists}    Should Match Regexp    ${innerHTML}    <s>(.|\n)+?<\/s>
    ${links_exists}    Should Match Regexp    ${innerHTML}    <a(.|\n)+?<\/a>
    ###
    # Block level elements
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${blockquote_exists}    Should Match Regexp    ${innerHTML}    <blockquote(.|[\r\n])+?<\/blockquote>    global=true
    ${subheader_exists}    Should Match Regexp    ${innerHTML}    (?i)<h3(.|[\r\n])*?subheader(.|[\r\n])*?<\/h3>
    # definition List
    #dl dd p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dd//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #dl dt p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dt//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #ul li ul li p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//ul//li//ul//li//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    ${background_image_exists}    Execute JavaScript    var sectionElement = document.getElementById("${section_id}"); var backgroundImage = sectionElement.querySelector(".responsive-image-element.background"); if (backgroundImage) { var urlExists = backgroundImage.style.backgroundImage.includes("cloudfront.net")} else { var urlExists=true }; return urlExists;
    Should Be True    ${background_image_exists}==${True}
    ###
    # Video and video caption
    ###
    # video URL
    Page Should Contain Element    xpath=.//video
    ${innerHTML}    Get Element Attribute    xpath=.//video    innerHTML
    Should Contain    ${innerHTML}    cloudfront.net
    #
    Page Should Contain Element    xpath=.//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]
    ${innerHTML}    Get Element Attribute    xpath=.//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Close Extra Panel


Check Common Exercise Instruction Elements
    Set Section Elements
    ${section_inner_html}    Get Element Attribute    ${section_element}    innerHTML
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img
    ${width}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img    naturalWidth
    Should Be True    ${width} > 0
    # Math exists #
    # here a check for the math formula
    #
    #
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    Should Contain    ${innerHTML}    <sup>4</sup>-y<sup>e/6</sup>. Chemistry H<sub>2</sub>
    Should Contain Any    ${innerHTML}    Advanced MathML: <svg    Advanced MathML:<svg
    ###
    # Side bar
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${bold_exists}    Should Match Regexp    ${innerHTML}    <b>(.|\n)+?<\/b>
    ${italics_exists}    Should Match Regexp    ${innerHTML}    <i>(.|\n)+?<\/i>
    ${undeline_exists}    Should Match Regexp    ${innerHTML}    <u>(.|\n)+?<\/u>
    ${strikethrough_exists}    Should Match Regexp    ${innerHTML}    <s>(.|\n)+?<\/s>
    ${links_exists}    Should Match Regexp    ${innerHTML}    <a(.|\n)+?<\/a>
    ###
    # Block level elements
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${blockquote_exists}    Should Match Regexp    ${innerHTML}    <blockquote(.|[\r\n])+?<\/blockquote>    global=true
    ${subheader_exists}    Should Match Regexp    ${innerHTML}    (?i)<h3(.|[\r\n])*?subheader(.|[\r\n])*?<\/h3>
    # definition List
    #dl dd p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dd//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #dl dt p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dt//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #ul li ul li p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//ul//li//ul//li//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    ${background_image_exists}    Execute JavaScript    var sectionElement = document.getElementById("${section_id}"); var backgroundImage = sectionElement.querySelector(".responsive-image-element.background"); if (backgroundImage) { var urlExists = backgroundImage.style.backgroundImage.includes("cloudfront.net")} else { var urlExists=true }; return urlExists;
    Should Be True    ${background_image_exists}==${True}
    ###
    # Video and video caption
    ###
    # video URL
    Page Should Contain Element    xpath=.//video
    ${innerHTML}    Get Element Attribute    xpath=.//video    innerHTML
    Should Contain    ${innerHTML}    cloudfront.net
    #
    Page Should Contain Element    xpath=.//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]
    ${innerHTML}    Get Element Attribute    xpath=.//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0


# chapters
Validate Chapter - Text A + Notes + Popup Image
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_1}
    Go To    ${url}
    ${width_type}    Execute JavaScript    var imageElement = document.querySelector(".section-figure[data-mime-type='image/gif']"); return typeof imageElement.offsetWidth;
    ${width}    Execute JavaScript    var imageElement = document.querySelector(".section-figure[data-mime-type='image/gif']"); return imageElement.offsetWidth;
    Should Not Be True    '${width_type}'=='undefined'
    Should Be True    ${width} > 0
    Wait Until Element Is Enabled    xpath=.//section[@id="CAF41FC1-A930-4042-89C1-FCEFF122C0CD"]//h2[text()="Text A"]
    Check Extra Panel    text-a
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//header//*[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//section[@data-content-type="text-a"]//header//*[@class="audio-controls"]//*[@class="audio-play-pause audio-pause"]
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//button[@class="note-toggle icon-pen"]
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//input[@class="note-field"]
    Input Text    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//input[@class="note-field"]    note in text a
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[text()="Lisää"]
    Wait Until Element Is Enabled    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="note in text a"]
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//input[@class="note-field"]
    Input Text    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//input[@class="note-field"]    another note
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[text()="Lisää"]
    Wait Until Element Is Enabled    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="another note"]
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="edit-note icon-pen"]
    Click Element On Page    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="note in text a"]
    Input Text    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="note in text a"]    note in text a that has been edited
    Wait Until Element Is Enabled    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="note in text a that has been edited"]
    Wait Until Element Is Enabled    xpath=.//section[@data-content-type="text-a"]//*[@class="note-container"]//*[@class="notes"]//*[@class="note-item-text" and text()="another note"]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:8A3636A9-9C56-4FF2-BB71-D2F59B540842"]//*[@class="image-container"]
    Mouse Over    xpath=.//*[@id="es:8A3636A9-9C56-4FF2-BB71-D2F59B540842"]//*[@class="image-container"]//img
    Click Element On Page    xpath=.//*[@id="es:8A3636A9-9C56-4FF2-BB71-D2F59B540842"]//*[@class="image-container"]//i[@class="icon-zoom-plus"]
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-container image is-visible"]/img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-container image is-visible"]/img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'
    Click Element On Page    xpath=.//div[@class="modal is-visible"]
    Check Sound In Header    text-a


Validate Chapter - Text A Extra
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_2}
    Go To    ${url}
    Check Extra Panel    text-a


Validate Chapter - Text B
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_3}
    Go To    ${url}
    Check Common Chapter Elements    text-b
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="text-b"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Text B"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//p[@id="F168E4F0-8707-482F-9143-5FF84F0FB67A"]
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//p[@id="F168E4F0-8707-482F-9143-5FF84F0FB67A"]
    ${selected_text_stripped}    Strip String    ${selected_text}
    ${expected_text}    Set Variable    Text B body. With arkku-link. Keskiajaksi kutsutaan aikaa, joka alkoi Rooman valtakunnan jakaantumisen ja Länsi-Rooman tuhoutumisen jälkeen. Keskiajan kuninkailla oli vähän valtaa, ja he tarvitsivat aatelisten apua valtakuntiensa hallitsemiseen. Uskonto ja katolinen kirkko vaikuttivat vahvasti ihmisten elämään.\n Forced linebreak. Keskiajan alussa monet ihmiset muuttivat maaseudulle ja kaupunkien väkiluku laski.
    ${expected_text_stripped}    Strip String    ${expected_text}
    Should Be Equal As Strings    ${selected_text_stripped}    ${expected_text_stripped}
    Check Sound In Header    text-b


Validate Chapter - Text B Extra
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_4}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="text-b"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="toggle-panel"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//aside//h2[text()="Text B extra"]
    Check Common Chapter Elements    text-b


Validate Chapter - Text C
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_5}
    Go To    ${url}
    Check Common Chapter Elements    text-c
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="text-c"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Text C"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//p[@id="EC27D504-53BF-4DE8-901F-715A70C4445E"]
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//p[@id="EC27D504-53BF-4DE8-901F-715A70C4445E"]
    ${selected_text_stripped}    Strip String    ${selected_text}
    ${expected_text}    Set Variable    Text C body. With arkku-link. Keskiajaksi kutsutaan aikaa, joka alkoi Rooman valtakunnan jakaantumisen ja Länsi-Rooman tuhoutumisen jälkeen. Keskiajan kuninkailla oli vähän valtaa, ja he tarvitsivat aatelisten apua valtakuntiensa hallitsemiseen. Uskonto ja katolinen kirkko vaikuttivat vahvasti ihmisten elämään.\n Forced linebreak. Keskiajan alussa monet ihmiset muuttivat maaseudulle ja kaupunkien väkiluku laski.
    ${expected_text_stripped}    Strip String    ${expected_text}
    Should Be Equal As Strings    ${selected_text_stripped}    ${expected_text_stripped}
    Check Common Chapter Elements    text-c
    Check Sound In Header    text-c
    Wait Until Element Is Enabled    xpath=.//*[@id="es:CB7AA5E1-D7B0-4CCA-8199-CD91B55A5168"]//img[@class="fig-image"]
    Mouse Over    xpath=.//*[@id="es:CB7AA5E1-D7B0-4CCA-8199-CD91B55A5168"]//img[@class="fig-image"]
    Click Element On Page    xpath=.//*[@id="es:CB7AA5E1-D7B0-4CCA-8199-CD91B55A5168"]//span[@class="zoom-icon"]/i[@class="icon-zoom-plus"]
    Wait Until Element Is Enabled    xpath=.//div[@class="modal-container image is-visible"]/img
    ${width}    Get Element Attribute    xpath=.//div[@class="modal-container image is-visible"]/img    naturalWidth
    Should Be True    ${width} > 0
    Should Not Be True    '${width}'=='undefined'
    Click Element On Page    xpath=.//div[@class="modal is-visible"]


Validate Chapter - Text C Extra
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_6}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="text-c"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="toggle-panel"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//aside//h2[text()="Text C extra"]
    Check Common Chapter Elements    text-c


Validate Chapter - Validate Story
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_7}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="story"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Story"]
    ${screen_width}    Execute JavaScript    var screenWidth = screen.width; return screenWidth;
    Run Keyword If    ${screen_width} <= 600    Element Should Not Be Visible    xpath=.//section[@id="${section_id}"]//*[@id="es:A40DC38F-A5A2-4DCB-AA3A-AD7DFF520633"]//img[@class="fig-image"]
    Run Keyword If    ${screen_width} <= 600    Element Should Not Be Visible    xpath=.//section[@id="${section_id}"]//*[@id="es:A40DC38F-A5A2-4DCB-AA3A-AD7DFF520633"]//*[@class="figcaption"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="story-container"]//*[@class="story"]/p
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//*[@class="story-container"]//*[@class="story"]/p
    ${selected_text_length}    Get Length    ${selected_text}
    Should Be True    ${selected_text_length} > 0
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="story-container"]//*[@class="story"]//blockquote/p[text()="Kalevala"]
    Check Common Chapter Elements    story


Validate Chapter - Validate Story Extra
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_8}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="story"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="toggle-panel"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//aside//h2[text()="Story extra"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//aside//*[@class="text-extra"]//p
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//aside//*[@class="text-extra"]//p
    ${selected_text_length}    Get Length    ${selected_text}
    Should Be True    ${selected_text_length} > 0
    Check Common Chapter Elements    story


Validate Chapter - Validate Article
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_9}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="article"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Article"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="article-text"]/p
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//*[@class="article-text"]/p
    ${selected_text_length}    Get Length    ${selected_text}
    Should Be True    ${selected_text_length} > 0
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[text()="Hangon Sanomat 12.4.1984"]
    Check Common Chapter Elements    article


Validate Chapter - Validate Article Extra
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_10}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="article"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="toggle-panel"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//aside//h2[text()="Article extra"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//p[@id="18BDE5D1-0A38-4DA0-84BC-4935DEDECD63"]
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//p[@id="18BDE5D1-0A38-4DA0-84BC-4935DEDECD63"]
    ${selected_text_stripped}    Strip String    ${selected_text}
    ${expected_text}    Set Variable    Article extra body. With hs-arkku-link. Keskiajaksi kutsutaan aikaa, joka alkoi Rooman valtakunnan jakaantumisen ja Länsi-Rooman tuhoutumisen jälkeen. Keskiajan kuninkailla oli vähän valtaa, ja he tarvitsivat aatelisten apua valtakuntiensa hallitsemiseen. Uskonto ja katolinen kirkko vaikuttivat vahvasti ihmisten elämään.\n Forced linebreak. Keskiajan alussa monet ihmiset muuttivat maaseudulle ja kaupunkien väkiluku laski.
    ${expected_text_stripped}    Strip String    ${expected_text}
    Should Be Equal As Strings    ${selected_text_stripped}    ${expected_text_stripped}
    Check Common Chapter Elements    article


Validate Chapter - Validate Song
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_11}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//section[@id="993847B0-FDE5-4C42-A6E0-4FC336B15977"]//h2[text()="Song"]
    Wait Until Element Is Enabled    xpath=.//*[@id="C6C9FFCD-7D75-49A9-A753-F346F2C66C98"]
    Wait Until Element Is Enabled    xpath=.//div[@class="lyrics"]//*[text()="© Heikki Salo"]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:2825428A-702B-4584-B36D-30AA34FADC4F"]//*[@class="image-container"]//img[@class="fig-image"]
    Check Sound In Header    song
    Check Styling Exists    song
    Check Video Exists    song
    Check That Hidden Text Is Not Visible    song


Validate Chapter - Validate Dialogue
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_12}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="dialogue"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Dialogue"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="dialogue"]//*[@class="subtitle"]/p[@id="A0267100-DDC2-4395-8F1C-EC8A7D2F79DE"]
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//*[@class="dialogue"]//*[@class="subtitle"]/p[@id="A0267100-DDC2-4395-8F1C-EC8A7D2F79DE"]
    ${expected_text}    Set Variable    Dialogue body with math. Simple math 3x4‐ye/6. Chemistry H20. Erityisesti elämä kaupungeissa oli vaarallista ja vaivalloista. Kaupunkien aarteet houkuttelivat ryösteleviä armeijoita. Vesijohdot katkesivat ja sillat sortuivat, kun ihmiset eivät pitäneet niistä huolta. Advanced MathML: .
    Should Be Equal As Strings    ${selected_text}    ${expected_text}
    ${background_image_exists}    Execute JavaScript    var sectionElement = document.getElementById("${section_id}"); var backgroundImage = sectionElement.querySelector(".responsive-image-element.background"); var urlExists = backgroundImage.style.backgroundImage.includes("cloudfront.net"); return urlExists;
    Should Be True    ${background_image_exists}==${True}
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${hidden_text_exists}    Should Not Match Regexp    ${innerHTML}    Should not be visible
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="dialogue-comment"]/p[text()="Mitä ihmettä on tapahtunut!?"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="dialogue-container"]//*[@class="character"]/img[@class="dialogue-icon-image"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="dialogue-container"]//*[@class="character"]//h4/b[text()="Liisa"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@id="es:C8EA7774-ACE1-4C8D-A60F-A440B9033B2C"]/*[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@id="es:C8EA7774-ACE1-4C8D-A60F-A440B9033B2C"]//*[@class="audio-controls"]//*[@class="audio-controls-play"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@id="es:C8EA7774-ACE1-4C8D-A60F-A440B9033B2C"]//*[@class="audio-controls"]//*[@class="audio-stop"]


Validate Chapter - Validate Grammar And Grammar Annotation
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_13}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="grammar"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Grammar and Grammar Annotation"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="grammar-intro"]/p
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="grammar-intro"]//*[name()="svg"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="grammar-special-content"]//*[@class="grammar-text"]
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//*[@class="grammar-special-content"]//*[@class="grammar-text"]
    Should Start With    ${selected_text}    Grammar body with math. Simple math 3x4-ye/6. Chemistry H20.
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="annotations-container"]//*[@class="text-definition has-definition"]/p/b[text()="Ensimmäinen-pitkä-huomio"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="annotations-container"]//*[@class="text-definition has-definition"]/p/i[text()="apu-verbi"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="grammar-annotation"]//*[@class="text-wrapper"]//sup[text()="4"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//*[text()="Tämän on lauseen keskiosa."]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="grammar-annotation"]//*[@class="text-wrapper"]//u[text()="ostamaan"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="grammar-annotation"]//*[@class="text-wrapper"]//*[normalize-space(text())="Piia"]/b[normalize-space(text())="meni"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//s[text()="Tämä"]
    Wait Until Element Is Enabled    xpath=.//*[@class="tooltip"]//u[text()="lauseen"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${hidden_text_exists}    Should Not Match Regexp    ${innerHTML}    Should not be visible
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="grammar-content"]//*[@class="grammar-special-content"]//*[@class="grammar-examples"]//p[starts-with(text(), "Tässä jotain esimerkkejä.")]


Validate Chapter - Validate Phonetics
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_14}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="phonetics"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//h2[text()="Phonetics"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="phonetics-container"]//p
    ${selected_text}    Get Text    xpath=.//section[@id="${section_id}"]//*[@class="phonetics-container"]//p
    ${expected_text}    Set Variable    Phonetics body with math. Simple math 3x4-ye/6. Chemistry H20. Erityisesti elämä kaupungeissa oli vaarallista ja vaivalloista. Kaupunkien aarteet houkuttelivat ryösteleviä armeijoita. Vesijohdot katkesivat ja sillat sortuivat, kun ihmiset eivät pitäneet niistä huolta. Advanced MathML: .
    Should Be Equal As Strings    ${selected_text}    ${expected_text}
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="table-container"]//td//*[text()="The birthday party"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="table-container"]//td//*[text()="/æpl dʒu:s/"]
    Check Common Chapter Elements    phonetics


Validate Chapter - Validate Image Carousel And Flashcard
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_15}
    Go To    ${url}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="carousel"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]/ul[@class="flashcards flip-container"]
    ${cards_to_flip}    Get Element Count    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]/ul[@class="flashcards flip-container"]//li[starts-with(@class, "flashcard flipper")]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="prev-flashcard"]/i[@class="icon-left"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//h3/b[text()="Kortti 1, a-puoli, otsikko"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//i[@class="flip-card icon-redo"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//div[@class="card-back is-flipped"]//h3/b[text()="Kortti 1, b-puoli, subheader"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//h3/b[text()="Kortti 2 - yksipuolinen kortti (otsikko)"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//p[text()="Kolmas tapa syöttää kortin teksti on olla käyttämättä otsikoita ollenkaan."]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//*[@class="image-container"]//img[@class="fig-image"]
    Sleep    2
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//i[@class="flip-card icon-redo"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//div[@class="card-back is-flipped"]//p[starts-with(text(), "Kortin 1 b-puolen teksti. Teksti")]
    Sleep    2
    Click Element On Page    xpath=(.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-back is-flipped"]//i[@class="flip-card icon-redo"])[2]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//div[@class="card-front"]//p[text()="Kolmas tapa syöttää kortin teksti on olla käyttämättä otsikoita ollenkaan."]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//div[@class="card-back is-flipped"]//h3/b[text()="Kortti 1, b-puoli, subheader"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Click Element On Page    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="buttons"]//div[@class="next-flashcard"]/i[@class="icon-right"]
    Wait Until Element Is Enabled    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="flashcard-indicator-container"]//li[3]
    ${style}    Get Element Attribute    xpath=.//section[@id="${section_id}"]//*[@class="flashcards-wrapper"]//*[@class="flashcard-indicator-container"]//li[3]    class
    Should Be Equal As Strings    ${style}    flashcard-indicator
    Check Common Chapter Elements    carousel


# Desktop specifics
Navigate To First Chapter In Test Outline
    Navigate To First Chapter


Validate Arkku Link In Text A
    Navigate To First Chapter
    Click Element On Page    xpath=.//section//header/h2[text()="Text A"]/parent::header/parent::div//a[@class="open-lightbox"]
    Wait Until Element Is Enabled    xpath=(.//iframe)[1]
    Select Frame    xpath=(.//iframe)[1]
    Page Should Contain    Futurice
    Unselect Frame
    Click Element On Page    xpath=.//*[contains(@class, "close-lightbox")]


Navigate To Second Chapter In Test Outline
    Navigate To Second Chapter


Validate First Image In Story
    Navigate To Second Chapter
    Wait Until Element Is Enabled    xpath=(.//section[@data-content-type="story"]//aside//span[@class="image-container"]//img[@class="fig-image"])[1]


Validate Exercise - Play Crossword (Desktop)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_desktop_3}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]
    Execute JavaScript    document.querySelector("textarea").style.position = "static";
    Input Text    xpath=.//textarea    SÄÄNNÖT
    Input Text    xpath=.//textarea    TANKÖ
    Input Text    xpath=.//textarea    QWI
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[3 and text()="i" and contains(@class, "incorrect")]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="actual-content" and @data-content-type="crossword"]//img[@class="fig-image"]
    Click Element On Page    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[5 and text()="ö" and contains(@class, "incorrect")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="audio-element"]/i[@class="icon-audio"]
    Input Text    xpath=.//textarea    O
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="incorrect"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Input Text    xpath=.//textarea    SÄÄNNÖT
    Click Element On Page    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[5]
    Input Text    xpath=.//textarea    HEI
    Click Element On Page    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[6]
    Input Text    xpath=.//textarea    TANKO
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback correct"]/*[text()="correct"]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[7 and text()="a" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[7 and text()="i" and contains(@class, "correct")]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//*[text()="Tässä näet oikeat vastaukset!"]
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[2 and text()="ä" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[7 and text()="i" and contains(@class, "correct")]
    Check Extra Panel


# mobile specifics
Navigate To Second Chapter In Test Outline Mobile
    Navigate To Second Chapter
    Wait Until Element Is Enabled    xpath=.//div[@class="chapter responsive-container tiny"]//div[@class="lead-content"]/p
    ${element_text}    Get Text    xpath=.//div[@class="chapter responsive-container tiny"]//div[@class="lead-content"]/p
    Should Start With    ${element_text}    Blockquote that is shown as ingress.
    Wait Until Element Is Enabled    xpath=.//div[@class="chapter responsive-container tiny"]//div[@class="chapter-start-audio audio"]/*[@class="audio-element"]/i[@class="icon-audio"]


Validate Hotspot Startpage Mobile
    Navigate To Second Chapter
    Wait Until Element Is Enabled    xpath=.//div[@class="chapter responsive-container tiny"]//div[@class="lead-content"]/p


Validate Story
    Navigate To Second Chapter
    Validate Chapter - Validate Story


Validate Exercise - Match - Multiple Response (Assets)
    Navigate To Second Chapter
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_mobile_2}
    Go To    ${url}
    Set Section Elements
    Wait Until Element Is Enabled    xpath=.//h1[@class="exercise-title" and text()="Match - Multiple Response (Assets)"]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:7AFB2AA1-9161-4B13-BD9C-C72CEE6F2FE6"]//*[@class="video-container"]/video
    Wait Until Element Is Enabled    xpath=.//*[@id="es:1C9995A7-BF6D-4486-8DBE-5B145CDA74FA"]//*[@class="audio-element"]/i[@class="icon-audio"]
    Wait Until Element Is Enabled    xpath=.//*[@id="es:387C8ABB-7DF3-4A1A-ACC2-5A9332796859"]//*[@class="image-container"]/img[@class="fig-image"]
    Click Element On Page    xpath=.//*[@data-id="F5B17964-B395-425A-962A-05DE15C93C3F"]
    Click Element On Page    xpath=.//*[@data-id="98EA7F90-60A5-451E-9122-F42E312B27E0"]
    Click Element On Page    xpath=.//*[@data-id="7D56E2FE-1F08-4A76-B376-88C2CB70AB77"]
    Click Element On Page    xpath=.//*[@data-id="3F34052F-D822-4EE8-9942-07D1D037E639"]
    Click Element On Page    xpath=.//*[@data-id="BC938DF3-2537-4C11-8B9D-CCFC520DAB34"]
    Sleep    60
    Click Element On Page    xpath=.//*[@data-id="E75E0166-28FD-4A03-86F4-71C539F43202"]
    Click Element On Page    xpath=.//*[@data-id="BC938DF3-2537-4C11-8B9D-CCFC520DAB34"]
    Click Element On Page    xpath=.//*[@data-id="BC938DF3-2537-4C11-8B9D-CCFC520DAB34"]
    Sleep    5
    Click Element On Page    xpath=.//*[@data-id="3AED0C2C-AC76-4E1D-A5D4-F985D4893E2E"]
    Click Element On Page    xpath=.//*[@data-id="1B6B6FFF-448F-4C74-AF29-E6CDCF3699C7"]
    Click Element On Page    xpath=.//*[@data-id="6165C261-BA0F-40E1-A333-AB8B51D586E3"]
    Click Element On Page    xpath=.//*[@data-id="CA61196E-0FFF-4268-B176-D50BC849B38A"]
    Click Element On Page    xpath=.//*[@data-id="E967F1D8-614B-4915-A464-0EDF0F9A0B17"]
    Click Element On Page    xpath=.//*[@data-id="527532FD-DDC9-4129-974E-6C83B4B35018"]
    Click Element On Page    xpath=.//*[@data-id="F5B17964-B395-425A-962A-05DE15C93C3F"]
    Click Element On Page    xpath=.//*[@data-id="57855958-B687-4FF6-950B-381196F55D1D"]
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]


Validate Exercise - Play Crossword (Mobile)
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_desktop_3}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]
    Execute JavaScript    document.querySelector("textarea").style.position = "static";
    Input Text    xpath=.//textarea    SÄÄNNÖT
    Input Text    xpath=.//textarea    TANKÖ
    Input Text    xpath=.//textarea    QWI
    Set Section Elements
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[3 and text()="i" and contains(@class, "incorrect")]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":1
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//div[@class="actual-content" and @data-content-type="crossword"]//img[@class="fig-image"]
    ###
    # For mobile the following line
    ###
    Click Element On Page    xpath=(.//td[@class="nav-button next"])[1]
    Click Element On Page    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[4 and text()="k" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//*[@id="${section_id}"]//*[@class="audio-element"]/i[@class="icon-audio"]
    Input Text    xpath=.//textarea    K
    Input Text    xpath=.//textarea    O
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="2"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback incorrect"]/*[text()="incorrect"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":2
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Korjaa"]
    Click Element On Page    xpath=(.//td[@class="nav-button next"])[1]
    Click Element On Page    xpath=(.//td[@class="nav-button next"])[2]
    Click Element On Page    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[1 and text()="q" and contains(@class, "incorrect")]
    Input Text    xpath=.//textarea    HEI
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/div[@class="feedback correct"]/*[text()="correct"]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[7 and text()="a" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[7 and text()="i" and contains(@class, "correct")]
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "raw":3
    Wait Until Keyword Succeeds    10 sec    1 sec    Check Value In SessionStorage    ${section_id}    "max":3
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Aloita alusta"]
    Input Text    xpath=.//textarea    SÄÄNNÖT
    Input Text    xpath=.//textarea    TANKO
    Input Text    xpath=.//textarea    HEI
    Click Element On Page    xpath=.//*[@id="${section_id}"]//div[@class="control-button-container"]//button[text()="Tarkista"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="correct-count number" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]/span[@class="show-points"]/span[@class="total-count" and text()="3"]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][1]/span[7 and text()="t" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][2]/span[7 and text()="a" and contains(@class, "correct")]
    Wait Until Element Is Enabled    xpath=.//section[@data-type="crossword"]//div[@class="puzzle"]//div[starts-with(@class, "word")][3]/span[7 and text()="i" and contains(@class, "correct")]
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Wait Until Element Is Enabled    xpath=.//div[@class="results"]//*[text()="Tässä näet oikeat vastaukset!"]
    Click Element On Page    xpath=.//div[@title="Näytä oikeat vastaukset" and contains(@class, "correct-answers")]
    Check Common Chapter Elements    crossword


Check Sound In Header
    [Arguments]    ${section_type}
    Navigate To Second Chapter


Check Styling Exists
    [Arguments]    ${section_type}
    ${innerHTML}    Get Element Attribute    xpath=.//section[@data-content-type="${section_type}"]    innerHTML
    ${bold_exists}    Should Match Regexp    ${innerHTML}    <b>(.|\n)+?<\/b>
    ${italics_exists}    Should Match Regexp    ${innerHTML}    <i>(.|\n)+?<\/i>
    ${undeline_exists}    Should Match Regexp    ${innerHTML}    <u>(.|\n)+?<\/u>
    ${strikethrough_exists}    Should Match Regexp    ${innerHTML}    <s>(.|\n)+?<\/s>
    ${links_exists}    Should Match Regexp    ${innerHTML}    <a(.|\n)+?<\/a>


Check Video Exists
    [Arguments]    ${section_type}
    Page Should Contain Element    xpath=.//section[@data-content-type="${section_type}"]//video
    ${innerHTML}    Get Element Attribute    xpath=.//section[@data-content-type="${section_type}"]//video    innerHTML
    Should Contain    ${innerHTML}    cloudfront.net
    #
    Page Should Contain Element    xpath=.//section[@data-content-type="${section_type}"]//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]
    ${innerHTML}    Get Element Attribute    xpath=.//section[@data-content-type="${section_type}"]//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0


Check That Hidden Text Is Not Visible
    [Arguments]    ${section_type}
    ${innerHTML}    Get Element Attribute    xpath=.//section[@data-content-type="${section_type}"]    innerHTML
    ${hidden_text_exists}    Should Not Match Regexp    ${innerHTML}    Should not be visible


Check Common Chapter Elements
    [Arguments]    ${section_type}
    ${section_element}    Get Webelement    xpath=.//section[@data-content-type="${section_type}"]
    ${section_id}    Get Element Attribute    ${section_element}    id
    ${section_data_type}    Get Element Attribute    ${section_element}    data-content-type
    ${section_inner_html}    Get Element Attribute    ${section_element}    innerHTML
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//td[@class="heading"]/p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="audio-element"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img
    ${width}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//table//span[@class="image-container"]//img    naturalWidth
    Should Be True    ${width} > 0
    # Math exists #
    # here a check for the math formula
    #
    #
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//sup[text()="4"]
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    Should Contain    ${innerHTML}    <sup>4</sup>-y<sup>e/6</sup>. Chemistry H<sub>2</sub>
    Should Contain Any    ${innerHTML}    Advanced MathML: <svg    Advanced MathML:<svg
    ###
    # Side bar
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${bold_exists}    Should Match Regexp    ${innerHTML}    <b>(.|\n)+?<\/b>
    ${italics_exists}    Should Match Regexp    ${innerHTML}    <i>(.|\n)+?<\/i>
    ${undeline_exists}    Should Match Regexp    ${innerHTML}    <u>(.|\n)+?<\/u>
    ${strikethrough_exists}    Should Match Regexp    ${innerHTML}    <s>(.|\n)+?<\/s>
    ${links_exists}    Should Match Regexp    ${innerHTML}    <a(.|\n)+?<\/a>
    ###
    # Block level elements
    ###
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${blockquote_exists}    Should Match Regexp    ${innerHTML}    <blockquote(.|[\r\n])+?<\/blockquote>    global=true
    ${subheader_exists}    Should Match Regexp    ${innerHTML}    (?i)<h3(.|[\r\n])*?subheader(.|[\r\n])*?<\/h3>
    # definition List
    #dl dd p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dd//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #dl dt p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//dl//dt//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    #ul li ul li p
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//ul//li//ul//li//p    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0
    ### ------------------------------------
    ### flashcards do not support H3 headers
    ### ------------------------------------
    ###
    # Check background image exists - NEEDS MORE INVESTIGATION #
    ###
    #---var backgroundImage = sectionElement.querySelector(".responsive-image-element.background")
    #---if backgroundImage ->>>> var urlExists = backgroundImage.style.backgroundImage.includes("cloudfront.net");
    ${background_image_exists}    Execute JavaScript    var sectionElement = document.getElementById("${section_id}"); var backgroundImage = sectionElement.querySelector(".responsive-image-element.background"); if (backgroundImage) { var urlExists = backgroundImage.style.backgroundImage.includes("cloudfront.net")} else { var urlExists=true }; return urlExists;
    Should Be True    ${background_image_exists}==${True}
    ###
    # Video and video caption
    ###
    # video URL
    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//video
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//video    innerHTML
    Should Contain    ${innerHTML}    cloudfront.net
    #
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Page Should Contain Element    xpath=.//*[@id="${section_id}"]//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]
    #Page Should Contain Element    xpath=.//*[@id="${section_id}"]//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]
    Run Keyword If    '${status}'=='PASS'    Get Video Caption    ${section_id}
    # hidden text
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]    innerHTML
    ${hidden_text_exists}    Should Not Match Regexp    ${innerHTML}    Should Not Be Visible


Get Video Caption
    [Arguments]    ${section_id}
    ${innerHTML}    Get Element Attribute    xpath=.//*[@id="${section_id}"]//span[normalize-space(@class)="section-figure video" and @data-mime-type="video/mp4"]//span[@class="figcaption"]    innerHTML
    ${element_length}    Get Length    ${innerHTML}
    Should Be True    ${element_length} > 0


Navigate To First Chapter
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_1}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1/*[@class="chapter-title"]
    ${chapter_title}    Get Text    xpath=.//h1/*[@class="chapter-title"]
    ${chapter_title_length}    Get Length    ${chapter_title}
    Should Be True    ${chapter_title_length} > 0


Navigate To Second Chapter
    ${url}    Catenate    SEPARATOR=    ${leo_base}    ${leo_chapter_desktop_2}
    Go To    ${url}
    Wait Until Element Is Enabled    xpath=.//h1/*[@class="chapter-title"]
    ${chapter_title}    Get Text    xpath=.//h1/*[@class="chapter-title"]
    ${chapter_title_length}    Get Length    ${chapter_title}
    Should Be True    ${chapter_title_length} > 0


Scroll Page To Top
    Execute JavaScript    window.scrollTo(0, 0);


Scroll Page To Bottom
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);


Close Extra Panel
    [Arguments]    ${chapter_section}=${EMPTY}
    Run Keyword If    '${chapter_section}'=='${EMPTY}'    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//button[@class="toggle-extra icon-cross"]
    ...    ELSE  Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//section[@data-content-type="${chapter_section}"]//button[@class="toggle-extra icon-cross"]


Maximize Browser
    Maximize Browser Window


Seleniumlibrary Common Failure
    [Documentation]    This keyword registered to be used in the SeleniumLibrary when error occurs in library keyword execution.
    SeleniumLibrary.Maximize Browser Window
    SeleniumLibrary.Log Source
    SeleniumLibrary.Capture Page Screenshot

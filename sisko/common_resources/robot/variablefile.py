# -*- coding: utf-8 -*-
# import re
# import platform
# import socket


def get_variables(environment):
    environment = environment.strip().lower()
    if environment == "local":
        return local()
    elif environment == "dev":
        return dev()
    elif environment == "qa":
        return qa()
    elif environment == "dev_su":
        return dev_su()
    elif environment == "qa_su":
        return qa_su()
    elif environment == "prod":
        return prod()
    else:
        message = 'Could not match to predefined environments: Use local, dev, dev_su, qa, qa_su or prod'
        print(message)
        return qa()


def default_var():
    main_dictionary = {}

    main_dictionary['BROWSERSTACK_USER'] =                     "browserstack_user_here"
    main_dictionary['BROWSERSTACK_KEY'] =                      "browserstack_key_here"
    main_dictionary['ROLE_TEACHER'] =                          "teacher"
    main_dictionary['ROLE_STUDENT'] =                          "student"
    main_dictionary['ROLE_PUPIL'] =                            "pupil"
    main_dictionary['DEFAULT_SCHOOL'] =                        "Pohjois-Haagan Yhteiskoulu"
    main_dictionary['DEFAULT_SCHOOL_LICENSE'] =                "SP test lukio"
    main_dictionary['DEFAULT_METHOD'] =                        "0 Sisko Manual MR"
    main_dictionary['DEFAULT_METHOD_FOR_LICENSES'] =           "FyKe 7-9 Fysiikka"
    main_dictionary['DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO'] = "On the Go 1"
    main_dictionary['DEFAULT_METHOD_FOR_LICENSES_VERSO'] =     "Verso 1"
    main_dictionary['DEFAULT_METHOD_FOR_LICENSES_GEOIDI'] =    "Geoidi Muuttuvat maisemat ja elinympäristöt"
    main_dictionary['DEFAULT_METHOD_SU'] =                     "SUD to SISKO NEW"
    # This will work in SP Dev1 but not in SP QA as SP QA doesn't have 'På gång 1' method published
    # main_dictionary['ASSIGNMENT_PUBLISHER_FEED'] =             "0 Sisko Manual MR"
    main_dictionary['ASSIGNMENT_PUBLISHER_FEED'] =             "På gång 1"
    main_dictionary['DEFAULT_CHAPTER_1_FOR_ASSIGNMENT'] =      "Koll på dagen"
    main_dictionary['DEFAULT_CHAPTER_2_FOR_ASSIGNMENT'] =      "Koll på fritiden"
    # NOTE: Can be used only when needed content items for Assignments are available in "0 Sisko Manual MR"
    # main_dictionary['DEFAULT_METHOD_FOR_ASSIGNMENT'] =         "Sisko Manual MR FOR ASSIGNMENT"
    main_dictionary['DEFAULT_METHOD_FOR_ASSIGNMENT'] =         "På gång 1 FOR ASSIGNMENT"
    main_dictionary['DEFAULT_METHOD_FOR_DASHBOARD'] =          "Sisko Manual MR FOR DASHBOARD"
    main_dictionary['DEFAULT_METHOD_FOR_HOMEWORK'] =           "Sisko Manual FOR HOMEWORK"
    main_dictionary['DEFAULT_METHOD_FOR_FEEDS'] =              "Sisko Manual FOR FEEDS"
    main_dictionary['DEFAULT_METHOD_FOR_FEEDS_RENAME'] =       "Sisko Manual FOR FEED RENAMING"
    main_dictionary['DEFAULT_METHOD_FOR_EXERCISES'] =          "Sisko Manual FOR EXERCISES"
    main_dictionary['DEFAULT_METHOD_FOR_SMOKE'] =              "Sisko Manual FOR SMOKE"
    main_dictionary['DEFAULT_METHOD_FOR_THEORY'] =             "Test outline with real content"
    main_dictionary['TAB_TOC'] =                               "side-nav-mode-toc"
    main_dictionary['TAB_HOMEWORK'] =                          "side-nav-mode-homework"
    main_dictionary['MENU_HOMEWORK_NEW'] =                     "new-homework-accordion"
    main_dictionary['MENU_HOMEWORK_CURRENT'] =                 "current-homework-accordion"
    main_dictionary['MENU_HOMEWORK_OLD'] =                     "old-homework-accordion"
    main_dictionary['FEED_TYPE_CURRENT'] =                     "new"
    main_dictionary['FEED_TYPE_HOMEWORK'] =                    "homework"
    main_dictionary['FEED_TYPE_PUBLISHER'] =                   "publisher"
    main_dictionary['FEED_TYPE_TEACHER'] =                     "teacher"
    main_dictionary['FEED_TYPE_PUBLISHER_TXT_FI'] =            "Kustantaja"
    main_dictionary['FEED_TYPE_TEACHER_TXT_FI'] =              "Opettaja"
    main_dictionary['FEED_TYPE_PUBLISHER_TXT_SV'] =            "Förläggare"
    main_dictionary['FEED_TYPE_TEACHER_TXT_SV'] =              "Lärare"
    main_dictionary['FEED_TYPE_PUBLISHER_TXT_EN'] =            "Publisher"
    main_dictionary['FEED_TYPE_TEACHER_TXT_EN'] =              "Teacher"
    main_dictionary['FEED_DELETION_CONFIRM_TXT_FI'] =          "poista"
    main_dictionary['FEED_DELETION_CONFIRM_TXT_SV'] =          "radera"
    main_dictionary['FEED_DELETION_CONFIRM_TXT_EN'] =          "delete"
    main_dictionary['LTI_LINK_TEAS'] =                         "TEAS"
    main_dictionary['LTI_LINK_EKOETEHTAVAPANKKI'] =            "eKoetehtäväpankki"
    main_dictionary['LTI_LINK_VIOPE'] =                        "Viope"
    main_dictionary['NAVIGATION_LEFT'] =                       "left"
    main_dictionary['NAVIGATION_RIGHT'] =                      "right"
    main_dictionary['CONTENT_ITEM_TYPE_TEXT'] =                "text"
    main_dictionary['CONTENT_ITEM_TYPE_EXERCISE'] =            "exercise"
    main_dictionary['CONTENT_ITEM_TYPE_TEACHER_MATERIAL'] =    "teachers_material"
    main_dictionary['CONTENT_ITEM_TYPE_CLASSWORK'] =           "classwork"
    main_dictionary['CONTENT_ITEM_TYPE_TEACHERS_GUIDE'] =      "teachers_guide"
    main_dictionary['CONTENT_ITEM_TYPE_RESEARCH'] =            "research"
    main_dictionary['CONTENT_ITEM_TYPE_MATERIAL'] =            "material"
    main_dictionary['CONTENT_ITEM_TYPE_RESEARCH_QUESTIONS'] =  "reaserch_questions"
    main_dictionary['CONTENT_ITEM_TYPE_HOMEWORK'] =            "homework"
    main_dictionary['CONTENT_ITEM_TYPE_CHAPTER'] =             "chapter"
    main_dictionary['CONTENT_ITEM_TYPE_EXTRA_EXERCISE'] =      "SP - Lisätehtävä"
    main_dictionary['CONTENT_ITEM_TYPE_SELF_ASSESSMENT'] =     "SP - Itsearvionti"
    main_dictionary['OPERATION_SUCCESS'] =                     "success"
    main_dictionary['OPERATION_FAILURE'] =                     "failure"
    main_dictionary['UPLOAD_IMAGE'] =                          "sanomapro.jpg"
    main_dictionary['FEEDBACK_STATUS_SUBMITTED'] =             ["SUBMITTED", "LÄHETETTY", "SKICKAD"]
    main_dictionary['FEEDBACK_STATUS_NOT_SUBMITTED'] =         ["NOT SUBMITTED", "EI LÄHETETTY", "INTE SKICKAD"]
    main_dictionary['DUEDATE_FORMAT_FI'] = "%d.%m.%Y"
    main_dictionary['DUEDATE_FORMAT_SV'] = "%Y-%m-%d"
    main_dictionary['DUEDATE_FORMAT_EN'] = "%d/%m/%Y"
    main_dictionary['DUEDATE_INVALID_FORMAT_ERROR_FI'] =       "Kirjoita päivä muotoon pp.kk.vvvv"
    main_dictionary['DUEDATE_INVALID_FORMAT_ERROR_SV'] =       "Datum måste anges i formatet åååå-mm-dd"
    main_dictionary['DUEDATE_INVALID_FORMAT_ERROR_EN'] =       "Date should be in the format dd/mm/yyyy"
    main_dictionary['DUEDATE_PAST_DATE_ERROR_FI'] = "Päivämäärä ei voi olla menneisyydessä"
    main_dictionary['DUEDATE_PAST_DATE_ERROR_SV'] = "Inlämningsdatum kan inte vara tidigare än dagens datum"
    main_dictionary['DUEDATE_PAST_DATE_ERROR_EN'] = "Due date cannot be in the past"
    main_dictionary['DUEDATE_FUTURE_DATE_OVER_1_YEAR_ERROR_FI'] = "Päivämäärä ei voi olla yli vuoden päässä"
    main_dictionary['DUEDATE_FUTURE_DATE_OVER_1_YEAR_ERROR_SV'] = "Inlämningsdatum kan inte vara senare än 1 år framåt i tiden"
    main_dictionary['DUEDATE_FUTURE_DATE_OVER_1_YEAR_ERROR_EN'] = "Due date cannot be more than 1 year in the future"
    return main_dictionary


def default_suite_credentials_dev(main_dictionary):

    # Dashboard
    main_dictionary['USER_FIRST_NAME_TEACHER_1_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1_DASHBOARD']   =   "Teacher501"
    main_dictionary['USER_NAME_TEACHER_1_DASHBOARD']        =   "siskoautomation.teacher501"
    main_dictionary['USER_PASSWORD_TEACHER_1_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_TEACHER_2_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_2_DASHBOARD']   =   "Teacher502"
    main_dictionary['USER_NAME_TEACHER_2_DASHBOARD']        =   "siskoautomation.teacher502"
    main_dictionary['USER_PASSWORD_TEACHER_2_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_1_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1_DASHBOARD']   =   "Student501"
    main_dictionary['USER_NAME_STUDENT_1_DASHBOARD']        =   "siskoautomation.student501"
    main_dictionary['USER_PASSWORD_STUDENT_1_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_2_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_2_DASHBOARD']   =   "Student502"
    main_dictionary['USER_NAME_STUDENT_2_DASHBOARD']        =   "siskoautomation.student502"
    main_dictionary['USER_PASSWORD_STUDENT_2_DASHBOARD']    =   "Sisko18"

    # Security
    main_dictionary['USER_NAME_TEACHER_1_SECURITY']         =   "siskoautomation.teacher503"
    main_dictionary['USER_PASSWORD_TEACHER_1_SECURITY']     =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_SECURITY']         =   "siskoautomation.teacher504"
    main_dictionary['USER_PASSWORD_TEACHER_2_SECURITY']     =   "Sisko18"

    # Exercises
    main_dictionary['USER_NAME_TEACHER_1_EXERCISES1']        =   "siskoautomation.teacher505"
    main_dictionary['USER_PASSWORD_TEACHER_1_EXERCISES1']    =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_EXERCISES1']        =   "siskoautomation.student503"
    main_dictionary['USER_PASSWORD_STUDENT_1_EXERCISES1']    =   "Sisko18"

    main_dictionary['USER_NAME_TEACHER_1_EXERCISES2']        =   "siskoautomation.teacher516"
    main_dictionary['USER_PASSWORD_TEACHER_1_EXERCISES2']    =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_EXERCISES2']        =   "siskoautomation.student511"
    main_dictionary['USER_PASSWORD_STUDENT_1_EXERCISES2']    =   "Sisko18"

    # Feeds
    main_dictionary['USER_NAME_TEACHER_1_FEEDS1']            =   "siskoautomation.teacher506"
    main_dictionary['USER_PASSWORD_TEACHER_1_FEEDS1']        =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_FEEDS1']            =   "siskoautomation.teacher507"
    main_dictionary['USER_PASSWORD_TEACHER_2_FEEDS1']        =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_FEEDS1']            =   "siskoautomation.student504"
    main_dictionary['USER_PASSWORD_STUDENT_1_FEEDS1']        =   "Sisko18"
    main_dictionary['USER_NAME_PUPIL_1_FEEDS1']              =   "siskoautomation.pupil501"
    main_dictionary['USER_PASSWORD_PUPIL_1_FEEDS1']          =   "Sisko18"

    main_dictionary['USER_NAME_TEACHER_1_FEEDS2']            =   "siskoautomation.teacher514"
    main_dictionary['USER_PASSWORD_TEACHER_1_FEEDS2']        =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_FEEDS2']            =   "siskoautomation.teacher515"
    main_dictionary['USER_PASSWORD_TEACHER_2_FEEDS2']        =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_FEEDS2']            =   "siskoautomation.student505"
    main_dictionary['USER_PASSWORD_STUDENT_1_FEEDS2']        =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_2_FEEDS2']            =   "siskoautomation.student512"
    main_dictionary['USER_PASSWORD_STUDENT_2_FEEDS2']        =   "Sisko18"

    # Homework Central
    main_dictionary['USER_NAME_TEACHER_1_HOMEWORK']         =   "siskoautomation.teacher508"
    main_dictionary['USER_PASSWORD_TEACHER_1_HOMEWORK']     =   "Sisko18"

    # Monitoring
    main_dictionary['USER_NAME_TEACHER_1_MONITORING']       =   "siskoautomation.teacher509"
    main_dictionary['USER_PASSWORD_TEACHER_1_MONITORING']   =   "Sisko18"

    # My Page
    main_dictionary['USER_FIRST_NAME_TEACHER_1_MY_PAGE']    =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1_MY_PAGE']     =   "Teacher510"
    main_dictionary['USER_NAME_TEACHER_1_MY_PAGE']          =   "siskoautomation.teacher510"
    main_dictionary['USER_PASSWORD_TEACHER_1_MY_PAGE']      =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_1_MY_PAGE']    =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1_MY_PAGE']     =   "Student506"
    main_dictionary['USER_NAME_STUDENT_1_MY_PAGE']          =   "siskoautomation.student506"
    main_dictionary['USER_PASSWORD_STUDENT_1_MY_PAGE']      =   "Sisko18"

    # Session Handling
    main_dictionary['USER_NAME_TEACHER_1_SESSION']          =   "siskoautomation.teacher511"
    main_dictionary['USER_PASSWORD_TEACHER_1_SESSION']      =   "Sisko18"

    # Sweden
    main_dictionary['USER_NAME_TEACHER_1_SWEDEN']           =   "siskoautomation.teacher512"
    main_dictionary['USER_PASSWORD_TEACHER_1_SWEDEN']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_SWEDEN']           =   "siskoautomation.student507"
    main_dictionary['USER_PASSWORD_STUDENT_1_SWEDEN']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_2_SWEDEN']           =   "siskoautomation.student508"
    main_dictionary['USER_PASSWORD_STUDENT_2_SWEDEN']       =   "Sisko18"

    # Theory Elements
    main_dictionary['USER_NAME_TEACHER_1_THEORY']           =   "siskoautomation.teacher513"
    main_dictionary['USER_PASSWORD_TEACHER_1_THEORY']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_THEORY']           =   "siskoautomation.student509"
    main_dictionary['USER_PASSWORD_STUDENT_1_THEORY']       =   "Sisko18"

    return main_dictionary


def default_suite_credentials_qa(main_dictionary):

    # Dashboard
    main_dictionary['USER_FIRST_NAME_TEACHER_1_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1_DASHBOARD']   =   "Teacher1101"
    main_dictionary['USER_NAME_TEACHER_1_DASHBOARD']        =   "siskoautomation.teacher1101"
    main_dictionary['USER_PASSWORD_TEACHER_1_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_TEACHER_2_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_2_DASHBOARD']   =   "Teacher1102"
    main_dictionary['USER_NAME_TEACHER_2_DASHBOARD']        =   "siskoautomation.teacher1102"
    main_dictionary['USER_PASSWORD_TEACHER_2_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_1_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1_DASHBOARD']   =   "Student 1101"
    main_dictionary['USER_NAME_STUDENT_1_DASHBOARD']        =   "siskoautomation.student1101"
    main_dictionary['USER_PASSWORD_STUDENT_1_DASHBOARD']    =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_2_DASHBOARD']  =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_2_DASHBOARD']   =   "Student 1102"
    main_dictionary['USER_NAME_STUDENT_2_DASHBOARD']        =   "siskoautomation.student1102"
    main_dictionary['USER_PASSWORD_STUDENT_2_DASHBOARD']    =   "Sisko18"

    # Security
    main_dictionary['USER_NAME_TEACHER_1_SECURITY']         =   "siskoautomation.teacher1103"
    main_dictionary['USER_PASSWORD_TEACHER_1_SECURITY']     =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_SECURITY']         =   "siskoautomation.teacher1104"
    main_dictionary['USER_PASSWORD_TEACHER_2_SECURITY']     =   "Sisko18"

    # Exercises
    main_dictionary['USER_NAME_TEACHER_1_EXERCISES1']        =   "siskoautomation.teacher1105"
    main_dictionary['USER_PASSWORD_TEACHER_1_EXERCISES1']    =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_EXERCISES1']        =   "siskoautomation.student1103"
    main_dictionary['USER_PASSWORD_STUDENT_1_EXERCISES1']    =   "Sisko18"

    main_dictionary['USER_NAME_TEACHER_1_EXERCISES2']        =   "siskoautomation.teacher1116"
    main_dictionary['USER_PASSWORD_TEACHER_1_EXERCISES2']    =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_EXERCISES2']        =   "siskoautomation.student1111"
    main_dictionary['USER_PASSWORD_STUDENT_1_EXERCISES2']    =   "Sisko18"

    # Feeds
    main_dictionary['USER_NAME_TEACHER_1_FEEDS1']           =   "siskoautomation.teacher1106"
    main_dictionary['USER_PASSWORD_TEACHER_1_FEEDS1']       =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_FEEDS1']           =   "siskoautomation.teacher1107"
    main_dictionary['USER_PASSWORD_TEACHER_2_FEEDS1']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_FEEDS1']           =   "siskoautomation.student1104"
    main_dictionary['USER_PASSWORD_STUDENT_1_FEEDS1']       =   "Sisko18"
    main_dictionary['USER_NAME_PUPIL_1_FEEDS1']             =   "siskoautomation.pupil1101"
    main_dictionary['USER_PASSWORD_PUPIL_1_FEEDS1']         =   "Sisko18"

    main_dictionary['USER_NAME_TEACHER_1_FEEDS2']           =   "siskoautomation.teacher1114"
    main_dictionary['USER_PASSWORD_TEACHER_1_FEEDS2']       =   "Sisko18"
    main_dictionary['USER_NAME_TEACHER_2_FEEDS2']           =   "siskoautomation.teacher1115"
    main_dictionary['USER_PASSWORD_TEACHER_2_FEEDS2']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_FEEDS2']           =   "siskoautomation.student1105"
    main_dictionary['USER_PASSWORD_STUDENT_1_FEEDS2']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_2_FEEDS2']           =   "siskoautomation.student1110"
    main_dictionary['USER_PASSWORD_STUDENT_2_FEEDS2']       =   "Sisko18"

    # Homework Central
    main_dictionary['USER_NAME_TEACHER_1_HOMEWORK']         =   "siskoautomation.teacher1108"
    main_dictionary['USER_PASSWORD_TEACHER_1_HOMEWORK']     =   "Sisko18"

    # Monitoring
    main_dictionary['USER_NAME_TEACHER_1_MONITORING']       =   "siskoautomation.teacher1109"
    main_dictionary['USER_PASSWORD_TEACHER_1_MONITORING']   =   "Sisko18"

    # My Page
    main_dictionary['USER_FIRST_NAME_TEACHER_1_MY_PAGE']    =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1_MY_PAGE']     =   "Teacher1110"
    main_dictionary['USER_NAME_TEACHER_1_MY_PAGE']          =   "siskoautomation.teacher1110"
    main_dictionary['USER_PASSWORD_TEACHER_1_MY_PAGE']      =   "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_1_MY_PAGE']    =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1_MY_PAGE']     =   "Student1106"
    main_dictionary['USER_NAME_STUDENT_1_MY_PAGE']          =   "siskoautomation.student1106"
    main_dictionary['USER_PASSWORD_STUDENT_1_MY_PAGE']      =   "Sisko18"

    # Session Handling
    main_dictionary['USER_NAME_TEACHER_1_SESSION']          =   "siskoautomation.teacher1111"
    main_dictionary['USER_PASSWORD_TEACHER_1_SESSION']      =   "Sisko18"

    # Sweden
    main_dictionary['USER_NAME_TEACHER_1_SWEDEN']           =   "siskoautomation.teacher1112"
    main_dictionary['USER_PASSWORD_TEACHER_1_SWEDEN']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_SWEDEN']           =   "siskoautomation.student1107"
    main_dictionary['USER_PASSWORD_STUDENT_1_SWEDEN']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_2_SWEDEN']           =   "siskoautomation.student1108"
    main_dictionary['USER_PASSWORD_STUDENT_2_SWEDEN']       =   "Sisko18"

    # Theory Elements
    main_dictionary['USER_NAME_TEACHER_1_THEORY']           =   "siskoautomation.teacher1113"
    main_dictionary['USER_PASSWORD_TEACHER_1_THEORY']       =   "Sisko18"
    main_dictionary['USER_NAME_STUDENT_1_THEORY']           =   "siskoautomation.student1109"
    main_dictionary['USER_PASSWORD_STUDENT_1_THEORY']       =   "Sisko18"

    return main_dictionary


def local():
    main_dictionary = default_var()

    main_dictionary['LOGIN_URL'] =                  "http://localhost:49152"
    main_dictionary['PORTAL_URL'] =                 "https://spro-wordpress-int3.sanomapro.fi"

    main_dictionary['ASSIGNMENT_TEACHER_1_USERNAME'] =    "REPLACE_WITH_USER_NAME"
    main_dictionary['ASSIGNMENT_TEACHER_1_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_1_USERNAME'] =    "REPLACE_WITH_USER_NAME"
    main_dictionary['ASSIGNMENT_STUDENT_1_PASSWORD'] =    "Sisko18"

    main_dictionary['ASSIGNMENT_TEACHER_2_USERNAME'] =    "REPLACE_WITH_USER_NAME"
    main_dictionary['ASSIGNMENT_TEACHER_2_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_2_USERNAME'] =    "REPLACE_WITH_USER_NAME"
    main_dictionary['ASSIGNMENT_STUDENT_2_PASSWORD'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_SMOKE_1'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_TEACHER_SMOKE_1'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_TEACHER_SMOKE_1'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_TEACHER_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_SMOKE_1'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_STUDENT_SMOKE_1'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_STUDENT_SMOKE_1'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_STUDENT_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_SMOKE_1'] =    "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_PUPIL_SMOKE_1'] =     "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_PUPIL_SMOKE_1'] =          "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_PUPIL_SMOKE_1'] =      "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_1'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_TEACHER_1'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_TEACHER_1'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Sisko18"
    main_dictionary['USER_FIRST_NAME_TEACHER_2'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_TEACHER_2'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_TEACHER_2'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_TEACHER_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_1'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_STUDENT_1'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_STUDENT_1'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_2'] =  "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_STUDENT_2'] =   "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_STUDENT_2'] =        "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_STUDENT_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_1'] =    "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_PUPIL_1'] =     "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_PUPIL_1'] =          "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_PUPIL_1'] =      "Sisko18"
    main_dictionary['USER_FIRST_NAME_PUPIL_2'] =    "REPLACE_WITH_FIRST_NAME"
    main_dictionary['USER_LAST_NAME_PUPIL_2'] =     "REPLACE_WITH_LAST_NAME"
    main_dictionary['USER_NAME_PUPIL_2'] =          "REPLACE_WITH_USER_NAME"
    main_dictionary['USER_PASSWORD_PUPIL_2'] =      "Sisko18"

    main_dictionary['LICENSE_ON'] =                 "False"

    return main_dictionary


# SP DEV env
def dev():
    # Get default values
    main_dictionary = default_var()
    main_dictionary = default_suite_credentials_dev(main_dictionary)
    main_dictionary['LOGIN_URL'] =                  "https://kampusdev1.sanomapro.fi"
    main_dictionary['PORTAL_URL'] =                 "https://spro-wordpress-int3.sanomapro.fi"

    main_dictionary['ASSIGNMENT_TEACHER_1_USERNAME'] =    "siskoautomation.teacher001assignment"
    main_dictionary['ASSIGNMENT_TEACHER_1_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_1_USERNAME'] =    "siskoautomation.student001assignment"
    main_dictionary['ASSIGNMENT_STUDENT_1_PASSWORD'] =    "Sisko18"

    # TODO - Assignment test accounts to be created in SP Dev1 and updated here when possible (AMS issue: SPD-1545)
    main_dictionary['ASSIGNMENT_TEACHER_2_USERNAME'] =    "siskoint.teacher048"
    main_dictionary['ASSIGNMENT_TEACHER_2_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_2_USERNAME'] =    "siskoint.student052"
    main_dictionary['ASSIGNMENT_STUDENT_2_PASSWORD'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_SMOKE_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_SMOKE_1'] =   "Teacher030"
    main_dictionary['USER_NAME_TEACHER_SMOKE_1'] =        "siskoautomation.teacher030"
    main_dictionary['USER_PASSWORD_TEACHER_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_SMOKE_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_SMOKE_1'] =   "Student030"
    main_dictionary['USER_NAME_STUDENT_SMOKE_1'] =        "siskoautomation.student030"
    main_dictionary['USER_PASSWORD_STUDENT_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_SMOKE_1'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_SMOKE_1'] =     "Pupil030"
    main_dictionary['USER_NAME_PUPIL_SMOKE_1'] =          "siskoautomation.pupil030"
    main_dictionary['USER_PASSWORD_PUPIL_SMOKE_1'] =      "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1'] =   "Teacher040"
    main_dictionary['USER_NAME_TEACHER_1'] =        "siskoautomation.teacher040"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Sisko18"
    main_dictionary['USER_FIRST_NAME_TEACHER_2'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_2'] =   "Teacher043"
    main_dictionary['USER_NAME_TEACHER_2'] =        "siskoautomation.teacher043"
    main_dictionary['USER_PASSWORD_TEACHER_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1'] =   "Student040"
    main_dictionary['USER_NAME_STUDENT_1'] =        "siskoautomation.student040"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_2'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_2'] =   "Student043"
    main_dictionary['USER_NAME_STUDENT_2'] =        "siskoautomation.student043"
    main_dictionary['USER_PASSWORD_STUDENT_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_1'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_1'] =     "Pupil040"
    main_dictionary['USER_NAME_PUPIL_1'] =          "siskoautomation.pupil040"
    main_dictionary['USER_PASSWORD_PUPIL_1'] =      "Sisko18"
    main_dictionary['USER_FIRST_NAME_PUPIL_2'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_2'] =     "Pupil043"
    main_dictionary['USER_NAME_PUPIL_2'] =          "siskoautomation.pupil043"
    main_dictionary['USER_PASSWORD_PUPIL_2'] =      "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_DATA'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_DATA'] =  "Teacher Datachange"
    main_dictionary['USER_NAME_TEACHER_DATA'] =       "siskoautomation.teacherdatachange"
    main_dictionary['USER_EMAIL_TEACHER_DATA'] =      "siskoautomation.teacher.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_DATA'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_DATA'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_DATA'] =  "Student Datachange"
    main_dictionary['USER_NAME_STUDENT_DATA'] =       "siskoautomation.studentdatachange"
    main_dictionary['USER_EMAIL_STUDENT_DATA'] =      "siskoautomation.student.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_DATA'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_DATA'] =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_DATA'] =    "Pupil Datachange"
    main_dictionary['USER_NAME_PUPIL_DATA'] =         "siskoautomation.pupildatachange"
    main_dictionary['USER_EMAIL_PUPIL_DATA'] =        "siskoautomation.pupil.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_PUPIL_DATA'] =     "Sisko18"

    main_dictionary['USER_FIRST_NAME_PERSONAL_LICENSE_1'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PERSONAL_LICENSE_1'] =  "Student Personal License001"
    main_dictionary['USER_NAME_PERSONAL_LICENSE_1'] =       "siskoautomation.studentpersonallicense001"
    main_dictionary['USER_EMAIL_PERSONAL_LICENSE_1'] =      "siskoautomation.student.personal.license001@mailinator.com"
    main_dictionary['USER_PASSWORD_PERSONAL_LICENSE_1'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_PERSONAL_LICENSE_2'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PERSONAL_LICENSE_2'] =  "Personal License 002"
    main_dictionary['USER_NAME_PERSONAL_LICENSE_2'] =       "siskoautomation.studentpersonallicense002"
    main_dictionary['USER_EMAIL_PERSONAL_LICENSE_2'] =      "siskoautomation.student.personal.license002@mailinator.com"
    main_dictionary['USER_PASSWORD_PERSONAL_LICENSE_2'] =   "Sisko18"
    main_dictionary['METHODS_PERSONAL_LICENSE_2']       =   ["Jukola 1", "Plan D 1 - 2"]

    main_dictionary['LICENSE_ON'] =                   "False"

    return main_dictionary


# SP QA env
def qa():
    # Get default values
    main_dictionary = default_var()
    main_dictionary = default_suite_credentials_qa(main_dictionary)

    main_dictionary['LOGIN_URL'] =                  "https://kampusqa.sanomapro.fi"
    main_dictionary['PORTAL_URL'] =                 "https://spro-wordpress-qa3.sanomapro.fi"

    main_dictionary['ASSIGNMENT_TEACHER_1_USERNAME'] =    "siskoautomation.teacher001assignment"
    main_dictionary['ASSIGNMENT_TEACHER_1_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_1_USERNAME'] =    "siskoautomation.student001assignment"
    main_dictionary['ASSIGNMENT_STUDENT_1_PASSWORD'] =    "Sisko18"

    main_dictionary['ASSIGNMENT_TEACHER_2_USERNAME'] =    "siskoautomation.teacher002assignment"
    main_dictionary['ASSIGNMENT_TEACHER_2_PASSWORD'] =    "Sisko18"
    main_dictionary['ASSIGNMENT_STUDENT_2_USERNAME'] =    "siskoautomation.student002assignment"
    main_dictionary['ASSIGNMENT_STUDENT_2_PASSWORD'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_SMOKE_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_SMOKE_1'] =   "TeacherSmoke002"
    main_dictionary['USER_NAME_TEACHER_SMOKE_1'] =        "siskoautomation.teachersmoke002"
    main_dictionary['USER_PASSWORD_TEACHER_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_SMOKE_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_SMOKE_1'] =   "StudentSmoke002"
    main_dictionary['USER_NAME_STUDENT_SMOKE_1'] =        "siskoautomation.studentsmoke002"
    main_dictionary['USER_PASSWORD_STUDENT_SMOKE_1'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_SMOKE_1'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_SMOKE_1'] =     "PupilSmoke002"
    main_dictionary['USER_NAME_PUPIL_SMOKE_1'] =          "siskoautomation.pupilsmoke002"
    main_dictionary['USER_PASSWORD_PUPIL_SMOKE_1'] =      "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_1'] =   "Teacher055"
    main_dictionary['USER_NAME_TEACHER_1'] =        "siskoautomation.teacher055"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Sisko18"
    main_dictionary['USER_LAST_NAME_TEACHER_2'] =   "Teacher056"
    main_dictionary['USER_NAME_TEACHER_2'] =        "siskoautomation.teacher056"
    main_dictionary['USER_PASSWORD_TEACHER_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_1'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_1'] =   "Student055"
    main_dictionary['USER_NAME_STUDENT_1'] =        "siskoautomation.student055"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Sisko18"
    main_dictionary['USER_FIRST_NAME_STUDENT_2'] =  "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_2'] =   "Student056"
    main_dictionary['USER_NAME_STUDENT_2'] =        "siskoautomation.student056"
    main_dictionary['USER_PASSWORD_STUDENT_2'] =    "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_1'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_1'] =     "Pupil055"
    main_dictionary['USER_NAME_PUPIL_1'] =          "siskoautomation.pupil055"
    main_dictionary['USER_PASSWORD_PUPIL_1'] =      "Sisko18"
    main_dictionary['USER_FIRST_NAME_PUPIL_2'] =    "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_2'] =     "Pupil056"
    main_dictionary['USER_NAME_PUPIL_2'] =          "siskoautomation.pupil056"
    main_dictionary['USER_PASSWORD_PUPIL_2'] =      "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_DATA'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_DATA'] =  "Teacher Datachange"
    main_dictionary['USER_NAME_TEACHER_DATA'] =       "siskoautomation.teacherdatachange"
    main_dictionary['USER_EMAIL_TEACHER_DATA'] =      "siskoautomation.teacher.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_DATA'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_DATA'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_DATA'] =  "Student Datachange"
    main_dictionary['USER_NAME_STUDENT_DATA'] =       "siskoautomation.studentdatachange"
    main_dictionary['USER_EMAIL_STUDENT_DATA'] =      "siskoautomation.student.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_DATA'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_PUPIL_DATA'] =   "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PUPIL_DATA'] =    "Pupil Datachange"
    main_dictionary['USER_NAME_PUPIL_DATA'] =         "siskoautomation.pupildatachange"
    main_dictionary['USER_EMAIL_PUPIL_DATA'] =        "siskoautomation.pupil.datachange@mailinator.com"
    main_dictionary['USER_PASSWORD_PUPIL_DATA'] =     "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_LICENSE_1'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_LICENSE_1'] =  "Teacher License 001"
    main_dictionary['USER_NAME_TEACHER_LICENSE_1'] =       "siskoautomation.teacherlicense001"
    main_dictionary['USER_EMAIL_TEACHER_LICENSE_1'] =      "siskoautomation.teacherlicense001@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_LICENSE_1'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_LICENSE_2'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_LICENSE_2'] =  "Teacher License 002"
    main_dictionary['USER_NAME_TEACHER_LICENSE_2'] =       "siskoautomation.teacherlicense002"
    main_dictionary['USER_EMAIL_TEACHER_LICENSE_2'] =      "siskoautomation.teacherlicense002@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_LICENSE_2'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_TEACHER_LICENSE_3'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_TEACHER_LICENSE_3'] =  "Teacher License 003"
    main_dictionary['USER_NAME_TEACHER_LICENSE_3'] =       "siskoautomation.teacherlicense003"
    main_dictionary['USER_EMAIL_TEACHER_LICENSE_3'] =      "siskoautomation.teacherlicense003@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_LICENSE_3'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_LICENSE_1'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_LICENSE_1'] =  "Student License 001"
    main_dictionary['USER_NAME_STUDENT_LICENSE_1'] =       "siskoautomation.studentlicense001"
    main_dictionary['USER_EMAIL_STUDENT_LICENSE_1'] =      "siskoautomation.studentlicense001@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_LICENSE_1'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_STUDENT_LICENSE_2'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_STUDENT_LICENSE_2'] =  "Student License 002"
    main_dictionary['USER_NAME_STUDENT_LICENSE_2'] =       "siskoautomation.studentlicense002"
    main_dictionary['USER_EMAIL_STUDENT_LICENSE_2'] =      "siskoautomation.studentlicense002@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_LICENSE_2'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_PRIVATE_LICENSE_1'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PRIVATE_LICENSE_1'] =  "Private License 001"
    main_dictionary['USER_NAME_PRIVATE_LICENSE_1'] =       "siskoautomation.privatelicense001"
    main_dictionary['USER_EMAIL_PRIVATE_LICENSE_1'] =      "siskoautomation.privatelicense001@mailinator.com"
    main_dictionary['USER_PASSWORD_PRIVATE_LICENSE_1'] =   "Sisko18"

    main_dictionary['USER_FIRST_NAME_PRIVATE_LICENSE_2'] = "Sisko Automation"
    main_dictionary['USER_LAST_NAME_PRIVATE_LICENSE_2'] =  "Private License 002"
    main_dictionary['USER_NAME_PRIVATE_LICENSE_2'] =       "siskoautomation.privatelicense002"
    main_dictionary['USER_EMAIL_PRIVATE_LICENSE_2'] =      "siskoautomation.privatelicense002@mailinator.com"
    main_dictionary['USER_PASSWORD_PRIVATE_LICENSE_2'] =   "Sisko18"

    main_dictionary['LICENSE_ON'] =                 "True"

    return main_dictionary


# SU DEV env
def dev_su():
    # Get default values
    main_dictionary = default_var()

    main_dictionary['LOGIN_URL'] =                  "https://kampusdev1.sanomautbildning.io"
    main_dictionary['PORTAL_URL'] =                 "https://portal-int.sanomautbildning.io"
    main_dictionary['DEFAULT_GRADE'] =              "7"
    main_dictionary['DEFAULT_SCHOOL_YEAR'] =        "19/20"
    main_dictionary['ADD_STUDENTS_DIALOG'] =        "dialog"
    main_dictionary['ADD_STUDENTS_FEED'] =          "feed"      
    main_dictionary['USER_NAME_TEACHER_1'] =        "siskoautomation_teacher1@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Siskoauto1"
    main_dictionary['USER_NAME_TEACHER_2'] =        "siskoautomation_teacher2@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_2'] =    "Siskoauto2"

    main_dictionary['USER_NAME_STUDENT_1'] =        "siskoautomation_student1@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Siskoauto1"
    main_dictionary['USER_NAME_STUDENT_2'] =        "siskoautomation_student2@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_2'] =    "Siskoauto2"

    main_dictionary['LICENSE_ON'] =                 "False"

    return main_dictionary


# SU QA env
def qa_su():
    # Get default values
    main_dictionary = default_var()

    main_dictionary['LOGIN_URL'] =                  "https://kampusqa.sanomautbildning.io"
    main_dictionary['PORTAL_URL'] =                 "https://portal-qa.sanomautbildning.io"
    main_dictionary['DEFAULT_GRADE'] =              "7"
    main_dictionary['DEFAULT_SCHOOL_YEAR'] =        "19/20"
    main_dictionary['ADD_STUDENTS_DIALOG'] =        "dialog"
    main_dictionary['ADD_STUDENTS_FEED'] =          "feed"   
    main_dictionary['USER_NAME_TEACHER_1'] =        "siskoautomation_teacher1@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Siskoauto1"
    main_dictionary['USER_NAME_TEACHER_2'] =        "siskoautomation_teacher2@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_2'] =    "Siskoauto2"
    main_dictionary['USER_NAME_TEACHER_3'] =        "siskoautomation_teacher3@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_3'] =    "Siskoauto3"
    main_dictionary['USER_NAME_TEACHER_4'] =        "siskoautomation_teacher4@mailinator.com"
    main_dictionary['USER_PASSWORD_TEACHER_4'] =    "Siskoauto4"

    main_dictionary['USER_NAME_STUDENT_1'] =        "siskoautomation_student1@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Siskoauto1"
    main_dictionary['USER_NAME_STUDENT_2'] =        "siskoautomation_student2@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_2'] =    "Siskoauto2"
    main_dictionary['USER_NAME_STUDENT_3'] =        "siskoautomation_student3@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_3'] =    "Siskoauto3"
    main_dictionary['USER_NAME_STUDENT_4'] =        "siskoautomation_student4@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_4'] =    "Siskoauto4"
    main_dictionary['USER_NAME_STUDENT_5'] =        "siskoautomation_student5@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_5'] =    "Siskoauto5"
    main_dictionary['USER_NAME_STUDENT_6'] =        "siskoautomation_student6@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_6'] =    "Siskoauto6"
    main_dictionary['USER_NAME_STUDENT_7'] =        "siskoautomation_student7@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_7'] =    "Siskoauto7"
    main_dictionary['USER_NAME_STUDENT_8'] =        "siskoautomation_student8@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_8'] =    "Siskoauto8"
    main_dictionary['USER_NAME_STUDENT_9'] =        "siskoautomation_student9@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_9'] =    "Siskoauto9"
    main_dictionary['USER_NAME_STUDENT_10'] =        "siskoautomation_student10@mailinator.com"
    main_dictionary['USER_PASSWORD_STUDENT_10'] =    "Siskoauto10"

    main_dictionary['LICENSE_ON'] =                 "False"

    return main_dictionary


# SP PROD env
def prod():
    # Get default values
    main_dictionary = default_var()

    main_dictionary['LOGIN_URL'] =                  "https://www.sanomapro.fi"
    main_dictionary['USER_NAME_TEACHER_1'] =        "kampus.testiopettaja001"
    main_dictionary['USER_PASSWORD_TEACHER_1'] =    "Kampus001"
    main_dictionary['USER_NAME_STUDENT_1'] =        "kampus.testioppilas001"
    main_dictionary['USER_PASSWORD_STUDENT_1'] =    "Kampus001"
    main_dictionary['MONITORING_SCHOOL'] =          "Sanoma Pro yläkoulu"

    return main_dictionary

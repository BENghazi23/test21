"""
Utility Keywords - To hold custom python utility keywords
"""
from robot.api.deco import keyword
from robot.libraries.BuiltIn import BuiltIn
from datetime import date

DATE_FORMAT_DICT = {
    'EN': '%d/%m/%Y',
    'FI': '%d.%m.%Y',
    'SV': '%Y-%m-%d'
}


@keyword("Get First Date Of Next Month")
def get_first_date_of_next_month():
    locale = BuiltIn().get_variable_value("${LOCALE}")
    if not locale:
        raise ValueError("Variable ${LOCALE} not defined in test scope. Ensure ${LOCALE} is set to any of : FI, SV, EN")
    date_format = DATE_FORMAT_DICT[locale]
    today = date.today()
    next_month = today.month + 1
    year = today.year
    if next_month > 12:
        next_month = 1
        year += 1
    return date(year, next_month, 1).strftime(date_format)

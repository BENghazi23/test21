#!/bin/bash

BROWSER="firefox"
BROWSER_FROM="local"
ENVIRONMENT="qa"
SHM_SIZE="2048m"
TAG="regression"

docker run \
    -v $(pwd)/reports:/opt/robotframework/reports:Z \
    -v $(pwd):/opt/robotframework/tests:Z \
    -e ROBOT_OPTIONS="--loglevel DEBUG --variable environment:$ENVIRONMENT --variable browser:$BROWSER --variable browser_version:$BROWSER_VERSION --variable os:$OS --variable os_version:$OS_VERSION --variable screen_resolution:$SCREEN_RESOLUTION --variable browser_from:$BROWSER_FROM --variable browserstack_user:$BROWSERSTACK_USER --variable browserstack_key:$BROWSERSTACK_KEY --variable browserstack.localIdentifier:$BROWSERSTACK_LOCAL_IDENTIFIER -i $TAG" \
    -e BROWSER=$BROWSER --shm-size=$SHM_SIZE unikiepublic/robot-framework-python3:latest
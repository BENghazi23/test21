*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Suite Setup       Create Feed For Exercises     ${USER_NAME_TEACHER_1_EXERCISES1}    ${USER_PASSWORD_TEACHER_1_EXERCISES1}
Test Teardown     Custom Teardown

*** Test Cases ***
Check Combi Content Classwork
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Check Combi Content Classwork

Unscored: Cloze Combi Drop
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Automation testing    Automation testing    Unscored: Cloze combi drop
    Do Exercise Unscored: Cloze Combi Drop

Unscored: Cloze Combi Open
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Automation testing    Automation testing    Unscored: Cloze combi open
    Do Exercise Unscored: Cloze Combi Open

Unscored: Single Response
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Automation testing    Automation testing    Unscored: Single response
    Do Exercise Unscored: Single Response

Unscored: Choice With Many Possible Answers
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Automation testing    Automation testing    Unscored: Choice with many possible answers
    Do Exercise Unscored: Choice With Many Possible Answers

Unscored: Choice With Single Possible Answer
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Automation testing    Automation testing    Unscored: Choice with single possible answer
    Do Exercise Unscored: Choice With Single Possible Answer

UI: Multi Choices - Metadata Set To Research_question
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Multi Choices - metadata set to research_question
    Do Exercise UI: Multi Choices - Metadata Set To Research_question

# UI: Content Item - Metadata Set To Homework
#     [Tags]    regression    exercise
#     Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
#     Open Content Item        Content testing    Exercises / interactions    Content item - metadata set to homework
#     Do Exercise UI: Content Item - Metadata Set To Homework

UI: New Content Item - Metadata Set To Classwork
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    New content item - metadata set to classwork
    Do Exercise UI: New Content Item - Metadata Set To Classwork

UI: Choice With Single Possible Answer
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Choice with single possible answer
    Do Exercise UI: Choice With Single Possible Answer

UI: Choice With Question And Answer Level Feedback
    [Tags]    qurantine    exercise    9e2b4a34-9bd6-11e9-a2a3-2a2ae2dbcce4
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Choice with question and answer level feedback
    Do Exercise UI: Choice With Question And Answer Level Feedback

UI: Multi Choice
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Choice exercises    UI: Multi choice
    Do Exercise UI: Multi Choice

UI: Arrange Horisontal
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Arrange horisontal
    Do Exercise UI: Arrange Horisontal

UI: Arrange Vertical
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Arrange vertical
    Do Exercise UI: Arrange Vertical

UI: Match Single Response
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Match Single response
    Do Exercise UI: Match Single Response

UI: Cloze Combi Drop
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Cloze combi drop
    Do Exercise UI: Cloze Combi Drop

UI: Cloze Combi Drop 2
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Cloze combi drop 2
    Do Exercise UI: Cloze Combi Drop 2

UI: Cloze Combi Edit
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Cloze combi edit
    Do Exercise UI: Cloze Combi Edit

UI: Cloze Combi Drag
    [Tags]    quarantine    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Cloze combi drag
    Do Exercise UI: Cloze Combi Drag

UI: Cloze Combi All
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Cloze combi all
    Do Exercise UI: Cloze Combi All

UI: Select Words
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Select words
    Do Exercise UI: Select Words

UI: Match Matrix
    [Tags]    quarantine    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Match Matrix
    Do Exercise UI: Match Matrix

UI: Open Question Short Response - Explanation Of Solution
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Open Question Short Response - Explanation of Solution    ${CONTENT_ITEM_TYPE_EXERCISE}
    Do Exercise UI: Open Question Short Response - Explanation Of Solution

UI: Open Question Long Response - Explanation Of Solution
    [Tags]    quarantine    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Open Question Long Response - Explanation Of Solution
    Do Exercise UI: Open Question Long Response - Explanation Of Solution

Choice - Itsearviointitehtävä
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    Choice - itsearviointitehtävä
    Do Exercise Choice - Itsearviointitehtävä

Fill In - Open Question Long Response
    [Tags]    quarantine    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Fill in - Open Question Long Response    ${CONTENT_ITEM_TYPE_CLASSWORK}
    Do Exercise Fill In - Open Question Long Response

Fill In - Open Question Long Answer Matikkaeditorilla
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Fill in - Open Question Long Answer matikkaeditorilla
    Do Exercise Fill In - Open Question Long Answer Matikkaeditorilla

Fill In - Open Question With Table
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Fill in - Open Question with table
    Do Exercise Fill In - Open Question With Table

Match - Cloze combi - Itsearviointitehtävä
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Match - Cloze combi - itsearviointitehtävä
    Do Exercise Match - Cloze combi - Itsearviointitehtävä

Match - Cloze Combi - Hide Gaps
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Match - Cloze combi - hide gaps
    Do Exercise Match - Cloze Combi - Hide Gaps

UI: Match - Single Response - Perus
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES1}    ${USER_PASSWORD_STUDENT_1_EXERCISES1}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    UI: Match - Single response - perus
    Do Exercise UI: Match - Single Response - Perus

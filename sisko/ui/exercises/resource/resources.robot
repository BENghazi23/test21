*** Settings ***
Resource          ../../common/theoryelement_resources.robot

*** Variables ***
${method_to_select}    0 Sisko Manual MR

*** Keywords ***
Check Combi Content Text
    Open Content Item        Metadata    Text templates    Using combiContent metadata
    ${documents}    Content Item Contains Combi Content
    FOR    ${index}    IN RANGE     0       ${documents.number_of_documents}
        ${index}        Evaluate    ${index}+1
        ${innerHTML}    Get Element Attribute    xpath=(.//app-document)[${index}]    innerHTML
        ${innerHTML_length}    Get Length    ${innerHTML}
        Should Be True    ${innerHTML_length}>0
    END

Check Combi Content Classwork
    Open Content Item        Combi Content    Combi-content exercise    Using combiContent metadata    ${CONTENT_ITEM_TYPE_CLASSWORK}
    ${documents}    Content Item Contains Combi Content
    FOR    ${index}    IN RANGE     0       ${documents.number_of_documents}
        ${index}        Evaluate    ${index}+1
        ${innerHTML}    Get Element Attribute    xpath=(.//app-document)[${index}]    innerHTML
        ${innerHTML_length}    Get Length    ${innerHTML}
        Should Be True    ${innerHTML_length}>0
    END

Do Combi Content Exercise 1
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    ${radio_buttons}    Get Element Count    xpath=.//eb-choice-list//eb-radio-button
    Click Element On Page     xpath=(.//eb-choice-list//eb-radio-button)[1]
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-radio-button)[1]
    ${radio_button_selected}    Get Element Attribute    xpath=(.//eb-choice-list//eb-radio-button)[1]    class
    Should Contain    ${radio_button_selected}    checked
    Click Element On Page     xpath=.//*[@id="start-over-button"]
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-radio-button)[1]
    Sleep    1
    ${radio_button_selected}    Get Element Attribute    xpath=(.//eb-choice-list//eb-radio-button)[1]    class
    Should Not Contain    ${radio_button_selected}    checked

Do Exercise Unscored: Cloze Combi Drop
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    ${answers_to_select}    Create List    a    a    a    an    an
    ${answers_selected}    Create List
    ${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Scroll Element Into View   xpath=(.//eb-cloze-drop//eb-select)[${drop_downs}]
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Check Exercise Score    5
    Start Exercise Over

Do Exercise Unscored: Cloze Combi Open
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    Start Exercise Over
    ${correct_answers}    Create List    Hämeessä    niittu    erämaita    Jukolan
    ${answers_selected}    Create List
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit/input
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-cloze-edit/input
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ${correct_answers[${index_answer}]}
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Check Exercise Score    4
    Start Exercise Over

Do Exercise Unscored: Single Response
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    World war I    World war II    Cold war    Kosovo War
    ${right_elements}    Create List    1914–1918    1939-1945    1947-1991    1998-1999
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Sleep    1
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Sleep     1
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Start Exercise Over

Do Exercise Unscored: Choice With Many Possible Answers
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    ${answers}    Create List    off    on    on    on    off    on
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Scroll To Answer    xpath=(.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")])[${index}]
        Run Keyword If    '${answers[${index_list}]}'=='on'    Click Element On Page    xpath=(.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")])[${index}]
    END
    Submit Exercise
    Start Exercise Over

Undo Exercise Unscored: Choice With Many Possible Answers
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    ${answers}    Create List    on    off    off    off    on    off
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Scroll To Answer    xpath=(.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")])[${index}]
        Run Keyword If    '${answers[${index_list}]}'=='on'    Click Element On Page    xpath=(.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")])[${index}]
    END
    Submit Exercise

Do Exercise Unscored: Choice With Single Possible Answer
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li//*[normalize-space(text())="Stockholm"]
    Click Element On Page    xpath=.//app-document//eb-choice-list//li//*[normalize-space(text())="Stockholm"]
    Submit Exercise
    Check Exercise Score    1
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label)[${index}]
        ${label_text}    Get Text
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label)[${index}]
        Run Keyword If    "${label_text}"=="Stockholm"    Wait Until Page Contains Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
        Run Keyword If    "${label_text}"!="Stockholm"    Wait Until Page Does Not Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
    END
    Start Exercise Over

Do Exercise UI: Multi Choices - Metadata Set To Research_question
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answers_correct}    Create List    a whale    a cat    a dog
    ${answers_incorrect}    Create List    an ostrich    a worm    a perch
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${randon_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Scroll To Answer    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    #
    ${answer_random_text}    Get Text
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]/parent::*[@class="eb-control"]/following-sibling::*[@class="eb-label"]
    #
    Should Contain    ${answer_random}    eb-checked
    Submit Exercise
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    2
    Open Content Item    Content testing    Exercises / interactions    UI: Multi Choices - metadata set to research_question
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//li[starts-with(@class, "eb-choice") and normalize-space(string())="${answer_random_text}"]//eb-checkbox
    ${answer_random}    Get Element Attribute
    ...    xpath=.//eb-choice-list//li[starts-with(@class, "eb-choice") and normalize-space(string())="${answer_random_text}"]//eb-checkbox   class
    Should Contain    ${answer_random}    eb-checked
    Start Exercise Over
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_incorrect}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]
        Sleep    1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_incorrect}    Run Keyword And Return Status    Should Contain    ${answer}    eb-incorrect
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_incorrect}'=='True'
        ...    List Should Contain Value    ${answers_incorrect}    ${answer_text}
    END
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_correct}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Check Exercise Score    3
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_correct}    Run Keyword And Return Status    Should Contain    ${answer}    eb-correct
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_correct}'=='True'
        ...    List Should Contain Value    ${answers_correct}    ${answer_text}
    END
    Start Exercise Over

Do Exercise UI: Content Item - Metadata Set To Homework
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice[2]    xpath=.//eb-ordering//eb-order-choice[1]
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Should Not Be Equal    ${selection_texts_initial}    ${selection_texts_final}
    Submit Exercise
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    Continue Exercise
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[2]
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice[2]    xpath=.//eb-ordering//eb-order-choice[1]
    Submit Exercise
    Check Exercise Score    2
    Start Exercise Over

Do Exercise UI: New Content Item - Metadata Set To Classwork
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    Sleep    1
    Mouse Down    xpath=.//eb-ordering//eb-order-choice[2]/span
    Mouse Up    xpath=.//eb-ordering//eb-order-choice[1]/span
    Sleep    1
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Should Not Be Equal    ${selection_texts_initial}    ${selection_texts_final}
    Submit Exercise
    Start Exercise Over

Do Exercise Check For Choice With Single Possible Answer Above
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answer_correct}    Set Variable    Yes
    ${answer_incorrect}    Set Variable    No
    Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item        Content testing    Exercises / interactions    Check for Choice with single possible answer above
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button   class
    Should Contain    ${answer_saved}    eb-checked
    Start Exercise Over
    Sleep    1
    Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button
    Submit Exercise
    Sleep    1
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_incorrect}"]/ancestor::label//eb-radio-button/span    class
    Should Contain    ${answer_saved}    eb-incorrect
    Start Exercise Over
    Sleep    1
    Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_correct}"]/ancestor::label//eb-radio-button
    Submit Exercise
    Check Exercise Score    1
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_correct}"]/ancestor::label//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_correct}"]/ancestor::label//eb-radio-button/span    class
    Should Contain    ${answer_saved}    eb-correct
    Start Exercise Over

Do Exercise UI: Choice With Single Possible Answer
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Click Element On Page    xpath=.//app-document//eb-choice-list//li//*[normalize-space(text())="Stockholm"]
    Submit Exercise
    Check Exercise Score    1
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index}    Evaluate    ${index}+1
        ${label_text}    Get Text
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label)[${index}]
        Run Keyword If    "${label_text}"=="Stockholm"        Page Should Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
        Run Keyword If    "${label_text}"!="Stockholm"        Page Should Not Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
        Page Should Contain Element    xpath=(.//eb-choice-list//eb-radio-button[@disabled="true"])[${index}]
    END
    Start Exercise Over

Do Exercise UI: Choice With Question And Answer Level Feedback
    ${answer_correct}    Set Variable    Stockholm
    Start Exercise Over
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]
    ${random_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    Sleep    1
    Check Exercise Buttons In Initial State
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    Content testing    Exercises / interactions    UI: Choice with question and answer level feedback
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]   class
    Should Contain    ${answer_saved}    eb-checked
    Start Exercise Over
    Sleep    1
    Check Exercise Buttons In Initial State
    ###
    # Here check the answer level feedback (wrong answer -> style and feedback)
    ${choices_incorrect}    Get Element Count    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button
    ${random_selection_incorrect}   Evaluate    random.randint(1, ${choices_incorrect})    modules=random
    Click Element On Page    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]
    Submit Exercise
    Sleep    1
    Check Exercise Score    0
    # HERE check for feedback icon
    Element Should Be Enabled    xpath=.//eb-choice-list//eb-feedback
    Wait Until Element Is Enabled
    ...    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]/span    class
    Should Contain    ${answer_saved}    eb-incorrect
    ###
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    Element Should Not Be Visible    xpath=.//eb-choice-list//eb-feedback
    Click Element On Page    xpath=.//app-document//eb-choice-list//li//*[normalize-space(text())="${answer_correct}"]
    Submit Exercise
    Check Exercise Score    1
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index}    Evaluate    ${index}+1
        ${label_text}    Get Text
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label)[${index}]
        Run Keyword If    "${label_text}"=="${answer_correct}"        Page Should Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
        Run Keyword If    "${label_text}"!="${answer_correct}"        Page Should Not Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
    END
    Start Exercise Over

Do Exercise UI: Match Cloze Combi With Answer Level Feedback
    ${answers}    Create List    1    2
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//eb-cloze-edit
    Check Exercise Buttons In Initial State
    ${answer_fields}    Get Element Count    xpath=.//eb-cloze-edit
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[1]/input
    Press Keys    xpath=(.//eb-cloze-edit)[1]/input    @{answers}[0]
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[2]/input
    Press Keys    xpath=(.//eb-cloze-edit)[2]/input    @{answers}[1]
    Sleep    1
    Check Exercise Buttons In Initial State
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    Content testing    Exercises / interactions    UI: Match cloze combi with answer level feedback
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[1]/input
    ${answer_1}    Get Element Attribute    xpath=(.//eb-cloze-edit)[1]/input    ng-reflect-model
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[2]/input
    ${answer_2}    Get Element Attribute    xpath=(.//eb-cloze-edit)[2]/input    ng-reflect-model
    Should Be Equal As Strings    @{answers}[0]    ${answer_1}
    Should Be Equal As Strings    @{answers}[1]    ${answer_2}
    Start Exercise Over
    Sleep    1
    Check Exercise Buttons In Initial State
    ${answer_fields}    Get Element Count    xpath=.//eb-cloze-edit
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[1]/input
    Press Keys    xpath=(.//eb-cloze-edit)[1]/input    @{answers}[1]
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[2]/input
    Press Keys    xpath=(.//eb-cloze-edit)[2]/input    @{answers}[0]
    Submit Exercise
    Sleep    1
    ${style_1}    Get Element Attribute    xpath=(.//eb-cloze-edit)[1]    class
    ${style_2}    Get Element Attribute    xpath=(.//eb-cloze-edit)[2]    class
    Should Contain    ${style_1}    eb-incorrect
    Should Contain    ${style_2}    eb-incorrect
    Start Exercise Over
    Sleep    1
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[1]/input
    Press Keys    xpath=(.//eb-cloze-edit)[1]/input    @{answers}[0]
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[2]/input
    Press Keys    xpath=(.//eb-cloze-edit)[2]/input    @{answers}[1]
    Submit Exercise
    Sleep    1
    Wait Until Element Is Visible    xpath=(.//eb-cloze-edit)[1]/input
    ${style_1}    Get Element Attribute    xpath=(.//eb-cloze-edit)[1]    class
    Wait Until Element Is Visible    xpath=(.//eb-cloze-edit)[2]/input
    ${style_2}    Get Element Attribute    xpath=(.//eb-cloze-edit)[2]    class
    Should Contain    ${style_1}    eb-incorrect
    Should Contain    ${style_2}    eb-incorrect
    Check Page Contains X Defined Elements      2       xpath=.//eb-feedback//*[@class="eb-feedback"]
    Continue Exercise
    Sleep    1
    Wait Until Element Is Visible    xpath=(.//eb-cloze-edit)[1]/input
    ${style_1}    Get Element Attribute    xpath=(.//eb-cloze-edit)[1]    class
    Wait Until Element Is Visible    xpath=(.//eb-cloze-edit)[2]/input
    ${style_2}    Get Element Attribute    xpath=(.//eb-cloze-edit)[2]    class
    Should Not Contain    ${style_1}    eb-incorrect
    Should Not Contain    ${style_2}    eb-incorrect
    Check Page Contains X Defined Elements      2       xpath=.//eb-feedback//*[@class="eb-feedback"]
    Start Exercise Over

Do Exercise UI: Multi Choice
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answers_correct}    Create List    a whale    a cat    a dog
    ${answers_incorrect}    Create List    an ostrich    a worm    a perch
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${randon_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Scroll To Answer    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    1
    Open Content Item    Content testing    Choice exercises    UI: Multi choice
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    Start Exercise Over
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_incorrect}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]
        #Sleep    1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_incorrect}    Run Keyword And Return Status    Should Contain    ${answer}    eb-incorrect
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_incorrect}'=='True'
        ...    List Should Contain Value    ${answers_incorrect}    ${answer_text}
        Page Should Contain Element    xpath=(.//eb-choice-list//eb-checkbox[@disabled="true"])[${index}]
    END
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_correct}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Check Exercise Score    3
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_correct}    Run Keyword And Return Status    Should Contain    ${answer}    eb-correct
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_correct}'=='True'
        ...    List Should Contain Value    ${answers_correct}    ${answer_text}
    END
    Start Exercise Over

Do Exercise UI: Arrange Horisontal
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${correct_answers}    Create List    one    Two    five    twelve    sixteen    twenty
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="one"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="one"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Two"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Two"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[2]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="five"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="five"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[3]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="twelve"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="twelve"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[4]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="sixteen"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="sixteen"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[5]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="twenty"]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="twenty"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[6]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Submit Exercise
    Check Exercise Score    1
    Lists Should Be Equal    ${selection_texts_final}    ${correct_answers}
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Page Should Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Page Should Not Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END

Do Exercise UI: Arrange Vertical
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${selection_texts_initial}    Create List
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
        ${vPos}    Get Vertical Position    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        ${vPos2}    Get Vertical Position    xpath=.//*[@id="submit-button"]
        ${vertical_difference}    Evaluate    ${vPos} - ${vPos2}
        ${xPos}    Evaluate    ${vPos} + ${vertical_difference}
        Run Keyword If    ${vPos} > ${vPos2}    Execute JavaScript    window.scrollTo(0, ${xPos});
        ${curPosition}    Get Position
    END
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Silloinpa Jukolan")]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Silloinpa Jukolan")]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Muutoin on")]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Muutoin on")]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Peltojen alla")]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Peltojen alla")]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Sen läheisin")]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Sen läheisin")]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Jukolan talo")]/ancestor::eb-order-choice
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[starts-with(text(), "Jukolan talo")]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Wait Until Page Contains Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Wait Until Page Does Not Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END

Do Exercise UI: Drag Image
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    Sleep    2
    Start Exercise Over
    ${selections}    Get Element Count    xpath=.//eb-cloze-drag
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${vPos}    Get Vertical Position    xpath=(.//eb-cloze-drag)[${index}]
        ${vPos2}    Get Vertical Position    xpath=.//*[@id="submit-button"]
        ${vertical_difference}    Evaluate    ${vPos} - ${vPos2}
        ${xPos}    Evaluate    ${vPos} + ${vertical_difference}
        Run Keyword If    ${vPos} > ${vPos2}    Execute JavaScript    window.scrollTo(0, ${xPos});
        ${curPosition}    Get Position
        Log    ${curPosition.top}
    END

    Sleep    5
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[1]/span/span    xpath=(.//eb-cloze-drag)[1]/span
    Sleep    5
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[2]/span/span   xpath=(.//eb-cloze-drag)[2]/span
    Sleep    2
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[3]/span/span   xpath=(.//eb-cloze-drag)[3]/span
    Sleep    2
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[4]/span/span   xpath=(.//eb-cloze-drag)[4]/span
    Sleep    5
    Sleep    5
    Start Exercise Over

Do Exercise UI: Drag Image Hide Drop Targets
    Log    here

Do Exercise UI: Drag Image Hide Drop Targets 2
    Log    here

Do Exercise UI: Match Single Response
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    World war I    World war II    Cold war    Kosovo War
    ${right_elements}    Create List    1914-1918    1939-1945    1947-1991    1998-1999
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        #Scroll Element Into View    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        Sleep    1
        Scroll Element Into View    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index}        Evaluate    ${index}+1
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_correct}    eb-correct
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_correct}    eb-correct
    END
    Exercise Points Should Be Visible
    Check Exercise Score    4
    Start Exercise Over
    Exercise Points Should Not Be Visible
    ${left_elements}    Create List    World war I    World war II    Cold war    Kosovo War
    ${right_elements}    Create List    1998-1999    1947-1991    1939-1945    1914-1918
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        #Scroll Element Into View    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"
        Sleep    1
        Execute JavaScript    window.document.evaluate('.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index}        Evaluate    ${index}+1
        ${is_marked_incorrect}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_incorrect}    eb-incorrect
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_incorrect}    eb-incorrect
    END
    Exercise Points Should Be Visible
    Check Exercise Score    0
    Start Exercise Over
    Exercise Points Should Not Be Visible
    #Start Exercise Over

Do Exercise UI: Cloze Combi Drop
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    ${answers_to_select}    Create List    a    a    a    an    an
    ${answers_selected}    Create List
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    2
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi drop
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()="${answers_to_select[0]}"]
    Wait Until Element Is Enabled    xpath=.//eb-select//*[contains(@class, "eb-value")]/span
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        ${answer_text}    Get Text    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        Should Be Equal As Strings    ${answers_to_select[${index_answer}]}    ${answer_text}
    END
    Start Exercise Over
    Sleep    1
    ${drop_downs_one_not_answered}    Evaluate    ${drop_downs}-1
    FOR    ${index}    IN RANGE     0       ${drop_downs_one_not_answered}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//*[@id="exercise-score"]
    ${score}    Get Text    xpath=.//*[@id="exercise-score"]
    ${drop_downs_one_not_answered}    Convert To String    ${drop_downs_one_not_answered}
    Should Contain    ${score}    ${drop_downs_one_not_answered}
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Page Contains Element    xpath=(.//eb-select[contains(@class, "eb-disabled")])[${index}]
    END
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    ${answers_expected}    Create List    right    right    right    right    wrong
    Lists Should Be Equal    ${answers_selected}    ${answers_expected}
    Start Exercise Over
    Sleep    1
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Page Does Not Contain Element    xpath=(.//eb-select[contains(@class, "eb-disabled")])[${index}]
    END
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    Check Exercise Score    5
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
    END
    Start Exercise Over

Do Exercise UI: Cloze Combi Drop 2
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    ${answers_to_select}    Create List    a    a    a    an    an
    ${answers_selected}    Create List
    ${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    2
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi drop 2
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()="${answers_to_select[0]}"]
    Wait Until Element Is Enabled    xpath=.//eb-select//*[contains(@class, "eb-value")]/span
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        ${answer_text}    Get Text    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        Should Be Equal As Strings    ${answers_to_select[${index_answer}]}    ${answer_text}
    END
    Start Exercise Over
    Sleep    1
    ${drop_downs_one_not_answered}    Evaluate    ${drop_downs}-1
    FOR    ${index}    IN RANGE     0       ${drop_downs_one_not_answered}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//*[@id="exercise-score"]
    ${score}    Get Text    xpath=.//*[@id="exercise-score"]
    ${drop_downs_one_not_answered}    Convert To String    ${drop_downs_one_not_answered}
    Should Contain    ${score}    ${drop_downs_one_not_answered}
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    ${answers_expected}    Create List    right    right    right    right    wrong
    Lists Should Be Equal    ${answers_selected}    ${answers_expected}
    Start Exercise Over
    Sleep    1
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    Check Exercise Score    5
    # FOR    ${index}    IN RANGE     0       ${drop_downs}
    #     ${index}        Evaluate    ${index}+1
    #     ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
    Start Exercise Over

Do Exercise UI: Cloze Combi Edit
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    Start Exercise Over
    Sleep    1
    ${correct_answers}    Create List    Hämeessä    niittu    erämaita    Jukolan
    ${answers_selected}    Create List
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-cloze-edit/input
    Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[1]
    Input Text    xpath=(.//app-document//eb-cloze-edit/input)[1]    ${correct_answers[0]}
    Sleep    1
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi edit
    Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[1]
    ${answer_text}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit/input)[1]   value
    Should Be Equal As Strings    ${correct_answers[0]}    ${answer_text}
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ${correct_answers[${index_answer}]}
    END
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    Check Exercise Score    4
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        Should Contain    ${answer_style}    eb-correct
        Page Should Contain Element    xpath=(.//eb-cloze-edit[contains(@class, "eb-readonly")])[${index}]
    END
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Page Should Not Contain Element    xpath=(.//eb-cloze-edit[contains(@class, "eb-readonly")])[${index}]
    END

Do Exercise UI: Cloze Combi Drag
    Log    here
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    Sleep    2
    Start Exercise Over
    ${selections}    Get Element Count    xpath=.//eb-cloze-drag
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${vPos}    Get Vertical Position    xpath=(.//eb-cloze-drag)[${index}]
        ${vPos2}    Get Vertical Position    xpath=.//*[@id="submit-button"]
        ${vertical_difference}    Evaluate    ${vPos} - ${vPos2}
        ${xPos}    Evaluate    ${vPos} + ${vertical_difference}
        Run Keyword If    ${vPos} > ${vPos2}    Execute JavaScript    window.scrollTo(0, ${xPos});
        ${curPosition}    Get Position
        Log    ${curPosition.top}
    END
    Sleep    5
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[1]/span/span    xpath=(.//eb-cloze-drag)[1]/span
    Sleep    5
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[2]/span/span   xpath=(.//eb-cloze-drag)[2]/span
    Sleep    2
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[3]/span/span   xpath=(.//eb-cloze-drag)[3]/span
    Sleep    2
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[4]/span/span   xpath=(.//eb-cloze-drag)[4]/span
    Sleep    5
    Sleep    5
    Start Exercise Over

Do Exercise UI: Cloze Combi All
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    Sleep    2
    Reset Answers
    ${answers}    Get Element Count    xpath=.//eb-source-list//eb-source-list-item
    Start Exercise Over

Do Exercise UI: Select Words
    Wait Until Element Is Enabled    xpath=.//eb-cloze-id
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[1]
    ${style_before}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[1]/parent::span/parent::span    class
    Should Not Contain    ${style_before}    eb-selected
    Click Element On Page    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[1]
    Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[1]
    ${style_after}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[1]/parent::span/parent::span    class
    Should Contain    ${style_after}    eb-selected
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[2]
    ${style_before}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[2]/parent::span/parent::span    class
    Should Not Contain    ${style_before}    eb-selected
    Click Element On Page    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[2]
    Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[2]
    ${style_after}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Jukolan"])[2]/parent::span/parent::span    class
    Should Contain    ${style_after}    eb-selected
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Hämeessä"])[1]
    ${style_before}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Hämeessä"])[1]/parent::span/parent::span    class
    Should Not Contain    ${style_before}    eb-selected
    Click Element On Page    xpath=(.//eb-cloze-id//span[text()="Hämeessä"])[1]
    Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Hämeessä"])[1]
    ${style_after}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Hämeessä"])[1]/parent::span/parent::span    class
    Should Contain    ${style_after}    eb-selected
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Toukolan"])[1]
    ${style_before}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Toukolan"])[1]/parent::span/parent::span    class
    Should Not Contain    ${style_before}    eb-selected
    Click Element On Page    xpath=(.//eb-cloze-id//span[text()="Toukolan"])[1]
    Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-id//span[text()="Toukolan"])[1]
    ${style_after}    Get Element Attribute    xpath=(.//eb-cloze-id//span[text()="Toukolan"])[1]/parent::span/parent::span    class
    Should Contain    ${style_after}    eb-selected
    Submit Exercise
    Check Exercise Score    4
    ${words}    Get Element Count    xpath=.//eb-cloze-id
    FOR    ${index}    IN RANGE    0    ${words}
        ${index}        Evaluate    ${index}+1
        Page Should Contain Element    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index}]
    END
    ${style_correct}    Get Element Attribute    xpath=(.//span[text()="Jukolan"])[1]/ancestor::eb-cloze-id    class
    Should Contain    ${style_correct}    eb-correct
    ${style_correct}    Get Element Attribute    xpath=(.//span[text()="Jukolan"])[2]/ancestor::eb-cloze-id   class
    Should Contain    ${style_correct}    eb-correct
    ${style_correct}    Get Element Attribute    xpath=(.//span[text()="Hämeessä"])[1]/ancestor::eb-cloze-id    class
    Should Contain    ${style_correct}    eb-correct
    ${style_correct}    Get Element Attribute    xpath=(.//span[text()="Toukolan"])[1]/ancestor::eb-cloze-id    class
    Should Contain    ${style_correct}    eb-correct
    Start Exercise Over
    Sleep    1
    FOR    ${index}    IN RANGE    0    ${words}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Not Visible    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index}]
    END

Do Exercise UI: Match Matrix
    Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix
    @{options_to_select}    Create List    Finland belongs to EU    France is a continent    Climate change is a fact    Carrots are blue    Kampus is the best service ever    Autumn is wonderful
    @{options_right}    Create List    Finland belongs to EU    Climate change is a fact    Kampus is the best service ever    Autumn is wonderful
    @{options_wrong}    Create List    France is a continent    Carrots are blue
    @{answers_to_select}    Create List    Right    Wrong
    Start Exercise Over
    ${options}    Get Element Count    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]
    ${answer_options}    Get Element Count    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]
    ${elements}    Set Variable    0
    FOR    ${choice_right}    IN    @{options_right}
        ${elements}    Evaluate    ${elements}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Scroll To Answer_v4    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Click Element On Page    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Scroll Element Into View    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Click Element On Page    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]
        Wait Until Element Is Enabled    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[starts-with(@class, "eb-connection-count") and normalize-space(text())=${elements}]
        Wait Until Element Is Enabled    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        ${elements_added_right}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        Should Be Equal As Integers    ${elements}    ${elements_added_right}
        ${elements_number_right}    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
        ${elements_number_right}    Strip String    ${elements_number_right}
        Should Be Equal As Integers    ${elements}    ${elements_number_right}
    END
    ${elements}    Set Variable    0
    FOR    ${choice_wrong}    IN    @{options_wrong}
        ${elements}    Evaluate    ${elements}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Scroll To Answer_v4    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Wait Until Element Is Enabled    Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Click Element On Page    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Scroll Element Into View    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]
        Click Element On Page    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]
        ${elements_added_wrong}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        Should Be Equal As Integers    ${elements}    ${elements_added_wrong}
        ${elements_number_wrong}    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
        ${elements_number_wrong}    Strip String    ${elements_number_wrong}
        Should Be Equal As Integers    ${elements}    ${elements_number_wrong}
    END
    Submit Exercise
    Check Exercise Score    6
    Start Exercise Over
    ${elements_added_right}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Should Be Equal As Integers    ${elements_added_right}    0
    Element Should Not Be Visible    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
    ${elements_added_wrong}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Should Be Equal As Integers    ${elements_added_wrong}    0
    Element Should Not Be Visible    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]

Do Exercise UI: Cloze Combi Open
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    Start Exercise Over
    ${correct_answers}    Create List    Hämeessä    niittu    erämaita    Jukolan
    ${answers_selected}    Create List
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-cloze-edit/input
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ${correct_answers[${index_answer}]}
    END
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    Check Exercise Score    4
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        Should Contain    ${answer_style}    eb-correct
    END
    Start Exercise Over

Do Exercise Fill In - Open Question Long Response
    Wait Until Element Is Enabled    xpath=.//eb-editing
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    ${clickable_element}    Get Vertical Position    xpath=.//eb-editing//textarea
    ${exercise_buttons}    Get Vertical Position    xpath=.//app-module-content-buttons//*[starts-with(@class, "exercise-buttons")]
    ${position_difference}    Evaluate    ${exercise_buttons}-${clickable_element}
    Run Keyword If    ${position_difference} <= 100    Scroll Page To Bottom
    ${line_1_text}    Set Variable    rivi 1
    @{line_1_text_characters}    Split String To Characters    ${line_1_text}
    ${line_2_text}    Set Variable    rivi 2
    @{line_2_text_characters}    Split String To Characters    ${line_2_text}
    ${line_3_text}    Set Variable    rivi 3
    @{line_3_text_characters}    Split String To Characters    ${line_3_text}
    FOR    ${character}    IN    @{line_1_text_characters}
        Press Keys    xpath=.//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//eb-editing//textarea    \\13
    FOR    ${character}    IN    @{line_2_text_characters}
        Press Keys    xpath=.//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//eb-editing//textarea    \\13
    FOR    ${character}    IN    @{line_3_text_characters}
        Press Keys    xpath=.//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//eb-editing//textarea    \\13
    ${textarea_text}    Get Value    xpath=.//eb-editing//textarea
    Scroll Page To Bottom
    Should Contain    ${textarea_text}    rivi 1
    Should Contain    ${textarea_text}    rivi 2
    Should Contain    ${textarea_text}    rivi 3
    Submit Exercise
    Check Exercise Buttons In Continue State
    Continue Exercise
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//eb-editing//textarea
    Should Not Be Equal As Strings    ${textarea_text}    ${EMPTY}
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//eb-editing//textarea
    Should Be Equal As Strings    ${textarea_text}    ${EMPTY}

Do Exercise Fill In - Open Question Short Response - Matikkaeditori
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    ${correct_answers}    Create List    10    20    30
    ${answer_fields}    Get Element Count    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    Wait Until Element Is Enabled    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]
    Scroll Element Into View    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]
    Input Text    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]    ${correct_answers[1]}
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    Content testing    Exercises / interactions    Fill in - Open Question Short reponse - matikkaeditori
    Wait Until Element Is Enabled    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]
    #Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]
    ${answer_text}    Get Element Attribute    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[1]    ng-reflect-model
    Should Be Equal As Strings    ${answer_text}    ${correct_answers[1]}
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${answer_fields}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index}]
        Scroll Element Into View    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index}]
        Input Text    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index}]    ${correct_answers[${index_answer}]}
    END
    Submit Exercise

Do Exercise Match - Cloze Combi - Kaikki
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block
    Start Exercise Over
    ${correct_answer}    Set Variable    monkey
    ${incorrect_answer}    Set Variable    apina
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]
    Scroll Element Into View    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]
    Input Text    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]    ${incorrect_answer}
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    Content testing    Exercises / interactions    Match - Cloze combi - kaikki
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit/input)[1]
    ${answer_text}    Get Element Attribute    xpath=(.//eb-cloze-content-block//eb-cloze-edit/input)[1]    ng-reflect-model
    Should Be Equal As Strings    ${answer_text}    ${incorrect_answer}
    Submit Exercise
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit)[1]
    ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-content-block//eb-cloze-edit)[1]    class
    Should Contain    ${answer_style}    eb-incorrect
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]
    Scroll Element Into View    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]
    Input Text    xpath=(.//eb-cloze-content-block//eb-cloze-edit//input)[1]    ${correct_answer}
    Submit Exercise
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit)[1]
    ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-content-block//eb-cloze-edit)[1]    class
    Should Contain    ${answer_style}    eb-correct

Do Exercise Match - Cloze Combi - Gap Match
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    Start Exercise Over
    ${answers}    Get Element Count    xpath=.//eb-source-list//eb-source-list-item
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index}        Evaluate    ${index}+1
        ${answer_text}    Get Text    xpath=(.//eb-source-list//eb-source-list-item)[${index}]
    END
    ${answer_options}    Create List    Dienstag (1)    Dienstag (2)    Dezember    December    Mittag
    ${answer_slots}    Create List    1    0    2    0    3
    FOR    ${index}    IN RANGE     0       ${answers}
        ${status}    ${error_msg}    Run Keyword And Ignore Error
        ...    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item//*[text()="${answer_options[${index}]}"]/ancestor::eb-source-list-item
        ...    xpath=(.//eb-cloze-drag)[${answer_slots[${index}]}]
        Sleep    2
    END
    Submit Exercise
    Check Exercise score    3

Do Exercise Match - Cloze combi - Drop
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    ${answers_correct}    Create List    Tuesday    June    week
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_correct[${index_answer}]}"]
    END
    Submit Exercise
    Check Exercise score    3

Do Exercise Match - Cloze Combi - Open Question
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block
    Start Exercise Over
    ${answers_correct}    Create List    lumme    something    something else    wooo    booohooo
    ${index}    Set Variable    ${0}
    FOR    ${answer}    IN    @{answers_correct}
        Log    ${answer}
        ${index}    Evaluate    ${index}+1
        Input Text    xpath=(.//eb-cloze-edit/input)[${index}]    ${answer}
    END
    Submit Exercise

Do Exercise File As Answer
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button")]
    Wait Until Element Is Enabled    xpath=.//input[@type="file"]
    Choose File    xpath=.//input[@type="file"]    ${CURDIR}${/}${UPLOAD_IMAGE}
    Wait Until Page Contains Element    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button") and @disabled]
    Wait Until Element Is Visible    xpath=.//eb-file-upload/*[starts-with(@class, "eb-file-upload-file-info")]
    # ###
    # Wait Until Keyword Succeeds    30s    1s
    # ...    Wait Until Element Is Enabled
    # ...    xpath=.//eb-file-upload/*[@class="eb-file-upload-files"]/*[starts-with(@class, "eb-file-upload-file") and normalize-space(string())="sanomapro.jpg"]//*[contains(@class, "eb-icon-delete")]/parent::eb-icon
    # Wait Until Element Is Not Visible
    # ...    xpath=.//eb-file-upload/*[@class="eb-file-upload-files"]/*[starts-with(@class, "eb-file-upload-file") and normalize-space(string())="sanomapro.jpg"]//*[contains(@class, "eb-icon-delete")]/parent::eb-icon
    # Wait Until Element Is Enabled    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button")]
    # ###
    Start Exercise Over
    Wait Until Page Does Not Contain Element    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button") and @disabled]
    Wait Until Element Is Not Visible    xpath=.//eb-file-upload/*[starts-with(@class, "eb-file-upload-file-info")]
    #
    Wait Until Element Is Enabled    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button")]
    Wait Until Element Is Enabled    xpath=.//input[@type="file"]
    Choose File    xpath=.//input[@type="file"]    ${CURDIR}${/}${UPLOAD_IMAGE}
    Wait Until Page Contains Element    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button") and @disabled]
    Wait Until Element Is Visible    xpath=.//eb-file-upload/*[starts-with(@class, "eb-file-upload-file-info")]
    Submit Exercise

Do Exercise File Upload In Combi Content
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-file-upload/button[contains(@class, "eb-file-upload-button")]
    Wait Until Element Is Enabled    xpath=.//eb-file-upload/input[@type="file"]
    ${file_uploads}    Get Element Count    xpath=.//eb-file-upload/input[@type="file"]
    FOR    ${index}    IN RANGE    0    ${file_uploads}
        ${index}    Evaluate    ${index}+1
        Choose File    xpath=(.//eb-file-upload/input[@type="file"])[${index}]    ${CURDIR}${/}${UPLOAD_IMAGE}
        Wait Until Page Contains Element    xpath=(.//eb-file-upload/button[contains(@class, "eb-file-upload-button") and @disabled])[${index}]
        Wait Until Element Is Visible    xpath=(.//eb-file-upload/*[starts-with(@class, "eb-file-upload-file-info")])[${index}]
    END

Do Exercise Match - Single Response
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    apina    mies    teksti täytyy rivittää eri riville kuvan kanssa
    ${right_elements}    Create List    asuu viidakossa    asuu talossa    koska muuten se ei näy
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Sleep    1
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Sleep     1
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
    END

Do Exercise Select - Words - Refllink To Combi Content
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block

Do Exercise Select - Words 2
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block

Do Exercise Select - Words (No Introduction)
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block

Do Exercise Simple Select Words Exercise
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block

Do Exercise UI: Match Single Response - Long Text On Puzzle
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    1323    1917    1995    2017    2019
    ${right_elements}    Create List    Pähkinäsaaren rauha    Suomi itsenäistyy    Suomi liittyy EU:iin    Trump aloittaa presidenttinä    Educa    Kampus!
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Sleep    1
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Sleep     1
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise

Do Exercise Fill In - Open Question Long Answer Matikkaeditorilla
    #Sleep    1
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Be Equal As Strings    ${textarea_text}    ${EMPTY}
    #Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    ${clickable_element}    Get Vertical Position   xpath=.//app-document//eb-editing//textarea
    ${exercise_buttons}    Get Vertical Position    xpath=.//app-module-content-buttons//*[starts-with(@class, "exercise-buttons")]
    ${position_difference}    Evaluate    ${exercise_buttons}-${clickable_element}
    Run Keyword If    ${position_difference} <= 100    Scroll Page To Bottom
    ${line_1_text}    Set Variable    rivi 1
    ${line_2_text}    Set Variable    rivi 2
    ${line_3_text}    Set Variable    rivi 3
    Click Element On Page    xpath=.//app-document//eb-editing//textarea
    Press Keys    xpath=.//app-document//eb-editing//textarea
    ...    ${line_1_text}    ENTER    ${line_2_text}    ENTER    ${line_3_text}    ENTER
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Contain    ${textarea_text}    ${line_1_text}
    Should Contain    ${textarea_text}    ${line_2_text}
    Should Contain    ${textarea_text}    ${line_3_text}
    Submit Exercise
    Check Exercise Buttons In Continue State
    # Start Exercise Over
    # Sleep    1
    # Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    # ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    # Should Be Equal As Strings    ${textarea_text}    ${EMPTY}

Do Exercise Fill In - Open Question With Table
    Wait Until Element Is Enabled    xpath=.//*[@class="title-feedback"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    ${clickable_element}    Get Vertical Position    xpath=.//app-document//eb-editing//textarea
    ${exercise_buttons}    Get Vertical Position    xpath=.//app-module-content-buttons//*[starts-with(@class, "exercise-buttons")]
    ${position_difference}    Evaluate    ${exercise_buttons}-${clickable_element}
    Run Keyword If    ${position_difference} <= 100    Scroll Page To Bottom
    ${line_1_text}    Set Variable    rivi 1
    ${line_2_text}    Set Variable    rivi 2
    ${line_3_text}    Set Variable    rivi 3
    Click Element On Page    xpath=.//app-document//eb-editing//textarea
    Press Keys    xpath=.//app-document//eb-editing//textarea
    ...    ${line_1_text}    ENTER    ${line_2_text}    ENTER    ${line_3_text}    ENTER
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Contain    ${textarea_text}    ${line_1_text}
    Should Contain    ${textarea_text}    ${line_2_text}
    Should Contain    ${textarea_text}    ${line_3_text}
    Submit Exercise
    Check Exercise Buttons In Continue State
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Be Equal As Strings    ${textarea_text}    ${EMPTY}

Do Exercise Match - Cloze combi - Itsearviointitehtävä
    Scroll Page To Bottom
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-cloze-container
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    #${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//*[@class="eb-select-dropdown"]/eb-select-option//*[text()=.]
    ${randon_selection}   Evaluate    random.randint(1, ${answer_texts})    modules=random
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    ${drop_down_answers}    Create List
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Scroll To Answer    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=((.//eb-cloze-drop//eb-select)[${index}]//*[@class="eb-select-dropdown"]/eb-select-option//*[text()=.])[${randon_selection}]
        ${text}    Get Text    xpath=((.//eb-cloze-drop//eb-select)[${index}]//*[@class="eb-select-dropdown"]/eb-select-option//*[text()=.])[${randon_selection}]
        Append To List    ${drop_down_answers}    ${text}
        Click Element On Page    xpath=((.//eb-cloze-drop//eb-select)[${index}]//*[@class="eb-select-dropdown"]/eb-select-option//*[text()=.])[${randon_selection}]
    END
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-cloze-edit/input
    ${input_answers}    Create List
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Scroll To Answer    xpath=(.//app-document//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ${index}
        ${text}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ng-reflect-model
        Append To List    ${input_answers}    ${text}
    END
    Submit Exercise
    Log Many    ${drop_down_answers}
    Log Many    ${input_answers}

Do Exercise Match - Cloze Combi - Hide Gaps
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-cloze-edit/input
    ${text_fields}    Get Element Count    xpath=.//eb-cloze-edit/input
    ${text_placeholders}    Create List
    ${correct_answers}    Create List
    ...    Jukolan    talo    Hämeessä    rinteellä    Toukolan    kylää.    Sen    tanner,    pellot,    joissa    mennyt    vilja
    ...    peltojen    niittu    apilaäyräinen    halkileikkaama    monipolvisen    ojan    heiniä    karjalle    muutoin    metsiä,
    ...    erämaita,    jotka    kautta    sille    Iso jaon    aikoina.
    FOR    ${index}    IN RANGE     0       ${text_fields}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//eb-cloze-edit/input)[${index}]    ${correct_answers[${index_answer}]}
    END
    Submit Exercise

Do Exercise UI: Match - Single Response - Perus
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    1323    1917    1995    2017
    ${right_elements}    Create List    Pähkinäsaaren rauha    Suomi itsenäistyy    Suomi liittyy EU:iin    Trump aloittaa presidenttinä
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        Sleep    1
        Execute JavaScript    window.document.evaluate('.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index}        Evaluate    ${index}+1
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_correct}    eb-correct
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_correct}    eb-correct
    END
    Exercise Points Should Be Visible
    Check Exercise Score    4
    Start Exercise Over
    Exercise Points Should Not Be Visible
    ${left_elements}    Create List    1323    1917    1995    2017
    ${right_elements}    Create List    Trump aloittaa presidenttinä    Suomi liittyy EU:iin    Suomi itsenäistyy    Pähkinäsaaren rauha
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        Sleep    1
        Execute JavaScript    window.document.evaluate('.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index}        Evaluate    ${index}+1
        ${is_marked_incorrect}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_incorrect}    eb-incorrect
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Should Contain    ${is_marked_incorrect}    eb-incorrect
    END
    Exercise Points Should Be Visible
    Check Exercise Score    0
    Start Exercise Over
    Exercise Points Should Not Be Visible

Do Exercise Text - Freehand Drawing
    ${url_link}    Set Variable    https://futurice.com
    ${url_in_browser}    Set Variable    https://www.futurice.com
    Wait Until Element Is Enabled    xpath=.//app-document//a[contains(@class, "eb-content-block") and @href="${url_link}"]
    Click Element On Page    xpath=.//app-document//a[contains(@class, "eb-content-block") and @href="${url_link}"]
    Switch Tabs To Index    1
    ${tab_url}    Get Location
    ${tab_url}    Get Substring    ${tab_url}    0    -1
    Should Be Equal As Strings    ${tab_url}    ${url_in_browser}

Do Exercise Text - Arkku-linkki
    #Force Links To Open In One Window
    Wait Until Element Is Enabled    xpath=.//app-document//*[@class="eb-content-block"]//a[@href]
    ${url_to_click}    Get Element Attribute    xpath=.//app-document//*[@class="eb-content-block"]//a[@href]    href
    Click Element On Page    xpath=.//app-document//*[@class="eb-content-block"]//a[@href]
    Switch Tabs To Index    1
    ${tab_url}    Get Location
    Should Be Equal As Strings    ${tab_url}    ${url_to_click}

Do Exercise Non-scorable Exercise With "done"
    #Force Links To Open In One Window
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-checkbox
    Click Element On Page    xpath=.//eb-choice-list//eb-checkbox
    #${url_to_click}    Get Element Attribute    xpath=.//app-document//*[@class="eb-content-block"]//a[@href]    href
    #Click Element On Page    xpath=.//app-document//*[@class="eb-content-block"]//a[@href]
    #Switch Tabs To Index    1
    #${tab_url}    Get Location
    #Should Be Equal As Strings    ${tab_url}    ${url_to_click}

Do Exercise Arrange
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Kirahvi"]/ancestor::eb-order-choice
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[1]
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Kirahvi"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    ${selection_texts_saved_before}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_saved_before}    ${selection_text}
    END
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item        Content testing    Exercises / interactions    Arrange
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_saved_after}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_saved_after}    ${selection_text}
    END
    Should Be Equal    ${selection_texts_saved_before}    ${selection_texts_saved_after}
    #
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Norsu"]/ancestor::eb-order-choice
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[2]
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Norsu"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[2]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Sarvikuono"]/ancestor::eb-order-choice
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[3]
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Sarvikuono"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[3]
    Wait Until Element Is Enabled    xpath=(.//eb-ordering//eb-order-choice)[1]
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Submit Exercise
    Should Not Be Equal    ${selection_texts_initial}    ${selection_texts_final}
    Continue Exercise
    Check Exercise Buttons In Initial State

Do Exercise Text - Mind Map - Multiple Meaningful Relations
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Wait Until Element Is Visible    xpath=.//*[@id="referral-relations-button"]
    Run Keyword If    '${status}'=='PASS'    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
    Wait Until Element Is Enabled    xpath=.//*[@class="relation-content"]//span[text()[not(.="")]]
    ${extra_links}    Get Element Count    xpath=.//*[@class="relation-content"]//span[text()[not(.="")]]
    FOR    ${index}    IN RANGE     0       ${extra_links}
        ${index}    Evaluate    ${index}+1
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Element Should Be Visible    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Run Keyword If    '${status}'=='FAIL'    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
        Wait Until Element Is Enabled    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        ${extra_material_text}    Get Text    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Click Element On Page    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    2
        Click Element On Page     xpath=.//*[@id="referral-relations-button"]
        Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    1
    END

Do Exercise Choice - Multiple Reflinks Added
    Start Exercise Over
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Wait Until Element Is Visible    xpath=.//*[@id="referral-relations-button"]
    Run Keyword If    '${status}'=='PASS'    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
    Wait Until Element Is Enabled    xpath=.//*[@class="relation-content"]//span[text()[not(.="")]]
    ${extra_links}    Get Element Count    xpath=.//*[@class="relation-content"]//span[text()[not(.="")]]
    FOR    ${index}    IN RANGE     0       ${extra_links}
        ${index}    Evaluate    ${index}+1
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Element Should Be Visible    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Run Keyword If    '${status}'=='FAIL'    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
        Wait Until Element Is Enabled    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        ${extra_material_text}    Get Text    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Wait Until Keyword Succeeds    5s    50ms
        ...    Click Element On Page    xpath=(.//*[@class="relation-content"]//span[text()[not(.="")]])[${index}]
        Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    2
        Click Element On Page     xpath=.//*[@id="referral-relations-button"]
        Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    1
    END
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${randon_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Scroll To Answer    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    1
    Open Content Item    Content testing    Exercises / interactions    Choice - multiple Reflinks added
    Sleep    1
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-checkbox/input[@type="checkbox"]
    ${selections}    Get Element Count    xpath=.//eb-choice-list//eb-checkbox
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}        Evaluate    ${index}+1
        Scroll To Answer    xpath=(.//eb-choice-list//eb-checkbox)[${index}]
        Click Element On Page    xpath=(.//eb-choice-list//eb-checkbox)[${index}]
    END
    Submit Exercise

Do Exercise Open Question Short Response - Single Meaningful Relations
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Wait Until Element Is Visible    xpath=.//*[@id="referral-relations-button"]
    Run Keyword If    '${status}'=='PASS'    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
    Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    2
    Click Element On Page     xpath=.//*[@id="referral-relations-button"]
    Wait Until Keyword Succeeds    10    1    Check Number Of Split Areas    1
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    Start Exercise Over
    ${correct_answers}    Create List    gap    test
    ${text_fields}    Get Element Count    xpath=.//eb-open-question-short-response//eb-cloze-edit
    FOR    ${index}    IN RANGE     0       ${text_fields}
        ${list_index}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Input Text    xpath=(.//eb-open-question-short-response//textarea)[${index}]    ${correct_answers[${list_index}]}
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//*[@id="exercise-score"]
    ${score}    Get Text    xpath=.//*[@id="exercise-score"]

Do ExercDo Exercise Unscored: Cloze Combi Openise UI: Open Question Long
    Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    Start Exercise Over
    Press Keys    xpath=.//app-document//eb-editing//textarea    r
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    v
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    1
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    Press Keys    xpath=.//app-document//eb-editing//textarea    r
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    v
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    2
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    Press Keys    xpath=.//app-document//eb-editing//textarea    r
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    v
    Press Keys    xpath=.//app-document//eb-editing//textarea    i
    Press Keys    xpath=.//app-document//eb-editing//textarea    3
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Contain    ${textarea_text}    rivi1
    Should Contain    ${textarea_text}    rivi2
    Should Contain    ${textarea_text}    rivi3
    Wait Until Element Is Enabled    xpath=.//input[@type="file"]
    Choose File    xpath=.//input[@type="file"]    ${CURDIR}${/}${UPLOAD_IMAGE}
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Contain    ${textarea_text}    ${UPLOAD_IMAGE}
    Submit Exercise
    Start Exercise Over

Do Exercise UI: Open Question Short Response - Explanation Of Solution
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    ${line_1_text}    Set Variable    rivi 1
    Click Element On Page    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    Press Keys    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea    ${line_1_text}
    ${input_text}    Get Value    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    Should Contain    ${input_text}    ${line_1_text}
    Submit Exercise
    Page Should Contain Element    xpath=.//eb-cloze-edit[contains(@class, "eb-readonly")]
    Start Exercise Over
    Page Should Not Contain Element    xpath=.//eb-cloze-edit[contains(@class, "eb-readonly")]

Do Exercise UI: Open Question Long Response - Explanation Of Solution
    Start Exercise Over
    @{line_3_text_characters}    Split String To Characters    ${line_3_text}
    Sleep    1
    FOR    ${character}    IN    @{line_1_text_characters}
        Press Keys    xpath=.//app-document//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    FOR    ${character}    IN    @{line_2_text_characters}
        Press Keys    xpath=.//app-document//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    FOR    ${character}    IN    @{line_3_text_characters}
        Press Keys    xpath=.//app-document//eb-editing//textarea    ${character}
    END
    Press Keys    xpath=.//app-document//eb-editing//textarea    \\13
    ${textarea_text}    Get Value    xpath=.//app-document//eb-editing//textarea
    Should Contain    ${textarea_text}    rivi 1
    Should Contain    ${textarea_text}    rivi 2
    Should Contain    ${textarea_text}    rivi 3
    Submit Exercise
    Check Exercise Buttons In Continue State
    Continue Exercise
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//eb-editing//textarea
    Should Not Be Equal As Strings    ${textarea_text}    ${EMPTY}
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    ${textarea_text}    Get Value    xpath=.//eb-editing//textarea
    Should Be Equal As Strings    ${textarea_text}    ${EMPTY}

Do Exercise UI: Open Question Long Response - Explanation Of Solution - For Teacher Assessment
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-editing//textarea
    ${line_1_text}    Set Variable    rivi 1
    Press Keys    xpath=.//app-document//eb-editing//textarea    ${line_1_text}
    Click On Exercise Title
    Submit Exercise

Do Exercise Choice - Itsearviointitehtävä
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    ${correct_answer}    Set Variable    2
    Scroll To Answer_v4    xpath=(.//eb-choice-list//eb-radio-button)[${correct_answer}]
    ${selections}    Get Element Count    xpath=.//eb-choice-list//eb-radio-button
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[${correct_answer}]
    Submit Exercise
    #Start Exercise Over

Start Over Choice - Itsearviointitehtävä
    Open Content Item        Content testing    Exercises / interactions    Choice - itsearviointitehtävä
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button

Do Exercise Y
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    ${answers_to_select}    Create List    tv-uutiset    ilmastodiagrammi    reittiopas
    ${answers_selected}    Create List
    ${answer_texts}    Get Element Count    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()=.]
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    #Close Content Item
    Return To Content Feed From Content Item View
    #Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi drop
    Open Content Item    General and real content    Real content    CLOZE DROP: 2 Tietoa alueista
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[1]//eb-select-option//*[text()="${answers_to_select[0]}"]
    Wait Until Element Is Enabled    xpath=.//eb-select//*[contains(@class, "eb-value")]/span
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        ${answer_text}    Get Text    xpath=(.//eb-select)[${index}]//*[contains(@class, "eb-value")]/span
        Should Be Equal As Strings    ${answers_to_select[${index_answer}]}    ${answer_text}
    END
    Start Exercise Over
    Sleep    1
    ${drop_downs_one_not_answered}    Evaluate    ${drop_downs}-1
    FOR    ${index}    IN RANGE     0       ${drop_downs_one_not_answered}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    Submit Exercise
    Wait Until Element Is Enabled    xpath=.//*[@id="exercise-score"]
    ${score}    Get Text    xpath=.//*[@id="exercise-score"]
    ${drop_downs_one_not_answered}    Convert To String    ${drop_downs_one_not_answered}
    Should Contain    ${score}    ${drop_downs_one_not_answered}
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    #${answers_expected}    Create List    right    right    right    right    wrong
    ${answers_expected}    Create List    right    right    wrong
    Lists Should Be Equal    ${answers_selected}    ${answers_expected}
    Start Exercise Over
    Sleep    1
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]
        Click Element On Page    xpath=(.//eb-cloze-drop//eb-select)[${index}]//eb-select-option//*[text()="${answers_to_select[${index_answer}]}"]
    END
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    #Check Exercise Score    5
    Check Exercise Score    3
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-drop//eb-select)[${index}]    class
    END
    Start Exercise Over

Do Exercise Z
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    Start Exercise Over
    ${answers}    Get Element Count    xpath=.//eb-source-list//eb-source-list-item
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index}        Evaluate    ${index}+1
        ${answer_text}    Get Text    xpath=(.//eb-source-list//eb-source-list-item)[${index}]
    END
    ${answer_options}    Create List    positiivinen tai negatiivinen    Neutraalilla    erimerkkiset    samanmerkkiset    esineen
    ${answer_slots}    Create List    1    2    3    4    5
    # FOR    ${index}    IN RANGE     0       ${answers}
    #     ${status}    ${error_msg}    Run Keyword And Ignore Error
    #     ...    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item//*[text()="${answer_options[${index}]}"]/ancestor::eb-source-list-item
    #     ...    xpath=(.//eb-cloze-drag/span)[${answer_slots[${index}]}]
    #     Sleep    2
    # END
    FOR    ${index}    IN RANGE     0       ${answers}
        #\    ${status}    ${error_msg}    Run Keyword And Ignore Error
        Mouse Down    xpath=.//eb-source-list//eb-source-list-item//*[text()="${answer_options[${index}]}"]/ancestor::eb-source-list-item
        Sleep    2
        Mouse Up    xpath=(.//eb-cloze-drag/span)[${answer_slots[${index}]}]
        Sleep    2
    END
    Submit Exercise
    Check Exercise score    3

Do Exercise X
    Wait Until Element Is Enabled    xpath=.//eb-cloze-renderer
    Start Exercise Over
    ${answers}    Get Element Count    xpath=.//eb-cloze-edit/input
    ${answer_texts}    Create List    ResQ    Google Maps    done
    Input Text    xpath=(.//eb-cloze-edit/input)[1]    ${answer_texts[1]}
    Submit Exercise
    Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[1]//*[contains(@class, "eb-icon-correct")]
    Page Should Not Contain Element    xpath=(.//eb-cloze-edit)[2]//*[contains(@class, "eb-icon-correct")]
    Page Should Not Contain Element    xpath=(.//eb-cloze-edit)[3]//*[contains(@class, "eb-icon-correct")]
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        Input Text    xpath=(.//eb-cloze-edit/input)[${index_text}]    ${answer_texts[${index}]}
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[${index}]//*[contains(@class, "eb-icon-correct")]
    END
    #.//eb-cloze-edit//*[contains(@class, "eb-icon-correct")]
    #Check Exercise score    3

Do Exercise XY
    Wait Until Element Is Enabled    xpath=.//eb-cloze-renderer
    Start Exercise Over
    ${answers}    Get Element Count    xpath=.//eb-cloze-edit/input
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        Input Text    xpath=(.//eb-cloze-edit/input)[${index_text}]    ${index}
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[${index}]//*[contains(@class, "eb-icon-correct")]
    END
    Wait Until Element Is Enabled    xpath=.//eb-feedback/a
    Click Element On Page    xpath=.//eb-feedback/a
    Wait Until Element Is Enabled    xpath=.//eb-popover
    Click Element On Page    xpath=.//eb-popover//a[@class="eb-close"]
    Element Should Not Be Visible    xpath=.//eb-popover
    Click Element On Page    xpath=.//eb-feedback/a
    Wait Until Element Is Enabled    xpath=.//eb-popover
    Click Element On Page    xpath=.//div[@class="cdk-overlay-container"]
    Element Should Not Be Visible    xpath=.//eb-popover

Do Exercise XYZ
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    Start Exercise Over
    ${answers}    Get Element Count    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        Press Keys    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index_text}]    rivi ${index}
    END
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    General and real content    Real content    OQS NON-SCORABLE: 1 Selitä käsitteet
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        ${textarea_text}    Get Value    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index_text}]
        Should Be Equal As Strings    ${textarea_text}    rivi ${index}
    END
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        ${textarea_text}    Get Value    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index_text}]
        Should Be Equal As Strings    ${textarea_text}    ${EMPTY}
    END
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index_text}        Evaluate    ${index}+1
        Press Keys    xpath=(.//eb-open-question-short-response//eb-cloze-edit//textarea)[${index_text}]    rivi ${index}
    END
    Submit Exercise
    FOR    ${index}    IN RANGE     0       ${answers}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-edit)[${index}]//*[contains(@class, "eb-icon-correct")]
    END
    # Wait Until Element Is Enabled    xpath=.//eb-feedback/a
    # Click Element On Page    xpath=.//eb-feedback/a
    # Wait Until Element Is Enabled    xpath=.//eb-popover
    # Click Element On Page    xpath=.//eb-popover//a[@class="eb-close"]
    # Element Should Not Be Visible    xpath=.//eb-popover
    # Click Element On Page    xpath=.//eb-feedback/a
    # Wait Until Element Is Enabled    xpath=.//eb-popover
    # Click Element On Page    xpath=.//div[@class="cdk-overlay-container"]
    # Element Should Not Be Visible    xpath=.//eb-popover

Do Exercise OQS ANSWER LVL FEEDBACK: 1.3 Translate the idioms
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    ${textareas}    Get Element Count    xpath=.//eb-open-question-short-response//textarea
    FOR    ${index}    IN RANGE     0       ${textareas}
        ${index}    Evaluate    ${index}+1
        Scroll Element Into View    xpath=(.//eb-open-question-short-response//textarea)[${index}]
        Sleep    5
    END

Do Exercise MATCH SINGLE: 4. Taulukkolaskenta*
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Sleep    2
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    Solujen A1:A5 pienin arvo    Sarakkeen A1:A5 summa    Solujen A1:A5 suurin arvo    Solujen A1:A5 keskiarvo    Solujen A1:A2 erotus    Solujen A1:A5 tulo
    ${right_elements}    Create List    =MIN(A1:A5)    =A1+A2+A3+A4+A5    =MAKS(A1:A5)    =KESKIARVO(A1:A5)    =A1-A2    =A1*A2*A3*A4*A5
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Sleep    1
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Sleep     1
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Start Exercise Over

Do Exercise MATCH MATRIX: C Paikkatieto koostuu sijainti- ja ominaisuustiedosta
    Wait Until Element Is Enabled    xpath=.//app-document//eb-connection-matrix
    Start Exercise Over
    ${question_rows}    Create List
    ${rows}    Get Element Count    xpath=.//app-document//eb-connection-matrix//tr
    FOR    ${index}    IN RANGE     0       ${rows}
        ${index}        Evaluate    ${index}+1
        ${status}    Run Keyword And Return Status    Page Should Contain Element    xpath=(.//app-document//eb-connection-matrix//tr[${index}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True'    Append To List    ${question_rows}    ${index}
    END
    ${questions_in_total}    Get Length    ${question_rows}
    ${exercise_buttons_position}    Get Vertical Position    xpath=.//*[contains(@class, "exercise-buttons")]
    &{correct_answers}    Create Dictionary
    ...    1=correct    2=incorrect   3=incorrect    4=correct   5=correct    6=incorrect   7=correct    8=correct
    &{incorrect_answers}    Create Dictionary
    ...    1=incorrect    2=correct   3=correct    4=incorrect   5=incorrect    6=correct   7=incorrect    8=incorrect
    Scroll Page To Bottom
    FOR    ${index}    IN RANGE    0    ${questions_in_total}
        ${index_list}        Evaluate    ${index}+1
        ${index_string}    Convert To String    ${index_list}
        ${status}    Run Keyword And Return Status    Page Should Contain Element    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        ${answer_to_choose}    Get From Dictionary    ${correct_answers}    ${index_string}
        #Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='correct'    Scroll To Answer    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='correct'    Scroll Element Into View    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='correct'    Click Element On Page    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='incorrect'    Scroll Element Into View    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[2]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='incorrect'    Click Element On Page    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[2]
    END
    Submit Exercise
    Check Exercise Score    8
    ${answers_correct}    Get Element Count    xpath=.//eb-checkbox[contains(@class, "eb-checked")]
    FOR    ${index}    IN RANGE    0    ${answers_correct}
        ${index}        Evaluate    ${index}+1
        Page Should Contain Element    xpath=(.//eb-checkbox[contains(@class, "eb-checked")])[${index}]//span[contains(@class, "eb-control") and contains(@class, "eb-correct")]
    END
    Start Exercise Over
    Scroll Page To Bottom
    FOR    ${index}    IN RANGE    0    ${questions_in_total}
        ${index_list}        Evaluate    ${index}+1
        ${index_string}    Convert To String    ${index_list}
        ${status}    Run Keyword And Return Status    Page Should Contain Element    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        ${answer_to_choose}    Get From Dictionary    ${incorrect_answers}    ${index_string}
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='correct'    Scroll Element Into View    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='correct'    Click Element On Page    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[1]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='incorrect'    Scroll Element Into View    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[2]
        Run Keyword If    '${status}'=='True' and '${answer_to_choose}'=='incorrect'    Click Element On Page    xpath=(.//app-document//eb-connection-matrix//tr[${question_rows[${index}]}]//eb-checkbox)[2]
    END
    Submit Exercise
    Element Should Not Be Visible    xpath=.//*[@id="exercise-score"]
    FOR    ${index}    IN RANGE    0    ${questions_in_total}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-checkbox[contains(@class, "eb-checked")])[${index}]//span[contains(@class, "eb-control") and contains(@class, "eb-incorrect")]
        Page Should Contain Element    xpath=(.//eb-checkbox[contains(@class, "eb-checked")])[${index}]//span[contains(@class, "eb-control") and contains(@class, "eb-incorrect")]
    END
    Start Exercise Over

Do Exercise SELECT WORDS: Tehtävä 2, Lauseenvastike
    Wait Until Element Is Enabled    xpath=.//eb-cloze-id
    Start Exercise Over
    @{answers_correct}    Create List    Kirjoittaessaan    yllättyvänsä    romaanihenkilöidensä    päähänpistoista    nähdäkseen    päähenkilönsä    kohtalon
    Wait Until Element Is Enabled    xpath=.//eb-cloze-id
    FOR    ${answer_text}    IN    @{answers_correct}
        Wait Until Element Is Enabled    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]
        ${style_before}    Get Element Attribute    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]/parent::span/parent::span    class
        Should Not Contain    ${style_before}    eb-selected
        Click Element On Page    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]
        ${style_after}    Get Element Attribute    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]/parent::span/parent::span    class
        Should Contain    ${style_after}    eb-selected
    END
    Submit Exercise
    Check Exercise Score    7
    FOR    ${answer_text}    IN    @{answers_correct}
        Wait Until Element Is Enabled    xpath=.//eb-cloze-id//span[starts-with(text(),"${answer_text}")]
        ${style_correct}    Get Element Attribute    xpath=.//span[starts-with(text(),"${answer_text}")]/ancestor::eb-cloze-id    class
        Should Contain    ${style_correct}    eb-correct
    END
    Start Exercise Over

Do Exercise CHOICE MULTI: D Mitä paikkatiedon avulla voidaan esittää?
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answers_correct}    Create List    naapurisopu    lentokoneen matkanopeus    valmisaterian viimeinen käyttöpäivä
    ${answers_incorrect}    Create List    kotiosoite    ruoka-arvostelu huippuravintolasta    valokuva Näsinneulasta    lähikaupan aukioloaika    meriveden lämpötila    kuntosalien sijainti    bussireitti    gneissin esiintymisalue    kasvillisuusalue
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${randon_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Scroll To Answer    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    General and real content    Real content    CHOICE MULTI: D Mitä paikkatiedon avulla voidaan esittää?
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ${answer_random}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]   class
    Should Contain    ${answer_random}    eb-checked
    Start Exercise Over
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_incorrect}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]
        Sleep    1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_incorrect}    Run Keyword And Return Status    Should Contain    ${answer}    eb-incorrect
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_incorrect}'=='True'
        ...    List Should Contain Value    ${answers_incorrect}    ${answer_text}
    END
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//*[@class="eb-label"]
    Sleep    1
    FOR    ${answer_text}    IN    @{answers_correct}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Check Exercise Score    3
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_correct}    Run Keyword And Return Status    Should Contain    ${answer}    eb-correct
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_correct}'=='True'
        ...    List Should Contain Value    ${answers_correct}    ${answer_text}
    END
    Start Exercise Over

Do Exercise CHOICE SINGLE: 3.8 Choose the correct picture
    ${answer_correct}    Set Variable    3
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//li[starts-with(@class, "eb-choice")]
    Start Exercise Over
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${choices}    Get Element Count    xpath=.//eb-choice-list//li[contains(@class, "eb-choice")]
    ${random_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    #Scroll To Answer_v2    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    Scroll Element Into View    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    Sleep    1
    Check Exercise Buttons In Initial State
    #Close Content Item
    Return To Content Feed From Content Item View
    Open Content Item    General and real content    Real content    CHOICE SINGLE: 3.8 Choose the correct picture
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]   class
    Should Contain    ${answer_saved}    eb-checked
    Start Exercise Over
    Sleep    1
    Check Exercise Buttons In Initial State
    #Scroll To Answer_v2    xpath=(.//eb-choice-list//eb-radio-button)[1]
    Scroll To Element Into View    xpath=(.//eb-choice-list//eb-radio-button)[1]
    Wait Until Element Is Enabled    xpath=(.//eb-choice-list//eb-radio-button)[1]
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[1]
    Submit Exercise
    Page Should Contain Element
    ...    xpath=(.//eb-radio-button)[1]//span[contains(@class,"eb-incorrect")]//*[contains(@class,"eb-icon-incorrect")]
    Check Exercise Score    0
    Start Exercise Over
    #
    #Scroll To Answer_v2    xpath=(.//eb-choice-list//eb-radio-button)[${answer_correct}]
    Scroll To Element Into View    xpath=(.//eb-choice-list//eb-radio-button)[${answer_correct}]
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[${answer_correct}]
    Submit Exercise
    Page Should Contain Element
    ...    xpath=(.//eb-radio-button)[${answer_correct}]//span[contains(@class,"eb-correct")]//*[contains(@class,"eb-icon-correct")]
    Check Exercise Score    1
    # ###
    # # Here check the answer level feedback (wrong answer -> style and feedback)
    # ${choices_incorrect}    Get Element Count    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button
    # ${random_selection_incorrect}   Evaluate    random.randint(1, ${choices_incorrect})    modules=random
    # Click Element On Page    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]

Do Exercise ARRANGE VERTICAL: A - Kokoeroja
    # bakteeri punasolu ihosolu valkosolu munasolu pölypunkki muurahainen (2/2 pistettä)
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${answers_correct}    Create List    Bakteeri    Punasolu    Ihosolu    Valkosolu    Munasolu    Pölypunkki    Muurahainen
    ${selection_texts_initial}    Create List
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
        ${vPos}    Get Vertical Position    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        ${vPos2}    Get Vertical Position    xpath=.//*[@id="submit-button"]
        ${vertical_difference}    Evaluate    ${vPos} - ${vPos2}
        ${xPos}    Evaluate    ${vPos} + ${vertical_difference}
        Run Keyword If    ${vPos} > ${vPos2}    Execute JavaScript    window.scrollTo(0, ${xPos});
        ${curPosition}    Get Position
    END
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="${answers_correct[6]}"]/ancestor::eb-order-choice
    FOR    ${index}    IN RANGE     ${selections}    0    -1
        ${index_list}    Evaluate    ${index}-1
        Log    ${index}
        Log    ${answers_correct[${index_list}]}
        Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="${answers_correct[${index_list}]}"]/ancestor::eb-order-choice
        Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[${index}]
        ${status}    ${error_msg}    Run Keyword And Ignore Error
        ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="${answers_correct[${index_list}]}"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[${index}]
    END

Do Exercise ARRANGE HORIZONTAL: G8.8a
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    @{correct_answers}    Create List    where    did    you    leave    the book
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    ${index}    Set Variable    0
    FOR    ${answer_text}    IN    @{correct_answers}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="${answer_text}"]/ancestor::eb-order-choice
        ${status}    ${error_msg}    Run Keyword And Ignore Error
        ...    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="${answer_text}"]/ancestor::eb-order-choice
        ...    xpath=.//eb-ordering//eb-order-choice[${index}]
    END
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="one"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="one"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[1]
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Two"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Two"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[2]
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="five"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="five"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[3]
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="twelve"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="twelve"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[4]
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="sixteen"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="sixteen"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[5]
    # Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="twenty"]/ancestor::eb-order-choice
    # ${status}    ${error_msg}    Run Keyword And Ignore Error    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="twenty"]/ancestor::eb-order-choice    xpath=.//eb-ordering//eb-order-choice[6]
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Submit Exercise
    Check Exercise Score    1
    Lists Should Be Equal    ${selection_texts_final}    ${correct_answers}
    Start Exercise Over

Do Exercise DRAG IMAGE: 3. Taulukkolaskenta*
    Wait Until Element Is Enabled    xpath=.//eb-source-list
    Wait Until Element Is Enabled    xpath=.//eb-cloze-image
    ${image_loaded}    Get Element Attribute    xpath=.//eb-cloze-image    innerHTML
    ${innerHTML_length}    Get Length    ${image_loaded}
    Should Be True    ${innerHTML_length}>0
    #.//eb-cloze-drag/*[contains(@class, "eb-droppable")]
    ${status}    ${error_msg}    Run Keyword And Ignore Error
    ...    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[1]    xpath=(.//eb-cloze-drag)[1]/*[contains(@class, "eb-droppable")]

Do Exercise DRAG IMAGE HIDE TARGETS: 2a Konfliktit kartalla
    Wait Until Element Is Enabled    xpath=.//eb-source-list
    Wait Until Element Is Enabled    xpath=.//eb-cloze-image-hide-targets
    ${image_loaded}    Get Element Attribute    xpath=.//eb-cloze-image-hide-targets    innerHTML
    ${innerHTML_length}    Get Length    ${image_loaded}
    Should Be True    ${innerHTML_length}>0
    #.//eb-cloze-drag/*[contains(@class, "eb-droppable")]
    #${status}    ${error_msg}    Run Keyword And Ignore Error
    #...    Drag And Drop    xpath=.//eb-source-list//eb-source-list-item[1]    xpath=(.//eb-cloze-drag)[1]/*[contains(@class, "eb-droppable")]

Do Exercise DIALOGUE: 1.6 Dialogue
    Wait Until Element Is Enabled    xpath=.//eb-dialogue//button[contains(@class, "eb-actor-selection-button")]
    ${buttons}    Get Element Count    xpath=.//eb-dialogue//button[contains(@class, "eb-actor-selection-button")]
    Should Be Equal As Integers    ${buttons}    2
    Click Element On Page    xpath=.//eb-dialogue//button[contains(@class, "eb-actor-selection-button") and normalize-space(text())="A"]
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="0"]
    Wait Until Element Is Enabled    xpath=.//*[@class="eb-actor-selected" and text()="A"]
    #eb-message-navigate-button-backward
    Wait Until Element Is Enabled    xpath=.//*[@class="eb-progress-bar"]//*[contains(@class, "eb-progress-bar-element")]
    ${progress_bar_elements}    Get Element Count    xpath=.//*[@class="eb-progress-bar"]//*[contains(@class, "eb-progress-bar-element")]
    Click Element On Page    xpath=.//button[contains(@class, "eb-message-navigate-button-forward")]
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="1"]
    Click Element On Page    xpath=.//button[contains(@class, "eb-message-navigate-button-backward")]
    Page Should Not Contain Element    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="1"]
    Wait Until Element Is Enabled    xpath=.//button[contains(@class, "eb-show-answer-button")]
    #Click Element On Page    xpath=.//button[contains(@class, "eb-show-answer-button")]
    #Element Should Not Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
    FOR    ${index}    IN RANGE     0       ${progress_bar_elements}
        ${index_next}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index}"]
        ${even_odd_check}    Evaluate    ${index_next}%2
        Run Keyword If    ${even_odd_check}==0    Element Should Not Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${even_odd_check}!=0    Element Should Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-inactive") and @data-index="${index_next}"]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Click Element On Page    xpath=.//button[contains(@class, "eb-message-navigate-button-forward")]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index_next}"]
        Run Keyword If    ${index}==${progress_bar_elements}
        ...    Wait Until Page Contains Element    xpath=.//button[contains(@class, "eb-message-navigate-button-forward") and contains(@class, "disabled")]
    END
    FOR    ${index}    IN RANGE     ${progress_bar_elements}    0    -1
        ${index_next}    Evaluate    ${index}-1
        Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index_next}"]
        ${even_odd_check}    Evaluate    ${index_next}%2
        Run Keyword If    ${even_odd_check}==0    Element Should Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${even_odd_check}!=0    Element Should Not Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index_next}"]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Click Element On Page    xpath=.//button[contains(@class, "eb-message-navigate-button-backward")]
        Run Keyword If    ${index_next}<${progress_bar_elements} and ${index_next}>1
        ...    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-inactive") and @data-index="${index_next}"]
        Run Keyword If    ${index_next}==0
        ...    Wait Until Page Contains Element    xpath=.//button[contains(@class, "eb-message-navigate-button-backward") and contains(@class, "disabled")]
    END
    FOR    ${index}    IN RANGE     0       ${progress_bar_elements}
        ${index_next}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index}"]
        ${even_odd_check}    Evaluate    ${index_next}%2
        Run Keyword If    ${even_odd_check}==0    Element Should Not Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${even_odd_check}!=0    Element Should Be Visible    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${even_odd_check}!=0    Click Element On Page    xpath=.//button[contains(@class, "eb-show-answer-button")]
        Run Keyword If    ${even_odd_check}!=0    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-content-correct-answer")]//span[text()[not(.="")]]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-inactive") and @data-index="${index_next}"]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Click Element On Page    xpath=.//button[contains(@class, "eb-message-navigate-button-forward")]
        Run Keyword If    ${index_next}<${progress_bar_elements}    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-progress-bar-element-active") and @data-index="${index_next}"]
        Run Keyword If    ${index}==${progress_bar_elements}
        ...    Wait Until Page Contains Element    xpath=.//button[contains(@class, "eb-message-navigate-button-forward") and contains(@class, "disabled")]
    END

###
# Exercises for the dashboard
###
Do Exercise UI: Multi Choice Incorrectly
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answers_correct}    Create List    a whale    a cat    a dog
    ${answers_incorrect}    Create List    an ostrich    a worm    a perch
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    FOR    ${answer_text}    IN    @{answers_incorrect}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]
        Sleep    1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_incorrect}    Run Keyword And Return Status    Should Contain    ${answer}    eb-incorrect
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_incorrect}'=='True'
        ...    List Should Contain Value    ${answers_incorrect}    ${answer_text}
    END

Do Exercise UI: Multi Choice Correctly
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${answers_correct}    Create List    a whale    a cat    a dog
    ${answers_incorrect}    Create List    an ostrich    a worm    a perch
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    FOR    ${answer_text}    IN    @{answers_correct}
        Scroll To Answer    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
        Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()="${answer_text}"]/ancestor::label//eb-checkbox
    END
    Submit Exercise
    Check Exercise Score    3
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index_list}    Set Variable    ${index}
        ${index}    Evaluate    ${index}+1
        ${answer}    Get Element Attribute
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label//eb-checkbox/span)[${index}]   class
        ${marked_as_correct}    Run Keyword And Return Status    Should Contain    ${answer}    eb-correct
        Wait Until Element Is Enabled    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        ${answer_text}    Get Text    xpath=(.//eb-choice-list//*[@class="eb-label"])[${index}]
        Run Keyword If    '${marked_as_correct}'=='True'
        ...    List Should Contain Value    ${answers_correct}    ${answer_text}
    END

Do Self Assessment Exercise
    Open Content Item        Content testing    Exercises / interactions    Choice - itsearviointitehtävä
    Do Exercise Choice - Itsearviointitehtävä
    #Close Content Item
    Return To Content Feed From Content Item View

# Abitti
Check Normal Abitti Functionality
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}
    Switch Schools In Kampus    ${DEFAULT_SCHOOL}
    Select Feed    FyKe 7-9 Fysiikka    ${FEED_TYPE_PUBLISHER}
    #
        ${status}    Run Keyword And Return Status    Wait Until Keyword Succeeds    300s    2s
    ...    Wait Until Element Is Not Visible    xpath=.//mat-dialog-container    timeout=300s
    Run Keyword If    '${status}'=='False'    Reload Page
    Run Keyword If    '${status}'=='False'    Wait Until Content Feed Has Loaded
    #
    Open Content Item    Liike ja voima    Nopeus kuvaa liikettä    TT 11_9
    Verify Abitti Functionality

Check Combi-content Abitti Functionality
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}
    Switch Schools In Kampus    ${DEFAULT_SCHOOL}
    Select Feed    FyKe 7-9 Fysiikka    ${FEED_TYPE_PUBLISHER}
    Open Content Item    Liike ja voima    Nopeus kuvaa liikettä    TT 11_6
    Verify Abitti Functionality

Verify Abitti Functionality
    Start Exercise Over
    Scroll Page To Bottom
    #Scroll Page To Top
    Sleep    2
    ${abitti_editors}    Get Element Count    xpath=.//*[starts-with(@id, "math-editor-iframe-")]
    FOR    ${index_main}    IN RANGE    0    ${abitti_editors}
        Scroll To Answer_v4    xpath=.//*[@id="math-editor-iframe-${index_main}"]
        Select Frame    xpath=.//*[@id="math-editor-iframe-${index_main}"]
        Wait Until Element Is Enabled    xpath=.//*[@id="answer1"]
        Sleep    1
        Click Element On Page    xpath=.//*[@id="answer1"]
        Press Keys    xpath=.//*[@id="answer1"]    something here
        Wait Until Element Is Visible    xpath=.//*[@class="rich-text-editor-toolbar-wrapper"]
        #Scroll To Answer_v4    xpath=.//*[@class="rich-text-editor-characters-expand-collapse"]
        Click Element On Page    xpath=.//*[@class="rich-text-editor-characters-expand-collapse"]
        ${elements_in_editor}    Get Element Count
        ...    xpath=.//*[@class="rich-text-editor-toolbar-characters-group"]//button[starts-with(@class, "rich-text-editor-button")]
        Click All Abitti Buttons    ${elements_in_editor}
        Click Element On Page    xpath=.//button[starts-with(@class, "rich-text-editor-new-equation") and @data-i18n="rich_text_editor.insert_equation"]
        Wait Until Element Is Enabled    xpath=.//*[@class="math-editor"]/textarea[@placeholder="LaTeΧ"]
        ${latex_buttons}    Get Element Count    xpath=.//button[starts-with(@class, "rich-text-editor-button") and not(@data-latexcommand)=""]
        Click All Abitti LaTeΧ Buttons    ${latex_buttons}
        Unselect Frame
        Sleep    2
    END
    #Submit Exercise

Click All Abitti Buttons
    [Arguments]    ${elements_in_editor}
    FOR    ${index}    IN RANGE    0    ${elements_in_editor}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled
         ...    xpath=(.//*[@class="rich-text-editor-toolbar-characters-group"]//button[starts-with(@class, "rich-text-editor-button")])[${index}]
        Click Element On Page
         ...    xpath=(.//*[@class="rich-text-editor-toolbar-characters-group"]//button[starts-with(@class, "rich-text-editor-button")])[${index}]
        ${button_text}    Get Text
         ...    xpath=(.//*[@class="rich-text-editor-toolbar-characters-group"]//button[starts-with(@class, "rich-text-editor-button")])[${index}]
        Wait Until Page Contains Element    xpath=.//*[@id="answer1" and contains(text(), "${button_text}")]
        Wait Until Element Is Visible    xpath=.//*[@class="rich-text-editor-toolbar-wrapper"]
    END

Click All Abitti LaTeΧ Buttons
    [Arguments]    ${latex_buttons}
    FOR    ${index}    IN RANGE    0    ${latex_buttons}
        ${index}    Evaluate    ${index}+1
        Mouse Over    xpath=.//*[@class="rich-text-editor-characters-expand-collapse"]
        Click Element On Page    xpath=(.//button[starts-with(@class, "rich-text-editor-button") and not(@data-latexcommand)=""])[${index}]
        Wait Until Element Is Enabled    xpath=.//*[@class="math-editor"]/textarea
        ${latex_text}    Get Value    xpath=.//*[@class="math-editor"]/textarea
        Should Not Be Empty    ${latex_text}
        Clear Element Text    xpath=.//*[@class="math-editor"]/textarea
        Click Element On Page    xpath=.//*[@id="answer1"]
        Click Element On Page    xpath=.//button[starts-with(@class, "rich-text-editor-new-equation") and @data-i18n="rich_text_editor.insert_equation"]
        Wait Until Element Is Enabled    xpath=.//*[@class="math-editor"]/textarea[@placeholder="LaTeΧ"]
    END

Check Abitti Functionality
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}
    Switch Schools In Kampus    ${DEFAULT_SCHOOL}
    Select Feed    ${DEFAULT_METHOD_FOR_THEORY}    ${FEED_TYPE_PUBLISHER}
    Open Content Item    General and real content    Real content    OQL+ABITTI: TT 21_7
    Verify Abitti Functionality

# Exercises with hints
Do Exercise Choice With Hint
    ${answer_correct}    Set Variable    true
    Start Exercise Over
    Check Hint
    Check Exercise Buttons In Initial State
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    ${choices}    Get Element Count    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]
    ${random_selection}   Evaluate    random.randint(1, ${choices})    modules=random
    Click Element On Page    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]   class
    Should Contain    ${answer_saved}    eb-checked
    Sleep    1
    Check Exercise Buttons In Initial State
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    2
    Open Content Item    Content testing    Exercises with Hint    Choice with hint
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-radio-button
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//eb-radio-button)[${random_selection}]   class
    Should Contain    ${answer_saved}    eb-checked
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Start Exercise Over
    Sleep    1
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Check Exercise Buttons In Initial State
    ###
    # Here check the answer level feedback (wrong answer -> style and feedback)
    ${choices_incorrect}    Get Element Count    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button
    ${random_selection_incorrect}   Evaluate    random.randint(1, ${choices_incorrect})    modules=random
    Click Element On Page    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]
    Submit Exercise
    Sleep    1
    Wait Until Element Is Visible    xpath=.//*[@class="title-feedback"]/eb-feedback
    Wait Until Element Is Visible    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::*[starts-with(@class, "eb-choice")]//eb-feedback
    Check Exercise Score    0
    # HERE check for feedback icon
    Wait Until Element Is Enabled    xpath=.//eb-choice-list//eb-feedback
    Wait Until Element Is Enabled
    ...    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]
    ${answer_saved}    Get Element Attribute
    ...    xpath=(.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::label//eb-radio-button)[${random_selection_incorrect}]/span    class
    Should Contain    ${answer_saved}    eb-incorrect
    ###
    ###
    Click Element On Page    xpath=.//*[@class="title-feedback"]/eb-feedback
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]
    Click Element On Page    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]/ancestor::*[contains(@class, "eb-feedback-popover")]//*[@class="eb-close"]
    ###
    Click Element On Page    xpath=.//eb-choice-list//*[@class="eb-label"]//*[text()!="${answer_correct}"]/ancestor::*[starts-with(@class, "eb-choice")]//eb-feedback
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]
    Click Element On Page    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]/ancestor::*[contains(@class, "eb-feedback-popover")]//*[@class="eb-close"]
    ###
    Start Exercise Over
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//app-document//eb-choice-list//li[starts-with(@class, "eb-choice")]
    Wait Until Element Is Not Visible    xpath=.//eb-choice-list//eb-feedback
    Click Element On Page    xpath=.//app-document//eb-choice-list//li//*[normalize-space(text())="${answer_correct}"]
    Submit Exercise
    Check Exercise Score    1
    FOR    ${index}    IN RANGE     0       ${choices}
        ${index}    Evaluate    ${index}+1
        ${label_text}    Get Text
        ...    xpath=(.//app-document//eb-choice-list//li[contains(@class, "eb-choice")]//label)[${index}]
        Run Keyword If    "${label_text}"=="${answer_correct}"        Wait Until Page Contains Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
        Run Keyword If    "${label_text}"!="${answer_correct}"        Wait Until Page Does Not Contain Element
        ...    xpath=.//app-document//eb-choice-list//li[contains(@class, "eb-choice") and contains(@class, "eb-correct")]//*[normalize-space(text())="${label_text}"]
    END
    Click Element On Page    xpath=.//*[@class="title-feedback"]/eb-feedback
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]
    Click Element On Page    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]/ancestor::*[contains(@class, "eb-feedback-popover")]//*[@class="eb-close"]
    Start Exercise Over

Do Exercise Arrange With Hint
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    Start Exercise Over
    ${correct_answers}    Create List    Choice 1    Choice 2    Choice 3
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    ${selection_texts_initial}    Create List
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        ${selection_text}    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_initial}    ${selection_text}
    END
    ###
    Check Hint
    ###
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice//*[text()="Choice 1"]/ancestor::eb-order-choice
    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Choice 1"]/ancestor::eb-order-choice    xpath=.//eb-ordering
    Submit Exercise
    #
    Click Element On Page    xpath=.//*[@class="title-feedback"]/eb-feedback
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]
    Click Element On Page    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]/ancestor::*[contains(@class, "eb-feedback-popover")]//*[@class="eb-close"]
    #
    Drag And Drop    xpath=.//eb-ordering//eb-order-choice//*[text()="Choice 2"]/ancestor::eb-order-choice    xpath=.//eb-ordering
    Click Element On Page    xpath=.//*[@class="title-feedback"]/eb-feedback
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]
    Click Element On Page    xpath=.//*[contains(@class, "eb-feedback-popover")]/*[not(string())=""]/ancestor::*[contains(@class, "eb-feedback-popover")]//*[@class="eb-close"]
    Start Exercise Over

Do Exercise Match Single With Hint
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    ${left_elements}    Create List    AAAA    BBBB
    ${right_elements}    Create List    aaaa    bbbb
    Check Hint
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_left}    eb-connected
        Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Not Contain    ${is_connected_right}    eb-connected
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Scroll To Answer_v4    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]
        Sleep    1
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Click Element On Page    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        Sleep    1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_connected_left}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_left}    eb-connected
        ${is_connected_right}    Get Element Attribute    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_connected_right}    eb-connected
    END
    Submit Exercise
    #
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index_list}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]
        ${is_disabled_left}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${left_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_disabled_left}    eb-disabled
        ${is_disabled_right}    Get Element Attribute
        ...    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")]//*[text()="${right_elements[${index}]}"]/ancestor::li[starts-with(@class, "eb-connection-item")]    class
        Should Contain    ${is_disabled_right}    eb-connected
    END
    Continue Exercise

Do Exercise Cloze Combi With Hint
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Start Exercise Over
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    Scroll Element Into View   xpath=.//eb-cloze-drop//eb-select
    Click Element On Page    xpath=.//eb-cloze-drop//eb-select
    Click Element On Page    xpath=.//eb-cloze-drop//eb-select//eb-select-option//*[normalize-space(text())="drop"]
    Check Hint
    Submit Exercise
    Start Exercise Over

Do Exercise OQLR With Hint
    Wait Until Element Is Enabled    xpath=.//section[starts-with(@class, "eb-open-question-long-response")]
    Start Exercise Over
    Click Element On Page    xpath=.//eb-editing//textarea
    Submit Exercise
    Check Hint
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Start Exercise Over

Do Exercise OQSR With Hint
    Wait Until Element Is Enabled    xpath=.//eb-open-question-short-response
    Start Exercise Over
    Click Element On Page    xpath=.//eb-cloze-edit//textarea
    Submit Exercise
    Check Hint
    Continue Exercise
    Start Exercise Over

Do Exercise Select Words With Hint
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block
    Start Exercise Over
    Check Hint
    Submit Exercise
    Sleep    1
    Start Exercise Over
    ${words}    Create List    I     knowz     you're     a     zealot
    ${words_length}    Get Length    ${words}
    ${character_to_search}    Set Variable    z
    Wait Until Element Is Enabled    xpath=.//eb-cloze-id
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Start Exercise Over
    Sleep    1
    FOR    ${index}    IN RANGE    0    ${words_length}
        ${contains_selected_character}    Run Keyword And Return Status
        ...    Should Contain    ${words}[${index}]    ${character_to_search}
        Wait Until Element Is Enabled    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]
        ${style_before}    Get Element Attribute    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]/parent::span/parent::span    class
        Should Not Contain    ${style_before}    eb-selected
        Run Keyword If    '${contains_selected_character}'=='True'    Click Element On Page    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]
        Sleep    1
        Run Keyword If    '${contains_selected_character}'=='True'    Wait Until Element Is Enabled    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]
        ${style_after}    Get Element Attribute    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]/parent::span/parent::span    class
        Run Keyword If    '${contains_selected_character}'=='True'    Should Contain    ${style_after}    eb-selected
    END
    Submit Exercise
    Sleep    1
    Check Exercise Score    2
    FOR    ${index}    IN RANGE    0    ${words_length}
        ${index_list}        Evaluate    ${index}+1
        ${contains_selected_character}    Run Keyword And Return Status
        ...    Should Contain    ${words}[${index}]    ${character_to_search}
        Wait Until Page Contains Element    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]/ancestor::eb-cloze-id
        ${style_correct}    Get Element Attribute    xpath=.//eb-cloze-id//span[text()="${words}[${index}]"]/ancestor::eb-cloze-id    class
        Run Keyword If    '${contains_selected_character}'=='True'    Should Contain    ${style_correct}    eb-correct
        Wait Until Page Contains Element    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index_list}]
    END
    Start Exercise Over
    FOR    ${index}    IN RANGE    0    ${words_length}
        Wait Until Page Does Not Contain Element    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index_list}]
    END

Do Exercise Match Matrix With Hint
    Wait Until Element Is Enabled    xpath=.//eb-match-matrix
    Start Exercise Over
    Check Hint
    Submit Exercise
    Start Exercise Over
    #
    Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix
    @{options_to_select}    Create List    1    2    3    4
    @{options_right}    Create List    2    4
    @{options_wrong}    Create List    1    3
    @{answers_to_select}    Create List    even    odd
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Start Exercise Over
    ${options}    Get Element Count    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]
    ${answer_options}    Get Element Count    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]
    ${elements}    Set Variable    0
    FOR    ${choice_right}    IN    @{options_right}
        ${elements}    Evaluate    ${elements}+1
        Click Element On Page    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_right}"]
        Click Element On Page    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[0]"]
        Wait Until Element Is Enabled    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[starts-with(@class, "eb-connection-count") and normalize-space(text())=${elements}]
        Wait Until Element Is Enabled    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        ${elements_added_right}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[0]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        Should Be Equal As Integers    ${elements}    ${elements_added_right}
        ${elements_number_right}    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[0]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
        ${elements_number_right}    Strip String    ${elements_number_right}
        Should Be Equal As Integers    ${elements}    ${elements_number_right}
    END
    ${elements}    Set Variable    0
    FOR    ${choice_wrong}    IN    @{options_wrong}
        ${elements}    Evaluate    ${elements}+1
        Scroll Page To Top
        Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Scroll Element Into View    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Click Element On Page    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]//*[normalize-space(text())="${choice_wrong}"]
        Scroll Page To Bottom
        Scroll Element Into View    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[1]"]
        Click Element On Page    xpath=.//*[@class="eb-connection-answers"]//*[contains(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[1]"]
        ${elements_added_wrong}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[1]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
        Should Be Equal As Integers    ${elements}    ${elements_added_wrong}
        ${elements_number_wrong}    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[1]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
        ${elements_number_wrong}    Strip String    ${elements_number_wrong}
        Should Be Equal As Integers    ${elements}    ${elements_number_wrong}
    END
    Submit Exercise
    Check Exercise Score    4
    Start Exercise Over
    ${elements_added_right}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[0]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Should Be Equal As Integers    ${elements_added_right}    0
    Element Should Not Be Visible    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
    ${elements_added_wrong}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[1]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Should Be Equal As Integers    ${elements_added_wrong}    0
    Element Should Not Be Visible    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="${answers_to_select}[0]"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]

Do Exercise Cloze Combi Edit With Hint
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block
    Start Exercise Over
    Check Hint
    Submit Exercise
    Start Exercise Over
    #
    Wait Until Element Is Enabled    xpath=.//app-document//eb-cloze-edit
    Click Element On Page    xpath=.//*[@class="title-feedback"]
    Start Exercise Over
    ${correct_answers}    Create List    white
    ${answers_selected}    Create List
    ${choice_list}    Get Element Count    xpath=.//app-document//eb-cloze-edit/input
    Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[1]
    Input Text    xpath=(.//app-document//eb-cloze-edit/input)[1]    ${correct_answers[0]}
    #Close Content Item
    Return To Content Feed From Content Item View
    Sleep    2
    Open Content Item    Content testing    Exercises with Hint    Cloze combi edit with hint
    Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[1]
    ${answer_text}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit/input)[1]   value
    Should Be Equal As Strings    ${correct_answers[0]}    ${answer_text}
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index_answer}    Set Variable    ${index}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//app-document//eb-cloze-edit/input)[${index}]    ${correct_answers[${index_answer}]}
    END
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-document//eb-cloze-edit)[${index}]
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        ${status}    Run Keyword And Return Status    Should Contain    ${answer_style}    eb-correct
        Run Keyword If    '${status}'=='True'    Append To List    ${answers_selected}    right    ELSE    Append To List    ${answers_selected}    wrong
    END
    Submit Exercise
    Check Exercise Score    1
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        ${answer_style}    Get Element Attribute    xpath=(.//app-document//eb-cloze-edit)[${index}]    class
        Should Contain    ${answer_style}    eb-correct
        Page Should Contain Element    xpath=(.//eb-cloze-edit[contains(@class, "eb-readonly")])[${index}]
    END
    Start Exercise Over
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${index}        Evaluate    ${index}+1
        Wait Until Keyword Succeeds    5s    50ms
        ...    Page Should Not Contain Element    xpath=(.//eb-cloze-edit[contains(@class, "eb-readonly")])[${index}]
    END

Do Exercise Combi-content With Hints
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block
    Start Exercise Over
    Check Hints
    Submit Exercise
    Continue Exercise
    Start Exercise Over

Check Teacher View In Choice With Answer Level Feedback
    [Arguments]    ${return_to_page}=${False}
    ${answer_correct}    Set Variable    Stockholm
    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Wait Until Page Contains Element    xpath=.//eb-choice-list//li[string()="${answer_correct}"]//eb-radio-button//*[contains(@class, "eb-correct")]
    Run Keyword If    '${return_to_page}'!='${False}'
    ...    Page Should Not Contain Element    xpath=.//eb-choice-list//li[string()="${answer_correct}"]//eb-radio-button//*[contains(@class, "eb-correct")]
    ${radio_buttons}    Get Element Count    xpath=.//eb-radio-button
    FOR    ${index}    IN RANGE    0    ${radio_buttons}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Page Should Contain Element    xpath=(.//eb-radio-button[@disabled="true"])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element    xpath=(.//eb-radio-button[@disabled="true"])[${index}]
    END

Check Teacher View In UI: Multi Choice
    [Arguments]    ${return_to_page}=${False}
    @{correct_answers}    Create List    a whale    a dog    a cat
    @{incorrect_answers}    Create List    a perch    an ostrich    a worm
    ${checkboxes}    Get Element Count    xpath=.//eb-checkbox
    FOR    ${answer_correct}    IN    @{correct_answers}
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Page Contains Element    xpath=.//eb-choice-list//li[string()="${answer_correct}"]//eb-checkbox//*[contains(@class, "eb-correct")]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Page Should Not Contain Element    xpath=.//eb-choice-list//li[string()="${answer_correct}"]//eb-checkbox//*[contains(@class, "eb-correct")]
    END
    FOR    ${answer_incorrect}    IN    @{incorrect_answers}
        Page Should Not Contain Element    xpath=.//eb-choice-list//li[string()="${answer_incorrect}"]//eb-checkbox//*[contains(@class, "eb-correct")]
    END
    FOR    ${index}    IN RANGE    0    ${checkboxes}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Page Should Contain Element    xpath=(.//eb-checkbox[@disabled="true"])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element    xpath=(.//eb-checkbox[@disabled="true"])[${index}]
    END

Check Teacher View In UI: Match Single Response
    [Arguments]    ${return_to_page}=${False}
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]
    Wait Until Element Is Enabled    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]
    ${connection_elements}    Get Element Count    xpath=.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")]
    Sleep    1
    FOR    ${index}    IN RANGE     0       ${connection_elements}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]
        ${is_marked_correct}    Get Element Attribute
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Page Contains Element
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item") and contains(@class, "eb-correct") and contains(@class, "eb-disabled")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Wait Until Page Does Not Contain Element
        ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item") and contains(@class, "eb-correct") and contains(@class, "eb-disabled")])[${index}]
    END
    # \    ${is_marked_correct}    Get Element Attribute
    # ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-left"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
    # \    Run Keyword If    '${return_to_page}'=='${False}'    Should Contain    ${is_marked_correct}    eb-correct
    # \    Run Keyword If    '${return_to_page}'!='${False}'    Should Not Contain    ${is_marked_correct}    eb-correct
    # \    ${is_marked_correct}    Get Element Attribute
    # ...    xpath=(.//eb-match-single-response//eb-connection-blocks//*[@class="eb-connection-set-right"]//li[starts-with(@class, "eb-connection-item")])[${index}]    class
    # \    Run Keyword If    '${return_to_page}'=='${False}'    Should Contain    ${is_marked_correct}    eb-correct
    # \    Run Keyword If    '${return_to_page}'!='${False}'    Should Not Contain    ${is_marked_correct}    eb-correct

Check Teacher View In UI: Arrange Horisontal
    [Arguments]    ${return_to_page}=${False}
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${correct_answers}    Create List    one    Two    five    twelve    sixteen    twenty
    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    Sleep    2
    ${selection_texts_final}    Create List
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        ${selection_text}    Wait Until Keyword Succeeds    5s    1s    Get Text    xpath=(.//eb-ordering//eb-order-choice)[${index}]
        Append To List    ${selection_texts_final}    ${selection_text}
    END
    Run Keyword If    '${return_to_page}'=='${False}'    Lists Should Be Equal    ${selection_texts_final}    ${correct_answers}
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Page Should Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END

Check Teacher View In UI: Arrange Vertical
    [Arguments]    ${return_to_page}=${False}
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Enabled    xpath=.//eb-ordering//eb-order-choice
    ${correct_answers}    Create List    Jukolan talo    Sen läheisin    Peltojen alla    Muutoin on    Silloinpa Jukolan
    ${selections}    Get Element Count    xpath=.//eb-ordering//eb-order-choice
    FOR    ${index}    IN RANGE     0       ${selections}
        ${list_index}    Evaluate    ${index}+0
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Element Is Enabled    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]//*[starts-with(text(), "${correct_answers}[${list_index}]")]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Page Should Not Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]//*[starts-with(text(), "${correct_answers}[${list_index}]")]
    END
    FOR    ${index}    IN RANGE     0       ${selections}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Page Should Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element    xpath=(.//eb-ordering//eb-order-choice[contains(@class, "eb-readonly")])[${index}]
    END

Check Teacher View In UI: Drag Image
    [Arguments]    ${return_to_page}=${False}
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-cloze-image//eb-cloze-drag[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Enabled    xpath=.//eb-cloze-image//eb-cloze-drag
    ${correct_answers}    Create List    a sky    a lake    a sports field    houses
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Not Visible    xpath=.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")]
    ${draggables}    Get Element Count    xpath=.//eb-source-list//eb-source-list-item
    FOR    ${index}    IN RANGE     0       ${draggables}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Element Is Enabled    xpath=(.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Wait Until Element Is Not Visible    xpath=(.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")])[${index}]
    END
    ${drag_targets}    Get Element Count    xpath=.//eb-cloze-image//eb-cloze-drag
    FOR    ${index}    IN RANGE     0       ${drag_targets}
        ${list_index}    Evaluate    ${index}+0
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Element Is Enabled    xpath=(.//eb-cloze-image//eb-cloze-drag[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]//*[normalize-space(text())="${correct_answers}[${list_index}]"]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Wait Until Element Is Not Visible    xpath=(.//eb-cloze-image//eb-cloze-drag[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]//*[normalize-space(text())="${correct_answers}[${list_index}]"]
    END

Check Teacher View In UI: Cloze Combi Drop
    [Arguments]    ${return_to_page}=${False}
    Wait Until Element Is Enabled    xpath=.//eb-cloze-drop//eb-select
    ${correct_answers}    Create List    a    a    a    an    an
    ${answers_selected}    Create List
    ${drop_downs}    Get Element Count    xpath=.//eb-cloze-drop//eb-select
    FOR    ${index}    IN RANGE     0       ${drop_downs}
        ${list_index}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled
        ...    xpath=(.//eb-select[contains(@class, "eb-correct") and contains(@class, "eb-disabled")])[${index}]/*[normalize-space(string())="${correct_answers}[${list_index}]"]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element
        ...    xpath=(.//eb-select[contains(@class, "eb-correct") and contains(@class, "eb-disabled")])[${index}]/*[normalize-space(string())="${correct_answers}[${list_index}]"]
    END

Check Teacher View In UI: Cloze Combi Edit
    [Arguments]    ${return_to_page}=${False}
    Wait Until Page Contains Element    xpath=.//eb-cloze-edit/input
    ${correct_answers}    Create List    Hämeessä    niittu    erämaita    Jukolan
    ${choice_list}    Get Element Count    xpath=.//eb-cloze-edit/input
    FOR    ${index}    IN RANGE     0       ${choice_list}
        ${list_index}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Page Contains Element
        ...    xpath=(.//eb-cloze-edit[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element
        ...    xpath=(.//eb-cloze-edit[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]
    END

Check Teacher View In UI: Cloze Combi Drag
    [Arguments]    ${return_to_page}=${False}
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag[contains(@class, "eb-readonly") and contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Element Is Enabled    xpath=.//eb-cloze-drag
    ${correct_answers}    Create List    Hämeessä    Toukolan    Jukolan    naapurinsa    seitsemän
    Run Keyword If    '${return_to_page}'=='${False}'    Wait Until Element Is Enabled    xpath=.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'    Wait Until Page Does Not Contain Element    xpath=.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")]
    ${draggables}    Get Element Count    xpath=.//eb-source-list//eb-source-list-item
    FOR    ${index}    IN RANGE     0       ${draggables}
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Element Is Enabled    xpath=(.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Wait Until Page Does Not Contain Element    xpath=(.//eb-source-list//eb-source-list-item[contains(@class, "eb-readonly")])[${index}]
    END
    ${drag_targets}    Get Element Count    xpath=.//eb-cloze-drag
    FOR    ${index}    IN RANGE     0       ${drag_targets}
        ${list_index}    Evaluate    ${index}+0
        ${index}    Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'
        ...    Wait Until Page Contains Element    xpath=(.//eb-cloze-drag[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]//*[normalize-space(text())="${correct_answers}[${list_index}]"]
        Run Keyword If    '${return_to_page}'!='${False}'
        ...    Wait Until Page Does Not Contain Element    xpath=(.//eb-cloze-drag[contains(@class, "eb-correct") and contains(@class, "eb-readonly")])[${index}]//*[normalize-space(text())="${correct_answers}[${list_index}]"]
    END

Check Teacher View In UI: Select Words
    [Arguments]    ${return_to_page}=${False}
    Wait Until Element Is Enabled    xpath=.//eb-cloze-id
    ${words}    Get Element Count    xpath=.//eb-cloze-id
    FOR    ${index}    IN RANGE    0    ${words}
        ${index}        Evaluate    ${index}+1
        Run Keyword If    '${return_to_page}'=='${False}'    Page Should Contain Element    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index}]
        Run Keyword If    '${return_to_page}'!='${False}'    Page Should Not Contain Element    xpath=(.//eb-cloze-id[contains(@class, "eb-readonly")])[${index}]
    END
    Run Keyword If    '${return_to_page}'=='${False}'    Words Are Selected
    Run Keyword If    '${return_to_page}'!='${False}'    Words Are Not Selected

Words Are Selected
    Wait Until Page Contains Element    xpath=(.//span[text()="Jukolan"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Wait Until Page Contains Element    xpath=(.//span[text()="Jukolan"])[2]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Wait Until Page Contains Element    xpath=(.//span[text()="Hämeessä"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Wait Until Page Contains Element    xpath=(.//span[text()="Toukolan"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]

Words Are Not Selected
    Page Should Not Contain Element    xpath=(.//span[text()="Jukolan"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Page Should Not Contain Element    xpath=(.//span[text()="Jukolan"])[2]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Page Should Not Contain Element    xpath=(.//span[text()="Hämeessä"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]
    Page Should Not Contain Element    xpath=(.//span[text()="Toukolan"])[1]/ancestor::eb-cloze-id[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]

Check Teacher View In UI: Match Matrix
    [Arguments]    ${return_to_page}=${False}
    Wait Until Element Is Enabled    xpath=.//eb-match-matrix//eb-connection-match-matrix
    @{options_to_select}    Create List    Finland belongs to EU    France is a continent    Climate change is a fact    Carrots are blue    Kampus is the best service ever    Autumn is wonderful
    @{options_right}    Create List    Finland belongs to EU    Climate change is a fact    Kampus is the best service ever    Autumn is wonderful
    ${options_right_length}    Get Length    ${options_right}
    @{options_wrong}    Create List    France is a continent    Carrots are blue
    ${options_wrong_length}    Get Length    ${options_wrong}
    @{answers_to_select}    Create List    Right    Wrong
    Sleep    5
    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Wait Until Page Contains Element    xpath=.//eb-match-matrix//eb-connection-match-matrix[contains(@class, "eb-readonly")]
    Run Keyword If    '${return_to_page}'!='${False}'
    ...    Page Should Not Contain Element    xpath=.//eb-match-matrix//eb-connection-match-matrix[contains(@class, "eb-readonly")]
    ${options}    Get Element Count    xpath=.//eb-match-matrix//eb-connection-match-matrix//*[@class="eb-connection-selection-list"]//*[starts-with(@class, "eb-connection-option")]
    Run Keyword If    '${return_to_page}'=='${False}'    Should Be Equal As Integers    ${options}    0
    ${elements_added_right}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Run Keyword If    '${return_to_page}'=='${False}'    Should Be Equal As Integers    ${elements_added_right}    ${options_right_length}
    Run Keyword If    '${return_to_page}'!='${False}'    Should Be Equal As Integers    ${elements_added_right}    0
    ${elements_number_right}    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Right"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
    ...    ELSE
    ...    Set Variable    0
    ${elements_number_right}    Strip String    ${elements_number_right}
    Run Keyword If    '${return_to_page}'=='${False}'    Should Be Equal As Integers    ${elements_number_right}    ${options_right_length}
    Run Keyword If    '${return_to_page}'!='${False}'    Should Be Equal As Integers    ${elements_number_right}    0
    ${elements_added_wrong}    Get Element Count    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-drop-zone")]/*[starts-with(@class, "eb-connection-option")]
    Run Keyword If    '${return_to_page}'=='${False}'    Should Be Equal As Integers    ${elements_added_wrong}    ${options_wrong_length}
    Run Keyword If    '${return_to_page}'!='${False}'    Should Be Equal As Integers    ${elements_added_wrong}    0
    ${elements_number_wrong}    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Get Text    xpath=.//*[@class="eb-connection-answers"]//*[starts-with(@class, "eb-connection-answer")]//*[normalize-space(text())="Wrong"]/ancestor::*[starts-with(@class, "eb-connection-answer")]/*[starts-with(@class, "eb-connection-count")]
    ...    ELSE
    ...    Set Variable    0
    ${elements_number_wrong}    Strip String    ${elements_number_wrong}
    Run Keyword If    '${return_to_page}'=='${False}'    Should Be Equal As Integers    ${elements_number_wrong}    ${options_wrong_length}
    Run Keyword If    '${return_to_page}'!='${False}'    Should Be Equal As Integers    ${elements_number_wrong}    0

Check Teacher View In UI: Open Question Short Response - Explanation of Solution
    [Arguments]    ${return_to_page}=${False}
    Wait Until Page Contains Element    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea
    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Wait Until Page Contains Element
    ...    xpath=.//eb-open-question-short-response//eb-cloze-edit[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]//textarea
    Run Keyword If    '${return_to_page}'!='${False}'
    ...    Page Should Not Contain Element
    ...    xpath=.//eb-open-question-short-response//eb-cloze-edit[contains(@class, "eb-correct") and contains(@class, "eb-readonly")]//textarea
    Sleep    2
    ${readonly}    Get Element Attribute    xpath=.//eb-open-question-short-response//eb-cloze-edit//textarea    readonly
    Run Keyword If    '${return_to_page}'=='${False}'
    ...    Should Be Equal As Strings    ${readonly}    true
    Run Keyword If    '${return_to_page}'!='${False}'
    ...    Should Be Equal As Strings    ${readonly}    None

Do Exercise UI: Correct Text
    Wait Until Element Is Enabled    xpath=.//eb-correcting-text//eb-word
    Start Exercise Over
    Answer Correct Text Exercise First Row Correctly
    Submit Exercise
    Check Exercise Score    2
    Verify Correct Text Answers Are Marked Correctly
    ${words_correct}    ${words_incorrect}    Get Correct Text Exercise Statuses
    Should Be Equal As Strings    ${words_correct}    2
    Should Be Equal As Strings    ${words_incorrect}    0
    Start Exercise Over
    Answer Correct Text Exercise First Row Correctly
    Press Keys    xpath=.//eb-correcting-text//eb-paragraph-set[2]//eb-word[string()="practically"]    SPACE
    Submit Exercise
    Check Exercise Score    2
    Verify Correct Text Answers Are Marked Correctly
    ${words_correct}    ${words_incorrect}    Get Correct Text Exercise Statuses
    Should Be Equal As Strings    ${words_correct}    2
    Should Be Equal As Strings    ${words_incorrect}    1

Answer Correct Text Exercise First Row Correctly
    Wait Until Element Is Enabled    xpath=.//eb-correcting-text//eb-word
    Sleep    5
    Press Keys    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="se"]    BACKSPACE
    Sleep    5
    Press Keys    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="e"]    ARROW_LEFT+S
    Sleep    5
    Press Keys    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="t"]    ARROW_RIGHT
    Sleep    1
    Press Keys    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="t"]    BACKSPACE
    Sleep    5

Verify Correct Text Answers Are Marked Correctly
    Wait Until Element Is Enabled    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="Se" and contains(@class, "eb-correct")]
    Wait Until Element Is Enabled    xpath=.//eb-correcting-text//eb-paragraph-set[1]//eb-word[string()="it" and contains(@class, "eb-correct")]

Get Correct Text Exercise Statuses
    Wait Until Element Is Enabled    xpath=.//eb-correcting-text//eb-paragraph-set//eb-word
    ${words_correct}    Get Element Count    xpath=.//eb-correcting-text//eb-paragraph-set//eb-word[not(string()="") and contains(@class, "eb-correct")]
    ${words_incorrect}    Get Element Count    xpath=.//eb-correcting-text//eb-paragraph-set//eb-word[not(string()="") and contains(@class, "eb-incorrect")]
    [Return]    ${words_correct}    ${words_incorrect}


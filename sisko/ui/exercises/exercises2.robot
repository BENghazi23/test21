*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Suite Setup       Create Feed For Exercises     ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}
Test Teardown     Custom Teardown

*** Test Cases ***
Choice - Multiple Reflinks Added
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item        Content testing    Exercises / interactions    Choice - multiple Reflinks added
    Do Exercise Choice - Multiple Reflinks Added

Match - Cloze Combi - Gap Match
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    Match - Cloze Combi - gap match
    Do Exercise Match - Cloze Combi - Gap Match

File As Answer
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    File as answer
    Do Exercise File As Answer

File Upload In Combi Content
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    File upload in combi-content
    Do Exercise File Upload In Combi Content

ABITTI: TT 11_9 In FyKe Fysiikka
    [Tags]    regression    exercise
    Check Normal Abitti Functionality

ABITTI: TT 11_6 In FyKe Fysiikka (combi-content)
    [Tags]    quarantine    exercise
    Check Combi-content Abitti Functionality

# Real life content
ABITTI: TT 21_7 (OQL)
    [Tags]    regression    exercise
    Check Abitti Functionality

# Exercises with hints
Choice With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Choice with hint ( 3 attempts dis-2375)
    Do Exercise Choice With Hint

Arrange With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Arrange with hint ( 2 attempts dis-2375)
    Do Exercise Arrange With Hint

Match Single With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Match Single with hint
    Do Exercise Match Single With Hint

Cloze Combi With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Cloze combi with hint
    Do Exercise Cloze Combi With Hint

OQLR With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    OQLR with hint
    Do Exercise OQLR With Hint

OQSR With Hint
    [Tags]    regression_WAITING_EDUBASE_IMPLEMENTATION    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    OQSR with hint
    Do Exercise OQSR With Hint

Select Words With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Select words with hint
    Do Exercise Select Words With Hint

Match Matrix With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Match matrix with hint
    Do Exercise Match Matrix With Hint

Cloze Combi Edit With Hint
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Cloze combi edit with hint
    Do Exercise Cloze Combi Edit With Hint

Combi-content With Hints
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_EXERCISES2}    ${USER_PASSWORD_STUDENT_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises with Hint    Combi-content with hints
    Do Exercise Combi-content With Hints

###
Teacher View Functionality - Choice With Answer Level Feedback
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Choice exercises    Choice with answer level feedback
    Turn Teacher View On
    Check Teacher View In Choice With Answer Level Feedback
    Go To Next Page
    Go To Previous Page
    Check Teacher View In Choice With Answer Level Feedback    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Multi Choice
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Choice exercises    UI: Multi choice
    Turn Teacher View On
    Check Teacher View In UI: Multi Choice
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Multi Choice    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Match Single Response
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Match Single response
    Turn Teacher View On
    Check Teacher View In UI: Match Single Response
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Match Single Response    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Arrange Horisontal
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Arrange horisontal
    Turn Teacher View On
    Check Teacher View In UI: Arrange Horisontal
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Arrange Horisontal    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Arrange Vertical
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Arrange vertical
    Turn Teacher View On
    Check Teacher View In UI: Arrange Vertical
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Arrange Vertical    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Drag Image
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Drag image
    Turn Teacher View On
    Check Teacher View In UI: Drag image
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Drag image    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Cloze Combi Drop
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi drop
    Turn Teacher View On
    Check Teacher View In UI: Cloze Combi Drop
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Cloze Combi Drop    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Cloze Combi Edit
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi edit
    Turn Teacher View On
    Check Teacher View In UI: Cloze Combi Edit
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Cloze Combi Edit    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Cloze Combi Drag
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Cloze combi drag
    Turn Teacher View On
    Check Teacher View In UI: Cloze Combi Drag
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Cloze Combi Drag    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Select Words
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Select words
    Turn Teacher View On
    Check Teacher View In UI: Select Words
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Select Words    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Match Matrix
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Match Matrix
    Turn Teacher View On
    Check Teacher View In UI: Match Matrix
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Match Matrix    return
    Turn Teacher View On
    Turn Teacher View Off

Teacher View Functionality - UI: Open Question Short Response - Explanation of Solution
    [Tags]    regression    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    Exercises / interactions    UI: Open Question Short Response - Explanation of Solution
    Turn Teacher View On
    Check Teacher View In UI: Open Question Short Response - Explanation of Solution
    Go To Next Page
    Go To Previous Page
    Check Teacher View In UI: Open Question Short Response - Explanation of Solution    return
    Turn Teacher View On
    Turn Teacher View Off

Check Correcting Text
    [Tags]    quarantine    exercise
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_EXERCISES2}    ${USER_PASSWORD_TEACHER_1_EXERCISES2}    ${METHOD_FOR_EXERCISES}    ${ACCESS_KEY_FOR_EXERCISES}
    Open Content Item    Content testing    OQSR and OQLR    UI: Correct text
    Do Exercise UI: Correct Text

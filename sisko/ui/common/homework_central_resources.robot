*** Keywords ***
Create Feed For Homework
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_HOMEWORK}    ${USER_PASSWORD_TEACHER_1_HOMEWORK}
    ${status}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//div[contains(@class, "feed-title")]/*[normalize-space(text())="${DEFAULT_METHOD_FOR_HOMEWORK}"]
    ${teacher_feed}    Run Keyword If    '${status}'=='False'
    ...    Add Teacher Feed    ${DEFAULT_METHOD}    off    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_HOMEWORK}    scroll_off
    ...    ELSE
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_HOMEWORK}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_HOMEWORK}    ${access_key}
    Close Access Key Dialog Window
    Logout Sisko

Open Homework Central Tab Content
    [Arguments]    ${tab_content}
    ${accordion_expansion_indicator}    Get Element Attribute    xpath=.//*[@id="${tab_content}"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="${tab_content}"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${tab_content}"]
    ${accordion_expansion_indicator}    Get Element Attribute    xpath=.//*[@id="${tab_content}"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Contain    ${accordion_expansion_indicator}    opened
    Should Be True    '${status}'=='PASS'
    Wait Until Element Is Enabled    xpath=.//*[@id="create-homework-reset-button"]

Add Exercises To Homework
    [Arguments]    @{arguments}
    Scroll Page To Top
    Check Not Possible To Add Teacher Material To Homework
    ${args_length}    Get Length    ${arguments}
    ${elements_all}    Get Element Count   xpath=.//mat-checkbox[starts-with(@id, "mat-checkbox-")]
    ${elements_to_add}    Run Keyword If    ${args_length}==0    Set Variable    ${elements_all}    ELSE    Set Variable    ${args_length}
    Page Should Contain Element    xpath=.//*[@id="create-homework-submit-button" and @disabled]
    FOR    ${index}    IN RANGE     0       ${elements_to_add}
        ${index_list}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Run Keyword If    ${args_length}==0    Click Element On Page    xpath=.//*[@id="mat-checkbox-${index}"]
        Run Keyword If    ${args_length}!=0    Click Element On Page    xpath=.//*[@id="mat-checkbox-${arguments[${index_list}]}"]
        ${exercises_added}    Check Added Exercises    ${index}
        # HERE ADD CHECK FOR ADDED EXERCISE TEXT
        Check Counter Text    ${exercises_added}
        Run Keyword If    ${args_length}==0    Check Correct Exercise Is Added    ${index}
        Run Keyword If    ${args_length}!=0    Check Correct Exercise Is Added    ${arguments[${index_list}]}
        Page Should Not Contain Element    xpath=.//*[@id="create-homework-submit-button" and @disabled]
    END

Check Added Exercises
    [Arguments]    ${number_of_exercises}
    ${exercises}    Get Element Count   xpath=.//*[@id="create-homework-form"]/*[@id="exercise-list"]//div[starts-with(@class, "exercise-item")]
    Should Be Equal As Integers    ${number_of_exercises}    ${exercises}
    [Return]    ${exercises}

Check Counter Text
    [Arguments]    ${number_of_added_exercises}
    Wait Until Element Is Enabled    xpath=.//*[@id="create-homework-form"]/label[1]
    ${element_counter_text}    Get Text    xpath=.//*[@id="create-homework-form"]/label[1]
    Should Contain    ${element_counter_text}    (${number_of_added_exercises})

Check Not Possible To Add Teacher Material To Homework
    ${teacher_content_items_all}    Get Element Count    xpath=.//mat-list-item[starts-with(@content-type, "teacher")]//mat-checkbox
    Should Be Equal As Integers    ${teacher_content_items_all}    0

Check Correct Exercise Is Added
    [Arguments]    ${exercise_added}
    ${exercise_text_in_feed}    Get Text    xpath=.//*[@id="mat-checkbox-${exercise_added}"]/following-sibling::*[@class="mat-list-text"]/*[contains(@class, "title mat-line")]
    ${exercise_text_in_feed}    Strip String    ${exercise_text_in_feed}
    Page Should Contain Element    xpath=.//*[@id="create-homework-form"]/*[@id="exercise-list"]//div[starts-with(@class, "exercise-item")]/*[@class="exercise-title" and starts-with(normalize-space(text()), "${exercise_text_in_feed}")]

Remove Added Exercises From Homework
    Scroll Page To Top
    ${exercises_added}    Get Element Count   xpath=.//*[@id="create-homework-form"]/*[@id="exercise-list"]//div[starts-with(@class, "exercise-item")]
    FOR    ${index}    IN RANGE     ${exercises_added}    0    -1
        ${exercises_added_before}    Check Added Exercises    ${index}
        Check Counter Text    ${exercises_added_before}
        ${exercise_to_remove}    Get Text    xpath=(.//*[@id="create-homework-form"]/*[@id="exercise-list"]//div[starts-with(@class, "exercise-item")])[1]/*[@class="exercise-title"]
        Wait Until Element Is Enabled    xpath=.//*[starts-with(@id,"mat-checkbox-")]/following-sibling::*[@class="mat-list-text"]/*[contains(@class, "title mat-line") and normalize-space(text())= "${exercise_to_remove}"]/parent::div[@class="mat-list-text"]/preceding-sibling::mat-checkbox[contains(@class, "mat-checkbox-checked")]
        Click Element On Page    xpath=(.//*[@id="create-homework-form"]/*[@id="exercise-list"]//div[starts-with(@class, "exercise-item")])[1]//div[@class="exercise-close"]
        ${index_after}    Evaluate    ${index}-1
        ${exercises_added_after}    Check Added Exercises    ${index_after}
        Check Counter Text    ${exercises_added_after}
        Wait Until Keyword Succeeds    10    1    Wait Until Page Does Not Contain Element    xpath=.//*[starts-with(@id,"mat-checkbox-")]/following-sibling::*[@class="mat-list-text"]/*[@class="title mat-line" and normalize-space(text())="${exercise_to_remove}"]/parent::div[@class="mat-list-text"]/preceding-sibling::mat-checkbox[contains(@class, "mat-checkbox-checked")]
    END

Remove Latest Added Exercise
    Scroll Page To Top
    ${accordion_expansion_indicator}    Get Element Attribute    xpath=.//*[@id="current-homework-accordion"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="current-homework-accordion"]
    Wait Until Element Is Enabled    xpath=.//app-current-homework-list/app-homework-item
    ${exercises_before}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
    Click Element On Page    xpath=.//app-current-homework-list/app-homework-item[1]/button
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]/button[@role="menuitem"]
    Click Element On Page    xpath=.//app-homework-remove-confirm-dialog//button
    Wait Until Element Is Enabled    xpath=.//app-current-homework-list
    ${exercises_after}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
    ${exercises_removed}    Evaluate    ${exercises_before}-${exercises_after}
    Should Be Equal As Numbers    ${exercises_removed}    1

Remove All Added Exercises
    Scroll Page To Top
    ${accordion_expansion_indicator}    Get Element Attribute    xpath=.//*[@id="current-homework-accordion"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="current-homework-accordion"]
    ${exercises_in_total}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
    FOR    ${index}    IN RANGE     0    ${exercises_in_total}
        ${index}    Evaluate    ${index}+1
        ${exercises_before}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
        Click Element On Page    xpath=(.//app-current-homework-list/app-homework-item)[1]/button
        Click Element On Page    xpath=.//*[@class="mat-menu-content"]/button[@role="menuitem"]
        Click Element On Page    xpath=.//app-homework-remove-confirm-dialog//button
        Sleep    1
        Run Keyword If    ${exercises_in_total} > 1    Wait Until Element Is Enabled    xpath=.//app-current-homework-list/app-homework-item
        ${exercises_after}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
        ${exercises_removed}    Evaluate    ${exercises_before}-${exercises_after}
        Should Be Equal As Numbers    ${exercises_removed}    1
    END
    ${exercises_in_end}    Get Element Count    xpath=.//app-current-homework-list/app-homework-item
    Should Be Equal As Numbers    ${exercises_in_end}    0

Fill In Exercise Instructions
    [Arguments]    ${instructions_text}=some default text
    ${instructions_text_length}    Get Length    ${instructions_text}
    @{instructions_text_characters}    Split String To Characters    ${instructions_text}
    Wait Until Element Is Enabled    xpath=.//*[@id="create-homework-instruction"]
    FOR    ${index}    IN RANGE     0    ${instructions_text_length}
        Press Keys    xpath=.//*[@id="create-homework-instruction"]    ${instructions_text_characters[${index}]}
    END
    ${filled_text_value}    Get Value    xpath=.//*[@id="create-homework-instruction"]
    ${filled_text_value_length}    Get Length    ${filled_text_value}
    Should Be Equal As Integers    ${filled_text_value_length}    ${instructions_text_length}

Fill In Return Date For Exercise
    [Arguments]    ${year}=2019    ${month}=1    ${day}=1
    Click Element On Page    xpath=.//*[@id="create-homework-form"]//mat-datepicker-toggle/button
    Click Element On Page    xpath=.//mat-calendar//*[@class="mat-calendar-controls"]//button[contains(@class, "mat-calendar-period-button")]
    Wait Until Element Is Enabled    xpath=.//mat-multi-year-view//div[contains(@class,"mat-calendar-body-cell-content")]
    Click Element On Page    xpath=.//table[@class="mat-calendar-table"]//div[contains(@class,"mat-calendar-body-cell-content") and text()="${year}"]
    Click Element On Page    xpath=(.//table[@class="mat-calendar-table"]//div[contains(@class, "mat-calendar-body-cell-content")])[${month}]
    Click Element On Page    xpath=(.//table[@class="mat-calendar-table"]//div[contains(@class, "mat-calendar-body-cell-content")])[${day}]
    Wait Until Element Is Enabled    xpath=.//*[@id="create-homework-deadline"]
    ${selected_date}    Get Value    xpath=.//*[@id="create-homework-deadline"]
    Should Contain    ${selected_date}    ${year}
    Should Contain    ${selected_date}    ${month}
    Should Contain    ${selected_date}    ${day}

Fill In Assignees For Exercise
    Wait Until Element Is Enabled    xpath=.//*[@id="create-homework-assignees"]

Submit Homework
    Click Element On Page    xpath=.//*[@id="create-homework-submit-button"]
    Wait Until Page Contains Element    xpath=.//*[@id="create-homework-submit-button" and @disabled]

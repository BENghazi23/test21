*** Keywords ***
Go To Feed Of Theoryelements
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_THEORY}    ${USER_PASSWORD_TEACHER_1_THEORY}
    Switch Schools In Kampus    ${DEFAULT_SCHOOL}
    Select Feed    ${DEFAULT_METHOD_FOR_THEORY}    ${FEED_TYPE_PUBLISHER}

Create Feed For Theoryelements
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_THEORY}    ${USER_PASSWORD_TEACHER_1_THEORY}
    Switch Schools In Kampus    ${DEFAULT_SCHOOL}
    ${name}     Add Teacher Feed      ${DEFAULT_METHOD_FOR_THEORY}    on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_THEORY}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_THEORY}    ${access_key}
    Set Suite Variable    ${NAME_FOR_THEORY}    ${name}
    Close Browser

Create Feed For LTI links
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_THEORY}    ${USER_PASSWORD_TEACHER_1_THEORY}
    ${name}     Add Teacher Feed      ${DEFAULT_METHOD_FOR_THEORY}    on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_THEORY}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_THEORY}    ${access_key}
    Set Suite Variable    ${NAME_FOR_THEORY}    ${name}
    Close Access Key Dialog Window

Check Reflink Single
    Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
    ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
    Should Be Equal As Integers    ${split_areas}    1
    Click Element On Page    xpath=.//*[@id="referral-relations-button" and not(contains(@class, "default-toolbar-extraMaterial-button"))]
    Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
    ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
    Should Be Equal As Integers    ${split_areas}    2
    Element Should Be Visible   xpath = .//*[@class="extraMaterial-wrapper"]
    ${innerHTML}    Get Element Attribute    xpath=(.//as-split/as-split-area)[2]    innerHTML
    ${innerHTML_length}    Get Length    ${innerHTML}
    Should Be True    ${innerHTML_length}>0
    Click Element On Page    xpath=.//*[@id="referral-relations-button" and not(contains(@class, "default-toolbar-extraMaterial-button"))]
    Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
    ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
    Element Should Not Be Visible   xpath = .//*[@class="extraMaterial-wrapper"]
    Should Be Equal As Integers    ${split_areas}    1

Check Single Reflink
    [Arguments]     ${type}
    Element Should Not Be Visible   xpath = .//*[@class="extraMaterial-wrapper"]
    Click Element On Page    xpath=.//*[@id="referral-relations-button" and not(contains(@class, "default-toolbar-extraMaterial-button"))]
    Element Should Be Visible   xpath = .//*[@class="extraMaterial-wrapper"]
    Run Keyword If      ${type} = "single2single"     Element Should Contain       xpath = .//*[@class="extraMaterial-wrapper"]      "ref link single2combi"
    Run Keyword If      ${type} = "combi2single"     Element Should Contain       xpath = .//*[@class="extraMaterial-wrapper"]      "ref link single2combi"

Open Link
    [Arguments]     ${element}
    Wait Until Element Is Visible       ${element}
    Click Link      ${element}

Check Page Contains X Defined Elements
    [Arguments]     ${number}   ${element}
    Wait Until Element Is Visible    xpath=.//*[@class="splash-${element}"]
    Page Should Contain Element    xpath=.//*[@class="splash-${element}"]     limit=${number}

Check Page Contains X Extra Elements
    [Arguments]     ${number}     ${opened_or_closed}
    Wait Until Element Is Visible    xpath=.//*[@class="splash-extra ${opened_or_closed}"]
    Page Should Contain Element    xpath=.//*[@class="splash-extra ${opened_or_closed}"]     limit=${number}

Open Image For Larger View And Close View
    Wait Until Element Is Visible        xpath=.//eb-picture//figure//*[@class="eb-image-wrapper eb-zoom"]
    Element Should Not Be Visible       xpath=.//eb-dialog
    Click Element On Page    xpath=.//eb-picture//figure//span//eb-icon
    Wait Until Element Is Visible    xpath=.//eb-dialog
    Wait Until Element Is Visible    xpath=.//eb-dialog/img
    Click Element On Page    xpath=.//eb-dialog//a//eb-icon
    Element Should Not Be Visible    xpath=.//eb-dialog

Close Content And Go Back To Feed
    Wait Until Element Is Visible       xpath=.//mat-sidenav-container//mat-sidenav-content//*[@id="close-button"]
    Click Element On Page      xpath=.//mat-sidenav-container//mat-sidenav-content//*[@id="close-button"]

Check Texttype
    [Arguments]      ${type}
    Wait Until Element Is Visible    xpath=.//*[contains(@class, "eb-${type}")]
    [Return]    xpath=.//*[contains(@class, "eb-${type}")]

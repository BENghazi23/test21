*** Keywords ***
Create Feeds For Licenses
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Wordpress Site    ${USER_NAME_TEACHER_LICENSE_1}    ${USER_PASSWORD_TEACHER_LICENSE_1}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open User Materials In School
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select School From Portal    ${DEFAULT_SCHOOL}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Add Feed For FyKe - Geoidi - On the Go
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Add Feed For Verso

Add Feed For FyKe - Geoidi - On the Go
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD_FOR_LICENSES}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_LICENSES}    ${access_key}
    Set Suite Variable    ${NAME_FOR_LICENSES}    ${name}
    Close Access Key Dialog Window
    Go To My Page
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD_FOR_LICENSES_GEOIDI}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES_GEOIDI}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_LICENSES_GEOIDI}    ${access_key}
    Set Suite Variable    ${NAME_FOR_LICENSES_GEOIDI}    ${name}
    Close Access Key Dialog Window
    Go To My Page
    ${name}     Add Teacher Feed    ${DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO}    on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_LICENSES_ON_THE_GO}    ${access_key}
    Set Suite Variable    ${NAME_FOR_LICENSES_ON_THE_GO}    ${name}
    Close Browser

Add Feed For Verso
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_LICENSE_3}    ${USER_PASSWORD_TEACHER_LICENSE_3}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL_LICENSE}
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD_FOR_LICENSES_VERSO}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES_VERSO}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_LICENSES_VERSO}    ${access_key}
    Set Suite Variable    ${NAME_FOR_LICENSES_VERSO}    ${name}
    Close Browser

*** Keywords ***
Create Feed For Feeds Old
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_FEEDS}    ${USER_PASSWORD_TEACHER_1_FEEDS}
    ${status}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//div[contains(@class, "feed-title")]/*[normalize-space(text())="${DEFAULT_METHOD_FOR_FEEDS}"]
    ${teacher_feed}    Run Keyword If    '${status}'=='False'
    ...    Add Teacher Feed    ${DEFAULT_METHOD}    off    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_FEEDS}    scroll_off
    ...    ELSE
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_FEEDS}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_FEEDS}    ${access_key}
    Close Browser

Create Feed For Feeds
    [Arguments]  ${username}    ${password}
    Login Kampus    ${username}    ${password}
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_FEEDS}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_FEEDS}    ${access_key}
    Set Suite Variable    ${NAME_FOR_FEED}    ${name}
    Close Access Key Dialog Window
    Go To My Page
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD_FOR_LICENSES}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_FEEDS_MODIFICATION}    ${access_key}
    Set Suite Variable    ${NAME_FOR_FEED_MODIFICATION}    ${name}
    Close Browser

Create Feed For Feed Renaming
    [Arguments]  ${username}    ${password}
    Login Kampus    ${username}    ${password}
    ${name}     Add Teacher Feed     ${DEFAULT_METHOD_FOR_LICENSES}     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_LICENSES}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_FEED_RENAME}    ${access_key}
    Set Suite Variable    ${NAME_FOR_FEED_RENAME}    ${name}

Get Teacher Feed Names
    Wait Until Element Is Enabled    xpath=.//app-method-feeds
    @{teacher_feed_names}    Create List
    ${teacher_feeds}    Get Element Count    xpath=.//app-method-feeds//*[@feed-type="teacher"]
    FOR    ${index}    IN RANGE     0       ${teacher_feeds}
        ${index}        Evaluate    ${index}+1
        ${teacher_feed_text}    Get Text    xpath=(.//app-method-feeds//*[@feed-type="teacher"]//*[@class="teacher-feed-title"])[${index}]
        Append To List    ${teacher_feed_names}    ${teacher_feed_text}
    END
    [Return]    ${teacher_feed_names}

Delete Teacher Feeds
    [Arguments]    ${teacher_feeds}    ${deletion_message}
    FOR    ${teacher_feed_name}    IN    @{teacher_feeds}
        Delete Teacher Feed    ${teacher_feed_name}    ${deletion_message}
    END

Delete Teacher Feed
    [Arguments]    ${teacher_feed_name}    ${deletion_message}
    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Scroll Element Into View
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    ${feeds_in_begining}    Get Element Count
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Wait Until Element Is Enabled    xpath=.//*[@class="mat-menu-content"]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "delete-feed-button")]
    Wait Until Element Is Enabled    xpath=.//app-delete-teacher-feed-dialog
    Wait Until Element Is Enabled    xpath=.//app-delete-teacher-feed-dialog//*[@class="feed-name" and normalize-space(text())="${teacher_feed_name}"]
    Wait Until Element Is Enabled    xpath=.//*[@id="delete-feed-cancel-button"]
    Wait Until Keyword Succeeds    10s    1s    Click Element On Page    xpath=.//*[@id="delete-feed-cancel-button"]
    Sleep    1
    Wait For Condition  return window.document.readyState === 'complete'
    Wait Until Keyword Succeeds    10s    1s    Element Should Not Be Visible    xpath=.//app-delete-teacher-feed-dialog
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//app-method-feeds
    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Scroll Element Into View
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Wait For Condition  return window.document.readyState === 'complete'
    Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=.//*[@class="mat-menu-content"]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "delete-feed-button")]
    Wait Until Element Is Enabled    xpath=.//app-delete-teacher-feed-dialog
    Wait Until Element Is Enabled    xpath=.//*[@id="delete-confirm-input"]
    Enter Confirmation Text For Deletion    ${teacher_feed_name}    ${deletion_message}
    Wait Until Keyword Succeeds    20s    1s    Verify Feed Is Deleted    ${teacher_feed_name}    ${feeds_in_begining}

Deleting Teacher Feed Not Possible
    [Arguments]    ${teacher_feed_name}
    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Scroll Element Into View
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Wait Until Element Is Enabled    xpath=.//*[@class="mat-menu-content"]
    Page Should Not Contain Element    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "delete-feed-button")]

Verify Feed Is Deleted
    [Arguments]    ${teacher_feed_name}    ${feeds_in_begining}
    ${feeds_in_end}    Get Element Count
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Should Be True    ${feeds_in_begining} > ${feeds_in_end}
    ###
    # ID:s of the feeds could be checked for the delation verification
    ###

Verify Teacher Feed Not Shown
    [Arguments]    ${teacher_feed_name}
    Element Should Not Be Visible
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//*[@class="teacher-feed-title" and normalize-space(text())="${teacher_feed_name}"]

Get Confirmation Message For Deletion
    ${language_selected}    Get Selected Language
    ${delete_confirmation_message}    Set Variable If    '${language_selected}'=='English'    ${FEED_DELETION_CONFIRM_TXT_EN}
    ...    '${language_selected}'=='Suomi'    ${FEED_DELETION_CONFIRM_TXT_FI}
    ...    '${language_selected}'=='Svenska'    ${FEED_DELETION_CONFIRM_TXT_SV}
    [Return]    ${delete_confirmation_message}

Enter Confirmation Text For Deletion
    [Arguments]    ${teacher_feed_name}    ${deletion_message}
    Wait Until Page Contains Element    xpath=.//app-delete-teacher-feed-dialog//*[@class="feed-name" and text()="${teacher_feed_name}"]
    Element Should Be Disabled    id:delete-teacher-feed-button
    Verify Element is Visible    id:delete-confirm-input
    Focus And Click Element    id:delete-confirm-input
    Input Text    id:delete-confirm-input    ${deletion_message}
    # Press Keys    id:delete-confirm-input    TAB
    Click Element    xpath=//div[@class="dialog-title"]    # Click on dialog title to move focus out of textbox and allow for any js execution
    ${entered_text}=    Get Value    id:delete-confirm-input
    Should Be Equal As Strings    ${deletion_message}    ${entered_text}
    Wait Until Page Contains Element    xpath=.//*[@id="delete-teacher-feed-button"]

    #@{characters}    Split String to Characters    ${deletion_message}
    # ${answer_length}    Get Length    ${characters}
    # ${index}    Set Variable    ${0}
    # :FOR    ${character}    IN    @{characters}
    # \    Press Keys    xpath=.//*[@id="delete-confirm-input"]    ${character}
    # \    ${index}    Evaluate    ${index}+1
    # \    Run Keyword If    ${index} < ${answer_length}    Element Should Be Disabled    xpath=.//*[@id="delete-teacher-feed-button"]
    # \    Run Keyword If    ${index}==${answer_length}    Element Should Be Enabled    xpath=.//*[@id="delete-teacher-feed-button"]
    Click Element On Page    xpath=.//*[@id="delete-teacher-feed-button"]
    Wait For Condition  return window.document.readyState === 'complete'
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    xpath=.//app-delete-teacher-feed-dialog
    Wait Until Element Is Enabled    xpath=.//app-method-feeds

Turn Access Key On
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
    ${access_key_style}    Get Element Attribute    xpath=.//*[@id="shared-accesskey"]    class
    ${status}    Run Keyword And Return Status    Should Contain    ${access_key_style}    disabled
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="joining-allowed-switch"]//span
    Wait Until Keyword Succeeds    5s    50ms
    ...    Wait Until Page Contains Element    xpath=.//*[@id="joining-allowed-switch"]//span[@data-content="true"]
    Wait Until Element Is Enabled    xpath=.//*[@id="joining-allowed-switch"]//span[@data-content="true"]

Turn Access Key Off
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
    ${access_key_style}    Get Element Attribute    xpath=.//*[@id="shared-accesskey"]    class
    ${status}    Run Keyword And Return Status    Should Not Contain    ${access_key_style}    disabled
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="joining-allowed-switch"]//span
    Wait Until Keyword Succeeds    5s    50ms
    ...    Wait Until Page Contains Element    xpath=.//*[@id="joining-allowed-switch"]//span[@data-content="false"]
    Wait Until Element Is Enabled    xpath=.//*[@id="joining-allowed-switch"]//span[@data-content="false"]

Switch Access Key
    [Arguments]    ${access_key_switch}=on
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
    ${access_key_style}    Get Element Attribute    xpath=.//*[@id="shared-accesskey"]    class
    ${access_key_status}    Run Keyword And Return Status    Should Not Contain    ${access_key_style}    disabled
    Run Keyword If    '${access_key_switch}'=='on' and '${access_key_status}'=='False'
    ...    Turn Access Key On
    Run Keyword If    '${access_key_switch}'=='off' and '${access_key_status}'=='True'
    ...    Turn Access Key Off

Reset Access Key
    FOR    ${index}    IN RANGE    1    4
        Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
        Click Element On Page    xpath=.//*[@id="joining-allowed-switch"]//span
        Sleep    1
    END

Close Access Key Dialog Window
    Click Element On Page    xpath=.//*[@id="share-accesskey-close"]

Create Access Key
    Wait Until Keyword Succeeds    60s    1s    Wait Until Element Is Enabled    xpath=.//*[@id="accesskey-bar-button"]
    ${status}    Run Keyword And Return Status    Element Should Be Visible    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="accesskey-bar-button"]
    Wait Until Element Is Enabled    xpath=.//mat-dialog-container
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]
    Turn Access Key On
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
    Wait Until Keyword Succeeds    5s    50ms
    ...    Wait Until Page Contains Element    xpath=.//*[@id="shared-accesskey" and text()[not(.=" ")]]
    Sleep       2
    ${access_key}    Get Text    xpath=.//*[@id="shared-accesskey"]
    ${access_key}    Strip String    ${access_key}
    [Return]    ${access_key}

Renew Access Key
    ${access_key_original}    Create Access Key
    Click Element On Page    xpath=.//*[@id="delete-and-regenerate-key"]
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey"]
    Wait Until Keyword Succeeds    10    1
    ...    Wait Until Page Does Not Contain Element    xpath=.//*[@id="shared-accesskey" and contains(text(),"${access_key_original}")]
    ${access_key}    Get Text    xpath=.//*[@id="shared-accesskey"]
    ${access_key_renewed}    Strip String    ${access_key}
    Should Not Be Equal As Strings    ${access_key_original}    ${access_key_renewed}
    [Return]    ${access_key_renewed}

Open Access Key Dialog
    Wait Until Element Is Visible    xpath=.//*[starts-with(@class,"join-feed-dialog")]//button
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"join-feed-dialog")]/button
    Click Element On Page    xpath=.//*[starts-with(@class,"join-feed-dialog")]/button
    Element Should Be Enabled    xpath=.//*[@id="access-key"]

Join Feed
    [Arguments]    ${feed_name}    ${access_key}    ${operation_status}=${OPERATION_SUCCESS}
    Wait Until Keyword Succeeds    30    1
    ...    Open Access Key Dialog
    Wait Until Element Is Enabled    xpath=.//*[@id="access-key"]
    Input Text    xpath=.//*[@id="access-key"]    ${access_key}
    Wait Until Keyword Succeeds    30    1
    ...    Click Element On Page    xpath=.//*[@class="join-with-accesskey"]
    Run Keyword If    '${operation_status}'=='${OPERATION_SUCCESS}'
     ...    Wait Until Page Contains Element    xpath=//app-content-feed//div[@class="feed-container"]/app-container    # Locator updated to ensure content is loaded
    Run Keyword If    '${operation_status}'=='${OPERATION_FAILURE}'
    ...    Wait Until Page Contains Element    xpath=.//*[@class="minimal-form"]/p[not(normalize-space(text())="")]
    Run Keyword If    '${operation_status}'=='${OPERATION_FAILURE}'
    ...    Wait Until Page Does Not Contain Element    xpath=.//*[@id="content-feed-button"]//*[contains(text(), "${feed_name}")]

Add New Content To Feed
    [Arguments]    ${content_type_to_add}    ${position}    ${content_item_text}    ${content_item_type}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    Run Keyword If    '${position}'=='shortcut_before'
    ...    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/app-new-item-button//i/img
    Run Keyword If    '${position}'=='shortcut_after'
    ...    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::div[@class="module-block"]/following-sibling::app-new-item-button//i/img
    Run Keyword If    '${position}'=='menu_before' or '${position}'=='menu_after'
    ...    Add New Content To Feed From Shortcut Menu        ${content_type_to_add}    ${position}    ${content_item_text}    ${content_item_type}
    #####
    # Check correct content item and type was added to the feed and to correct place
    # Then check the content by opening it and checking all the elements are in place
    # THINK: adding timestamp to the added content item
    #####

Add New Content To Feed From Shortcut Menu
    [Arguments]    ${content_type_to_add}    ${position}    ${content_item_text}    ${content_item_type}
    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Run Keyword If    '${position}'=='menu_before'
    ...    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-es:") and contains(@id, "-insert-before")]
    Run Keyword If    '${position}'=='menu_after'
    ...    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-es:") and contains(@id, "-insert-after")]
    Run Keyword If    '${content_type_to_add}'=='text'
    ...    Add Text To Content
    Run Keyword If    '${content_type_to_add}'=='exercise'
    ...    Add Exercise To Content
    Run Keyword If    '${content_type_to_add}'=='link'
    ...    Add Link To Content
    Run Keyword If    '${content_type_to_add}'=='file'
    ...    Add File To Content

Wait For Content Addition Dialog To Open
    Wait Until Element Is Enabled    xpath=.//mat-dialog-content

Add Text To Content
    Wait For Content Addition Dialog To Open
    Click Element On Page    xpath=.//*[@id="insert-theory-button"]

Add Exercise To Content
    Wait For Content Addition Dialog To Open
    Click Element On Page    xpath=.//*[@id="insert-exercise-button"]

Add Link To Content
    Wait For Content Addition Dialog To Open
    Click Element On Page    xpath=.//*[@id="insert-link-button"]
    ${date}    Get Date
    ${link_title}    Catenate    Link title    ${date}
    ${link_url}    Set Variable    www.google.fi
    ${link_description}    Catenate    Link description    ${date}
    Add Link Title    ${link_title}
    Verify If Adding Content Item Is Disabled    true
    Input Text    xpath=.//*[@id="link-url"]    ${link_url}
    Verify If Adding Content Item Is Disabled    false
    Add Link Description    ${link_description}
    Verify If Adding Content Item Is Disabled    false
    Confirm Content Item Addition
    Verify Added Content Item Exists    ${link_title}
    Verify Added Link Opens    ${link_title}    ${link_url}    ${link_description}
    Verify Added Content Item Can Be Deleted    ${link_title}

Add File To Content
    Wait For Content Addition Dialog To Open
    Click Element On Page    xpath=.//*[@id="insert-file-button"]
    ${date}    Get Date
    ${link_title}    Catenate    File title    ${date}
    ${link_description}    Catenate    File description    ${date}
    Add Link Title    ${link_title}
    Verify If Adding Content Item Is Disabled    true
    Add Link Description    ${link_description}
    Verify If Adding Content Item Is Disabled    true
    Choose File    xpath=.//input[@type="file"]    ${CURDIR}${/}${UPLOAD_IMAGE}
    Verify If Adding Content Item Is Disabled    false
    Confirm Content Item Addition
    Verify Added Content Item Exists    ${link_title}
    Verify Added File Opens    ${link_title}    ${link_description}    ${UPLOAD_IMAGE}
    Verify Added Content Item Can Be Deleted    ${link_title}

Add Link Title
    [Arguments]    ${link_title}
    Wait Until Element Is Enabled    xpath=.//*[@id="item-title"]
    Input Text    xpath=.//*[@id="item-title"]    ${link_title}

Add Link Description
    [Arguments]    ${link_description}
    Wait Until Element Is Enabled    xpath=.//*[@id="link-description"]
    Input Text    xpath=.//*[@id="link-description"]    ${link_description}

Confirm Content Item Addition
    Wait Until Element Is Enabled    xpath=.//*[@id="create-content-item-button"]
    Click Element On Page    xpath=.//*[@id="create-content-item-button"]
    Wait For Dialog Popup To Close

Verify Added Content Item Exists
    [Arguments]    ${link_title}
    Wait For Dialog Popup To Close
    Wait Until Keyword Succeeds    10s    1s
    ...    Wait Until Element Is Enabled
    ...    xpath=.//app-content-item//mat-list-item[@content-type="${CONTENT_ITEM_TYPE_TEXT}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${link_title}"]

Verify Added Link Opens
    [Arguments]    ${link_title}    ${link_url}    ${link_description}
    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${CONTENT_ITEM_TYPE_TEXT}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${link_title}"]
    Open Content Item By Type    ${CONTENT_ITEM_TYPE_TEXT}    ${link_title}
    Wait Until Element Is Enabled    xpath=.//as-split-area
    Page Should Contain Element    xpath=.//as-split-area//*[normalize-space(text())="${link_title}"]
    Page Should Contain Element    xpath=.//as-split-area//*[normalize-space(text())="${link_description}"]
    Page Should Contain Element    xpath=.//as-split-area//a[contains(@href, ${link_url})]
    Page Should Contain Element    xpath=.//as-split-area//*[normalize-space(text())="${link_url}"]

Verify Added Content Item Can Be Deleted
    [Arguments]    ${link_title}
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//*[@id="close-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="close-button"]
    Delete Content Item    ${CONTENT_ITEM_TYPE_TEXT}    ${link_title}

Verify Added File Opens
    [Arguments]    ${link_title}    ${link_description}    ${UPLOAD_IMAGE}
    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${CONTENT_ITEM_TYPE_TEXT}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${link_title}"]
    Open Content Item By Type    ${CONTENT_ITEM_TYPE_TEXT}    ${link_title}
    Wait Until Element Is Enabled    xpath=.//as-split-area
    Page Should Contain Element    xpath=.//as-split-area//*[normalize-space(text())="${link_title}"]
    Page Should Contain Element    xpath=.//as-split-area//*[normalize-space(text())="${link_description}"]
    Page Should Contain Element    xpath=.//as-split-area//a//*[normalize-space(text())="${UPLOAD_IMAGE}"]

Verify If Adding Content Item Is Disabled
    [Arguments]    ${should_be_disabled}
    Wait Until Element Is Visible    xpath=.//*[@id="create-content-item-button"]
    ${disabled}    Get Element Attribute    xpath=.//*[@id="create-content-item-button"]    disabled
    Run Keyword If    '${should_be_disabled}'=='true'    Should Be Equal As Strings    ${disabled}    true
    Run Keyword If    '${should_be_disabled}'=='false'    Should Be Equal As Strings    ${disabled}    None

Check Content Feed Is Not Editable
    ${editable_elements}    Get Element Count    xpath=.//button[(starts-with(@id, "section") or starts-with(@id, "item")) and contains(@id, "-menu-button")]
    Should Be Equal As Integers    ${editable_elements}    0

Delete Content Items
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    Run Keyword If    '${content_item_type}'!='${CONTENT_ITEM_TYPE_CHAPTER}'    Delete Content Item    ${content_item_type}    ${content_item_text}
    Run Keyword If    '${content_item_type}'=='${CONTENT_ITEM_TYPE_CHAPTER}'    Delete Chapter    ${content_item_type}    ${content_item_text}

Delete Content Item
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    ${elements_before}    Get Element Count    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-") and contains(@id, "-delete")]
    Click Element On Page    xpath=.//button[@id="node-dialog-remove-button"]
    Wait For Dialog Popup To Close
    Wait Until Element Is Not Visible    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]

Delete Chapter
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    ${chapters_in_chapter_before}    Get Element Count
    ...    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]/ancestor::app-chapter-title/following-sibling::app-container//app-chapter-title
    ${content_items_in_chapter_before}    Get Element Count
    ...    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]/ancestor::app-chapter-title/following-sibling::app-container//app-content-item
    Click Element On Page    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "section-es:") and contains(@id, "-remove-button")]
    Click Element On Page    xpath=.//button[@id="node-dialog-remove-button"]
    Wait For Dialog Popup To Close
    Wait Until Page Does Not Contain Element
    ...    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]
    ${chapters_in_chapter_after}    Get Element Count
    ...    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]/ancestor::app-chapter-title/following-sibling::app-container//app-chapter-title
    ${content_items_in_chapter_after}    Get Element Count
    ...    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[contains(normalize-space(text()), "${content_item_text}")]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]/ancestor::app-chapter-title/following-sibling::app-container//app-content-item
    Should Be Equal As Numbers    ${chapters_in_chapter_after}    0
    Should Be Equal As Numbers    ${content_items_in_chapter_after}    0

Get Number Of Content Items
    Wait Until Element Is Enabled    xpath=.//app-content-item//*[@class="title mat-line"]
    ${content_items_in_total}    Get Element Count    xpath=.//app-content-item//*[@class="title mat-line"]
    [Return]    ${content_items_in_total}

Get Content Items List
    ${content_items_in_total}    Get Number Of Content Items
    ${content_items_list}    Create List
    FOR    ${index}    IN RANGE    0    ${content_items_in_total}
        ${index}    Evaluate    ${index}+1
        ${content_item_text}    Get Text    xpath=(.//app-content-item//*[@class="title mat-line"])[${index}]
        Append To List    ${content_items_list}    ${content_item_text}
    END
    [Return]    ${content_items_list}

Verify Teacher Feed Exists
    [Arguments]    ${method_to_select}
    ${teacher_feeds}    Get Element Count    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//div[@class="feed-title" and normalize-space(text())="${method_to_select}"]
    Run Keyword If    ${teacher_feeds}==0    Add Teacher Feed    ${method_to_select}

Add Date To Feed Name
    [Arguments]    ${method_name}
    ${date_stamp}    Get Current Date    result_format=%Y%m%d_%H%M%S
    ${feed_name}    Catenate    SEPARATOR=_    ${method_name}    ${date_stamp}
    Add New Feed Name    ${feed_name}

Add New Feed Name
    [Arguments]    ${feed_name_new}
    Input Text    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//*[starts-with(@id,"mat-input-")]    ${feed_name_new}

Add Teacher Feed
    [Arguments]    ${method_to_add}    ${date}=off    ${feed_origin}=${FEED_TYPE_PUBLISHER}    ${new_name}=${EMPTY}    ${scroll_to_element}=on
    Run Keyword If    '${feed_origin}'=='${FEED_TYPE_PUBLISHER}'    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="${feed_origin}"]//*[@class= "publisher-feed-title" and normalize-space(text())="${method_to_add}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Run Keyword If    '${feed_origin}'=='${FEED_TYPE_TEACHER}'    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="${feed_origin}"]//*[@class="teacher-feed-title" and normalize-space(text())="${method_to_add}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Wait Until Keyword Succeeds    30s    1s    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "create-teacher-feed-button")]
    Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    Run Keyword If    '${date}'!='off' and '${new_name}'!='${EMPTY}'    Add Date To Feed Name    ${new_name}
    Run Keyword If    '${date}'!='off' and '${new_name}'=='${EMPTY}'    Add Date To Feed Name    ${method_to_add}
    Run Keyword If    '${date}'=='off' and '${new_name}'!='${EMPTY}'    Add New Feed Name    ${new_name}
    ${method_name}    Get Value    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    Click Element On Page    xpath=.//*[@id="create-teacher-feed-button"]
    Wait Until Keyword Succeeds    180s    1s
    ...    Wait Until Element Is Not Visible    xpath=.//app-create-teacher-feed-dialog//*[contains(@class, "loading")]
    ${status}    Run Keyword And Return Status        Wait Until Keyword Succeeds    10s    1s
    ...    Wait Until Element Is Not Visible    xpath=.//app-create-teacher-feed-dialog//*[@id="group-creation-failed-error"]
    # ${status}    Run Keyword And Return Status    Wait Until Keyword Succeeds    120s    1s
    # ...    Wait Until Element Is Visible    xpath=.//app-method-feeds//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_name}"]
    #
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="create-teacher-feed-button"]
    Wait Until Keyword Succeeds    180s    1s
    ...    Wait Until Element Is Not Visible    xpath=.//app-create-teacher-feed-dialog//*[contains(@class, "loading")]
    ${status}    Run Keyword And Return Status    Wait Until Keyword Succeeds    120s    1s
    ...    Wait Until Element Is Visible    xpath=.//app-method-feeds//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_name}"]
    #
    ###--->Scroll Element Into View    xpath=.//app-method-feeds//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_name}"]
    ###--->Click Element On Page    xpath=.//app-method-feeds//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_name}"]
    Select Feed    ${method_name}    ${FEED_TYPE_TEACHER}
    #############Select Method With Method URL    ${method_name}    ${FEED_TYPE_TEACHER}
    [Return]    ${method_name}

Select Feed
    [Arguments]    ${item_to_select}    ${feed_type}
    #Scroll Element Into View    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[@class="${feed_type}-feed-title" and normalize-space(text())="${item_to_select}"]
    #Execute JavaScript    document.getElementById('feed-7e69e557-784c-4a95-bc8e-5e79e1282e2e-menu-button').scrollIntoView();
    ${item_locator}=    Set Variable    .//app-method-feeds//div[@feed-type="${feed_type}"]//div[@class="${feed_type}-feed-title" and normalize-space(text())="${item_to_select}"]
    Focus And Click Element    xpath=${item_locator}
    Wait Until Content Feed Has Loaded

Rearrange
    [Arguments]    ${index_start}    ${index_end}=last
    ${content_items_in_total}    Get Number Of Content Items
    ${index_end}    Run Keyword If    '${index_end}'!='last'    Set Variable    ${index_end}    ELSE    Set Variable    ${content_items_in_total}
    Wait Until Element Is Enabled    xpath=(.//mat-list-item)[1]
    Scroll Page To Top
    ${h1}    Get Vertical Position    xpath=(.//mat-list-item)[1]
    ${h2}    Get Vertical Position    xpath=(.//mat-list-item)[2]
    ${vPos1}    Get Vertical Position    xpath=(.//mat-list-item)[${index_start}]
    ${hPos1}    Get Horizontal Position    xpath=(.//mat-list-item)[${index_start}]
    ${vPos2}    Get Vertical Position    xpath=(.//mat-list-item)[${index_end}]
    ${hPos2}    Get Horizontal Position    xpath=(.//mat-list-item)[${index_end}]
    ${vPos3}    Evaluate    ${vPos2}+150
    ${vPos3}    Get Vertical Position    xpath=(.//mat-list-item)[${index_end}]
    ${new_pos}    Evaluate    ${vPos2}-${vPos1}
    ${new_pos2}    Evaluate    ${vPos1}+${new_pos}
    ${content_items_list_before}    Get Content Items List
    Log Many    @{content_items_list_before}
    Mouse Down    xpath=(.//mat-list-item)[${index_start}]
    Sleep    5
    Mouse Up    xpath=(.//mat-list-item)[${index_end}]
    ${content_items_list_after}    Get Content Items List
    Log Many    @{content_items_list_after}
    Should Not Be Equal    ${content_items_list_before}    ${content_items_list_after}

Select Method With Method URL
    [Documentation]    This keyword takes as a parameter either the name and type of the method to select and opens the method using its URL
    [Arguments]    ${method_to_select}    ${feed_type}=${FEED_TYPE_PUBLISHER}
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="publisher"]
    ${method_xpath}    Run Keyword If    '${feed_type}'=='${FEED_TYPE_PUBLISHER}'
    ...    Set Variable
    ...    .//app-method-feeds//*[contains(@class, "publisher-feed-title") and normalize-space(text())="${method_to_select}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]      #DOES NOT WORK!
    ...    ELSE IF    '${feed_type}'=='${FEED_TYPE_TEACHER}'
    ...    Set Variable
    ...    (.//app-method-feeds//*[@class="outer-container ng-star-inserted"]//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_to_select}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")])
    Wait Until Element Is Enabled    xpath=${method_xpath}
    ${id_for_url}    Get Element Attribute    xpath=${method_xpath}    id
    ${id_for_url}    Remove String    ${id_for_url}    feed-
    ${id_for_url}    Remove String    ${id_for_url}    -menu-button
    ${current_url}    Get Location
    ${goto_url}    Catenate    SEPARATOR=    ${current_url}    content-feed/    ${id_for_url}
    Go To    ${goto_url}
    #Wait Until Keyword Succeeds    60s    1s    Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]
    Wait Until Content Feed Has Loaded
    [Return]    ${method_to_select}

Select Method_2
    [Documentation]    This keyword takes as a parameter either the name of the method to select or the order number of the method
        ...
    ...    Arguments
    ...    - None
    ...    Returns
    ...    - None
    [Arguments]    ${method_to_select}    ${feed_type}=${FEED_TYPE_PUBLISHER}    ${teacher_feed_verification}=off
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="publisher"]
    Run Keyword If    ('${feed_type}'=='${FEED_TYPE_PUBLISHER}' or '${feed_type}'=='${FEED_TYPE_TEACHER}') and '${teacher_feed_verification}'!='off'    Verify Teacher Feed Exists    ${method_to_select}
    Run Keyword If    '${feed_type}'=='${FEED_TYPE_PUBLISHER}'    Get Element Into View    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[contains(@class, "Feeds-title") and normalize-space(text())="${method_to_select}"]    ${feed_type}
    Run Keyword If    '${feed_type}'=='${FEED_TYPE_TEACHER}'    Get Element Into View    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[contains(@class, "feed-title")]/*[normalize-space(text())="${method_to_select}"]    ${feed_type}
    Run Keyword If    '${feed_type}'=='${FEED_TYPE_PUBLISHER}'    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[contains(@class,"Feeds-title") and normalize-space(text())="${method_to_select}"]/ancestor::a
    Run Keyword If    '${feed_type}'=='${FEED_TYPE_TEACHER}'    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[contains(@class, "feed-title")]/*[normalize-space(text())="${method_to_select}"]/ancestor::a
    [Return]    ${method_to_select}

Open Method In Pop Up Menu
    [Arguments]    ${method_to_select}
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="publisher"]
    Scroll Page To Top
    Get Element Into View    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_PUBLISHER}"]//div[contains(@class, "feed-title") and normalize-space(text())="${method_to_select}"]    ${FEED_TYPE_PUBLISHER}
    Click Element On Page    xpath=.//app-method-feeds/div[@class="methods-list"]//img[@class="cover-image"]/following-sibling::div/div[contains(@class,"feed-title") and text()="${method_to_select}"]/ancestor::a/preceding-sibling::button
    Wait Until Element Is Enabled    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "open-button")]
    Wait Until Element Is Enabled    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "create-teacher-feed-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "create-teacher-feed-button")]
    Wait Until Element Is Enabled    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${feed_name_default}    Get Value    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${date_stamp}    Get Current Date    result_format=%Y%m%d_%H%M%S
    ${feed_name}    Catenate    SEPARATOR=_    ${feed_name_default}    ${date_stamp}
    Input Text    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]   ${feed_name}
    Click Element On Page    xpath=.//*[@id="create-teacher-feed-button"]
    Wait Until Element Is Enabled    xpath=.//app-method-feeds/div[@class="methods-list"]//img[@class="cover-image"]/following-sibling::div/div[contains(@class,"feed-title") and text()="${feed_name}"]
    [Return]    ${feed_name}

Set Method Name_2
    [Arguments]    ${method_texts}    ${method_to_select}    ${argument_type}
    Run Keyword If    '${argument_type}'=='int'    Set Method Name As Integer_2    ${method_texts}    ${method_to_select}
    Run Keyword If    '${argument_type}'=='str'    Set Method Name As String_2    ${method_texts}    ${method_to_select}

Set Method Name As Integer_2
    [Arguments]    ${method_texts}    ${method_to_select}
    Click Element On Page    xpath=(.//app-method-feeds/div[@class="methods-list"]//img[@class="cover-image"])[${method_to_select}]
    ${method_to_select}        Evaluate    ${method_to_select}-1
    ${method_text}    Get From List    ${method_texts}    ${method_to_select}
    Set Test Variable    ${method_name}    ${method_text}

Set Method Name As String_2
    [Arguments]    ${method_texts}    ${method_to_select}
    Click Element On Page    xpath=.//app-method-feeds/div[@class="methods-list"]//img[@class="cover-image"]/following-sibling::div/div[text()="${method_to_select}"]/ancestor::a
    ${method_to_select_index}    Get Index From List    ${method_texts}    ${method_to_select}
    ${method_text}    Get From List    ${method_texts}    ${method_to_select_index}
    Set Test Variable    ${method_name}    ${method_text}

Check Units_v2
    # app-in-view-item (check the contents of this element)
    # id=currentChapter (in TOC)
    Wait Until Element Is Enabled    xpath=.//mat-sidenav//app-table-of-contents
    ${toc_units}    Get Element Count    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]
    ${units_in_content_feed}    Get Element Count
    ...    xpath=.//*[@class="content-feed"]//app-chapter-title[starts-with(@id, "container_")]//*[@class="title-zero"]
    Should Be Equal As Numbers    ${toc_units}    ${units_in_content_feed}
    FOR    ${index}    IN RANGE     0       ${toc_units}
        ${index}        Evaluate    ${index}+1
        ${toc_unit}    Get Text    xpath=(.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[@class="title"])[${index}]
        ${toc_unit}    Strip String    ${toc_unit}
        ${toc_unit}    Replace String    ${toc_unit}    \n    ${SPACE}
        Page Should Contain Element    xpath=.//*[@class="content-feed"]//app-chapter-title[starts-with(@id, "container_")]//*[@class="chapter-title-0"]//span[text()="${toc_unit}"]
    END

Check Chapters_v2
    [Documentation]    This keyword checks chapters in a unit
    [Arguments]    ${filters}    ${index_selected}
    ${index_selected}        Evaluate    ${index_selected}-1
    ${toc_chapters}    Get Element Count    xpath=.//div[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]//*[@class="content"]//*[contains(@class, "text")]
    FOR    ${index}    IN RANGE     0       ${toc_chapters}
        ${index}        Evaluate    ${index}+1
        ${toc_chapter_text}    Get Text    xpath=(.//div[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]//*[@class="content"]//*[contains(@class, "text")])[${index}]
        Wait Until Keyword Succeeds    5s    50ms
        ...    Element Should Be Visible   xpath=.//*[@class="content-feed"]//app-chapter-title[starts-with(@id, "container_")]//*[@class="title-one" and normalize-space(text())="${toc_chapter_text}"]
    END
    #...    Page Should Contain Element    xpath=.//*[@class="content-feed"]//app-chapter-title[starts-with(@id, "container_")]//*[@class="title-one" and normalize-space(text())="${toc_chapter_text}"]

Open Chapter
    [Documentation]    This keyword opens a chapter in a unit. The name of the chapter or its order number can be passed as a parameter.
    [Arguments]    ${chapter_to_select}
    Wait Until Content Feed Has Loaded
    Open TOC
    Wait Until Element Is Enabled    xpath:.//*[starts-with(@class, "dropdown-sidebar")]//*[starts-with(@class, "title-text") and contains(text(), "${chapter_to_select}")]
    Sleep    1
    Collapse TOC Menu
    Click Element On Page    xpath:.//*[starts-with(@class, "dropdown-sidebar")]//*[starts-with(@class, "title-text") and contains(text(), "${chapter_to_select}")]
    #Wait Until Keyword Succeeds    10    1
    #...    Wait Until Element Is Enabled    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]
    #${accordion_expansion_indicator}    Wait Until Keyword Succeeds    10    1    Get Element Attribute    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]/ancestor::*[contains(@class, "dropdown-sidebar")]    class
    #${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
    #Wait Until Element Is Enabled    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]
    #Run Keyword If    '${status}'=='PASS'    Wait Until Keyword Succeeds    10s    50ms    Check Chapter Is Opened    ${chapter_to_select}

Check Chapter Is Opened
    [Arguments]    ${chapter_to_select}
    Wait Until Content Feed Has Loaded
    Click Element On Page    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]
    Wait Until Keyword Succeeds    10    1
    ...    Wait Until Element Is Enabled    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]
    ${accordion_expansion_indicator}    Wait Until Keyword Succeeds    10    1    Get Element Attribute    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and (normalize-space(text())="${chapter_to_select}" or normalize-space(string())="${chapter_to_select}")]/ancestor::*[contains(@class, "dropdown-sidebar")]     class
    Should Contain    ${accordion_expansion_indicator}    opened
    Click Element On Page    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]//*[starts-with(@class,"item")][1]//*[starts-with(@class, "text") and text()[not(.="")]]
    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="title-zero" and contains(normalize-space(text()),"${chapter_to_select}")]

Check Element Attribute
    [Arguments]    ${chapter_to_select}
    ${element_attribute}    Get Element Attribute    xpath=(.//mat-sidenav//*[@class="mat-content"])[${chapter_to_select}]/following-sibling::span[contains(@class, "mat-expansion-indicator")]    style
    Should Be True    '${element_attribute}'!='${EMPTY}'
    [Return]    ${element_attribute}

Open Unit In Chapter
    [Arguments]    ${chapter_to_select}    ${unit_to_select}
    Open Chapter    ${chapter_to_select}
    Open TOC
    #Scroll Element Into View    xpath:.//*[starts-with(@class, "dropdown-sidebar")]//*[starts-with(@class, "title-text") and contains(text(), "${chapter_to_select}")]/ancestor::*[starts-with(@class, "dropdown-sidebar")]/*[starts-with(@class, "content")]//*[starts-with(@class, "text") and contains(text(), "${unit_to_select}")]
    Click Element On Page    xpath:.//*[starts-with(@class, "dropdown-sidebar")]//*[starts-with(@class, "title-text") and contains(text(), "${chapter_to_select}")]/ancestor::*[starts-with(@class, "dropdown-sidebar")]/*[starts-with(@class, "content")]//*[starts-with(@class, "text") and contains(text(), "${unit_to_select}")]
    #Wait Until Element Is Enabled    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[(contains(@class, "title") or contains(@class, "chapterTitle")) and normalize-space(text())="${chapter_to_select}"]/ancestor::*[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]
    #Click Element On Page    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]//*[@class="content"]//*[starts-with(@class, "text") and normalize-space(text())="${unit_to_select}"]

Open Content Item
    [Arguments]    ${chapter_to_select}    ${unit_to_select}    ${content_item_to_select}    ${content_item_type}=${EMPTY}
    Open Unit In Chapter    ${chapter_to_select}    ${unit_to_select}
    ${content_type_tag}    Run Keyword If    '${content_item_type}'!='${EMPTY}'
    ...    Set Variable    and @content-type="${content_item_type}"
    ...    ELSE
    ...    Set Variable    ${EMPTY}
    Scroll Element Into View    xpath:.//*[contains(@class, "-wrapper") ${content_type_tag}]//*[contains(@class, "-text") and normalize-space(text())="${content_item_to_select}"]
    Verify TOC Is Hidden
    Click Element On Page    xpath:.//*[contains(@class, "-wrapper") ${content_type_tag}]//*[contains(@class, "-text") and normalize-space(text())="${content_item_to_select}"]
    #Wait Until Element Is Enabled
    #...    xpath=.//app-chapter-title//*[contains(normalize-space(text()),"${chapter_to_select}")]/ancestor::div//app-container//*[starts-with(@class, "title")]/*[normalize-space(text())="${unit_to_select}"]/ancestor::app-container//mat-list-item${content_type_tag}//*[@class="mat-list-text"]//*[starts-with(@class, "title") and normalize-space(text())="${content_item_to_select}"]
    #Scroll Element Into View
    #...    xpath=.//app-chapter-title//*[contains(normalize-space(text()),"${chapter_to_select}")]/ancestor::div//app-container//*[starts-with(@class, "title")]/*[normalize-space(text())="${unit_to_select}"]/ancestor::app-container//mat-list-item${content_type_tag}//*[@class="mat-list-text"]//*[starts-with(@class, "title") and normalize-space(text())="${content_item_to_select}"]
    #Wait Until Keyword Succeeds    5s    50ms
    #...    Click Element On Page    xpath=.//app-chapter-title//*[contains(normalize-space(text()),"${chapter_to_select}")]/ancestor::div//app-container//*[starts-with(@class, "title")]/*[normalize-space(text())="${unit_to_select}"]/ancestor::app-container//mat-list-item${content_type_tag}//*[@class="mat-list-text"]//*[starts-with(@class, "title") and normalize-space(text())="${content_item_to_select}"]

Show Element On Screen
    [Arguments]    ${content_item_type}    ${content_item_text}
    Run Keyword If    '${content_item_type}'!='${CONTENT_ITEM_TYPE_CHAPTER}'
    ...    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item
    ...    ELSE
    ...    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[normalize-space(text())="${content_item_text}"]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]
    ${element_position}    Run Keyword If    '${content_item_type}'!='${CONTENT_ITEM_TYPE_CHAPTER}'
    ...    Get Vertical Position    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item
    ...    ELSE
    ...    Get Vertical Position    xpath=.//app-chapter-title//*[contains(@class, "title-container")]/*[normalize-space(text())="${content_item_text}"]/following-sibling::button[starts-with(@id, "section-es:") and contains(@id, "-menu-button")]
    Scroll Page To Top
    ${top_bar_position}    Execute JavaScript    var rect = document.getElementsByClassName('header-container'); var topBar = rect[0].getBoundingClientRect(); return topBar.bottom;
    ${position_difference}    Evaluate    ${element_position}-${top_bar_position}-20
    Execute JavaScript    window.scrollTo(0, ${position_difference});

Open Content Item By Type
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    Click Element On Page    xpath=.//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]
    Wait Until Element Is Enabled    xpath=.//app-document

Hide Content Item By Type
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    ${element_visibility}    Get Element Attribute    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div    class
    ${element_hidden}    Run Keyword And Return Status    Should Not Contain    ${element_visibility}    hidden
    Run Keyword If    '${element_hidden}'=='False'    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Run Keyword If    '${element_hidden}'=='False'    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-") and contains(@id, "-show-button")]
    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-") and contains(@id, "-hide-button")]
    ${element_visibility}    Get Element Attribute    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div    class
    Should Contain    ${element_visibility}    hidden
    ###
    # check the hiding stays after visiting another method, etc.
    ###

Show Content Item By Type
    [Arguments]    ${content_item_type}    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    ${element_visibility}    Get Element Attribute    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div    class
    ${element_hidden}    Run Keyword And Return Status    Should Not Contain    ${element_visibility}    hidden
    Run Keyword If    '${element_hidden}'=='False'    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Run Keyword If    '${element_hidden}'=='False'    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-") and contains(@id, "-show-button")]
    Sleep    2
    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    ${element_visibility}    Get Element Attribute    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div    class
    Should Not Contain    ${element_visibility}    hidden

Get Content Item Text
    [Arguments]    ${content_item_type}    ${content_item_text}
    Wait Until Element Is Enabled
    ...    xpath=(.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title")])[${content_item_text}]
    ${content_item_text}    Get Text
    ...    xpath=(.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title")])[${content_item_text}]
    [Return]    ${content_item_text}

Edit Content Item Title
    [Arguments]    ${content_item_type}    ${content_item_text}    ${content_item_text_new}=""
    ${argument_type}    Evaluate    type($content_item_text).__name__
    ${content_item_text}    Run Keyword If    '${argument_type}'=='int'
    ...    Get Content Item Text    ${content_item_type}    ${content_item_text}
    ...    ELSE
    ...    Set Variable    ${content_item_text}
    Show Element On Screen    ${content_item_type}    ${content_item_text}
    Click Element On Page    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::mat-list-item//button[starts-with(@id, "item-") and contains(@id, "-menu-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "item-") and contains(@id, "-edit-button")]
    Wait For Condition    return document.readyState=="complete"
    Wait Until Element Is Enabled    xpath=.//input[starts-with(@id, "mat-input-")]
    ${value}    Get Value    xpath=.//input[starts-with(@id, "mat-input-")]
    ${value_original}    Set Variable    ${value}
    ${value}    Split String    ${value}    _    1
    ${value}    Set Variable    ${value}[0]
    Wait Until Element Is Enabled    xpath=.//input[starts-with(@id, "mat-input-")]
    Set Focus To Element    xpath=.//input[starts-with(@id, "mat-input-")]
    ${character count}    Get Length    ${value_original}
    FOR    ${index}    IN RANGE    ${character count}
        Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    BACKSPACE
    END
    #Clear Element Text    xpath=.//input[starts-with(@id, "mat-input-")]
    ${date}    Get Date
    ${value_updated}    Catenate    SEPARATOR=_    ${value}    ${date}
    Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    ${value_updated}
    Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    ENTER
    Wait Until Element Is Enabled    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${value_updated}"]

Edit Chapter Title
    [Arguments]    ${chapter}=chapter    ${chapter_text_new}=${EMPTY}
    Scroll Page To Top
    Run Keyword If    '${chapter}'=='chapter'    Click Element On Page    xpath=(.//app-chapter-title//*[@class="title-zero"]/following-sibling::button[starts-with(@id, "section-") and contains(@id, "-menu-button")])[1]
    Run Keyword If    '${chapter}'=='subchapter'    Click Element On Page    xpath=(.//app-chapter-title//*[@class="title-one"]/following-sibling::button[starts-with(@id, "section-") and contains(@id, "-menu-button")])[1]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "section-") and contains(@id, "-rename-button")]
    Wait Until Element Is Enabled    xpath=.//input[starts-with(@id, "mat-input-")]
    ${value}    Get Value    xpath=.//input[starts-with(@id, "mat-input-")]
    ${value_original}    Set Variable    ${value}
    ${value}    Split String    ${value}    _    1
    ${value}    Set Variable    ${value}[0]
    Wait Until Element Is Enabled    xpath=.//input[starts-with(@id, "mat-input-")]
    Set Focus To Element    xpath=.//input[starts-with(@id, "mat-input-")]
    ${character count}    Get Length    ${value_original}
    FOR    ${index}    IN RANGE    ${character count}
        Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    BACKSPACE
    END
    ${date}    Get Date
    ${value_updated}    Run Keyword If    '${chapter_text_new}'=='${EMPTY}'
    ...    Catenate    SEPARATOR=_    ${value}    ${date}
    ...    ELSE
    ...    Set Variable    ${chapter_text_new}
    Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    ${value_updated}
    Sleep    2
    Press Keys    xpath=.//input[starts-with(@id, "mat-input-")]    ENTER
    Page Should Not Contain Element    xpath=.//input[starts-with(@id, "mat-input-")]
    Run Keyword If    '${chapter}'=='chapter'    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="title-zero" and normalize-space(text())="${value_updated}"]
    ${subchapter_updated}    Run Keyword If    '${chapter}'=='subchapter'    Get Text    xpath=.//app-chapter-title//*[@class="chapter-title-1"]
    ${subchapter_updated}    Run Keyword If    '${chapter}'=='subchapter'    Strip String    ${subchapter_updated}
    ${subchapter_updated}    Run Keyword If    '${chapter}'=='subchapter'    Replace String    ${subchapter_updated}     \n     ${SPACE}
    Run Keyword If    '${chapter}'=='subchapter'    Should Be Equal As Strings    ${value_updated}    ${subchapter_updated}
    Sleep    5
    Run Keyword If    '${chapter}'=='chapter'
    ...    Wait Until Element Is Enabled    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]/*[(contains(@class,"title") or contains(@class, "chapterTitle")) and normalize-space(string())="${value_updated}"]
    Run Keyword If    '${chapter}'=='subchapter'
    ...    Wait Until Element Is Enabled    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[@class="content"]//*[starts-with(@class, "item")]//*[starts-with(@class, "text") and normalize-space(text())="${subchapter_updated}"]
    # Wait Until Element Is Enabled    xpath=(.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]/*[@class="title"])[1]
    # ${subchapter_text_in_toc}    Get Text    xpath=(.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]/*[@class="title"])[1]
    # ${subchapter_text_in_toc}    Strip String    ${subchapter_text_in_toc}
    # ${subchapter_text_in_toc}    Replace String    ${subchapter_text_in_toc}     \n     ${SPACE}
    # ${subchapter_text_no_number}    Split String    ${subchapter_text_in_toc}    ${SPACE}    1
    # ${subchapter_text_in_toc}    Replace String    ${subchapter_text_in_toc}    ${subchapter_text_no_number}[0]    ${SPACE}    1
    # ${subchapter_text_in_toc}    Strip String    ${subchapter_text_in_toc}
    # Run Keyword If    '${chapter_text_new}'=='${EMPTY}'    Open Chapter    ${subchapter_text_in_toc}
    # Run Keyword If    '${chapter_text_new}'!='${EMPTY}'    Open Chapter    ${value_updated}
    Run Keyword If    '${chapter}'=='chapter'    Open Chapter    ${value_updated}
    Run Keyword If    '${chapter}'=='subchapter'    Click Element On Page
    ...    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[@class="content"]//*[starts-with(@class, "item")]//*[starts-with(@class, "text") and normalize-space(text())="${subchapter_updated}"]

Rename Feed Name
    [Arguments]    ${feed_name_original}    ${feed_name_new}=date
    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name_original}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "change-teacher-feed-name-button")]
    Wait Until Element Is Enabled    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${feed_name_current}    Get Value    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${value}    Split String    ${feed_name_current}    _    1
    ${value}    Set Variable    ${value}[0]
    Clear Element Text    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${date}    Get Date
    ${feed_name_updated}    Catenate    SEPARATOR=_    ${value}    ${date}
    ${feed_name_updated}    Set Variable If
    ...    '${feed_name_new}'=='date'    ${feed_name_updated}
    ...    '${feed_name_new}'!='date'    ${feed_name_new}
    Input Text    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]    ${feed_name_updated}
    Click Element On Page    xpath=.//*[@id="change-teacher-feed-name-button"]
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name_updated}"]
    Set Suite Variable    ${NAME_FOR_FEED_RENAME_UPDATED}    ${feed_name_updated}

Check Renaming Allowed For Participant Teacher
    [Arguments]    ${feed_name}
    Go To My Page
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name}"]
    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Wait Until Element Is Visible    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "change-teacher-feed-name-button")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "change-teacher-feed-name-button")]
    Wait Until Element Is Enabled    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${feed_name_current}    Get Value    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${value}    Split String    ${feed_name_current}    _    1
    ${value}    Set Variable    ${value}[0]
    Clear Element Text    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    ${date}    Get Date
    ${feed_name_updated}    Catenate    SEPARATOR=_    ${value}    ${date}
    Input Text    xpath=.//app-change-teacher-feed-name-dialog//*[@id="teacher-feed-form"]//input[@name="title"]    ${feed_name_updated}
    Click Element On Page    xpath=.//*[@id="change-teacher-feed-name-button"]
    Wait Until Element Is Enabled    xpath=.//app-change-teacher-feed-name-dialog
    Element Should Not Be Visible    xpath=.//*[@id="change-feed-name-failed-error"]

Open Units_v2
    #Check Units_v2
    ${toc_units}    Get Element Count    xpath=.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]
    FOR    ${index}    IN RANGE     0       ${toc_units}
        ${index}        Evaluate    ${index}+1
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")])[${index}]    class
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
        Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=(.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")])[${index}]
        Sleep    2
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")])[${index}]    class
        Should Contain    ${accordion_expansion_indicator}    opened
        Click Element On Page    xpath=(.//mat-sidenav//app-table-of-contents//*[contains(@class, "dropdown-sidebar")])[${index}]//*[@class="content"]//*[starts-with(@class,"item")][1]
        Check Chapters_v2    ${toc_units}    ${index}
    END

Check Table Of Contents
    Select Show All Filter
    Open Units_v2

Browse Content Items
    Scroll Page To Top
    Select Show All Filter
    ${content_feed_items}    Get Element Count    xpath=.//mat-list-item/parent::a
    FOR    ${index}    IN RANGE     0       ${content_feed_items}
        ${list_index}        Evaluate    ${index}+0
        ${index}        Evaluate    ${index}+1
        Run Keyword If    ${index}==${1}    Click Element On Page    xpath=(.//mat-list//mat-list-item)[${index}]//*[@class="mat-list-text"]/p[2]
        Run Keyword If    ${index}==${1}    Wait Until Element Is Enabled    xpath=.//img[contains(@class, "module-close-button")]
        Sleep    1
        Run Keyword If    ${index}==${1}    Page Should Not Contain Element    xpath=.//*[@id="prev-button-bg"]
        Run Keyword If    ${index}<${content_feed_items}    Wait Until Element Is Enabled    xpath=.//*[@id="next-button-bg"]
        Run Keyword If    ${index}<${content_feed_items}    Wait Until Keyword Succeeds    10 sec    1 sec    Execute JavaScript    document.getElementById("next-button-bg").click()
    END
    FOR    ${index}    IN RANGE     ${content_feed_items}    0    -1
        ${index}        Evaluate    ${index}-1
        Sleep    1
        Run Keyword If    ${index}==${0}    Page Should Not Contain Element    xpath=.//*[@id="prev-button-bg"]
        Run Keyword If    ${index}>${0}    Wait Until Element Is Enabled    xpath=.//*[@id="prev-button-bg"]
        Run Keyword If    ${index}>${0}    Wait Until Keyword Succeeds    10 sec    1 sec    Execute JavaScript    document.getElementById("prev-button-bg").click()
    END
    Click Element On Page    xpath=.//app-module-content//*[@id="close-button"]/img

Close Content Item
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="close-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="close-button"]
    #Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]
    Wait Until Content Feed Has Loaded

Go To Library Directly
    Wait Until Element Is Visible     xpath=.//*[@class="library-button show-library"]
    Click Element On Page    xpath=.//*[@class="library-button show-library"]
    Wait Until Element Is Not Visible     xpath=.//*[@class="library-button show-library"]

Check Reflink
    #Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
    #Wait Until Element Is Enabled    xpath=.//app-module-content-documents
    #Wait Until Keyword Succeeds    10s    1s    Verify Reflink Count    1
    Wait Until Element Is Enabled    xpath=.//*[@id="referral-relations-button"]
    Wait Until Element Is Visible    xpath=.//*[@id="referral-relations-button"]/img
    #Sleep    2
    Click Element On Page        xpath=.//*[@id="referral-relations-button"]
    #Sleep    2
    #Wait Until Keyword Succeeds    10s    1s    Verify Reflink Count    2
    Wait Until Element Is Enabled    xpath=.//app-module-content-reference-item
    ${innerHTML}    Get Element Attribute    xpath=.//app-module-content-reference-item    innerHTML
    ${innerHTML_length}    Get Length    ${innerHTML}
    Should Be True    ${innerHTML_length}>0
    Wait Until Element Is Enabled    xpath=.//*[@id="referral-relations-button"]
    Wait Until Element Is Visible    xpath=.//*[@id="referral-relations-button"]/img
    Click Element On Page        xpath=.//*[@id="referral-relations-button"]
    #Wait Until Keyword Succeeds    10s    1s    Verify Reflink Count    1
    Wait Until Element Is Not Visible    xpath=.//app-module-content-reference-item

Verify Reflink Count
    [Arguments]    ${expected_reflink_count}
    Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
    ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
    Should Be Equal As Integers    ${split_areas}    ${expected_reflink_count}

Go To Previous Page
    ${header_before}    Get Page Header Text
    ${url_before}    Get Location
    Click Element On Page    xpath=.//*[@id="prev-button-bg"]
    ${header_after}    Get Page Header Text
    ${url_after}    Get Location
    Should Not Be Equal As Strings    ${header_before}    ${header_after}
    Should Not Be Equal As Strings    ${url_before}    ${url_after}

Go To Next Page
    ${header_before}    Get Page Header Text
    ${url_before}    Get Location
    Click Element On Page    xpath=.//*[@id="next-button-bg"]
    ${header_after}    Get Page Header Text
    ${url_after}    Get Location
    Should Not Be Equal As Strings    ${header_before}    ${header_after}
    Should Not Be Equal As Strings    ${url_before}    ${url_after}

Get Page Header Text
    Wait Until Element Is Enabled    xpath=.//*[@class="title-feedback" and not(string())=""]
    ${header}    Get Text    xpath=.//*[@class="title-feedback" and not(string())=""]
    [Return]    ${header}

Get Group Name
    Wait Until Element Is Enabled    xpath=.//app-context-header//*[@class="method-or-group-title"]
    ${group_name}    Get Text    xpath=.//app-context-header//*[@class="method-or-group-title"]
    ${group_name}    Strip String    ${group_name}
    [Return]    ${group_name}

Get Number Of Visible Content Items
    ${content_items}    Get Element Count    xpath=.//app-content-item[not(contains(@class, "hidden"))]
    [Return]    ${content_items}

Click Filter Button
    Click Element On Page    xpath=.//*[@id="filter-bar-button"]

Open TOC
    ${menu_visible}    Run Keyword And Return Status    Element Should Be Visble    xpath:.//app-table-of-contents
    Run Keyword If    '${menu_visible}'=='False'    Click Element On Page    xpath:.//*[@id="menu-panel"]
    Wait Until Page Contains Element    xpath=//mat-sidenav[contains(@style, "visible")]//img[@class="small-cover-image"]    # Check image is shown in TOC
    Wait Until Page Contains Element    xpath=//mat-sidenav[contains(@style, "visible")]//div[@class="feed-name" and not(normalize-space(text())="")]    # Check feed name is shown in TOC
    Wait Until Element Is Enabled    xpath:.//app-table-of-contents
    Wait Until Element Is Enabled    xpath:.//*[@class="toc-button"]/*[@class="toc-expand-icon"]
    Wait Until Element Is Enabled    xpath:.//*[@class="toc-button"]/*[@class="toc-collapse-icon"]

Close TOC From Inside TOC
    Click Element On Page    xpath:.//app-table-of-contents//*[@class="close-button hover_link"]/img
    Wait Until Element Is Not Visible    xpath:.//app-table-of-contents

Close TOC From Top Menu
    Click Element On Page    xpath:.//*[@id="menu-panel"]
    Wait Until Element Is Not Visible    xpath:.//app-table-of-contents

Verify TOC Is Hidden
    Wait Until Element Is Not Visible    xpath:.//app-table-of-contents

Expand TOC Menu
    Click Element On Page    xpath:.//*[@class="toc-button"]/*[@class="toc-expand-icon"]

Collapse TOC Menu
    Click Element On Page    xpath:.//*[@class="toc-button"]/*[@class="toc-collapse-icon"]

Return To Content Feed From Content Item View
    Click Element On Page    xpath:.//*[contains(@class, "navigation-back")]
    Wait Until Content Feed Has Loaded
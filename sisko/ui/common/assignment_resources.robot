*** Keywords ***
Create Feed For Assignment
    [Arguments]    ${username}    ${userpassword}    ${from_feed}    ${from_feed_type}    ${new_feed_name_prefix}
    Login Kampus    ${username}    ${userpassword}
    ${name}=    Add Teacher Feed     ${from_feed}     on    ${from_feed_type}    ${new_feed_name_prefix}
    ${access_key}=    Get Access Key For Feed    ${name}
    Set Suite Variable    ${ACCESS_KEY_FOR_ASSIGNMENT_FEED}    ${access_key}
    Set Suite Variable    ${NAME_FOR_ASSIGNMENT_FEED}    ${name}
    Close Browser

Get Access Key For Feed
    [Documentation]    Get the access key for teacher feed. A new implementation for 'Create Access Key' keyword (in feed_resources) as some issues are seen at times with the existing keyword
    [Arguments]    ${teacher_feed_name}
    Verify Element Is Visible    xpath=//div[@id="accesskey-bar-button"]
    Focus And Click Element    xpath=//div[@id="accesskey-bar-button"]
    Verify Element Is Visible    xpath=//mat-dialog-container//span[@class="dialog-title-feed"][contains(text(), "${teacher_feed_name}")]    # Teacher feed name shown in dialog

    ${status}=    Run Keyword And Return Status    Element Should Be Visible    xpath=//label[@id="joining-allowed-switch"]/span[@class="no-tap-highlight"][@data-content="true"]    # Is Access key allowed check
    Run Keyword If    '${status}'=='False'    Focus And Click Element    xpath=//label[@id="joining-allowed-switch"]/span[@class="no-tap-highlight"]    # Turn on Access key allowed if not allowed already
    Verify Element Is Visible    xpath=//label[@id="joining-allowed-switch"]/span[@class="no-tap-highlight"][@data-content="true"]    # Verify Access key is allowed now

    Verify Element Is Visible    xpath=//div[@id="shared-accesskey" and not(normalize-space(text())="")]    # Verify Access key text element is found and not empty (or just spaces)
    ${access_key}=    Get Text    xpath=//div[@id="shared-accesskey"]
    ${access_key}=    Strip String    ${access_key}
    Length Should Be    ${access_key}    4    msg=Fetched Access Key length is not 4

    Focus And Click Element    xpath=//button[@id="share-accesskey-close"]
    Wait Until Page Does Not Contain Element    xpath=//div[contains(@class, "cdk-overlay-dark-backdrop")]
    [Return]    ${access_key}

Student Joins Feed Using Access Key
    [Arguments]    ${username}    ${userpassword}    ${feed_name_to_join}    ${access_key}
    Login Kampus And Join Feed    ${username}    ${userpassword}    ${feed_name_to_join}    ${access_key}
    Go To My Page
    Check Feed With Original Name Is Visible    ${feed_name_to_join}
    Close Browser

Cleanup Teacher Feeds
    [Documentation]    Intended to be used in Suite Setup and Suite Teardown to cleanup unwanted feeds created.
    ...                Currently, this is used only in Assignments Suite but can be generalized and used
    ...                across suites after evaluating this (Juha) and moving this keyword to appropriate location
    ...                such as 'sisko/ui/common/feed_resources.robot'
    [Arguments]    ${username}    ${userpassword}    ${feed_name_prefixes_to_delete}
    Login Kampus    ${username}    ${userpassword}
    Delete Assignment Teacher Feeds    ${feed_name_prefixes_to_delete}
    Close Browser

Delete Assignment Teacher Feeds
    [Documentation]    Remove all teacher feeds from My Page that match the given feed names to be deleted.
    ...                Currently, this is used only in Assignments Suite but can be generalized and used
    ...                across suites after evaluating this (Juha) and replacing existng keyword 'Delete Teacher Feeds'
    ...                in 'sisko/ui/common/feed_resources.robot'
    [Arguments]    ${feed_name_prefix}
    ${deletion_message}=    Get Confirmation Message For Deletion
    ${count}=    Get Element Count    xpath=//div[@class="teacher-feed-title"][text()[starts-with(.,'${feed_name_prefix}')]]
    @{feed_names}=    Create List
    FOR    ${index}    IN RANGE    1    ${count} + 1
        ${feed_name}=    Get Text    xpath:(//div[@class="teacher-feed-title"][text()[starts-with(.,'${feed_name_prefix}')]])[${index}]
        Append To List    ${feed_names}    ${feed_name}
    END
    FOR     ${feed_name}    IN    @{feed_names}
        ${feed_menu}=    Set Variable    //div[@class="teacher-feed-title"][text()='${feed_name}']/ancestor::div[@feed-type="teacher"]/button[contains(@class, "feed-menu-buttons")]
        Delete Assignment Teacher Feed    ${feed_name}    ${feed_menu}    ${deletion_message}
    END

Delete Assignment Teacher Feed
    [Documentation]    Remove a teacher feed from My Page matching feed name to be deleted.
    ...                Currently, this is used only in Assignments Suite but can be generalized and used
    ...                across suites after evaluating this (Juha) and replacing existng keyword 'Delete Teacher Feed'
    ...                in 'sisko/ui/common/feed_resources.robot'
    [Arguments]    ${feed_name}    ${feed_menu}    ${deletion_message}
    Open Feed Menu & Select "Delete this group" Menu Item    ${feed_menu}
    Enter Confirmation Text For Deletion    ${feed_name}    ${deletion_message}
    Verify Teacher Feed Is Deleted    ${feed_name}

Open Feed Menu & Select "Delete this group" Menu Item
    [Documentation]    Ideally, to be moved to 'sisko/ui/common/feed_resources.robot' if new delete feed keywords
    ...                are also moved (after evaluation by Juha)
    [Arguments]    ${feed_menu}
    Focus And Click Element    ${feed_menu}
    Focus And Click Element    xpath://button[contains(@id, "delete-feed-button")]

Verify Teacher Feed Is Deleted
    [Documentation]    Ideally, to be moved to 'sisko/ui/common/feed_resources.robot' if new delete feed keywords
    ...                are also moved (after evaluation by Juha)
    [Arguments]    ${feed_name}
    Wait Until Page Does Not Contain Element    xpath=//div[text()="${feed_name}"]

Login Kampus And Select Feed
    [Arguments]    ${username}    ${userpassword}    ${feed_name}    ${feed_type}
    Login Kampus    ${username}    ${userpassword}
    ${locale}=    Get Locale
    Set Test Variable    ${LOCALE}    ${locale}    # ${LOCALE} will be available for Test Case scope
    Select Feed    ${feed_name}    ${feed_type}

Cleanup Assignments In Feed
    [Documentation]    Intended to be used in Test Setup or Test Teardown.
    ...                 Currently, there doesn't seem to be a better logic to uniquely identify an assignment from list
    ...                 other than deleting assignments between test cases
    [Arguments]    ${username}    ${userpassword}    ${feed_name}    ${feed_type}=${FEED_TYPE_TEACHER}
    Login Kampus And Select Feed    ${username}    ${userpassword}    ${feed_name}    ${feed_type}
    Open Assignments Panel
    Delete Assignments In Feed
    Close Browser

Delete Assignments In Feed
    [Documentation]    Remove all assignments in feed in reverse order (newest assignment deleted first).
    FOR    ${index}    IN RANGE    ${PREV_ASSIGNMENTS_COUNT}
        Expand Last Assignment Card
        Delete Assignment
    END

Get Count Of Assignments
    [Documentation]    Find the number of assignments listed in assignments panel
    ${count}=    Get Element Count    xpath=//app-assignment-sidebar//li[contains(@class, "assignments-list__item")]
    [Return]    ${count}

Get Count Of Published Assignments From Assignments Cart
    [Documentation]    Get the number of published assignments shown in nav-circle of assignments panel
    ${count_text}=    Get Text    xpath=//mat-sidenav-content//div[contains(@class, "published-assignments")]
    ${count_text}=    Strip String    ${count_text}
    ${count}=    Should Match Regexp    ${count_text}    ^\\d+    # Safari on mac fix
    [Return]    ${count}

Get Count Of Included Items In Assignment From Assignments Cart
    [Documentation]    Get the number of included items for assignment in edit/new view, shown in nav-circle of assignments panel
    ${count_text}=    Get Text    xpath=//mat-sidenav-content//div[contains(@class, "selected-assignments")]
    ${count_text}=    Strip String    ${count_text}
    ${count}=    Should Match Regexp    ${count_text}    ^\\d+
    [Return]    ${count}

Expand Last Assignment Card
    [Documentation]    Expand the last assignment in Assignments list (if not expanded already).
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    xpath=(//mat-expansion-panel)[${PREV_ASSIGNMENTS_COUNT}][contains(@class, "mat-expanded")]
    Run Keyword If    ${status}==False    Focus And Click Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[${PREV_ASSIGNMENTS_COUNT}]
    Wait For Condition  return window.document.readyState === 'complete'
    Verify Element Is Visible    xpath=(//mat-expansion-panel)[${PREV_ASSIGNMENTS_COUNT}][contains(@class, "mat-expanded")]/mat-expansion-panel-header[contains(@class, "mat-expanded")]    # Asssignment card is expanded
    Verify Element Is Visible    xpath=(//mat-expansion-panel)[${PREV_ASSIGNMENTS_COUNT}]/div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]
    Custom Scroll To Specific Element    xpath=(//mat-expansion-panel)[${PREV_ASSIGNMENTS_COUNT}]/div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]    alignment=top

Delete Assignment
    [Documentation]    Remove the assignment that has been currently expanded. Used in Test teardowns
    Verify Element Is Visible    xpath=//button[@test-delete]
    Custom Scroll To Specific Element    xpath=//button[@test-delete]    alignment=top    # Additional safeguards to avoid ElementNotInteractableException seen with Chrome at times
    Focus And Click Element    xpath=//button[@test-delete]

    ${status}=    Run Keyword And Return Status    Verify Element Is Visible    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--primary"]
    Run Keyword If    ${status}==False    Log    Delete Assignment from Read View failed as Confirm delete not shown, Skipping further steps, Not marking as failure    WARN    console=True
    Run Keyword If    ${status}==False    Capture Page Screenshot
    Return From Keyword If    ${status}==False
    Confirm Assignment Delete

Confirm Assignment Delete
    [Documentation]    Click on the confirmation button to delete the assignment
    Focus And Click Element    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--primary"]
    Wait Until Page Does Not Contain Element    xpath=//button/i[contains(@class, "icon__delete")]    # To verify delete from Read view
    Wait Until Page Does Not Contain Element    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]    # To verify delete from Edit view
    ${curr_count}=    Get Count Of Assignments
    Run Keyword If    ${PREV_ASSIGNMENTS_COUNT} - ${curr_count} != 1    Fail    Assignment delete had failed as number of asignments after delete is not 1 less than before delete
    Set Test Variable    ${PREV_ASSIGNMENTS_COUNT}    ${curr_count}    # Setting test variable to new assignment count after delete

Cancel Assignment Delete
    [Documentation]    Click on the confirmation button to delete the assignment
    Focus And Click Element    xpath=//*[@id="mat-dialog-0"]//button[@class="dialog-actions__btn dialog-actions__btn--secondary"]
    Verify Element Is Visible    xpath=//button/i[contains(@class, "icon__delete")]

Reload Feed
    [Documentation]    Reload the current feed, verify the content is showing up & assignments content panel loaded
    Reload Page
    Wait Until Content Feed Has Loaded
    Wait Until Page Contains Element    xpath=//div[contains(@class, "assignments__content")]    # Check Assignments content panel has loaded

# Block holding keywords for TOC related actions such as Opening Chapter: START

Get Current Chapter Title
    [Documentation]    Returns the Chapter title of the current chapter user is in
    ${current_chapter_title}=    Get Text    xpath=//app-chapter-title//span[@class="title-zero"]    # Chapter Title
    [Return]    ${current_chapter_title}

Open Chapter "${chapter_name}"
    [Documentation]
    Open TOC    # from 'feed_resources.robot'
    ${chapter_in_toc}=    Set Variable    //mat-sidenav[@id="dropdown-sidebar"]//div[contains(@class, "title-text") and contains(text(), "${chapter_name}")]
    Wait Until Page Contains Element    xpath=${chapter_in_toc}
    Wait Until Element Is Enabled    xpath=${chapter_in_toc}
    Custom Scroll To Specific Element    xpath=${chapter_in_toc}
    Focus And Click Element    xpath=${chapter_in_toc}
    Wait Until Page Does Not Contain Element    xpath=//div[@id="menu-panel"][contains(@class, "selected")]    # Verify TOC is closed
    # NOTE: Wait until loading icon is not seen. 20secs may not be enough as errors were seen at times so increased to 60 secs
    Wait Until Keyword Succeeds    60s    300ms    Page Should Not Contain Element    xpath=//div[@class="feed-container"]/div[contains(@class, "loading")]
    Verify Element Is Visible    xpath=//app-chapter-title//span[@class="title-zero" and contains(text(), "${chapter_name}")]    # Verify selected chapter title is visible
    Sleep    10s    # This is needed as after few secs of chapter load, bookmark api request is made which has an impact on the dom

Proceed To Chapter
    [Arguments]    ${chapter_name}
    ${current_chapter_title}=    Get Current Chapter Title
    ${status}=    Run Keyword And Return Status    Should Contain    ${current_chapter_title}    ${chapter_name}
    Run Keyword If    ${status}==False      Open Chapter "${chapter_name}"

# Block holding keywords for TOC related actions such as Opening Chapter: END

Open Assignments Panel
    [Documentation]    Click on Assignments icon in Content header
    ${pub_assignments}=    Get Count Of Published Assignments From Assignments Cart
    Set Test Variable    ${PUBLISHED_ASSIGNMENTS_COUNT}    ${pub_assignments}

    ${count}=    Get Count Of Assignments
    Set Test Variable    ${PREV_ASSIGNMENTS_COUNT}    ${count}
    ${status}=    Run Keyword And Return Status    Element Should Be Enabled    xpath=//button[@id="assignments-panel-toggle"][contains(@class, "selected")]
    Run Keyword If    '${status}'=='False'    Focus And Click Element    xpath=//button[@id="assignments-panel-toggle"]
    Wait Until Page Contains Element    xpath=//button[@id="assignments-panel-toggle"][contains(@class, "selected")]
    Wait Until Element Is Visible    xpath=//h2[@class="assignments__title"]

Verify Create Button Is Not Shown In Assignments Panel
    [Documentation]    Applies for Student user, Create button should not be shown in Assignments panel
    Page Should Not Contain Element    xpath=//button[contains(@class, "assignments__create")]    # Create button not shown

Get Count of Assignments In Draft Status
    [Documentation]    Returns number of assignments with Draft status (For Students, this should be 0 always)
    ${draft_assignments_count}=    Get Element Count    xpath=//li[contains(@class, "assignments-list__item")]//span[contains(@class, "chip--blue")]
    [Return]    ${draft_assignments_count}

Verify Draft Assignments Not Listed
    [Documentation]    Number of assignments in draft status is 0
    ${draft_assignments_count}=    Get Count of Assignments In Draft Status
    Run Keyword If    ${draft_assignments_count} != 0    Fail    Draft Assignments found in Assignments panel when it was expected otherwise

Setup Lists For Tracking Content Items In Assignment
    [Documentation]    Creates empty list variable in Test scope which will be later used to track titles part of assignment when Adding and Removing items
    ${variable}=    Get Variable Value    @{ALL_ITEMS_IN_ASSIGNMENT_LIST}    undefined
    Run Keyword If    '${variable}'=='undefined'    Set Test Variable    @{ALL_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${variable}=    Get Variable Value    @{THEORY_ITEMS_IN_ASSIGNMENT_LIST}    undefined
    Run Keyword If    '${variable}'=='undefined'    Set Test Variable    @{THEORY_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${variable}=    Get Variable Value    @{LINK_ITEMS_IN_ASSIGNMENT_LIST}    undefined
    Run Keyword If    '${variable}'=='undefined'    Set Test Variable    @{LINK_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${variable}=    Get Variable Value    @{EXERCISE_ITEMS_IN_ASSIGNMENT_LIST}    undefined
    Run Keyword If    '${variable}'=='undefined'    Set Test Variable    @{EXERCISE_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}

Create New Assignment
    [Documentation]    Click On "Create new" assignment button
    Setup Lists For Tracking Content Items In Assignment
    Verify Element Is Visible    xpath=//button[contains(@class, "assignments__create")]
    Focus And Click Element    xpath=//button[contains(@class, "assignments__create")]
    Wait Until Page Contains Element    xpath=//app-assignment-form//span[contains(@class, "chip--blue")]    # DRAFT text
    Verify Element Is Visible    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]    # Delete button is visible

Verify Due Date Validations
    [Documentation]    Verify Due Date Validations are in place in Assignment form view
    Verify Due Date Validation For Invalid Date Format
    Verify Due Date Validation For Past Date
    Verify Due Date Validation For Date Over 1 Year In Future
    Add Valid Due Date (Future Date) By Typing In
    Add Valid Due Date (Current Date) By Typing In
    Add Valid Due Date Using Calendar

Verify Due Date Validation For Invalid Date Format
    Verify Due Date Is Validated As Expected    invalid_date_value    ${DUEDATE_INVALID_FORMAT_ERROR_${LOCALE}}

Verify Due Date Validation For Past Date
    ${yesterday_date}=    Get Current Date    increment=-1 days    result_format=${DUEDATE_FORMAT_${LOCALE}}
    Verify Due Date Is Validated As Expected    ${yesterday_date}    ${DUEDATE_PAST_DATE_ERROR_${LOCALE}}

Verify Due Date Validation For Date Over 1 Year In Future
    ${over1year_future_date}=    Get Current Date    increment=366 days    result_format=${DUEDATE_FORMAT_${LOCALE}}
    Verify Due Date Is Validated As Expected    ${over1year_future_date}    ${DUEDATE_FUTURE_DATE_OVER_1_YEAR_ERROR_${LOCALE}}

Verify Due Date Validation For Valid Date In Future
    ${after1week_future_date}=    Get Current Date    increment=7 days    result_format=${DUEDATE_FORMAT_${LOCALE}}
    Verify Due Date Is Validated As Expected    ${after1week_future_date}    None

Verify Due Date Validation For Valid Current Date
    ${current_date}=    Get Current Date    result_format=${DUEDATE_FORMAT_${LOCALE}}
    Verify Due Date Is Validated As Expected    ${current_date}    None

Verify Due Date Is Validated As Expected
    [Documentation]    Verify that the actual due date validation error (if any) for given input date matches
    ...                 expected result
    [Arguments]    ${date_to_input}    ${expected_date_error}
    Input Due Date    ${date_to_input}
    ${status}    ${actual_date_error}=    Run Keyword And Ignore Error    Get Text    xpath=//mat-error[contains(@class, "mat-error")]
    ${actual_date_error}=    Set Variable If    '${status}'=='PASS'    ${actual_date_error}
    ...                      '${status}'=='FAIL'    None
    ${actual_date_error}=    Strip String    ${actual_date_error}    characters=.
    Should Be Equal As Strings    ${expected_date_error}    ${actual_date_error}

Input Due Date
    [Arguments]    ${date}
    Wait Until Element Is Enabled    id:dueDate
    Set Focus To Element    id:dueDate
    Clear Due Date
    Input Text    id:dueDate    ${date}    clear=True
    Click Element    id:instructions    # Click on Instructions to remove focus from Due date field
    ${entered_date}=    Get Value    id:dueDate
    Should Be Equal As Strings    ${date}    ${entered_date}

Add Valid Due Date (Future Date) By Typing In
    [Documentation]    Verify valid due date (future date) can be typed in and no validation error is raised
    Verify Due Date Validation For Valid Date In Future

Add Valid Due Date (Current Date) By Typing In
    [Documentation]    Verify valid due date (current date) can be typed in and no validation error is raised
    Verify Due Date Validation For Valid Current Date

Add Valid Due Date Using Calendar
    [Documentation]    Make use of calendar to select a valid due date
    Click Calendar Icon
    Proceed To Next Month
    Select First Day Of Month
    Verify Date Selected In Calendar Is Set To Due Date

Click Calendar Icon
    Focus And Click Element    xpath=//i[@class="icon__calendar"]    # Click Calendar icon
    Wait Until Page Contains Element    xpath=//mat-datepicker-toggle[contains(@class, "mat-datepicker-toggle-active")]    # Verify Datepicker is active
    Verify Element Is Visible    xpath=//mat-calendar//div[contains(@class, "mat-calendar-body-today")]

Proceed To Next Month
    Focus And Click Element    xpath=//mat-calendar//button[contains(@class, "mat-calendar-next-button")]    # Next month button
    Wait Until Page Contains Element    xpath=//mat-calendar//button[contains(@class, "mat-calendar-previous-button")][@ng-reflect-disabled="false"]    # Previous month button
    Verify Element Is Visible    xpath=(//mat-calendar//div[@class="mat-calendar-body-cell-content"])[1]    # First day of next month

Select First Day Of Month
    Focus And Click Element    xpath=(//mat-calendar//div[@class="mat-calendar-body-cell-content"])[1]    # Click first day of next month
    Wait Until Page Does Not Contain Element    xpath=//mat-datepicker-toggle[contains(@class, "mat-datepicker-toggle-active")]
    Verify Element Is Visible    id:dueDate

Verify Date Selected In Calendar Is Set To Due Date
    ${expected_date}=    Get First Date Of Next Month
    ${calendar_selected_date}=    Get Value    id:dueDate
    Should Be Equal As Strings    ${expected_date}    ${calendar_selected_date}    msg=Date entered using datepicker is not as expected

Add Instructions
    [Documentation]    Add instructions
    [Arguments]    ${instructions_text}
    Wait Until Element Is Enabled    id:instructions
    Set Focus To Element    id:instructions
    Clear Instructions
    Input Text    id:instructions    ${instructions_text}    clear=True
    Click Element    id:mat-hint-0    # Click on word counter hint to remove focus from Instructions field
    Textarea Value Should Be    id:instructions    ${instructions_text}

# Block holding keywords for adding content items to Assignment: START

Get Count Of Content Items In Assignment
    [Documentation]    Return the number of content items in the current assignment edit (or new) view
    ${count}=    Get Element Count    xpath=//div[@class="selected"]//li[contains(@class, "selected__item")]
    [Return]    ${count}

Get Count Of Addable Items In Chapter
    [Documentation]    Returns the current number of addable content items in current chapter based on provided xpath (for specific type)
    [Arguments]    ${addable_items_locator}
    ${count}=    Get Element Count    xpath=${addable_items_locator}
    [Return]    ${count}

Setup Step For Adding/Removing Content Item To Assignment
    [Documentation]    Common step to setup needed test case level variables prior to adding/removing content item from/to assignment
    [Arguments]    ${addable_items}    ${item_type}
    ${prev_items_count}=    Get Count Of Content Items In Assignment
    Set Test Variable    ${PREV_ITEMS_IN_ASSIGNMENT_COUNT}    ${prev_items_count}
    ${prev_count}=    Get Count Of Addable Items In Chapter    ${addable_items}
    Run Keyword If    '${item_type}'=='theory'    Set Test Variable    ${PREV_THEORY_COUNT}    ${prev_count}
    ...    ELSE IF    '${item_type}'=='link'    Set Test Variable    ${PREV_LINK_COUNT}    ${prev_count}
    ...    ELSE IF    '${item_type}'=='exercise'    Set Test Variable    ${PREV_EXERCISE_COUNT}    ${prev_count}

Verify Included Items Count In Assignment Cart Matches Number Of Items Listed
    [Documentation]    Verify included items count for assignment displayed in Assignment cart nav-circle in edit/new view matches number of content items listed in assignment
    ${included_items_count}=    Get Count Of Included Items In Assignment From Assignments Cart
    ${curr_items_count}=    Get Count Of Content Items In Assignment
    Should Be Equal As Integers    ${included_items_count}    ${curr_items_count}    msg=Included Items count in Assignment cart does not match the number of items listed in Assignment edit view

Verify Increase In Items Selected List For Assignment
    [Documentation]    Verify number of content items in assignment increases by 1
    ${curr_items_count}=    Get Count Of Content Items In Assignment
    Run Keyword If    ${curr_items_count} - ${PREV_ITEMS_IN_ASSIGNMENT_COUNT} != 1    Fail    Adding content item to assignment failed as content items count in assignment did not increase by 1
    Set Test Variable    ${PREV_ITEMS_IN_ASSIGNMENT_COUNT}    ${curr_items_count}    # ${PREV_ITEMS_IN_ASSIGNMENT_COUNT} will be one more

Verify Decrease In Count Of Addable Items In Chapter
    [Documentation]    Verify number of addable items of this type in content feed chapter decreases by 1
    [Arguments]    ${addable_items_locator}    ${item_type}
    ${curr_count}=    Get Count Of Addable Items In Chapter    ${addable_items_locator}
    ${prev_count}=    Set Variable If    '${item_type}'=='theory'    ${PREV_THEORY_COUNT}
    ...                                  '${item_type}'=='link'    ${PREV_LINK_COUNT}
    ...                                  '${item_type}'=='exercise'    ${PREV_EXERCISE_COUNT}
    Run Keyword If    ${prev_count} - ${curr_count} != 1    Fail    Adding content item to assignment failed as addable item counts did not decrease by 1
    Run Keyword If    '${item_type}'=='theory'    Set Test Variable    ${PREV_THEORY_COUNT}    ${curr_count}    # ${PREV_THEORY_COUNT} will be one less
    ...    ELSE IF    '${item_type}'=='link'    Set Test Variable    ${PREV_LINK_COUNT}    ${curr_count}
    ...    ELSE IF    '${item_type}'=='exercise'    Set Test Variable    ${PREV_EXERCISE_COUNT}    ${curr_count}

Click '+' Button In Content Item
    [Documentation]    From a content item (which can be addable to assignment), click on '+' button to add item to assignment
    [Arguments]    ${plus_button_locator}    ${content_item_title}
    Custom Scroll To Specific Element    xpath=${plus_button_locator}
    Focus And Click Element    xpath=${plus_button_locator}
    Verify Element Is Visible    xpath=//div[normalize-space(text())="${content_item_title}"]/parent::div/parent::a/following-sibling::label[contains(@class, "assignment-toggle--checked")]
    Verify Element Is Visible    xpath=//div[contains(@class, "content-item__content")]/span[normalize-space(text())="${content_item_title}"]    # Item added to assignment form

Click More (...) Button In Content Item
    [Documentation]    From a content item (which can be addable to assignment), click on ... button to show options in popup
    [Arguments]    ${more_button_locator}
    Custom Scroll To Specific Element    xpath=${more_button_locator}
    Focus And Click Element    xpath=${more_button_locator}
    Verify Element Is Visible    xpath=//button[contains(@class, "mat-menu-item")][@test-assignments-toggle]

Click 'Add to assignment' In Menu Popup For Content Item
    [Documentation]    From a content item (which can be added to assignment), in the resulting menu popup after
    ...                 clicking '...' button, Click on 'Add to assignment' menu item
    [Arguments]    ${content_item_title}
    Focus And Click Element    xpath=//button[contains(@class, "mat-menu-item")][@test-assignments-toggle]
    Verify Element Is Visible    xpath=//div[normalize-space(text())="${content_item_title}"]/parent::div/parent::a/following-sibling::label[contains(@class, "assignment-toggle--checked")]
    Verify Element Is Visible    xpath=//div[contains(@class, "content-item__content")]/span[normalize-space(text())="${content_item_title}"]    # Item added to assignment form

Add Multiple Content Items To Assignment
    [Documentation]    Add 2 theory, 2 link and 2 exercise items from the current chapter to the assignment
    Add "Theory" Item To Assignment By Clicking on "+" Icon
    Add "Theory" Item To Assignment By Using "Add to assignment" Option
    Add "Link" Item To Assignment By Clicking on "+" Icon
    Add "Link" Item To Assignment By Using "Add to assignment" Option
    Add "Exercise" Item To Assignment By Clicking on "+" Icon
    Add "Exercise" Item To Assignment By Using "Add to assignment" Option

Add "${type}" Item To Assignment By Clicking on "+" Icon
    [Documentation]    Add an item (which is available for adding) to assignment by clicking on the "+" icon in content item.
    ...                 This keyword is reusable for adding theory, link or exercise item based on ${type} provided as embedded argument in keyword name
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${addable_itemtype_items}=    Set Variable    //a[contains(@class, "${type_lower}-wrapper")]/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]/i
    Setup Step For Adding/Removing Content Item To Assignment    ${addable_itemtype_items}    ${type_lower}

    Run Keyword If    ${PREV_${type_upper}_COUNT}==0    Log    SKIPPING Adding Item: ${type} item cannot be added as there are no more addable ${type} items in current chapter    WARN    console=True
    Return From Keyword If    ${PREV_${type_upper}_COUNT}==0

    ${index}=    Evaluate    random.randint(1, ${PREV_${type_upper}_COUNT})
    ${title_name_to_add}=    Get Text    xpath=(${addable_itemtype_items})[${index}]/parent::label/preceding-sibling::a//div[contains(@class, "${type_lower}-title")]/div
    ${title_name_to_add}=    Strip String    ${title_name_to_add}

    Click '+' Button In Content Item    (${addable_itemtype_items})[${index}]    ${title_name_to_add}
    Verify Increase In Items Selected List For Assignment
    Verify Included Items Count In Assignment Cart Matches Number Of Items Listed    # Validate count shown in nav-circle in assignment new/edit view
    Verify Decrease In Count Of Addable Items In Chapter    ${addable_itemtype_items}    ${type_lower}
    Append To List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_add}
    Append To List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_add}

Add "${type}" Item To Assignment By Using "Add to assignment" Option
    [Documentation]    Add an item (which is available for adding) to assignment by choosing more and selecting "Add to assignment" from the corresponding content item.
    ...                 This keyword is reusable for adding theory, link or exercise item based on ${type} provided as embedded argument in keyword name
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${addable_itemtype_items}=    Set Variable    //a[contains(@class, "${type_lower}-wrapper")]/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]
    Setup Step For Adding/Removing Content Item To Assignment    ${addable_itemtype_items}    ${type_lower}

    Run Keyword If    ${PREV_${type_upper}_COUNT}==0    Log    SKIPPING Adding Item: ${type} item cannot be added as there are no more addable ${type} items in current chapter    WARN    console=True
    Return From Keyword If    ${PREV_${type_upper}_COUNT}==0

    ${index}=    Evaluate    random.randint(1, ${PREV_${type_upper}_COUNT})
    ${title_name_to_add}=    Get Text    xpath=(${addable_itemtype_items})[${index}]/preceding-sibling::a//div[contains(@class, "${type_lower}-title")]/div
    ${title_name_to_add}=    Strip String    ${title_name_to_add}

    ${more_button_locator}=    Set Variable    (${addable_itemtype_items})[${index}]/preceding-sibling::a//div[contains(@class, "more-button")]
    Click More (...) Button In Content Item    ${more_button_locator}

    Click 'Add to assignment' In Menu Popup For Content Item    ${title_name_to_add}
    Verify Increase In Items Selected List For Assignment
    Verify Included Items Count In Assignment Cart Matches Number Of Items Listed    # Validate count shown in nav-circle in assignment new/edit view
    Verify Decrease In Count Of Addable Items In Chapter    ${addable_itemtype_items}    ${type_lower}
    Append To List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_add}
    Append To List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_add}

# Block holding keywords for adding content items to Assignment: END

Back To Assignments List From New Assignment View
    [Documentation]    Click on 'Back to assignments list' from New Assignment form view
    ...                 A new draft assignment will be created
    Focus And Click Element    xpath=//button[@class="back"]
    Verify Element Is Visible    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]
    ${curr_count}=    Get Count Of Assignments
    Run Keyword If    ${curr_count} - ${PREV_ASSIGNMENTS_COUNT} != 1    Fail    Back To Assignments List From New Assignment View: Assignment creation failed as assignments count has not increased by 1
    Set Test Variable    ${PREV_ASSIGNMENTS_COUNT}    ${curr_count}    # Setting suite variable to new count after creating draft assignment

Verify Card Summary For Empty Draft Assignment
    [Documentation]    Verify from empty draft assignment content card header that due date is not shown and
    ...                 icons for exercise types are also not shown
    Verify Status Of Assignment Is Shown As Draft
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Assignment Due Date (DD.MM.) Is Not Shown
    Verify Assignment Items Summary Section Is Not Shown

Verify Read View Of Draft Assignment (Instructions & Items Not Set)
    [Documentation]    Verify expanded (Read) view for draft assignment does not contain instructions and
    ...                 list of content items, Edit & Delete buttons should be visible
    Verify Read View Of Assignment Does Not Contain Instructions
    Verify Read View Of Assignment Does Not Contain Content Items
    Verify Read View Of Assignment Contains Edit Button
    Verify Read View Of Assignment Contains Delete Button

Verify Card Summary For Draft Assignment (Due Date Set, Items Not Added)
    [Documentation]    Verify from empty draft assignment content card header that due date is shown but
    ...                 icons for exercise types are not shown
    Verify Status Of Assignment Is Shown As Draft
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Assignment Due Date (DD.MM.) Is Shown
    Verify Assignment Items Summary Section Is Not Shown

Verify Read View Of Draft Assignment (Instructions Set, Items Not Set)
    [Documentation]    Verify expanded (Read) view for draft assignment contains instructions, Edit & Delete buttons
    ...                 but list of content items should not be visible
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Does Not Contain Content Items
    Verify Read View Of Assignment Contains Edit Button
    Verify Read View Of Assignment Contains Delete Button

Verify Card Summary For Draft Assignment (Due Date Set, Items Set)
    [Documentation]    Verify from empty draft assignment content card header that due date is shown and
    ...                 icons for exercise types are also shown
    Set Test Variable    ${ASSIGNMENT_STATUS}    Draft
    Verify Status Of Assignment Is Shown As Draft
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Assignment Due Date (DD.MM.) Is Shown
    Verify Assignment Items Summary Section Is Shown

Verify Read View Of Draft Assignment (Instructions Set, Items Set)
    [Documentation]    Verify expanded (Read) view for draft assignment contains instructions, Edit & Delete buttons
    ...                 and list of content items should also be visible
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Contain Content Items
    Verify Read View Of Assignment Contains Edit Button
    Verify Read View Of Assignment Contains Delete Button

Verify Status Of Assignment Is Shown As Draft
    Wait Until Page Contains Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//span[contains(@class, "chip--blue")]

Verify Status Of Assignment Is Not Shown As Draft
    [Documentation]    Check applies for puclished and closed assignments
    Wait Until Page Does Not Contain Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//span[contains(@class, "chip--blue")]    # Draft icon not shown in summary

Verify Assignment Created Day Of Week Is Shown
    Wait Until Page Contains Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//span[@class="given-date__day"]
    ${given_date_day}=    Get Text    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//span[@class="given-date__day"]    # Check day is shown e.g. la
    ${given_date_day}=    Strip String    ${given_date_day}
    Should Not Be Empty    ${given_date_day}    msg=Assignment created day of the week is empty in Assignment Card

Verify Assignment Created Date (DD.MM.) Is Shown
    Wait Until Page Contains Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//time[@class="given-date__numeric"]
    ${given_date_ddmm}=    Get Text    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//time[@class="given-date__numeric"]    # Check date in dd.mm format shown e.g. 04.07
    ${given_date_ddmm}=    Strip String    ${given_date_ddmm}
    Should Not Be Empty    ${given_date_ddmm}    msg=Assignment created date is empty in Assignment Card
    ${length}=    Get Length    ${given_date_ddmm}
    Should Be Equal As Integers    ${length}    6    msg=Assignment created date is NOT of the format dd.mm. in Assignment Card

Verify Assignment Due Date (DD.MM.) Is Shown
    Wait Until Page Contains Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//time[@class="due-date__numeric"]
    ${due_date_ddmm}=    Get Text    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]//time[@class="due-date__numeric"]
    ${due_date_ddmm}=    Strip String    ${due_date_ddmm}
    ${length}=    Get Length    ${due_date_ddmm}
    Should Be Equal As Integers    ${length}    6

Verify Assignment Due Date (DD.MM.) Is Not Shown
    Wait Until Page Does Not Contain Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]/app-due-date-tag

Verify Assignment Items Summary Section Is Not Shown
    Wait Until Page Does Not Contain Element    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]/div[@class="summary"]/div

Verify Assignment Items Summary Section Is Shown
    [Documentation]    Verify from Assignment card (header) specific content item icons and counts are showing up as expected (after assignment has been created / updated)
    ...                 or any content item icons and counts are showing up (to check previously available assignment card)
    ${num_item_type_icons}=    Get Element Count    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]/div[@class="summary"]//i[contains(@class, "icon__")]    # General any icon
    Run Keyword If    ${num_item_type_icons}==0    Fail    msg=No content type icons (Theory, Link, Exercises icons) present in Assignment card
    ${num_item_type_counts}=    Get Element Count    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]/div[@class="summary"]//i[contains(@class, "icon__")]/following-sibling::span[@class="vis-hidden"]    # General: Any count is seen
    Run Keyword If    ${num_item_type_counts}==0    Fail    msg=No content type counts (Theory, Link, Exercises counts) present in Assignment card

    Verify "Theory" Icon And Count Assignment Items Summary Section
    Verify "Link" Icon And Count Assignment Items Summary Section
    Verify "Exercise" Icon And Count Assignment Items Summary Section

Verify "${type}" Icon And Count Assignment Items Summary Section
    [Documentation]    Verify from Assignment card (header) specific content item icons and counts are showing up as expected (after assignment has been created / updated)
    ...                 or any content item icons and counts are showing up (to check previously available assignment card)
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${itemtype_items}=    Get Variable Value    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${itemtype_items_len}=    Get Length    ${itemtype_items}
    ${icon_element}=    Set Variable    (//mat-expansion-panel-header//div[@class="heading"])[last()]/div[@class="summary"]//i[contains(@class, "icon__${type_lower}")]
    ${status}    ${value}=    Run Keyword And Ignore Error    Page Should Contain Element    xpath=${icon_element}      # Theory/Link/Exercise icon
    Run Keyword If    ${itemtype_items_len}>0 and '${status}'=='FAIL'    Fail    msg=${type} icon not present in Assignment card when it was expected to be present

    Run Keyword If    '${status}'=='PASS'    Verify Icon Color Matches Expected Color For Status    ${icon_element}    ${ASSIGNMENT_STATUS}    # Icon color check

    ${itemtype_counts_locator}=    Set Variable    ${icon_element}/following-sibling::span[@class="vis-hidden"]
    ${status}    ${itemtype_items_count_text}=    Run Keyword And Ignore Error    Execute JavaScript    return document.evaluate('${itemtype_counts_locator}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerHTML;
    ${itemtype_items_count}=    Run Keyword If    '${status}'=='PASS' and """${itemtype_items_count_text}"""!="${EMPTY}"    Should Match Regexp    ${itemtype_items_count_text}    ^\\d+

    Run Keyword If    '${status}'=='FAIL' and ${itemtype_items_len}>0    Fail    msg=${type} count not present in Assignment card when it was expected to be present
    ...    ELSE IF    '${status}'=='PASS' and ${itemtype_items_len}>0 and ${itemtype_items_count}!=${itemtype_items_len}    Fail    msg=${type} count in Assignment card did not match the expected count

Verify Icon Color Matches Expected Color For Status
    [Arguments]    ${icon_element}    ${assignment_status}
    ${status}    ${bg_color}=    Run Keyword And Ignore Error    Execute JavaScript    return document.defaultView.get
    ${icon_bg_color}=    Get CSS Background Color For Element    ${icon_element}
    ${expected_color_for_status}=    Set Variable If    '${assignment_status}'=='Draft'    rgb(100, 54, 149)
    ...                                                 '${assignment_status}'=='Published'    rgb(245, 143, 41)
    ...                                                 '${assignment_status}'=='Closed'    rgb(149, 150, 157)
    Should Be Equal    ${icon_bg_color}    ${expected_color_for_status}    msg=BG color for Icon element (${icon_bg_color}) did not match the expected color for ${assignment_status} status (${expected_color_for_status})

Verify Read View Of Assignment Contains Edit Button
    Wait Until Page Contains Element    xpath=//button/i[contains(@class, "icon__edit")]    # Edit button shown

Verify Read View Of Assignment Does Not Contain Edit Button
    [Documentation]    This check is applicable for Students accessing Assignment
    Wait Until Page Does Not Contain Element    xpath=//button/i[contains(@class, "icon__edit")]    # Edit button not shown

Verify Read View Of Assignment Contains Delete Button
    Wait Until Page Contains Element    xpath=//button/i[contains(@class, "icon__delete")]    # Delete button shown

Verify Read View Of Assignment Does Not Contain Delete Button
    [Documentation]    This check is applicable for Students accessing Assignment
    Wait Until Page Does Not Contain Element    xpath=//button/i[contains(@class, "icon__delete")]    # Delete button not shown

Verify Read View Of Assignment Contains Mark As Closed Button
    Wait Until Page Contains Element    xpath=//button/i[contains(@class, "icon__close")]    # Mark as closed button shown

Verify Read View Of Assignment Does Not Contain Mark As Closed Button
    [Documentation]    This check is applicable for Students accessing Assignment
    Wait Until Page Does Not Contain Element    xpath=//button/i[contains(@class, "icon__close")]    # Mark as closed button not shown

Verify Read View Of Assignment Does Not Contain Instructions
    Wait Until Page Does Not Contain Element    xpath=//div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]//p[contains(@class, "instructions")]    # Instructions not found

Verify Read View Of Assignment Contains Instructions
    Wait Until Page Contains Element    xpath=//div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]//p[contains(@class, "instructions")]    # Instructions found
    ${instructions}=    Get Text    xpath=//div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]//p[contains(@class, "instructions")]
    Should Not Be Empty    ${instructions}    msg=Instructions text in Assignment Read view empty, not as expected

Verify Read View Of Assignment Does Not Contain Content Items
    Wait Until Page Does Not Contain Element    xpath=//div[contains(@class, "mat-expansion-panel-content")][contains(@style, "visible")]//ul[contains(@class, "selected")]    # Content items are not found

Verify Read View Of Assignment Contain Content Items
    [Documentation]    Verify from Assignment read view (content) specific content item are showing up as expected (after assignment has been created / updated)
    ...                 or any content items are listed (to check previously available assignment card)

    ${all_items}=    Get Variable Value    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${all_items_len}=    Get Length    ${all_items}
    ${theory_items}=    Get Variable Value    ${THEORY_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${theory_items_len}=    Get Length    ${theory_items}
    ${link_items}=    Get Variable Value    ${LINK_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${link_items_len}=    Get Length    ${link_items}
    ${exercise_items}=    Get Variable Value    ${EXERCISE_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${exercise_items_len}=    Get Length    ${exercise_items}

    ${num_content_items}=    Get Element Count    xpath=(//div[contains(@class, "mat-expansion-panel-content")])[last()]//li    # List of content items in read view
    Run Keyword If    ${num_content_items}==0    Fail    msg=No content items present in Assignment read view
    ...    ELSE IF    ${num_content_items}>0 and ${all_items_len}>0 and ${num_content_items}!=${all_items_len}    Fail    msg=Number of content items in Assignment read view did not match the expected count

    @{all_item_titles_in_assignment}=    Create List
    @{theory_item_titles_in_assignment}=    Create List
    @{link_item_titles_in_assignment}=    Create List
    @{exercise_item_titles_in_assignment}=    Create List
    FOR    ${index}    IN RANGE    1    ${num_content_items} + 1
        ${title_locator}=    Set Variable    ((//div[contains(@class, "mat-expansion-panel-content")])[last()]//li//span[contains(@class, "content-item__title")])[${index}]
        ${item_title}=    Get Text    xpath=${title_locator}
        ${item_title}=    Strip String    ${item_title}
        Append To List    ${all_item_titles_in_assignment}    ${item_title}
        ${icon_locator}=    Set Variable    ((//div[contains(@class, "mat-expansion-panel-content")])[last()]//li//i[contains(@class, "content-item__icon--")])[${index}]
        ${icon_class_attribute}=    Run Keyword And Ignore Error    Get Element Attribute    xpath=${icon_locator}    class
        Run Keyword If    "theory" in """${icon_class_attribute}"""    Append To List    ${theory_item_titles_in_assignment}    ${item_title}
        ...    ELSE IF    "link" in """${icon_class_attribute}"""    Append To List    ${link_item_titles_in_assignment}    ${item_title}
        ...    ELSE IF    "exercise" in """${icon_class_attribute}"""    Append To List    ${exercise_item_titles_in_assignment}    ${item_title}

        ${icon_element}=    Set Variable    ((//div[contains(@class, "mat-expansion-panel-content")])[last()]//li//i[contains(@class, "content-item__icon")])[${index}]
        Verify Icon Color Matches Expected Color For Status    ${icon_element}    ${ASSIGNMENT_STATUS}    # Icon color check
    END

    Run Keyword If    ${all_items_len}==0    Set Test Variable    @{ALL_ITEMS_IN_ASSIGNMENT_LIST}    @{all_item_titles_in_assignment}
    ...    ELSE IF    ${all_items_len}>0    Lists Should Be Equal    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${all_item_titles_in_assignment}    msg=List of all content items in assignment read view does not match the list of all items added during assignment create/update
    Run Keyword If    ${theory_items_len}==0    Set Test Variable    @{THEORY_ITEMS_IN_ASSIGNMENT_LIST}    @{theory_item_titles_in_assignment}
    ...    ELSE IF    ${theory_items_len}>0    Lists Should Be Equal    ${THEORY_ITEMS_IN_ASSIGNMENT_LIST}    ${theory_item_titles_in_assignment}    msg=List of Theory items in assignment read view does not match the list of Theory items added during assignment create/update
    Run Keyword If    ${link_items_len}==0    Set Test Variable    @{LINK_ITEMS_IN_ASSIGNMENT_LIST}    @{link_item_titles_in_assignment}
    ...    ELSE IF    ${link_items_len}>0    Lists Should Be Equal    ${LINK_ITEMS_IN_ASSIGNMENT_LIST}    ${link_item_titles_in_assignment}    msg=List of Link items in assignment read view does not match the list of Link items added during assignment create/update
    Run Keyword If    ${exercise_items_len}==0    Set Test Variable    @{EXERCISE_ITEMS_IN_ASSIGNMENT_LIST}    @{exercise_item_titles_in_assignment}
    ...    ELSE IF    ${exercise_items_len}>0    Lists Should Be Equal    ${EXERCISE_ITEMS_IN_ASSIGNMENT_LIST}    ${exercise_item_titles_in_assignment}    msg=List of Exercise items in assignment read view does not match the list of Exercise items added during assignment create/update

# Block holding keywords for Editing existing Assignment: START

Proceed To Edit View Of Assignment
    [Documentation]    Edit the last assignment in the list of assignments. Argument is used to ignore or perform validations on item counts in edit view against counts from read view
    ...                 In some cases, this check should not be performed e.g. when proceeding to generic delete from assignment edit view (assignment may or may not contain items and with any status)
    [Arguments]    ${ignore_count_check}=False
    Verify Element Is Visible    xpath=//button/i[contains(@class, "icon__edit")]
    Custom Scroll To Specific Element    xpath=//button/i[contains(@class, "icon__edit")]    alignment=top    # Additional safeguards to avoid ElementNotInteractableException seen with Chrome at times
    Focus And Click Element    xpath=//button/i[contains(@class, "icon__edit")]
    Verify Element Is Visible    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]    # Delete button is visible

    ${all_items}=    Get Variable Value    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    @{EMPTY}
    ${all_items_len}=    Get Length    ${all_items}
    ${selected_count}=   Get Text    xpath=//span[contains(@class, "selected__title")]/strong
    Run Keyword If    ${ignore_count_check}==False    Should Be Equal As Integers    ${selected_count}    ${all_items_len}    msg=Selected count of items in Assignment Edit view does not match the expected count    values=False

Edit Due Date
    ${after2weeks_future_date}=    Get Current Date    increment=14 days    result_format=${DUEDATE_FORMAT_${LOCALE}}
    Verify Due Date Is Validated As Expected    ${after2weeks_future_date}    None

Edit Instructions
    [Arguments]    ${instructions_text}
    Add Instructions    ${instructions_text}

Edit Multiple Content Items In Assignment
    [Documentation]    Remove some existing items in assingment and add few new items
    Remove Multiple Content Items From Assignment
    Add Multiple Content Items To Assignment

Back To Assignments List From Edit Assignment View
    [Documentation]    Click on 'Back to assignments list' from Edit Assignment form view
    ...                 New assignment will not be created
    Focus And Click Element    xpath=//button[@class="back"]
    Verify Element Is Visible    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]

Clear Due Date
    Execute JavaScript    document.getElementById('dueDate').value="";

Clear Instructions
    Execute JavaScript    document.getElementById('instructions').value="";

Clear Content Items In Assignment
    [Documentation]    Remove all items currently in assignment by clicking on '...' and then clicking 'Remove' corresponding to each item in Assignment form.
    ${curr_items_count}=    Get Count Of Content Items In Assignment
    Set Test Variable    ${PREV_ITEMS_IN_ASSIGNMENT_COUNT}    ${curr_items_count}

    FOR    ${index}    IN RANGE    ${curr_items_count}    0    -1
        ${icon_class_attribute}=    Get Element Attribute    xpath=//div[@class="selected"]//li[contains(@class, "selected__item")][${index}]//i[contains(@class, "content-item__icon")]    class

        ${item_type}=    Set Variable If    "theory" in """${icon_class_attribute}"""    theory
        ...                                  "link" in """${icon_class_attribute}"""    link
        ...                                  "exercise" in """${icon_class_attribute}"""    exercise

        Remove "${item_type}" Item From Assignment Form View
    END

Rearrange Items In Assignment Edit View
    [Documentation]    Rearrange first 2 items in items list added to assignment
    ...                 Limitations: will work when there are atleast 3 items in assignment, do not procedd with rearranging if items in assignment edit view < 3
    ${curr_items_count}=    Get Count Of Content Items In Assignment
    Run Keyword If    ${curr_items_count} < 3    Log    SKIPPING Rearranging Items: Number of items in assignment edit view is less than 3, current implementation requires minimum 3 items to perform Rearranging successfully    WARN    console=True
    Return From Keyword If    ${curr_items_count} < 3

    ${first_item}=    Set Variable    (//div[@class="selected"]//li[contains(@class, "selected__item")])[1]
    ${second_item}=    Set Variable    (//div[@class="selected"]//li[contains(@class, "selected__item")])[2]/following-sibling::li[1]

    ${first_item_prev_title}=    Get Title Of Item With Index Position "1" In Assignment Form
    ${second_item_prev_title}=    Get Title Of Item With Index Position "2" In Assignment Form

    Custom Scroll To Specific Element    xpath=${first_item}
    Drag And Drop    xpath=${first_item}    xpath=${second_item}
    Sleep    1s

    ${first_item_new_title}=    Get Title Of Item With Index Position "1" In Assignment Form
    ${second_item_new_title}=    Get Title Of Item With Index Position "2" In Assignment Form
    Should Be Equal    ${first_item_new_title}    ${second_item_prev_title}
    Should Be Equal    ${second_item_new_title}    ${first_item_prev_title}

    Return From Keyword If    '${first_item_new_title}'=='${second_item_new_title}'    # Note: If First and second items have same title, no need to update the List variables, will cause problems if updated
    Update List Variables After Rearranging Items    ${first_item_new_title}    ${second_item_new_title}

Get Title Of Item With Index Position "${index_pos}" In Assignment Form
    ${item_title}=    Get Text    xpath=(//div[@class="selected"]//li[contains(@class, "selected__item")])[${index_pos}]//span[@class="content-item__title"]
    [Return]    ${item_title}

Update List Variables After Rearranging Items
    [Arguments]    ${first_item_title}    ${second_item_title}
    ${temp}=    Remove From List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    1
    ${temp}=    Remove From List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    0
    Insert Into List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    0    ${first_item_title}
    Insert Into List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    1    ${second_item_title}

    ${item_type_first}=    Get Item Type Of Title In Assignment Edit View Items List    //span[normalize-space(text())="${first_item_title}"]/parent::div/parent::div/i[contains(@class, "content-item__icon")]
    ${item_type_second}=    Get Item Type Of Title In Assignment Edit View Items List    //span[normalize-space(text())="${second_item_title}"]/parent::div/parent::div/i[contains(@class, "content-item__icon")]

    Return From Keyword If    '${item_type_first}'!='${item_type_second}'
    Remove Values From List    ${${item_type_first}_ITEMS_IN_ASSIGNMENT_LIST}    ${first_item_title}    ${second_item_title}
    Insert Into List    ${${item_type_first}_ITEMS_IN_ASSIGNMENT_LIST}    0    ${first_item_title}
    Insert Into List    ${${item_type_first}_ITEMS_IN_ASSIGNMENT_LIST}    1    ${second_item_title}

Get Item Type Of Title In Assignment Edit View Items List
    [Arguments]    ${item_title}
    ${item_icon_class_attribute}=    Get Element Attribute    xpath=${item_title}    class
    ${item_type_upper}=    Set Variable If    "theory" in """${item_icon_class_attribute}"""    THEORY
    ...                                       "link" in """${item_icon_class_attribute}"""    LINK
    ...                                       "exercise" in """${item_icon_class_attribute}"""    EXERCISE
    [Return]    ${item_type_upper}

# Block holding keywords for Editing existing Assignment: END

# Block holding keywords for Deleting Assignment From Edit view: START

Get Due Date Value In Edit View
    ${due_date}=    Execute JavaScript    return document.getElementById('dueDate').value;
    [Return]    ${due_date}

Get Instructions Value In Edit View
    ${instructions}=    Execute JavaScript    return document.getElementById('instructions').value;
    [Return]    ${instructions}

Get Selected Value In Edit View
    ${selected_count}=    Execute JavaScript    return document.evaluate('//span[contains(@class, "selected__title")]/strong', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerHTML;
    [Return]    ${selected_count}

Click Delete Button In Edit View
    Verify Element Is Visible    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]
    Custom Scroll To Specific Element    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]    # Additional safeguards to avoid ElementNotInteractableException seen with Chrome at times
    Focus And Click Element    xpath=//app-assignment-form//button[contains(@class, "action__btn--secondary")]
    Verify Element Is Visible    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--primary"]

Verify Values Unchanged In Delete Confirmation View
    [Documentation]    Verify that the values such as due date and instructions remain the same in view in edit view and delete confirmation view
    [Arguments]    ${prev_due_date}    ${prev_instructions}    ${prev_items_in_assignment}    ${prev_selected_count}
    ${confirmation_due_date}=    Get Due Date Value In Edit View
    ${confirmation_instructions}=    Get Instructions Value In Edit View
    ${confirmation_items_in_assignment}=    Get Count Of Content Items In Assignment
    ${confirmation_selected_count}=   Get Selected Value In Edit View
    Should Be Equal    ${confirmation_due_date}    ${prev_due_date}
    Should Be Equal    ${confirmation_instructions}    ${prev_instructions}
    Should Be Equal    ${confirmation_items_in_assignment}    ${prev_items_in_assignment}
    Should Be Equal    ${confirmation_selected_count}    ${prev_selected_count}

Delete Assignment From Edit View
    [Documentation]    Delete assignment by opening existing assignment in Edit view and then deleting it
    ${due_date}=    Get Due Date Value In Edit View
    ${instructions}=    Get Instructions Value In Edit View
    ${items_in_assignment}=    Get Count Of Content Items In Assignment
    ${selected_count}=   Get Selected Value In Edit View
    Click Delete Button In Edit View
    Verify Values Unchanged In Delete Confirmation View    ${due_date}    ${instructions}    ${items_in_assignment}    ${selected_count}
    Confirm Assignment Delete

# Block holding keywords for Deleting Assignment From Edit view: END

# Block holding keywords for removing content items from Assignment: START

Remove Multiple Content Items From Assignment
    [Documentation]    A total of 9 content items will be removed (three each of theory, link & exercise type)
    ...                 Each remove operation will be performed by making use of different removal methods per item type
    Remove "Theory" Item From Assignment By Clicking on "-" Icon
    Remove "Link" Item From Assignment By Clicking on "-" Icon
    Remove "Exercise" Item From Assignment By Clicking on "-" Icon
    Remove "Theory" Item From Assignment By Using "Remove from assignment" Option
    Remove "Link" Item From Assignment By Using "Remove from assignment" Option
    Remove "Exercise" Item From Assignment By Using "Remove from assignment" Option
    Remove "Theory" Item From Assignment Form View
    Remove "Link" Item From Assignment Form View
    Remove "Exercise" Item From Assignment Form View

Remove "${type}" Item From Assignment By Clicking on "-" Icon
    [Documentation]    Remove an item which has been previously added to assignment by clicking on the "-" icon in content item.
    ...                 This keyword is reusable for removing theory, link or exercise item based on ${type} provided as embedded argument in keyword name
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${titles_also_in_chapter}=    Get List Of Item Titles In Assignment And In Current Chapter    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${type_lower}
    ${titles_also_in_chapter_len}=    Get Length    ${titles_also_in_chapter}
    Run Keyword If    ${titles_also_in_chapter_len}==0    Log    SKIPPING Removing Item: ${type} item cannot be removed as there are no more removable ${type} items in current chapter    WARN    console=True
    Return From Keyword If    ${titles_also_in_chapter_len}==0

    ${addable_itemtype_items}=    Set Variable    //a[contains(@class, "${type_lower}-wrapper")]/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]/i
    Setup Step For Adding/Removing Content Item To Assignment    ${addable_itemtype_items}    ${type_lower}

    ${title_name_to_remove}=    Get From List    ${titles_also_in_chapter}    0    # Get the title of first given item type which will be removed from the assignment
    ${minus_button_locator}=    Set Variable    //div[contains(@class, "${type_lower}")][normalize-space(text())="${title_name_to_remove}"]/parent::div/parent::a/following-sibling::label[contains(@class, "assignment-toggle--checked")]/i
    Click '-' Button In Content Item    ${minus_button_locator}    ${title_name_to_remove}
    Verify Decrease In Items Selected List For Assignment
    Verify Included Items Count In Assignment Cart Matches Number Of Items Listed    # Validate count shown in nav-circle in assignment new/edit view
    Verify Increase In Count Of Addable Items In Chapter    ${addable_itemtype_items}    ${type_lower}
    Remove Values From List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}
    Remove Values From List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}

Remove "${type}" Item From Assignment By Using "Remove from assignment" Option
    [Documentation]    Remove an item which has been previously added to assignment by clicking on '...' and selecting 'Remove from assignment'.
    ...                 This keyword is reusable for removing theory, link or exercise item based on ${type} provided as embedded argument in keyword name
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${titles_also_in_chapter}=    Get List Of Item Titles In Assignment And In Current Chapter    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${type_lower}
    ${titles_also_in_chapter_len}=    Get Length    ${titles_also_in_chapter}
    Run Keyword If    ${titles_also_in_chapter_len}==0    Log    SKIPPING Removing Item: ${type} item cannot be removed as there are no more removable ${type} items in current chapter    WARN    console=True
    Return From Keyword If    ${titles_also_in_chapter_len}==0

    ${addable_itemtype_items}=    Set Variable    //a[contains(@class, "${type_lower}-wrapper")]/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]
    Setup Step For Adding/Removing Content Item To Assignment    ${addable_itemtype_items}    ${type_lower}

    ${title_name_to_remove}=    Get From List    ${titles_also_in_chapter}    0    # Get the title of first given item type which will be removed from the assignment
    ${more_button_locator}=    Set Variable    //div[contains(@class, "${type_lower}")][normalize-space(text())="${title_name_to_remove}"]/parent::div/parent::a//div[contains(@class, "more-button")]
    Click More (...) Button In Content Item    ${more_button_locator}
    Click 'Remove from assignment' In Menu Popup For Content Item    ${title_name_to_remove}
    Verify Decrease In Items Selected List For Assignment
    Verify Included Items Count In Assignment Cart Matches Number Of Items Listed    # Validate count shown in nav-circle in assignment new/edit view
    Verify Increase In Count Of Addable Items In Chapter    ${addable_itemtype_items}    ${type_lower}
    Remove Values From List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}
    Remove Values From List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}

Remove "${type}" Item From Assignment Form View
    [Documentation]    Remove an item which has been previously added to assignment by clicking on '...' and then clicking 'Remove' corresponding to the item in Assignment form.
    ...                 This keyword is reusable for removing theory, link or exercise item based on ${type} provided as embedded argument in keyword name
    ${type_lower}=    Convert To Lower Case    ${type}
    ${type_upper}=    Convert To Upper Case    ${type}

    ${items_in_assignment_list_len}=    Get Length    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}
    Run Keyword If    ${items_in_assignment_list_len}==0    Log    SKIPPING Removing Item: ${type} item cannot be removed as there are no more ${type} items to remove in current Assignment for view    WARN    console=True
    Return From Keyword If    ${items_in_assignment_list_len}==0

    ${title_name_to_remove}=    Get From List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    0    # Get the title of first given item type which will be removed from the assignment
    ${edit_button_locator}=    Set Variable    //div[contains(@class, "content-item__content")]/span[normalize-space(text())="${title_name_to_remove}"]/parent::div/following-sibling::button/i[contains(@class, "content-item__edit-icon")]
    Click Edit (...) Button Of Item In Assignment Form    ${edit_button_locator}
    Click 'Remove' In Menu Popup For Item In Assignment Form    ${title_name_to_remove}
    Verify Decrease In Items Selected List For Assignment
    Verify Included Items Count In Assignment Cart Matches Number Of Items Listed    # Validate count shown in nav-circle in assignment new/edit view
    Remove Values From List    ${ALL_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}
    Remove Values From List    ${${type_upper}_ITEMS_IN_ASSIGNMENT_LIST}    ${title_name_to_remove}

Get List Of Item Titles In Assignment And In Current Chapter
    [Documentation]    Returns list of item titles which are in assignment (previously added) and also visible in current chapter
    [Arguments]    ${titles_list}    ${type_lower}
    @{titles_also_in_chapter}=    Create List
    FOR     ${title_name}    IN    @{titles_list}
        ${count}=    Get Element Count    xpath=//div[contains(@class, "${type_lower}")][contains(text(), "${title_name}")]
        Run Keyword If    ${count}>0    Append To List    ${titles_also_in_chapter}    ${title_name}
    END
    [Return]    ${titles_also_in_chapter}

Click '-' Button In Content Item
    [Documentation]    From a content item (which is already added to assignment), click on '-' button to remove item to assignment
    [Arguments]    ${minus_button_locator}    ${content_item_title}
    Custom Scroll To Specific Element    xpath=${minus_button_locator}
    Focus And Click Element    xpath=${minus_button_locator}
    Verify Element Is Visible    xpath=//div[contains(text(), "${content_item_title}")]/parent::div/parent::a/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]
    Wait Until Page Does Not Contain Element    xpath=//div[@class="content-item__content"]/span[normalize-space(text())="${content_item_title}"]    # Item removed from assignment form

Verify Decrease In Items Selected List For Assignment
    [Documentation]    Verify number of content items in assignment decreases by 1
    ${curr_items_count}=    Get Count Of Content Items In Assignment
    Run Keyword If    ${PREV_ITEMS_IN_ASSIGNMENT_COUNT} - ${curr_items_count} != 1    Fail    Removing content item to assignment failed as content items count in assignment did not decrease by 1
    Set Test Variable    ${PREV_ITEMS_IN_ASSIGNMENT_COUNT}    ${curr_items_count}    # ${PREV_ITEMS_IN_ASSIGNMENT_COUNT} will be one less

Verify Increase In Count Of Addable Items In Chapter
    [Documentation]    Verify number of addable items of this type in content feed chapter increases by 1
    [Arguments]    ${addable_items_locator}    ${item_type}
    ${curr_count}=    Get Count Of Addable Items In Chapter    ${addable_items_locator}
    ${prev_count}=    Set Variable If    '${item_type}'=='theory'    ${PREV_THEORY_COUNT}
    ...                                  '${item_type}'=='link'    ${PREV_LINK_COUNT}
    ...                                  '${item_type}'=='exercise'    ${PREV_EXERCISE_COUNT}
    Run Keyword If    ${curr_count} - ${prev_count} != 1    Fail    Removing content item to assignment failed as addable item counts did not increase by 1
    Run Keyword If    '${item_type}'=='theory'    Set Test Variable    ${PREV_THEORY_COUNT}    ${curr_count}    # PREV_ Test variables will be will be one more
    ...    ELSE IF    '${item_type}'=='link'    Set Test Variable    ${PREV_LINK_COUNT}    ${curr_count}
    ...    ELSE IF    '${item_type}'=='exercise'    Set Test Variable    ${PREV_EXERCISE_COUNT}    ${curr_count}

Click 'Remove from assignment' In Menu Popup For Content Item
    [Documentation]    From a content item (which can be removed from assignment), in the resulting menu popup after
    ...                 clicking '...' button, Click on 'Remove from assignment' menu item
    [Arguments]    ${content_item_title}
    Focus And Click Element    xpath=//button[contains(@class, "mat-menu-item")][@test-assignments-toggle]
    Verify Element Is Visible    xpath=//div[contains(text(), "${content_item_title}")]/parent::div/parent::a/following-sibling::label[not(contains(@class, "assignment-toggle--checked"))]
    Wait Until Page Does Not Contain Element    xpath=//div[@class="content-item__content"]/span[normalize-space(text())="${content_item_title}"]    # Item removed from assignment form

Click Edit (...) Button Of Item In Assignment Form
    [Documentation]    From an item in Assignment form (which has been added to the assignment), click on ... button to show options in popup
    [Arguments]    ${edit_button_locator}
    Custom Scroll To Specific Element    xpath=${edit_button_locator}
    Focus And Click Element    xpath=${edit_button_locator}
    Wait Until Page Contains Element    xpath=//button[contains(@class, "content-item__edit")][@aria-expanded="true"]    # Expanded attribute for button is set to true
    Wait Until Page Contains Element    xpath=//div[@class="cdk-overlay-container"]/div[contains(@class, "cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing")]    # An extra check
    Verify Element Is Visible    xpath=//button[contains(@class, "mat-menu-item")][contains(@id, "delete")]    # Remove option is shown in menu popup

Click 'Remove' In Menu Popup For Item In Assignment Form
    [Documentation]    From an item in Assignment form (which has been added to the assignment), in the resulting menu
    ...                 popup after clicking '...' button, Click on 'Remove' menu item
    [Arguments]    ${content_item_title}
    Focus And Click Element    xpath=//button[contains(@class, "mat-menu-item")][contains(@id, "delete")]
    Wait Until Page Does Not Contain Element    xpath=//div[@class="content-item__content"]/span[normalize-space(text())="${content_item_title}"]    # Item removed from assignment form

# Block holding keywords for removing content items from Assignment: END

Get CSS Background Color For Element
    [Arguments]    ${element}
    # NOTE: Expected colors for icons - Published color: rgb(245, 143, 41), Draft color: rgb(100, 54, 149), Closed color: rgb(149, 150, 157)
    ${status}    ${bg_color}=    Run Keyword And Ignore Error    Execute JavaScript    return document.defaultView.getComputedStyle(document.evaluate('${element}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue, null)['background-color']
    ${bg_color}=    Set Variable If    '${status}'=='PASS'    ${bg_color}
    ...    '${status}'=='FAIL'    not_found
    [Return]    ${bg_color}

# Block holding keywords for Publishing Assignment: START

Verify Submit (Publish) Button Is Disabled
   Page Should Contain Element    xpath=//button[contains(@class, "action__btn--primary")][@type="submit"][@disabled]

Verify Submit (Publish) Button Is Enabled
   Page Should Contain Element    xpath=//button[contains(@class, "action__btn--primary")][@type="submit"][not(@disabled)]

Click Submit (Publish) Button
    Verify Submit (Publish) Button Is Enabled
    Focus And Click Element    xpath=//button[contains(@class, "action__btn--primary")][@type="submit"][not(@disabled)]
    Wait Until Page Does Not Contain Element    xpath=//button[contains(@class, "action__btn--secondary")][@type="button"]
    Verify Success Notification Is Shown As Expected
    Verify Element Is Visible    xpath=(//mat-expansion-panel-header//div[@class="heading"])[last()]
    Page Should Not Contain Element    xpath=(//li[contains(@class, "assignments-list__item")])[last()]//span[contains(@class, "chip--blue")]    # Draft icon not shown in summary

Verify Success Notification Is Shown As Expected
    Wait Until Page Contains Element    xpath=//app-notification/div[contains(@class, "notification--success")]     # Verify success notification from API is shown
    ${notification_icon_element}=    Set Variable    //app-notification/div[contains(@class, "notification--success")]/div[@class="notification__ball"]/i[@class="notification__icon"]
    Verify Element Is Visible    xpath=${notification_icon_element}    # Notification icon is shown
    ${status}    ${webkit_mask_image_value}=    Run Keyword And Ignore Error    Execute JavaScript    return document.defaultView.getComputedStyle(document.evaluate('${notification_icon_element}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue, null)['-webkit-mask-image']
    Run Keyword If    '${status}'=='FAIL'    Log    Computed style property 'webkit-mask-image' for notification icon not found (Tick mark missing in icon)    WARN    console=True
    ${status}=    Run Keyword If    '${status}'=='PASS'    Run Keyword And Return Status    Should Contain    ${webkit_mask_image_value}    done
    Run Keyword If    ${status}==False      Log    Computed style property 'webkit-mask-image' for notification icon found but did not have reference to 'done...svg'    WARN    console=True

Publish New Assignment
    [Documentation]    Publish a new assignment by clicking on Submit button in the Assignment form view abd verifying counts
    Publish Assignment
    ${new_pub_assignments}=    Get Count Of Published Assignments From Assignments Cart
    Run Keyword If    ${new_pub_assignments} - ${PUBLISHED_ASSIGNMENTS_COUNT} != 1    Fail    Publish From New Assignment View: Assignment creation failed as published assignments count has not increased by 1
    Set Test Variable    ${PUBLISHED_ASSIGNMENTS_COUNT}    ${new_pub_assignments}
    ${curr_count}=    Get Count Of Assignments
    Run Keyword If    ${curr_count} - ${PREV_ASSIGNMENTS_COUNT} != 1    Fail    Publish From New Assignment View: Assignment creation failed as assignments count has not increased by 1
    Set Test Variable    ${PREV_ASSIGNMENTS_COUNT}    ${curr_count}

Publish Assignment
    [Documentation]    Publish an assignment (new or existing) vby clicking on Submit button in the Assignment form view
    Click Submit (Publish) Button

Verify Last Assignment Is Indeed A Published Assignment
    [Documentation]    Ensure that the last assigment listed in Assignments panel is indeed a Published assignment by checking bg color of icons
    ${expected_color_for_published_status}=    Set Variable    rgb(245, 143, 41)
    ${bg_color}=    Get CSS Background Color For Element    (//mat-expansion-panel-header//div[@class="heading"])[last()]//i[contains(@class, "summary__icon")][1]
    Should Be Equal    ${bg_color}    ${expected_color_for_published_status}    msg=Last Assignment is NOT a 'Published Assignment': BG color for Icon elements (${bg_color}) did not match the expected color for 'Published' status (${expected_color_for_published_status})

Verify Card Summary For Published Assignment (Due Date Not Set, Items Set)
    Set Test Variable    ${ASSIGNMENT_STATUS}    Published
    Verify Last Assignment Is Indeed A Published Assignment
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Status Of Assignment Is Not Shown As Draft
    Verify Assignment Due Date (DD.MM.) Is Not Shown
    Verify Assignment Items Summary Section Is Shown

Verify Card Summary For Published Assignment (Due Date Set, Items Set)
    Set Test Variable    ${ASSIGNMENT_STATUS}    Published
    Verify Last Assignment Is Indeed A Published Assignment
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Status Of Assignment Is Not Shown As Draft
    Verify Assignment Due Date (DD.MM.) Is Shown
    Verify Assignment Items Summary Section Is Shown

Verify Read View Of Published Assignment (Instructions Set, Items Set)
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Contain Content Items
    Verify Read View Of Assignment Contains Edit Button
    Verify Read View Of Assignment Contains Mark As Closed Button
    Verify Read View Of Assignment Contains Delete Button

Verify Published Assignments Count Is Not Zero
    [Documentation]    Verify that number of published assignments count from nav-circle in Assignments panel is greater than 0
    ${pub_assignments}=    Get Count Of Published Assignments From Assignments Cart
    Run Keyword If    ${pub_assignments}==0    Fail    Published Assignments count from Assignments Cart nav-circle is 0 (Expected: >0)

Verify Published Assignments Count Is Zero
    [Documentation]    Verify that number of published assignments count from nav-circle in Assignments panel is 0
    ${pub_assignments}=    Get Count Of Published Assignments From Assignments Cart
    Run Keyword If    ${pub_assignments}!=0    Fail    Published Assignments count from Assignments Cart nav-circle is NOT 0 (Expected: 0)

Student - Verify Read View Of Published Assignment (Instructions Set, Items Set)
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Contain Content Items
    Verify Read View Of Assignment Does Not Contain Edit Button
    Verify Read View Of Assignment Does Not Contain Mark As Closed Button
    Verify Read View Of Assignment Does Not Contain Delete Button

# Block holding keywords for Publishing Assignment: END

# Block holding keywords for Closing Assignment (Mark As Closed): START

Mark Assignment As Closed
    [Documentation]    From read view of the assignment, proceed to mark the assignment as closed (using Mark As Closed button)
    Custom Scroll To Specific Element    xpath=//button[@test-close]    alignment=top
    Focus And Click Element    xpath=//button[@test-close]
    Wait Until Page Contains Element    xpath=//div[@class="cdk-overlay-container"]/div[contains(@class, "cdk-overlay-backdrop-showing")]
    Verify Element Is Visible    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--secondary"]    # Cancel button is shown in confirmation popup
    Verify Element Is Visible    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--primary"]    # Submit button is shown in confirmation popup
    Confirm Assignment Close

Confirm Assignment Close
    [Documentation]    Click on the confirmation button to close the assignment
    Focus And Click Element    xpath=//button[@class="dialog-actions__btn dialog-actions__btn--primary"]
    Wait Until Page Does Not Contain Element    xpath=//div[@class="cdk-overlay-container"]/div[contains(@class, "cdk-overlay-backdrop-showing")]
    Verify Read View Of Assignment Does Not Contain Mark As Closed Button
    Verify Read View Of Assignment Does Not Contain Edit Button

Verify Last Assignment Is Indeed A Closed Assignment
    [Documentation]    Ensure that the last assigment listed in Assignments panel is indeed a Closed assignment by checking bg color of icons
    ${expected_color_for_closed_status}=    Set Variable    rgb(149, 150, 157)
    ${bg_color}=    Get CSS Background Color For Element    (//mat-expansion-panel-header//div[@class="heading"])[last()]//i[contains(@class, "summary__icon")][1]
    Should Be Equal    ${bg_color}    ${expected_color_for_closed_status}    msg=Last Assignment is NOT a 'Closed Assignment': BG color for Icon elements (${bg_color}) did not match the expected color for 'Closed' status (${expected_color_for_closed_status})

Verify Card Summary For Closed Assignment (Due Date Set, Items Set)
    Set Test Variable    ${ASSIGNMENT_STATUS}    Closed
    Verify Last Assignment Is Indeed A Closed Assignment
    Verify Assignment Created Day Of Week Is Shown
    Verify Assignment Created Date (DD.MM.) Is Shown
    Verify Status Of Assignment Is Not Shown As Draft
    Verify Assignment Due Date (DD.MM.) Is Shown
    Verify Assignment Items Summary Section Is Shown

Verify Read View Of Closed Assignment (Instructions Set, Items Set)
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Contain Content Items
    Verify Read View Of Assignment Does Not Contain Edit Button
    Verify Read View Of Assignment Does Not Contain Mark As Closed Button
    Verify Read View Of Assignment Contains Delete Button

Student - Verify Read View Of Closed Assignment (Instructions Set, Items Set)
    Verify Read View Of Assignment Contains Instructions
    Verify Read View Of Assignment Contain Content Items
    Verify Read View Of Assignment Does Not Contain Edit Button
    Verify Read View Of Assignment Does Not Contain Mark As Closed Button
    Verify Read View Of Assignment Does Not Contain Delete Button

Get Count Of Closed Assignments
    [Documentation]    Find the number of Closed assignments listed in assignments panel
    ${total_assignments}=    Get Count Of Assignments
    ${closed_assignments_count}=    Set Variable    0
    FOR    ${index}    IN RANGE    1    ${total_assignments} + 1
        # NOTE: Get background color of the first item type image in assignment specified by position
        ${bg_color}=    Get CSS Background Color For Element    ((//app-assignment-sidebar//li[contains(@class, "assignments-list__item")])[${index}]//i[contains(@class, "summary__icon")])[1]
        Continue For Loop If    '${bg_color}'!='rgb(149, 150, 157)'
        ${closed_assignments_count}=    Evaluate    ${closed_assignments_count} + 1    # '${bg_color}'is equal to 'rgb(149, 150, 157)', color for closed assignments
    END
    [Return]    ${closed_assignments_count}

Verify Closed Assignments Count Is Not Zero
    [Documentation]    Verify that number of closed assignments count in list of assignments is not 0
    ${closed_assignments}=    Get Count Of Closed Assignments
    Run Keyword If    ${closed_assignments}==0    Fail    Closed Assignments count from Assignments list is 0 (Expected: >0)

Verify Closed Assignments Count Is Zero
    [Documentation]    Verify that number of closed assignments count in list of assignments is 0 (no closed assignments)
    ${closed_assignments}=    Get Count Of Closed Assignments
    Run Keyword If    ${closed_assignments}!=0    Fail    Closed Assignments count from Assignments list is NOT 0 (Expected: 0)

# Block holding keywords for Closing Assignment (Mark As Closed): END

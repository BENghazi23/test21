*** Keywords ***
Create Feed For Exercises
    [Arguments]  ${username}    ${password}
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${username}    ${password}
    ${name}     Add Teacher Feed     0 Sisko Manual MR     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_EXERCISES}    scroll_off
    Wait Until Element Is Enabled    xpath=.//*[@id="accesskey-bar-button"]
    ${access_key}    Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_EXERCISES}    ${access_key}
    Set Suite Variable    ${METHOD_FOR_EXERCISES}    ${name}
    Close Access Key Dialog Window
    Logout Sisko
    Close Browser

Create Feed For Exercises Real
    [Arguments]    ${method_to_copy}
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_EXERCISES}    ${USER_PASSWORD_TEACHER_1_EXERCISES}
    ${date_stamp}    Get Current Date    result_format=%Y%m%d
    ${method_to_search}    Catenate    SEPARATOR=_    ${method_to_copy}    ${date_stamp}
    ${status}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//div[contains(@class, "feed-title") and contains(normalize-space(text()),"${method_to_search}")]
    ${teacher_feed_name}    Run Keyword If    '${status}'=='True'
    ...    Get Text    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//div[contains(@class, "feed-title") and contains(normalize-space(text()),"${method_to_search}")]
    ${name}    Run Keyword If    '${status}'=='False'
    ...    Add Teacher Feed    ${method_to_copy}    on    ${FEED_TYPE_PUBLISHER}    ${EMPTY}    scroll_off
    ...    ELSE
    ...    Select Method With Method URL    ${teacher_feed_name}    ${FEED_TYPE_TEACHER}
    Wait Until Element Is Enabled    xpath=.//*[@id="accesskey-bar-button"]
    ${access_key}    Create Access Key
    Close Browser
    [Return]    ${name}    ${access_key}

Scroll To Answer
    [Arguments]    ${option}
    # ${vertical_position_element}    Get Vertical Position    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ###--->${top_bar_position}    Execute JavaScript    var rect = document.getElementsByClassName('side-hidden'); var topBar = rect[0].getBoundingClientRect(); return topBar.bottom;
    #${vertical_position_xyz}    Get Vertical Position    xpath=.//eb-choice-list
    ${vertical_position_element}    Get Vertical Position    ${option}
    ${vertical_position_buttons}    Get Vertical Position    xpath=.//app-module-content-buttons
    #Run Keyword If    ${vertical_position_element} >= ${vertical_position_buttons}
    #...    Execute JavaScript    window.scrollTo(0, ${vertical_position_element});
    ${position_difference}    Evaluate    ${vertical_position_buttons}-${vertical_position_element}-20
    Execute JavaScript    window.scrollTo(0, ${vertical_position_element});
    #Execute JavaScript    window.scrollTo(0, 300);

Scroll To Answer_v2
    [Arguments]    ${option}
    Wait Until Element Is Enabled    ${option}
    #Scroll Element Into View    ${option}
    Scroll Page To Top
    Wait Until Element Is Enabled    ${option}
    ####Scroll Page To Top
    # ${vertical_position_element}    Get Vertical Position    xpath=(.//eb-choice-list//eb-checkbox)[${randon_selection}]
    ###--->${top_bar_position}    Execute JavaScript    var rect = document.getElementsByClassName('side-hidden'); var topBar = rect[0].getBoundingClientRect(); return topBar.bottom;
    #${vertical_position_xyz}    Get Vertical Position    xpath=.//eb-choice-list
    ${vertical_position_element}    Get Vertical Position    ${option}
    ###${vertical_position_topbar}    Get Vertical Position    xpath=.//div[@class="header"]
    #${top_bar_position}    Execute JavaScript    var rect = document.getElementsByClassName('header-container'); var topBar = rect[0].getBoundingClientRect(); return topBar.bottom;
    ${vertical_position_topbar}    Execute JavaScript    var rect = document.getElementsByClassName('header'); var topBar = rect[0].getBoundingClientRect(); return topBar.bottom;
    ${vertical_position_buttons}    Get Vertical Position    xpath=.//app-module-content-buttons
    #Run Keyword If    ${vertical_position_element} >= ${vertical_position_buttons}
    #...    Execute JavaScript    window.scrollTo(0, ${vertical_position_element});
    ${position_difference}    Evaluate    ${vertical_position_buttons}-${vertical_position_element}-20
    ${vertical_position_element}    Run Keyword If    ${vertical_position_element}<${vertical_position_topbar} and ${vertical_position_element}<${vertical_position_buttons}
    ...    Evaluate    ${vertical_position_element}+100
    ...    ELSE IF    ${vertical_position_element}>${vertical_position_topbar} and ${vertical_position_element}>${vertical_position_buttons}
    ...    Evaluate    ${vertical_position_element}-100
    # ...    ELSE IF    ${vertical_position_element} > ${vertical_position_topbar} AND ${vertical_position_element} > ${vertical_position_buttons}
    # ...    Evaluate    ${vertical_position_element}-100
    ...    ELSE    Evaluate    0+0
    Execute JavaScript    window.scrollTo(0, ${vertical_position_element});
    #Execute JavaScript    window.scrollTo(0, 300);

Scroll To Answer_v3
    [Arguments]  ${locator}
    ${x}=        Get Horizontal Position  ${locator}
    ${y}=        Get Vertical Position    ${locator}
    #${z}    Execute JavaScript    var pos = document.querySelector(".content-toolbar-container").getBoundingClientRect(); return pos;
    ${z}    Execute JavaScript    var pos = document.querySelector(".module-wrapper").getBoundingClientRect(); return pos;
    #module-wrapper
    #Log Many    ${z}
    #${x}    Evaluate    ${x}-&{z}[height]-100
    ${x}    Evaluate    ${x}-&{z}[top]
    Execute Javascript  window.scrollTo(${x}, ${y})

Scroll To Answer_v4
    [Arguments]  ${locator}
    ${element_vertical_position}    Get Vertical Position    ${locator}
    ${close_button}    Execute JavaScript    var pos = document.getElementById("close-button").getBoundingClientRect(); return pos;
    ${start_over_button}    Execute JavaScript    var pos = document.getElementById("start-over-button").getBoundingClientRect(); return pos;
    ${card_top}    Execute JavaScript    var pos = document.querySelector(".module-wrapper").getBoundingClientRect(); return pos;
    ${above_hidden}    Evaluate    ${card_top}[y]-${element_vertical_position}
    ${below_hidden}    Evaluate    ${start_over_button}[y]-${element_vertical_position}
    ${scroll_page_to}    Run Keyword If    ${above_hidden} < 0    Evaluate    ${element_vertical_position}-100
    ...    ELSE IF    ${below_hidden} < 0    Evaluate    ${element_vertical_position}+100
    ...    ELSE    Set Variable    ${element_vertical_position}
    #Execute Javascript  window.scrollTo(0, ${scroll_page_to})
    ${x}    Evaluate    ${card_top}[top]+100
    Execute Javascript  window.scrollTo(0, ${x})

Reset Answers
    ${status}    Run Keyword And Return Status    Click Element On Page    xpath=.//*[@id="retry-button"]
    Wait Until Element Is Not Visible    xpath=.//*[@id="retry-button"]

Start Exercise Over
    #${button_enabled}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//*[@id="start-over-button"]    timeout=3s
    ${button_enabled}    Run Keyword And Return Status    Wait Until Keyword Succeeds    10s    1s
    ...    Wait Until Element Is Enabled    xpath=.//*[@id="start-over-button"]
    Run Keyword If    '${button_enabled}'=='False'    Activate Exercise Buttons
    Click Element On Page    xpath=.//*[@id="start-over-button"]
    Sleep    1

Activate Exercise Buttons
    ${buttons_location}    Execute JavaScript    var pos = document.querySelector(".module-content-buttons").getBoundingClientRect(); return pos;
    ${x}    Evaluate     ${buttons_location}[x]+0
    ${y}    Evaluate     ${buttons_location}[y]+0
    Execute JavaScript    document.elementFromPoint(${x}, ${y}).click();

Continue Exercise
    Click Element On Page    xpath=.//*[@id="continue-button"]
    Wait Until Element Is Enabled    xpath=.//*[@id="submit-button"]

Submit Exercise
    Click Element On Page    xpath=.//*[@id="submit-button"]
    Wait Until Page Does Not Contain Element    xpath=.//*[@id="submit-button"]

Check Exercise Buttons In Initial State
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@id="start-over-button"]
    Wait Until Element Is Enabled    xpath=.//*[@id="submit-button"]

Check Exercise Buttons In Continue State
    Sleep    1
    Wait Until Element Is Enabled    xpath=.//*[@id="start-over-button"]
    Wait Until Element Is Enabled    xpath=.//*[@id="continue-button"]

Check Exercise Score
    [Arguments]    ${score_expected}
    Wait Until Element Is Enabled    xpath=.//*[@id="exercise-score"]
    ${score_actual}    Get Text    xpath=.//*[@id="exercise-score"]
    Should Contain    ${score_actual}    ${score_expected}

Check Number Of Split Areas
    [Arguments]    ${split_areas_expected}
    Wait Until Element Is Enabled    xpath=.//as-split-area
    ${split_areas}    Get Element Count    xpath=.//as-split-area
    Should Be Equal As Numbers    ${split_areas}    ${split_areas_expected}
    FOR    ${index}    IN RANGE    0     ${split_areas_expected}
        ${index}    Evaluate    ${index}+1
        ${innerHTML}    Get Element Attribute    xpath=(.//as-split-area)[${index}]    innerHTML
        ${innerHTML_length}    Get Length    ${innerHTML}
        Should Be True    ${innerHTML_length}>0
    END

Exercise Points Should Be Visible
    Element Should Be Visible    xpath=.//*[@id="exercise-score"]

Exercise Points Should Not Be Visible
    Wait Until Element Is Not Visible    xpath=.//*[@id="exercise-score"]

Check Hint
    Wait Until Element Is Enabled    xpath=.//eb-hint/a
    ${hint_style}    Get Element Attribute    xpath=.//eb-hint/a    class
    Should Not Contain    ${hint_style}    eb-popover-active
    Click Element On Page    xpath=.//eb-hint
    Wait Until Element Is Enabled    xpath=.//eb-hint/a
    ${hint_style}    Get Element Attribute    xpath=.//eb-hint/a    class
    Should Contain    ${hint_style}    eb-popover-active
    Wait Until Element Is Enabled    xpath=.//eb-popover
    Close Hint Popup

Check Hints
    ${hints}    Get Element Count    xpath=.//eb-hint/a
    FOR    ${index}    IN RANGE    0     ${hints}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-hint/a)[${index}]
        Scroll To Answer_v4    xpath=(.//eb-hint/a)[${index}]
        ${hint_style}    Get Element Attribute    xpath=(.//eb-hint/a)[${index}]    class
        Should Not Contain    ${hint_style}    eb-popover-active
        Click Element On Page    xpath=(.//eb-hint/a)[${index}]
        Wait Until Element Is Enabled    xpath=(.//eb-hint/a)[${index}]
        ${hint_style}    Get Element Attribute    xpath=(.//eb-hint/a)[${index}]    class
        Should Contain    ${hint_style}    eb-popover-active
        Wait Until Element Is Enabled    xpath=.//eb-popover
        Close Hint Popup
    END

Close Hint Popup
    Execute JavaScript    document.querySelector(".eb-close").click()

Turn Teacher View On
    Start Exercise Over
    Wait For Condition    return document.readyState=="complete"
    ${turned_on}    Is Teacher View On
    Run Keyword If    '${turned_on}'=='False'    Click Teacher View On
    Check Teacher View Is On

Turn Teacher View Off
    ${turned_on}    Is Teacher View On
    Run Keyword If    '${turned_on}'=='True'    Click Teacher View On
    Check Teacher View Is Off

Click Teacher View On
    Click Element On Page    xpath=.//*[@id="teachers-view-button"]
    Wait Until Page Does Not Contain Element    xpath=.//*[starts-with(@class, "loading")]

Check Teacher View Is On
    Wait Until Element Is Enabled    xpath=.//*[@id="teachers-view-button" and contains(@class, "selected")]
    Wait Until Element Is Not Visible    xpath=.//app-module-content-buttons
    Wait Until Page Does Not Contain Element    xpath=.//*[starts-with(@class, "loading")]

Check Teacher View Is Off
    Wait Until Element Is Enabled    xpath=.//*[@id="teachers-view-button" and not(contains(@class, "selected"))]
    Wait Until Element Is Enabled    xpath=.//app-module-content-buttons
    Wait Until Page Does Not Contain Element    xpath=.//*[starts-with(@class, "loading")]

Is Teacher View On
    ${turned_on}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//*[@id="teachers-view-button" and contains(@class, "selected")]
    [Return]    ${turned_on}

Click On Exercise Title
    Click Element On Page    xpath=.//*[@class="title-feedback"]

Get Exercise Status
    [Arguments]    ${exercise_name}
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "result")]/*[@class="content"]/p[normalize-space(text())="${exercise_name}"]/ancestor::*[starts-with(@class, "result")][1]
    ${exercise_status}    Wait Until Keyword Succeeds    10s    1s    Get Element Attribute
    ...    xpath=.//*[starts-with(@class, "result")]/*[@class="content"]/p[normalize-space(text())="${exercise_name}"]/ancestor::*[starts-with(@class, "result")][1]
    ...    style
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "result")]/*[@class="content"]/p[normalize-space(text())="${exercise_name}"]/ancestor::*[starts-with(@class, "result")][1]
    ${exercise_class}    Wait Until Keyword Succeeds    10s    1s    Get Element Attribute
    ...    xpath=.//*[starts-with(@class, "result")]/*[@class="content"]/p[normalize-space(text())="${exercise_name}"]/ancestor::*[starts-with(@class, "result")][1]
    ...    class
    [Return]    ${exercise_status}    ${exercise_class}

Open Assessment For Exercise
    [Arguments]    ${exercise_name}
    Click Element On Page    xpath=.//*[@class="exercise-results"]//*[starts-with(@class, "result")]/*[@class="content"]/p[normalize-space(text())="${exercise_name}"]/ancestor::*[starts-with(@class, "result")][1]
    Wait Until Element Is Enabled     xpath=.//app-assessment-view

Open Teacher Assessment
    Click Element On Page    xpath=.//*[@class="title"]/*[starts-with(@class, "question-status-color")]

Select Teacher Assessment
    [Arguments]    ${status}
    Wait Until Element Is Enabled    xpath=.//*[@class="item"]/*[starts-with(@class, "orderer")]
    Run Keyword If    '${status}'=='correct'    Click Element On Page    xpath=.//*[@class="item"]/*[starts-with(@class, "orderer")]/*[@class="question-status-color correctFi"]
    Run Keyword If    '${status}'=='autoAssessed'    Click Element On Page    xpath=.//*[@class="item"]/*[starts-with(@class, "orderer")]/*[@class="question-status-color autoAssessedFi"]
    Run Keyword If    '${status}'=='incorrect'    Click Element On Page    xpath=.//*[@class="item"]/*[starts-with(@class, "orderer")]/*[@class="question-status-color incorrectFi"]
    Wait Until Page Does Not Contain Element    xpath=.//*[starts-with(@class, "loading")]
    #Wait Until Element Is Enabled    xpath=.//*[@class="title"]/*[@class="question-status-color correctFi"]
    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "assessment-tools-teacher")]

Close Assessment View
    Wait Until Element Is Enabled    xpath=.//app-assessment-view//app-module-content-documents
    Wait Until Keyword Succeeds    10s    1s    Click Element On Page    xpath=.//*[@id="assessment-view-close"]
    Wait Until Element Is Not Visible    xpath=.//app-assessment-view

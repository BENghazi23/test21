*** Settings ***
#Library      SeleniumLibrary    timeout=20.0    run_on_failure=common failure
Library      SeleniumLibrary    timeout=20.0    run_on_failure=Nothing
Library      Collections
Library      Screenshot
Library      DateTime
Library      String
Library    ../../common_resources/robot/utility_keywords.py
Resource     assignment_resources.robot
Resource     dashboard_resources.robot
Resource     exercise_resources.robot
Resource     feed_resources.robot
Resource     filter_resources.robot
Resource     homework_central_resources.robot
Resource     license_resources.robot
Variables    ../../common_resources/robot/variablefile.py    ${environment}

*** Keywords ***
Open Site
    [Arguments]   ${site}    ${browser}=Chrome    ${BROWSER_FROM}=local
    Run Keyword If    '${BROWSER_FROM}'!='browserstack'
    ...    Open Browser    ${site}    ${browser}
    ${test_name}    Get Variable Value    ${TEST_NAME}    SUITE SETUP
    ${test_name}    Replace String    ${test_name}    :    ${SPACE}
    ${test_name}    Replace String    ${test_name}    ,    ${SPACE}
    ${test_name}    Replace String    ${test_name}    (    ${EMPTY}
    ${test_name}    Replace String    ${test_name}    )    ${EMPTY}
    ${name}    Catenate    SEPARATOR=/    ${SUITE_NAME}    ${test_name}
    Run Keyword If    '${BROWSER_FROM}'=='browserstack'
    ...    Open Browser   url=${site}   browser=${browser}   remote_url=http://${BROWSERSTACK_USER}:${BROWSERSTACK_KEY}@hub.browserstack.com/wd/hub   desired_capabilities=browser:${browser},browser_version:${BROWSER_VERSION},os:${OS},os_version:${OS_VERSION},resolution:${SCREEN_RESOLUTION},browserstack.local:true,browserstack.localIdentifier:${browserstack.localIdentifier},name:${name}
    Maximize Browser
    Verify Login Page Is Displayed

Verify Login Page Is Displayed
    [Documentation]    Added this keyword to ensure that the login page is displayed before proceeding with Login
    ...                Reasoning: Random login failures were noted with Firefox
    Verify Element is Visible    username

Login To Site
    [Arguments]   ${user}    ${password}
    Wait Until Keyword Succeeds    5s    1s    Enter Username    ${user}
    Wait Until Keyword Succeeds    5s    1s    Enter Password    ${password}
    Click Element On Page    xpath=.//input[@value='Kirjaudu']
    # NOTE: Wait until School name in kampus is shown, 20secs may not be enough as errors were seen at times so increased to 60 secs
    Wait Until Keyword Succeeds    60s    500ms    Page Should Contain Element    xpath=//span[@class="school-name" and not(normalize-space(text())="")]
    Choose Default School In School Change Dialog
    Verify Element is Visible    xpath://div[@class="publisher-feed-title"][contains(text(), "${DEFAULT_METHOD}")]    # Added to ensure feeds are loaded after login
    Wait Until Keyword Succeeds    10s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="mypage-button"]

Enter Username
    [Arguments]    ${user}
    Wait Until Element Is Enabled    username
    Set Focus To Element    username
    Input Text    username    ${user}
    ${entered_user}    Get Value    username
    Should Be Equal As Strings    ${user}    ${entered_user}

Enter Password
    [Arguments]    ${password}
    Wait Until Element Is Enabled    password
    Set Focus To Element    password
    Input Text    password    ${password}
    ${entered_password}    Get Value    password
    Should Be Equal As Strings    ${password}    ${entered_password}

Choose Default School In School Change Dialog
    ${status}    Run Keyword And Return Status
    ...    Wait Until Keyword Succeeds    2s    50ms
    ...    Element Should Be Enabled
    ...    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")]/parent::button
    Run Keyword If    '${status}'=='True'
    ...    Click Element On Page    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and normalize-space(text())="${DEFAULT_SCHOOL}"]/parent::button

Login Kampus
    [Arguments]  ${username}    ${password}
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site   ${username}    ${password}

Login Kampus And Join Feed
    [Arguments]  ${username}    ${password}      ${name}    ${access_key}    ${operation_status}=${OPERATION_SUCCESS}
    Login Kampus   ${username}    ${password}
    Join Feed    ${name}    ${access_key}    ${operation_status}

Open Personal Information Dialog
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="logout-button"]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip//*[@class="name-holder"]

Login Wordpress Site
    [Arguments]   ${user}    ${password}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]
    Wait Until Element Is Enabled    username
    Set Focus To Element    username
    Input Text    username    ${user}
    Wait Until Element Is Enabled    password
    Set Focus To Element    password
    Input Password    password    ${password}
    Click Element On Page    xpath=.//input[@value='Kirjaudu']
    Wait Until Keyword Succeeds    20s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a[@role="button" and text()="Aineistot"]

Open User Info
    [Arguments]    ${user_name}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a[@role="button" and not(text()="Aineistot")]
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//*[@class="dropdown-menu basic-navigation"]//a[text()="Omat tiedot"]
    ${status}    Run Keyword And Return Status    Click Element On Page    xpath=.//button[text()="Hyväksyn käyttöehdot"]
    Wait Until Element Is Enabled    xpath=.//div[text()="Käyttäjätunnus ja salasana"]

Open School Info
    [Arguments]    ${user_name}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a[@role="button" and text()="${user_name}"]
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//*[@class="dropdown-menu basic-navigation"]//a[text()="Koulutiedot"]
    ${status}    Run Keyword And Return Status    Click Element On Page    xpath=.//button[text()="Hyväksyn käyttöehdot"]
    Wait Until Element Is Enabled    xpath=.//div[text()="Yhteystiedot"]

Open User Materials In School
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a[@role="button" and text()="Aineistot"]

Select School From Portal
    [Arguments]    ${school}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//div[contains(@class, "kampus")]//a[text()="${school}"]

Logout Sisko
    Sleep    1
    ${status}    Run Keyword And Return Status    Element Should Be Visible    xpath=.//*[@id="share-accesskey-close"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="share-accesskey-close"]
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="close-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="close-button"]
    Sleep   3
    Open User Information View
    Sleep    2
    Click Element On Page    xpath=.//*[@id="pi_link_logout"]
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    id=menu-paavalikko

Go To Login Page
    Sleep    1
    Go To    ${LOGIN_URL}

Get Date
    ${date_stamp}    Get Current Date    result_format=%Y%m%d_%H%M%S
    [Return]    ${date_stamp}

Click Element On Page
    [Arguments]    ${element}
    Wait Until Element Is Enabled    ${element}
    Run Keyword And Return If    '${BROWSER}'=='safari'    Click Element For Safari    ${element}
    Click Element    ${element}

Open New Tab
    [Arguments]    ${site}
    Sleep    5
    Execute JavaScript    window.open("${site}", "_blank");

Switch Tabs To Index
    [Arguments]    ${index}
    ${windowHandles}    Get Window Handles
    Sleep    2
    Select Window    @{windowHandles}[${index}]
    Sleep    2

Go To My Page
    Wait Until Keyword Succeeds    10s    1s    Click Element On Page    xpath=.//*[@id="mypage-button"]
    Verify Element is Visible    xpath://div[@class="publisher-feed-title"][contains(text(), "${DEFAULT_METHOD}")]    # Added to ensure feeds are loaded in MyPage

Check Feed With Original Name Is Visible
    [Documentation]    Keyword moved from 'sisko/ui/feeds/resource/resources.robot' so that this keyword can be used
    ...                 from other suites as well
    [Arguments]     ${feed_name}
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name}"]

Open User Information View
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="pi_link_logout"]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]

Close User Information View
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="pi_link_logout"]
    Run Keyword If    '${status}'!='False'    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]

Verify Element is Visible
    [Documentation]    Keyword to check if element is found in page and enabled and visible
    [Arguments]    ${element_locator}
    Wait Until Keyword Succeeds    20s    200ms    Element Should Be Enabled    ${element_locator}

Get Selected Language
    Open User Information View
    Verify Element is Visible    xpath=.//*[@id="select-language-button"][string-length(text()) > 2]
    ${language_selected}    Get Text    xpath=.//*[@id="select-language-button"][string-length(text()) > 2]
    ${language_selected}    Strip String    ${language_selected}
    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]
    [Return]    ${language_selected}

Get Locale
    [Documentation]    Returns locale (FI/EN/SV) based on the selected language profile
    ${language_selected}=    Get Selected Language
    ${locale}=    Set Variable If    '${language_selected}'=='English'    EN
    ...    '${language_selected}'=='Suomi'    FI
    ...    '${language_selected}'=='Svenska'    SV
    [Return]    ${locale}

Get Schools
    ${school_default}    Get Text    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name")]
    ${school_default}    Strip String    ${school_default}
    Click Element On Page    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    Wait Until Element Is Enabled    xpath=.//app-school-switching-dialog
    ${schools}    Get Element Count    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")]

Get Element Into View
    [Arguments]    ${element}    ${feed_type}
    Wait Until Element Is Enabled    ${element}
    ${element_position}    Get Horizontal Position    ${element}
    ${elements_in_total}    Get Element Count    xpath=.//*[@feed-type="${feed_type}"]
    ${left_button_exists}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="${feed_type}Feeds-button-left"]
    ${right_button_exists}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="${feed_type}Feeds-button-right"]
    ${left_button_position}    Run Keyword If    '${left_button_exists}'=='True'
    ...    Get Horizontal Position    xpath=.//*[@id="${feed_type}Feeds-button-left"]
    ${right_button_position}    Run Keyword If    '${right_button_exists}'=='True'
    ...    Get Horizontal Position    xpath=.//*[@id="${feed_type}Feeds-button-right"]
    Run Keyword If    '${left_button_exists}'=='True' and ${element_position}<${left_button_position}
    ...    Wait Until Keyword Succeeds    1000s    50ms    Click Element To View    ${element}    ${feed_type}    ${NAVIGATION_LEFT}
    Run Keyword If    '${right_button_exists}'=='True' and ${element_position}>${right_button_position}
    ...    Wait Until Keyword Succeeds    1000s    50ms    Click Element To View    ${element}    ${feed_type}    ${NAVIGATION_RIGHT}
    Run Keyword If    '${right_button_exists}'=='True' and ${element_position}>=${right_button_position} and ${element_position}<=${right_button_position}
    ...    Log    BETWEEN

Click Element To View
    [Arguments]    ${element}    ${feed_type}    ${direction}=right
    Log    ${element}
    Click Element On Page    xpath=.//*[@id="${feed_type}Feeds-button-${direction}"]
    Wait Until Element Is Enabled    ${element}
    ${element_position}    Get Horizontal Position    ${element}
    ${button_position}    Get Horizontal Position    xpath=.//*[@id="${feed_type}Feeds-button-${direction}"]
    ${left_button_exists}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="${feed_type}Feeds-button-left"]
    ${style_left}    Get Element Attribute    xpath=.//*[@id="${feed_type}Feeds-button-left"]    class
    ${style_left_is_hidden}    Run Keyword And Return Status    Should Not Contain    ${style_left}    visible
    ${right_button_exists}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="${feed_type}Feeds-button-right"]
    ${style_right}    Get Element Attribute    xpath=.//*[@id="${feed_type}Feeds-button-right"]    class
    ${style_right_is_hidden}    Run Keyword And Return Status    Should Not Contain    ${style_right}    visible
    Run Keyword If    '${direction}'=='${NAVIGATION_RIGHT}' and '${style_right_is_hidden}'!='True'    Should Be True    ${element_position}<${button_position}
    Run Keyword If    '${direction}'=='${NAVIGATION_LEFT}' and '${style_left_is_hidden}'!='True'    Should Be True    ${element_position}>${button_position}

Content Item Contains Combi Content
    Wait Until Element Is Enabled    xpath=.//app-document
    ${documents}    Get Element Count    xpath=.//app-document
    ${is_combi_content}    Run Keyword If    ${documents} <= 1    Set Variable    False    ELSE    Set Variable    True
    &{combi_data}    Create Dictionary    is_combi_content=${is_combi_content}    number_of_documents=${documents}
    [Return]    ${combi_data}

Get Attribute Of Element
    [Arguments]    ${element}    ${attribute}
    Wait Until Element Is Enabled    ${element}
    ${attribute_value}    Get Element Attribute    ${element}    ${attribute}
    Should Be True    '${attribute_value}'!='${EMPTY}'
    [Return]    ${attribute_value}

Close And Reopen Side Menu
    # Wait Until Keyword Succeeds    10s    50ms    Element Should Be Visible    xpath=.//mat-sidenav
    # Click Element On Page    xpath=.//*[@id="content-menu-button"]
    # Wait Until Keyword Succeeds    10s    50ms    Element Should Not Be Visible    xpath=.//mat-sidenav
    # Click Element On Page    xpath=.//*[@id="content-menu-button"]
    # Wait Until Keyword Succeeds    10s    50ms    Element Should Be Visible    xpath=.//mat-sidenav
    Open TOC
    #Close TOC From Inside TOC
    #Open TOC
    Close TOC From Top Menu

Check Page Header
    [Arguments]    ${method_name}
    Wait Until Element Is Enabled    xpath=.//*[@id="content-feed-button"]/span
    ${page_header}    Get Text    xpath=.//*[@id="content-feed-button"]/span
    ${page_header}    Strip String    ${page_header}
    Should Be Equal As Strings    ${method_name}    ${page_header}

Select Tab
    [Arguments]    ${tab_to_select}
    Click Element On Page    xpath=.//*[@id="${tab_to_select}"]
    Wait Until Element Is Enabled    xpath=.//*[@id="${tab_to_select}"]
    ${button_style}    Get Element Attribute    xpath=.//*[@id="${tab_to_select}"]    class
    ${status}    Run Keyword And Return Status    Should Contain    ${button_style}    selected
    Should Be True    '${status}'=='True'

Maximize Browser
    Maximize Browser Window

Scroll Page To Top
    Execute JavaScript    window.scrollTo(0, 0);

Scroll Page To Bottom
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);

Custom Scroll To Specific Element
    [Documentation]    Keyword to scroll to a particular element, verified in Chrome, Edge and Firefox browsers.
    [Arguments]    ${locator}    ${alignment}=bottom
    ${stripped_locator}=    Remove String Using Regexp    ${locator}    (^xpath:|^xpath=)
    Run Keyword If    '${alignment}'=='top'    Execute JavaScript  document.evaluate('${stripped_locator}',document.body,null,9,null).singleNodeValue.scrollIntoView();
    ...    ELSE    Execute JavaScript  document.evaluate('${stripped_locator}',document.body,null,9,null).singleNodeValue.scrollIntoView(false);

Focus And Click Element
    [Documentation]  Check element is enabled, set focus, mouse over and then click on element
    [Arguments]    ${element}
    Wait Until Element Is Enabled    ${element}
    Set Focus To Element  ${element}
    Run Keyword And Return If    '${BROWSER}'=='safari'    Click Element For Safari    ${element}
    Click Element  ${element}

Click Element For Safari
    [Documentation]    Click Element specific to Safari browser after checking if provided element is xpath or WebElement type
    ...                If element is css selector, i.e. string doesn't begin with 'xpath', use normal click instead of js
    [Arguments]    ${element}
    ${is_string}=    Run Keyword And Return Status    Should Be String    ${element}    # Check if object type is string
    ${is_css_selector}=    Run Keyword If    ${is_string}==True    Run Keyword And Return Status    Should Not Start With    ${element}    xpath
    ...    ELSE    Set Variable    ${FALSE}
    ${stripped_locator}=    Run Keyword If    ${is_string}==True    Replace String    ${element}    '    "
    ${stripped_locator}=    Run Keyword If    ${is_string}==True    Remove String Using Regexp    ${stripped_locator}    (^xpath:|^xpath=)
    Run Keyword If    ${is_string}==True and ${is_css_selector}==False    Execute Javascript    document.evaluate('${stripped_locator}',document.body,null,9,null).singleNodeValue.click();    # For xpath locator
    ...    ELSE    Click Element    ${element}    # For WebElement object

Force Links To Open In One Window
    Execute JavaScript    var a = document.getElementsByTagName("a"); for (i=0;i<a.length;i++) { if (a[i].target!="_self") { a[i].target="_self" }};

Common Failure
    [Documentation]    This keyword registered to be used in the SeleniumLibrary when error occurs in library keyword execution.
    ...
    ...    Arguments
    ...    - None
    ...    Returns
    ...    - None
    #SeleniumLibrary.Maximize Browser Window
    #¤SeleniumLibrary.Log Source
    SeleniumLibrary.Capture Page Screenshot

Custom Teardown
    Run Keyword If Test Failed    Common Failure
    Close Browser

Get Position
    ${t_position}    Execute JavaScript    var t_pos  = window.pageYOffset || document.documentElement.scrollTop; return t_pos;
    ${l_position}    Execute JavaScript    var l_pos = window.pageXOffset || document.documentElement.scrollLeft; return l_pos;
    &{positions}    Create Dictionary    top=${t_position}    left=${l_position}
    ${x_pos}    ${y_pos}    Get Window Position
    [Return]    ${positions}

Get Inner Size Of Browser Window
    ${width}    Execute JavaScript    var width  = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth; return width;
    ${height}    Execute JavaScript    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; return height;
    &{size}    Create Dictionary    width=${width}    height=${height}
    [Return]    ${size}

Create Feed For Smoke
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_SMOKE_1}    ${USER_PASSWORD_TEACHER_SMOKE_1}
    ${name}    Add Teacher Feed    ${DEFAULT_METHOD}    on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_SMOKE}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_SMOKE}    ${access_key}
    Set Suite Variable    ${NAME_FOR_SMOKE}    ${name}
    Close Browser

Switch Schools In Kampus
    [Arguments]    ${school}
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name") and not(normalize-space(text())="")]
    ${school_selected}    Get Text    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name")]
    Run Keyword If    '${school_selected}'!='${school}'    Switch School In Kampus    ${school}

Switch School In Kampus
    [Arguments]    ${school}
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user--detail")]//*[starts-with(@class, "change-school")]
    Click Element On Page    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    Wait Until Element Is Enabled    xpath=.//app-school-switching-dialog
    Click Element On Page    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and normalize-space(text())="${school}"]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name") and text()="${school}"]
    Wait Until Element Is Enabled    xpath=.//*[@class="content-container"]
    Wait Until Element Is Enabled    xpath=(.//*[@class="content-container"]//*[starts-with(@class, "feed-title")])[1]
    Wait Until Element Is Enabled    xpath=(.//*[@class="content-container"]//*[starts-with(@class, "feed-title")])[2]
    Wait For Condition    return document.readyState=="complete"

Wait For Dialog Popup To Close
    ${status}    Run Keyword And Return Status    Wait Until Keyword Succeeds    300s    2s
    ...    Wait Until Element Is Not Visible    xpath=.//mat-dialog-container    timeout=300s
    Run Keyword If    '${status}'=='False'    Verify Content Feed Loads After Page Refresh
    Wait Until Page Does Not Contain Element    xpath=.//*[contains(@class, "loading")]

Open Content Feed
    Click Element On Page    xpath=.//*[@id="theory-impl-button"]
    Wait Until Element Is Enabled    xpath=.//app-chapter-navigation-buttons
    Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]

Wait Until Content Feed Has Loaded
    [Documentation]    Locators updated based on issues noted during Feed creation.
    ...                 Fixed possible endless recursive loop by limiting the number of retries to 5
    FOR    ${loop_count}    IN RANGE    5
        ${status}    Run Keyword And Return Status    Wait Until Keyword Succeeds    60s    1s
        ...    Verify Element is Visible    xpath=//app-content-feed//div[@class="feed-container"]/app-container
        # NOTE: If current page url is not of content-feed, then clicking of feed didn't work, so fail it
        ${curr_page_url}=    Get Location
        Run Keyword If    "content-feed" not in """${curr_page_url}"""    Fail    Current Page URL doesn't reflect Content Feed URL, Previous Click on Feed name from MyPage has failed
        Run Keyword If    '${status}'=='True'    Exit For Loop
        ...    ELSE IF    ${loop_count}==4    Fail    Feed Content not loaded correctly even after 5 tries
        ...    ELSE    Reload Page
    END

Verify Content Feed Loads After Page Refresh
    Reload Page
    Wait Until Content Feed Has Loaded

###
# ReadSpeaker keywords
###
Start ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//a[starts-with(@class, "rsbtn_play") and contains(@href, "readspeaker.com")]
    Wait Until Element Is Enabled    xpath=.//app-readspeaker//button[contains(@class, "stop")]
    Wait Until Page Contains Element    xpath=.//*[@class="sync_word"]

Stop ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//button[contains(@class, "stop")]

Pause ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//button[contains(@class, "pause")]

Forward ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//button[contains(@class, "forward")]

Rewind ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//button[contains(@class, "rewind")]

Close ReadSpeaker
    Click Element On Page    xpath=.//app-readspeaker//button[contains(@class, "closer")]
    Wait Until Element Is Not Visible    xpath=.//app-readspeaker//button[contains(@class, "stop")]

Get Selected Word In ReadSpeaker
    ${selected_word_exists}    Run Keyword And Return Status    Page Should Contain Element    xpath=//*[@class="sync_word_highlighted"]
    ${selected_word}    Run Keyword If    '${selected_word_exists}'=='True'    Get Text    xpath=//*[@class="sync_word_highlighted"]    ELSE    Set Variable    ${EMPTY}
    [Return]    ${selected_word}

Get Selected Sentance In ReadSpeaker
    ${selected_sentance_exists}    Run Keyword And Return Status    Page Should Contain Element    xpath=//*[@class="sync_sent_highlighted"]
    ${selected_sentance}    Run Keyword If    '${selected_sentance_exists}'=='True'    Get Text    xpath=//*[@class="sync_sent_highlighted"]    ELSE    Set Variable    ${EMPTY}
    [Return]    ${selected_sentance}

Get All Readable Words In ReadSpeaker
    Wait Until Page Contains Element    xpath=.//*[starts-with(@class,"sync_word")]
    ${words}    Get Element Count    xpath=.//*[starts-with(@class,"sync_word")]
    @{words_all}    Create List
    FOR    ${index}    IN RANGE    0    ${words}
        ${index}    Evaluate    ${index}+1
        ${word}    Get Text    xpath=(.//*[starts-with(@class,"sync_word")])[${index}]
        Append to List    ${words_all}    ${word}
    END
    [Return]    ${words_all}

Follow Progress In ReadSpeaker
    [Arguments]    ${words_all}
    FOR    ${word}    IN    @{words_all}
        #Wait Until Page Contains Element    xpath=.//*[starts-with(@class,"sync_word_highlighted") and normalize-space(text())="${word}"]
        Wait Until Page Contains Element    xpath=.//*[starts-with(@class,"sync_word_highlighted") and normalize-space(text())="${word}"]
        ${index}    Get Index From List    ${words_all}    ${word}
    END

Get Selected Word Order Number
    [Arguments]    ${words_all}
    #${words_length}    Get Length    ${words_all}
    #:FOR    ${index}    IN    0    ${words_length}
    ${index}    Set Variable    ${0}
    FOR    ${word}    IN    @{words_all}
        ${index}    Evaluate    ${index}+1
        ${word_found}    Run Keyword And Return Status
        ...    Page Should Contain Element    xpath=.//*[starts-with(@class,"sync_word_highlighted") and normalize-space(text())="${word}"]
        Return From Keyword If    '${word_found}'=='True'    ${index}
    END

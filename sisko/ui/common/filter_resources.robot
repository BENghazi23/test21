*** Keywords ***
Turn Student View Filter On
    Wait Until Element Is Enabled    xpath=.//*[@id="students-view-button"]
    ${filter_style}    Wait Until Keyword Succeeds    10    1    Get Element Attribute    xpath=.//*[@id="students-view-button"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${filter_style}    selected
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="students-view-button"]
    Wait Until Keyword Succeeds    10    1    Wait Until Element Is Enabled    xpath=.//*[@id="students-view-button" and contains(@class, "selected")]

Turn Student View Filter Off
    Wait Until Element Is Enabled    xpath=.//*[@id="students-view-button"]
    ${filter_style}    Wait Until Keyword Succeeds    10    1    Get Element Attribute    xpath=.//*[@id="students-view-button"]    class
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Contain    ${filter_style}    selected
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="students-view-button"]
    Wait Until Keyword Succeeds    10    1    Wait Until Element Is Enabled    xpath=.//*[@id="students-view-button" and not(contains(@class, "selected"))]

Check That No Teacher Material Is Visible
    Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]
    ${teachers_material}    Get Element Count    xpath=.//*[starts-with(@id, "content-item") and starts-with(@content-type, "teacher")]
    Should Be Equal As Numbers    ${teachers_material}    0

Check That No Teacher Material Is Visible In Student View Filter
    Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]
    ${teacher_content_items_all}    Get Element Count    xpath=.//*[starts-with(@id, "content-item") and starts-with(@content-type, "teacher")]
    FOR    ${index}    IN RANGE     0       ${teacher_content_items_all}
        ${index}        Evaluate    ${index}+1
        ${content_item_hidden}    Get Element Attribute    xpath=(.//*[starts-with(@id, "content-item") and starts-with(@content-type, "teacher")])[${index}]/ancestor::app-content-item    class
        Should Contain    ${content_item_hidden}    hidden
    END

Check If Filter Dialog Is Open
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "filter-content")]
    [Return]    ${status}

Filter Dialog Is Visible
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "filter-content")]

Filter Dialog Is Hidden
    Wait Until Element Is Not Visible    xpath=.//*[starts-with(@class, "filter-content")]

Show Filters
    Wait Until Element Is Enabled    xpath=.//*[@id="filter-bar-button"]
    ${status}    Check If Filter Dialog Is Open
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="filter-bar-button"]
    Filter Dialog Is Visible

Hide Filters
    Wait Until Element Is Enabled    xpath=.//*[@id="filter-bar-button"]
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "filter-content")]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="all-filter-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//*[@id="filter-bar-button"]
    Filter Dialog Is Hidden

Get Filters
    Show Filters
    ${filters}    Get Element Count    xpath=.//app-content-filter//mat-button-toggle
    @{filter_list}    Create List
    FOR    ${index}    IN RANGE     0       ${filters}
        ${index}        Evaluate    ${index}+1
        ${filter_category}    Get Element Attribute    xpath=(.//app-content-filter//mat-button-toggle)[${index}]    id
        ${filter_category}    Remove String    ${filter_category}    -filter
        Append To List    ${filter_list}    ${filter_category}
    END
    [Return]    ${filter_list}

Is Student View On
    ${student_filter_exists}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//*[@id="students-view-button"]
    ${student_button_style}    Run Keyword If    '${student_filter_exists}'=='True'    Get Element Attribute    xpath=.//*[@id="students-view-button"]    class
    ...    ELSE    Set Variable    False
    ${student_view_selected}    Run Keyword And Return Status    Should Contain    ${student_button_style}    selected
    [Return]    ${student_view_selected}

Check Filters
    ${filters}    Get Filters
    ${filters_length}    Get Length    ${filters}
    ${student_view_selected}    Is Student View On
    FOR    ${index}    IN RANGE     0       ${filters_length}
        Show Filters
        Click Element On Page    xpath=.//*[@id="${filters[${index}]}-filter"]
        ${content_items}    Get Content Item Visibility    ${filters[${index}]}
        Check Content Item Visibility    ${content_items}    ${filters[${index}]}    ${student_view_selected}
    END
    Hide Filters

Check Filters In All Chapters
    ${chapter_names}    Get Chapter Names
    FOR    ${chapter_name}    IN    @{chapter_names}
        Open Chapter    ${chapter_name}
        Check Filters
    END

Get Content Item Visibility
    [Arguments]    ${filter_selected}
    ${content_item_list}    Create List
    ${content_items_all}    Get Element Count    xpath=.//*[starts-with(@id, "content-item")]
    FOR    ${index}    IN RANGE     0       ${content_items_all}
        ${index}        Evaluate    ${index}+1
        ${content_item_type}    Get Element Attribute    xpath=(.//*[starts-with(@id, "content-item")])[${index}]    content-type
        ${content_item_class}    Get Element Attribute    xpath=(.//*[starts-with(@id, "content-item")])[${index}]/ancestor::app-content-item    class
        ${content_item_hidden}    Run Keyword And Return Status    Should Contain    ${content_item_class}    hidden
        ${content_item_entry}    Catenate    SEPARATOR=:    ${content_item_type}    ${content_item_hidden}
        Append To List    ${content_item_list}    ${content_item_entry}
    END
    [Return]    ${content_item_list}

Check Content Item Visibility
    [Arguments]    ${content_item_list}    ${filter_selected}    ${student_view_selected}
    ${content_item_list_length}    Get Length    ${content_item_list}
    FOR    ${index}    IN RANGE     0       ${content_item_list_length}
        ${content}    Split String    ${content_item_list[${index}]}    :
        ${is_teacher_content_item}    Run Keyword And Return Status    Should Contain    ${content[0]}    teacher
        Run Keyword If    '${content[0]}'=='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='False'
        Run Keyword If    '${content[0]}'=='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='False'
        Run Keyword If    '${content[0]}'=='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='False'
        Run Keyword If    '${content[0]}'=='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${content[0]}'!='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${content[0]}'!='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${content[0]}'!='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${content[0]}'!='${filter_selected}' and '${filter_selected}'!='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${filter_selected}'=='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='True'
        Run Keyword If    '${filter_selected}'=='all' and '${is_teacher_content_item}'=='True' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='False'
        Run Keyword If    '${filter_selected}'=='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='True'    Should Be True    '${content[1]}'=='False'
        Run Keyword If    '${filter_selected}'=='all' and '${is_teacher_content_item}'=='False' and '${student_view_selected}'=='False'    Should Be True    '${content[1]}'=='False'
    END

Check Filtered Content Is Shown Correctly
    [Documentation]    This keyword checks that the selected filter shows correct content items only
    [Arguments]    ${filter_selected}
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@id, "content-item")]
    ${content_items_all}    Get Element Count    xpath=.//*[starts-with(@id, "content-item")]
    FOR    ${index}    IN RANGE     0       ${content_items_all}
        ${index}        Evaluate    ${index}+1
        ${content_item_class}    Get Element Attribute    xpath=(.//mat-list//*[starts-with(@id, "content-item")])[${index}]    content-type
        ${content_item_hidden}    Get Element Attribute    xpath=(.//mat-list//*[starts-with(@id, "content-item")])[${index}]/ancestor::app-content-item    hidden
        ${content_item_class}    Fetch From Left    ${content_item_class}    ${SPACE}
        Run Keyword If    '${content_item_class}'=='${filter_selected}' or '${filter_selected}'=='all'    Should Be True    '${content_item_hidden}'=='None'
        Run Keyword If    '${content_item_class}'!='${filter_selected}' and '${filter_selected}'!='all'    Should Be True    '${content_item_hidden}'=='true'
    END

Check Filtered Content Is Shown Correctly In Student View
    [Documentation]    This keyword checks that the selected filter shows correct content items only in the Student View
    [Arguments]    ${filter_selected}    ${test_param}
    Run Keyword If    '${test_param}'=='student_yes'    Select Show All Filter
    Run Keyword If    '${test_param}'=='student_yes'    Turn Student View Filter On
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@id, "content-item")]
    ${content_items_all}    Get Element Count    xpath=.//*[starts-with(@id, "content-item")]
    FOR    ${index}    IN RANGE     0       ${content_items_all}
        ${index}        Evaluate    ${index}+1
        ${content_item_class}    Get Element Attribute    xpath=(.//mat-list//*[starts-with(@id, "content-item")])[${index}]    content-type
        ${content_item_hidden}    Get Element Attribute    xpath=(.//mat-list//*[starts-with(@id, "content-item")])[${index}]/ancestor::app-content-item    hidden
        ${content_item_class}    Fetch From Left    ${content_item_class}    ${SPACE}
        ${status}    Run Keyword And Return Status    Should Start With    ${content_item_class}    teacher
        Run Keyword If    '${filter_selected}'!="students-view-button" and ('${content_item_class}'=='${filter_selected}' or '${filter_selected}'=='all') and '${status}'=='False'    Should Be True    '${content_item_hidden}'=='None'
        Run Keyword If    '${filter_selected}'!="students-view-button" and ('${content_item_class}'!='${filter_selected}' and '${filter_selected}'!='all') or '${status}'=='True'    Should Be True    '${content_item_hidden}'=='true'
    END

Select Show All Filter
    [Documentation]    This keyword turns the "Select all" filter on
    Show Filters
    Click Element On Page    xpath=.//*[@id="all-filter-button"]
    Hide Filters

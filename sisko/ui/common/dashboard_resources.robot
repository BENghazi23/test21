*** Keywords ***
Create Feed For Dashboard
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}
    ${name}     Add Teacher Feed     0 Sisko Manual MR     on    ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_DASHBOARD}    scroll_off
    ${access_key}       Create Access Key
    Set Suite Variable    ${ACCESS_KEY_FOR_DASHBOARD}    ${access_key}
    Set Suite Variable    ${NAME_FOR_DASHBOARD}    ${name}
    Close Browser

Open Dashboard
    #Wait Until Keyword Succeeds    30s    1s    Wait Until Element Is Enabled    xpath=.//*[@class="content-feed"]//app-container//app-section
    Wait Until Keyword Succeeds    30s    1s    Click Element On Page    xpath=.//*[@id="dashboard-impl-button"]
    Wait Until Element Is Enabled    xpath=.//app-dashboard-menu//*[@class="feed-dtoc"]

Check Name Appears In Student List
    [Arguments]    @{users}
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="student-results-container"]//*[@class="student-container"]//*[contains(@class, "student-name")]
    FOR   ${user}  IN  @{users}
        ${user_to_find}    Catenate    ${USER_FIRST_NAME_${user}}    ${USER_LAST_NAME_${user}}
        Page Should Contain      ${user_to_find}
    END

Check Name Does Not Appear
    [Arguments]    @{users}
    Wait Until Element Is Visible    xpath=.//mat-sidenav-content//*[@class="headings"]
    Sleep   3     #TODO: need other check!
    FOR   ${user}  IN  @{users}
        ${user_to_find}    Catenate    ${USER_FIRST_NAME_${user}}    ${USER_LAST_NAME_${user}}
        Page Should Not Contain      ${user_to_find}
    END

Check Pagination
    ${pagination_visible}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${single_student_view}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//app-single-student
    ${multiple_student_view}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//app-student-results
    Run Keyword If    '${pagination_visible}'=='True' and '${single_student_view}'=='True'    Check Pagination In Single Student View
    Run Keyword If    '${pagination_visible}'=='True' and '${multiple_student_view}'=='True'    Check Pagination In Multiple Student View

Check Pagination In Single Student View
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${exercise_list_for_comparison}    Create List
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        ${exercise_list}    Check Exercises In Single Student View
        Should Not Be Equal    ${exercise_list_for_comparison}    ${exercise_list}
        ${exercise_list_length}    Get Length    ${exercise_list}
        Should Be True    ${exercise_list_length} > 0
        ${exercise_list_for_comparison}    Copy List    ${exercise_list}
    END

Check Pagination In Multiple Student View
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${exercise_list_for_comparison}    Create List
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        ${exercise_list}    Check Exercises In Multiple Student View
        Should Not Be Equal    ${exercise_list_for_comparison}    ${exercise_list}
        ${exercise_list_length}    Get Length    ${exercise_list}
        Should Be True    ${exercise_list_length} > 0
        ${exercise_list_for_comparison}    Copy List    ${exercise_list}
    END

Open Exercise In Assessment View
    [Arguments]    ${exercise_title}
    Click Element On Page    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//app-assessment-view
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//app-assessment-view
    Should Be Equal As Strings    ${status}    True

Open Assessment View
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class, "result")]
    #${questions}    Get Element Count    xpath=(.//app-student-results//*[@class="exercise-results"])[1]//div[starts-with(@class, "result")]
    # Limiting the questions to go through to 1 to save running time
    ${questions}    Set Variable    ${1}
    FOR    ${index}    IN RANGE     0       ${questions}
        ${index}        Evaluate    ${index}+1
        Element Should Not Be Visible    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class,"content")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class, "result")])[${index}]
        Mouse Over    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class, "result")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class,"content")])[${index}]
        ${question_name}    Get Text    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class,"content")])[${index}]/p[1]
        Click Element On Page    xpath=(.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class, "result")])[${index}]
        Wait Until Element Is Enabled    xpath=.//app-assessment-view
        ${status}     Run Keyword And Return Status    Wait Until Keyword Succeeds    10s    50ms
        ...    Wait Until Element Is Enabled    xpath=.//app-assessment-view//*[@class="exercise-content"]//app-module-content-documents//div[starts-with(@class, "title-feedback")]/h2
        Run Keyword If    '${status}'=='False'    Reload Page
        Run Keyword If    '${status}'=='False'    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="exercise-results"]//div[starts-with(@class, "result")]
        Continue For Loop If    '${status}'=='False'
        ${question_name_assessment_view}    Get Text    xpath=.//app-assessment-view//*[@class="exercise-content"]//app-module-content-documents//div[starts-with(@class, "title-feedback")]/h2
        ${question_name}    Strip String    ${question_name}
        ${question_name_assessment_view}    Strip String    ${question_name_assessment_view}
        ${status}    Run Keyword And Return Status
        ...    Should Start With    ${question_name_assessment_view}    ${question_name}
        Run Keyword If    '${status}'=='False'
        ...    Wait Until Element Is Enabled    xpath=(.//app-assessment-view//*[@class="exercise-content"]//app-module-content-documents//div[@class="title-feedback"]/h3)[1]
        ${question_name_assessment_view}    Run Keyword If    '${status}'=='False'
        ...    Get Text    xpath=(.//app-assessment-view//*[@class="exercise-content"]//app-module-content-documents//div[@class="title-feedback"]/h3)[1]
        ${question_name_assessment_view}    Run Keyword If    '${status}'=='False'
        ...    Strip String    ${question_name_assessment_view}
        Run Keyword If    '${status}'=='False'
        ...    Should Be Equal As Strings    ${question_name}    ${question_name_assessment_view}
        Close Assessment View
    END

Check Assessment View
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${excercise_list_for_comparison}    Create List
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        Open Assessment View
    END

Is Exercise Visible
        Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${exercise_list_for_comparison}    Create List
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
    END

Show Exercise In View
    [Arguments]    ${exercise_title}
    Maximize Browser
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${exercise_list_for_comparison}    Create List
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        ${status}    Check If Exercise In View    ${exercise_title}
        Run Keyword If    '${status}'=='True'    Exit For Loop
    END
    [Return]    ${status}

Check If Exercise In View
    [Arguments]    ${exercise_title}
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled
    ...    xpath=.//app-student-results//*[@class="exercise-results"]//div[@ng-reflect-klass="result"]//div[starts-with(@class,"content")]/p[normalize-space(text())="${exercise_title}"]
    [Return]    ${status}

Check Exercise Status
    [Arguments]    ${exercise_title}
    Wait Until Element Is Enabled
    ...    xpath=.//app-single-student//*[starts-with(@class, "student-container")]/*[starts-with(@class, "student-name")]/parent::*[starts-with(@class, "student-container")]/*[@class="exercise-results"]/app-result/*[starts-with(@class, "result")]/*[@class="content"]/*[normalize-space(string())="${exercise_title}"]
    ${style}    Get Element Attribute
    ...    xpath=.//app-single-student//app-result/*[starts-with(@class, "result")]/*[@class="content"]/*[normalize-space(string())="${exercise_title}"]/ancestor::*[starts-with(@class, "result")][1]
    ...    style
    [Return]    ${style}

Check Exercises In Single Student View
    ${exercise_list}    Create List
    Wait Until Element Is Enabled    xpath=.//*[@class="page-content"]//app-single-student
    Wait Until Element Is Enabled
    ...    xpath=.//app-single-student//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1]
    ${exercises}    Get Element Count
    ...    xpath=.//app-single-student//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1]
    FOR    ${index}    IN RANGE     0       ${exercises}
        ${index}        Evaluate    ${index}+1
        ${exercise_title}    Get Element Attribute
        ...    xpath=(.//app-single-student//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1])[${index}]    innerHTML
        ${element_title}    Strip String    ${exercise_title}
        Append To List    ${exercise_list}    ${exercise_title}
    END
    [Return]    ${exercise_list}

Check Exercises In Multiple Student View
    ${exercise_list}    Create List
    Wait Until Element Is Enabled    xpath=.//*[@class="page-content"]//app-student-results
    Wait Until Element Is Enabled
    ...    xpath=.//app-student-results//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1]
    ${exercises}    Get Element Count
    ...    xpath=.//app-student-results//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1]
    FOR    ${index}    IN RANGE     0       ${exercises}
        ${index}        Evaluate    ${index}+1
        ${exercise_title}    Get Element Attribute
        ...    xpath=(.//app-student-results//div[@class="exercise-results"]//div[contains(@class, "result")]//div[@class="content"]/p[text()[not(.="")]][1])[${index}]    innerHTML
        ${element_title}    Strip String    ${exercise_title}
        Append To List    ${exercise_list}    ${exercise_title}
    END
    [Return]    ${exercise_list}

Get Answer Colors
    ${answers}    Create Dictionary
    Wait Until Element Is Enabled    xpath=.//*[@class="color-explanation-inner"]//span
    ${answer_colors}    Get Element Count    xpath=.//*[@class="color-explanation-inner"]//span
    FOR    ${index}    IN RANGE     0       ${answer_colors}
        ${index}        Evaluate    ${index}+1
        ${color}    Get Element Attribute    xpath=(.//*[@class="color-explanation-inner"]//span)[${index}]    style
        ${status}    Run Keyword And Return Status
        ...    Should Contain    ${color}    background-color
        ${color}    Run Keyword If    '${status}'=='True'
        ...    Set Variable    ${color}
        ...    ELSE
        ...    Set Variable    striped
        Run Keyword If    ${index}==1    Set To Dictionary    ${answers}    correct    ${color}
        Run Keyword If    ${index}==2    Set To Dictionary    ${answers}    incorrect    ${color}
        Run Keyword If    ${index}==3    Set To Dictionary    ${answers}    auto_assessed    ${color}
        Run Keyword If    ${index}==4    Set To Dictionary    ${answers}    unanswered    ${color}
    END
    [Return]    ${answers}

# Get Answer Statuses
#     ${answers_in_view}    Get Element Count    xpath=.//app-student-results//*[@class="exercise-results"]//div[@ng-reflect-klass="result"]
#     :FOR    ${index}    IN RANGE     0       ${answers_in_view}
#     \    ${index}        Evaluate    ${index}+1
#     \    ${style}    Get Element Attribute    xpath=(.//app-student-results//*[@class="exercise-results"]//div[@ng-reflect-klass="result"])[${index}]    style
#     \    ${class}    Get Element Attribute    xpath=(.//app-student-results//*[@class="exercise-results"]//div[@ng-reflect-klass="result"])[${index}]    class

# Browse Dashboard TOC
#     Wait Until Element Is Enabled    xpath=.//app-dashboard-menu
#     ${chapters}    Get Element Count    xpath=.//app-dashboard-menu//mat-accordion/mat-expansion-panel
#     :FOR    ${index_chapter}    IN RANGE     0       ${chapters}
#     \    ${index_chapter}        Evaluate    ${index_chapter}+1
#     \    Click Element On Page    xpath=(.//app-dashboard-menu//mat-accordion/mat-expansion-panel)[${index_chapter}]
#     \    Sleep    1
#     \    Check Pagination

Open Dashboard TOC
    [Arguments]    ${chapter}    ${subchapter}=${EMPTY}
    Wait Until Element Is Enabled    xpath=.//app-dashboard-menu
    # ${style}    Get Element Attribute
    # ...    xpath=.//app-dashboard-menu//mat-accordion/mat-expansion-panel/mat-expansion-panel-header//*[@class="chapter-title" and contains(text(), "${chapter}")]//ancestor::mat-expansion-panel/mat-expansion-panel-header//*[contains(@class, "mat-expansion-indicator")]
    # ...    style
    ${style}    Get Element Attribute
    ...    xpath=.//app-dashboard-menu//*[contains(@class, "chapter-title") and contains(string(), "${chapter}")]/ancestor::div[contains(@class, "dropdown-sidebar")]
    ...    class
    ${isClosed}    Run Keyword And Return Status    Should Not Contain    ${style}    open
    Run Keyword If    ${isClosed}==True
    ...    Click Element On Page    xpath=.//app-dashboard-menu//*[contains(@class, "chapter-title") and contains(string(), "${chapter}")]
    Mouse Over
    ...    xpath=.//*[@class="kampus-fox"]
    Run Keyword If    '${subchapter}'!='${EMPTY}'    Wait Until Element Is Visible
    ...    xpath=.//app-dashboard-menu//*[contains(@class, "chapter-title") and contains(string(), "${chapter}")]/ancestor::div[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]/*[@class="content"]/*[contains(@class, "item") and contains(string(), "${subchapter}")]
    Run Keyword If    '${subchapter}'!='${EMPTY}'    Click Element On Page
    ...    xpath=.//app-dashboard-menu//*[contains(@class, "chapter-title") and contains(string(), "${chapter}")]/ancestor::div[contains(@class, "dropdown-sidebar") and contains(@class, "opened")]/*[@class="content"]/*[contains(@class, "item") and contains(string(), "${subchapter}")]
    Run Keyword If    '${subchapter}'!='${EMPTY}'    Wait Until Element Is Enabled    xpath=.//*[@class="student-results-container"]

Check Dashboard TOC
    [Arguments]     ${isEmpty}==true
    Wait Until Element Is Enabled    xpath=.//*[@class="dtoc_feed"]//*[@class="feed-title"]
    ${toc_header}    Get Text    xpath=.//*[@class="dtoc_feed"]//*[@class="feed-title"]
    ${chapters}    Get Element Count    xpath=.//app-dashboard-menu//mat-accordion
    # : FOR    ${INDEX}    IN RANGE    1    ${chapters}  # TODO: check chapters

Handle Slider
    [Arguments]     ${passing_amount}
    Click Element On Page   xpath=.//*[@id="passing"]
    ${status} =  Evaluate   ${passing_amount} < 50
    Run Keyword If      ${status} == False      Slider Right        ${passing_amount}
    Run Keyword If      ${status} == True       Slider Left       ${passing_amount}

Slider Right
    [Arguments]     ${passing_amount}
    ${result} =    Convert To Integer      ${passing_amount}
    ${count} =    Evaluate     ${result} - 49
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \   Press Keys   xpath=.//*[@id="passing"]       \ue014

Slider Left
    [Arguments]    ${passing_amount}
    ${result} =    Convert To Integer     ${passing_amount}
    ${count} =    Evaluate    51 - ${result}
    : FOR    ${INDEX}    IN RANGE    1   ${count}
    \   Press Keys   xpath=.//*[@id="passing"]       \ue012

Check Exercise Circles If Slider Is Handled
    #TODO

Click Dashboard Top TOC
    Wait Until Element Is Enabled    xpath=.//app-dashboard-menu//*[@class="feed-dtoc"]
    Click Element On Page    xpath=.//app-dashboard-menu//*[@class="feed-dtoc"]
    Sleep    5

Check Dashboard TOC And Points In Exercises
    ${user}    Set Variable    STUDENT_1_DASHBOARD
    ${student_name}    Catenate    ${USER_FIRST_NAME_${user}}    ${USER_LAST_NAME_${user}}
    Select Student In Dashboard    ${student_name}
    ${answer_colors}    Get Answer Colors
    Set Test Variable    ${ANSWER_COLORS}    ${answer_colors}
    Click Dashboard Top TOC
    #
    # #--->>>Check Points In Single Student View
    # Loop Through Exercise Pages
    #
    Wait Until Element Is Enabled    xpath=.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")]
    ${chapters}    Get Element Count    xpath=.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")]
    FOR    ${index}    IN RANGE    0    ${chapters}
        ${index}    Evaluate    ${index}+1
        ${chapter_title}    Get Text    xpath=(.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")]//*[@class="title-text"])[${index}]
        ${chapter_class}    Get Element Attribute    xpath=(.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")])[${index}]    class
        ${chapter_status_check}    Run Keyword And Return Status    Should Contain    ${chapter_class}    opened
        ${chapter_status}    Set Variable If
        ...    '${chapter_status_check}'=='True'    opened
        ...    '${chapter_status_check}'=='False'    closed
        Open Dashboard TOC    ${chapter_title}
        Wait Until Element Is Enabled    xpath=(.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")])[${index}]
        ${chapter_class}    Get Element Attribute    xpath=(.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")])[${index}]    class
        ${chapter_status_check}    Run Keyword And Return Status    Should Contain    ${chapter_class}    opened
        ${chapter_status}    Set Variable If
        ...    '${chapter_status_check}'=='True'    opened
        ...    '${chapter_status_check}'=='False'    closed
        ${subchapters}    Get Dashboard TOC Subchapters    ${chapter_title}
        Get Subchapters In Single Student View
    END
    ${answer_colors}    Get Answer Colors
    Set Test Variable    ${ANSWER_COLORS}    ${answer_colors}
    Select Student In Dashboard
    Click Dashboard Top TOC
    ${points_total}    ${points_max}    ${points_exercise}    Calculate Exercise Points
    ${total_points}    ${total_max}    ${total_exercise}    Get Total Points
    Should Be Equal As Numbers    ${points_total}    ${total_points}
    Should Be Equal As Numbers    ${points_max}    ${total_max}
    # Check this -> fails at the moment
    #Should Be Equal As Numbers    ${points_exercise}    ${total_exercise}

Get Dashboard TOC Subchapters
    [Arguments]    ${chapter_title}
    ${subchapter_titles}    Create List
    ${subchapters}    Get Element Count
    ...    xpath=.//*[@class="title-text" and text()="${chapter_title}"]/ancestor::*[contains(@class, "dropdown-sidebar")]//*[@class="subchapter-container"]//*[contains(@class, "text")]
    FOR    ${index}    IN RANGE    0    ${subchapters}
        ${index}    Evaluate    ${index}+1
        ${subchapter_title}    Get Text
        ...    xpath=(.//*[@class="title-text" and text()="${chapter_title}"]/ancestor::*[contains(@class, "dropdown-sidebar")]//*[@class="subchapter-container"]//*[contains(@class, "text")])[${index}]
        #Append To List    ${subchapter_titles}    ${subchapter_title}
        ${subchapter_class}    Get Element Attribute
        ...    xpath=(.//*[@class="title-text" and text()="${chapter_title}"]/ancestor::*[contains(@class, "dropdown-sidebar")]//*[@class="content"]/*[contains(@class, "item")])[${index}]    class
        ${subchapter_status_check}    Run Keyword And Return Status    Should Contain    ${subchapter_class}    disabled
        # ${subchapter_status}    Set Variable If
        # ...    '${subchapter_status_check}'=='True'    disabled
        # ...    '${subchapter_status_check}'=='False'    enabled
        Run Keyword If    '${subchapter_status_check}'=='False'    Append To List    ${subchapter_titles}    ${subchapter_title}
    END
    [Return]    ${subchapter_titles}

Select Student In Dashboard
    [Arguments]    ${student_name}=${EMPTY}
    Click Element On Page    xpath=.//*[@id="select-user-button"]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p
    Run Keyword If    '${student_name}'!='${EMPTY}'
    ...    Click Element On Page    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p[normalize-space(text())="${student_name}"]
    Run Keyword If    '${student_name}'!='${EMPTY}'
    ...    Wait Until Element Is Enabled    xpath=.//app-single-student//app-result/*[starts-with(@class, "result")]/*[@class="content"]
    Run Keyword If    '${student_name}'=='${EMPTY}'
    ...    Click Element On Page    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p[1]
    Run Keyword If    '${student_name}'=='${EMPTY}'
    ...    Wait Until Element Is Enabled    xpath=.//app-student-results//app-result/*[starts-with(@class, "result")]/*[@class="content"]

Get Subchapters In Single Student View
    Sleep    2
    Wait Until Element Is Enabled    xpath=.//*[@class="student-results-container"]
    Wait Until Element Is Enabled    xpath=.//app-single-student//*[@class="student-name"]
    ${subchapters_in_dashboard}    Get Element Count    xpath=.//app-single-student//*[@class="student-name"]
    FOR    ${index}    IN RANGE    0    ${subchapters_in_dashboard}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-single-student//*[@class="student-name"])[${index}]
        ${subchapter_in_dashboard}    Wait Until Keyword Succeeds    5s    50ms
        ...    Get Text    xpath=(.//app-single-student//*[@class="student-name"])[${index}]
        ${subchapter_in_dashboard}    Strip String    ${subchapter_in_dashboard}
    END

# Get Dashboard TOC
#     #Wait Until Element Is Enabled    xpath=.//app-dashboard-menu//*[@class="feed-dtoc"]
#     #Click Element On Page    xpath=.//app-dashboard-menu//*[@class="feed-dtoc"]
#     ${chapters}    Get Element Count    xpath=

Check Points In Single Student View
    ${points_total}    ${points_max}    ${points_exercise}    Calculate Exercise Points In Single Student View

# For all student view
Calculate Exercise Points
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    Set Test Variable    ${points_total_in_total}    0
    Set Test Variable    ${points_max_in_total}    0
    Set Test Variable    ${points_exercise_in_total}    0
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        ${points_total}    ${points_max}    ${points_exercise}    Get Points
        ${points_total_in_total}    Evaluate    ${points_total_in_total}+${points_total}
        ${points_max_in_total}    Evaluate    ${points_max_in_total}+${points_max}
        ${points_exercise_in_total}    Evaluate    ${points_exercise_in_total}+${points_exercise}
    END
    [Return]    ${points_total_in_total}    ${points_max_in_total}    ${points_exercise_in_total}

# For single student view
Calculate Exercise Points In Single Student View
    ${chapters_with_exercises}    Get Element Count    xpath=.//app-single-student//app-result
    Set Test Variable    ${points_total_in_total_single_view}    0
    Set Test Variable    ${points_max_in_total_single_view}    0
    Set Test Variable    ${points_exercise_in_total_single_view}    0
    FOR    ${index}    IN RANGE     0       ${chapters_with_exercises}
        ${index}        Evaluate    ${index}+1
        ${points_total_single_view}    ${points_max_single_view}    ${points_exercise_single_view}    Get Points In Single Student View    ${index}
    END

Loop Through Exercise Pages
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    ${pages}    Get Element Count    xpath=.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")]
    FOR    ${index}    IN RANGE     0       ${pages}
        ${index}        Evaluate    ${index}+1
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Click Element On Page    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]
        ${page_selected_style}    Get Element Attribute    xpath=(.//*[starts-with(@class, "pagination")]//*[starts-with(@class, "page")])[${index}]    class
        Should Contain    ${page_selected_style}    selected
        Calculate Exercise Points In Single Student View
    END

# For all student view
Get Points
    Wait Until Element Is Enabled    xpath=.//app-student-results//app-result/*[starts-with(@class, "result")]
    ${exercises}    Get Element Count    xpath=.//app-student-results//app-result/*[starts-with(@class, "result")]
    ${total_points_added}    Set Variable    0
    ${total_max_added}    Set Variable    0
    ${total_exercise_added}    Set Variable    0
    FOR    ${index}    IN RANGE     0       ${exercises}
        ${index}        Evaluate    ${index}+1
        ${style}    Get Element Attribute    xpath=(.//app-student-results//app-result/*[starts-with(@class, "result")])[${index}]    style
        ${class}    Get Element Attribute    xpath=(.//app-student-results//app-result/*[starts-with(@class, "result")])[${index}]    class
        ${class_is_striped}    Run Keyword And Return Status    Should Contain    ${class}    striped
        ${style}    Set Variable If    '${class_is_striped}'=='True'    striped
        ...    '${class_is_striped}'=='False'    ${style}
        ${points}    Get Element Attribute    xpath=(.//app-student-results//app-result/*[starts-with(@class, "result")])[${index}]/*[starts-with(@class, "content")]/p[2]    innerHTML
        ${point_values}    Get Regexp Matches    ${points}    \\d+
        # the below line checks that points are in numbers, text is ignored
        ${point_values_length}    Get Length    ${point_values}
        ${total_points_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_points_added}+${point_values}[0]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_points_added}+${point_values}[0]
        ...    ELSE    Evaluate    ${total_points_added}+0
        ${total_max_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_max_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_max_added}+${point_values}[1]
        ...    ELSE    Evaluate    ${total_max_added}+0
        ${total_exercise_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.unanswered}' and ${point_values_length} > 0    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE    Evaluate    ${total_exercise_added}+0
    END
    [Return]    ${total_points_added}    ${total_max_added}    ${total_exercise_added}

# For single student view
Get Points In Single Student View
    [Arguments]    ${row_number}
    #Wait Until Element Is Enabled    xpath=(.//app-single-student//app-result)[${row_number}]/*[starts-with(@class, "result")]
    Sleep    2
    ${exercises}    Get Element Count    xpath=(.//app-single-student//app-result)[${row_number}]/*[starts-with(@class, "result")]
    ${total_points_added}    Set Variable    0
    ${total_max_added}    Set Variable    0
    ${total_exercise_added}    Set Variable    0
    FOR    ${index}    IN RANGE     0       ${exercises}
        ${index}        Evaluate    ${index}+1
        ${style}    Get Element Attribute    xpath=((.//app-single-student//app-result)[${row_number}]/*[starts-with(@class, "result")])[${index}]    style
        ${class}    Get Element Attribute    xpath=((.//app-single-student//app-result)[${row_number}]/*[starts-with(@class, "result")])[${index}]    class
        ${class_is_striped}    Run Keyword And Return Status    Should Contain    ${class}    striped
        ${style}    Set Variable If    '${class_is_striped}'=='True'    striped
        ...    '${class_is_striped}'=='False'    ${style}
        ${points}    Get Element Attribute    xpath=((.//app-single-student//app-result)[${row_number}]/*[starts-with(@class, "result")])[${index}]/*[starts-with(@class, "content")]/p[2]    innerHTML
        ${point_values}    Get Regexp Matches    ${points}    \\d+
        # the below line checks that points are in numbers, text is ignored
        ${point_values_length}    Get Length    ${point_values}
        ${total_points_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_points_added}+${point_values}[0]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_points_added}+${point_values}[0]
        ...    ELSE    Evaluate    ${total_points_added}+0
        ${total_max_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_max_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_max_added}+${point_values}[1]
        ...    ELSE    Evaluate    ${total_max_added}+0
        ${total_exercise_added}    Run Keyword If    '${style}'=='${ANSWER_COLORS.correct}'    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.incorrect}'    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE IF    '${style}'=='${ANSWER_COLORS.unanswered}' and ${point_values_length} > 0    Evaluate    ${total_exercise_added}+${point_values}[1]
        ...    ELSE    Evaluate    ${total_exercise_added}+0
    END
    [Return]    ${total_points_added}    ${total_max_added}    ${total_exercise_added}

# For all student view
Get Total Points
    Wait Until Element Is Enabled    xpath=.//app-student-results//app-result/*[starts-with(@class, "score")]
    ${total_points}    Get Text    xpath=.//app-student-results//app-result/*[starts-with(@class, "score")]/*[@class="score-total"]
    ${total_points}    Get Regexp Matches    ${total_points}    \\d+
    ${total_points}    Set Variable    ${total_points}[0]
    ${total_max}    Get Text    xpath=.//app-student-results//app-result/*[starts-with(@class, "score")]/*[@class="score-max"]
    ${total_max}    Get Regexp Matches    ${total_max}    \\d+
    ${total_max}    Set Variable    ${total_max}[0]
    ${total_exercise}    Get Text    xpath=.//*[@class="headings"]/*[starts-with(@class, "max-score")]
    ${total_exercise}    Get Regexp Matches    ${total_exercise}    \\d+
    ${total_exercise}    Set Variable    ${total_exercise}[0]
    [Return]    ${total_points}    ${total_max}    ${total_exercise}

Give Feedback
    [Arguments]    ${feedback_text}
    Wait Until Keyword Succeeds    5s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-textarea"]/textarea
    Press Keys    xpath=.//*[@id="feedback-textarea"]/textarea    ${feedback_text}

Modify Feedback
    [Arguments]    ${feedback_text}
    Wait Until Keyword Succeeds    5s    50ms    Wait Until Element Is Enabled    xpath=.//*[@class="feedback-text"]
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-modify-button"]
    Click Element On Page    xpath=.//*[@id="feedback-modify-button"]
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-textarea"]/textarea
    Clear Element Text    xpath=.//*[@id="feedback-textarea"]/textarea
    #Feedback Status Should Be Not Submitted
    Feedback Status Should Be Submitted
    Give Feedback    ${feedback_text}

Submit Feedback
    Click Element On Page    xpath=.//*[@id="feedback-submit-button"]
    Wait Until Keyword Succeeds    5s    50ms    Feedback Status Should Be Submitted
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-modify-button"]

Feedback Status Should Be Submitted
    ${feedback_status}    Get Feedback Status
    List Should Contain Value    ${FEEDBACK_STATUS_SUBMITTED}    ${feedback_status}

Feedback Status Should Be Not Submitted
    ${feedback_status}    Get Feedback Status
    List Should Contain Value    ${FEEDBACK_STATUS_NOT_SUBMITTED}    ${feedback_status}

Get Feedback Status
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-status"]
    ${feedback_status}    Get Text    xpath=.//*[@id="feedback-status"]
    ${feedback_status}    Strip String    ${feedback_status}
    [Return]    ${feedback_status}

Exercise Should Have Feedback
    [Arguments]    ${exercise_title}
    Wait Until Element Is Enabled    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]
    Wait Until Element Is Enabled    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]/*[starts-with(@class, "feedback-notification")]

Exercise Should Not Have Feedback
    [Arguments]    ${exercise_title}
    Wait Until Element Is Enabled    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]
    Wait Until Element Is Not Visible    xpath=.//p[normalize-space(text())="${exercise_title}"]/ancestor::div[starts-with(@class, "result")][1]/*[starts-with(@class, "feedback-notification")]

Check Student Feedback
    [Arguments]    ${feedback_text_expected}
    Wait Until Element Is Enabled    xpath=.//*[@id="readonly-feedback" and @class="feedback-text"]
    ${feedback_text_actual}    Get Text    xpath=.//*[@id="readonly-feedback" and @class="feedback-text"]
    ${feedback_text_actual}    Strip String    ${feedback_text_actual}
    Should Be Equal As Strings    ${feedback_text_actual}    ${feedback_text_expected}

Check That No Student Feedback Exists
    Element Should Not Be Visible    xpath=.//*[@id="readonly-feedback" and @class="feedback-text"]

Save Feedback As Draft
    Click Element On Page    xpath=.//*[@id="assessment-view-close"]
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-warning"]
    Click Element On Page    xpath=.//*[@id="feedback-warning-return-button"]
    Wait Until Element Is Not Visible    xpath=.//app-assessment-view

Save Feedback In Feedback Confirmation Window
    Sleep    2
    Wait Until Element Is Enabled    xpath=.//*[@id="assessment-view-close"]
    Click Element On Page    xpath=.//*[@id="assessment-view-close"]
    Wait Until Element Is Enabled    xpath=.//*[@id="feedback-warning"]
    Click Element On Page    xpath=.//*[@id="feedback-warning-submit-button"]
    Wait Until Element Is Not Visible    xpath=.//app-assessment-view

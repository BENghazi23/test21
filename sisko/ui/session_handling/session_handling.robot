*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Suite Teardown    Close All Browsers
Test Teardown     Custom Teardown

*** Test Cases ***
Check Login - Sisko Opens Other Sites
    [Tags]    regression_x    DIS-50    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_qa3_page}
    Check Login Into Other Sites

Check Login - Other Sites Open Sisko
    [Tags]    regression_x    DIS-50    session
    Open Site    ${url_qa3_page}    ${BROWSER}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_main_page}
    Check Login Into Sisko

Check Login - Session Is Not Active In Other Browsers
    [Tags]    regression_x    DIS-50    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Check Not Logged In Another Browser To Sisko
    Open Site    ${url_qa3_page}    ${BROWSER}
    Check Not Logged In Another Browser To Another Site

Check Logout - Closing Browser Ends Session
    [Tags]    regression_x    DIS-106    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    ${method_name}    Select Method_2    ${DEFAULT_METHOD}
    Sleep    5
    Click Element On Page    xpath=(.//mat-list//mat-list-item)[1]//*[@class="mat-list-text"]/p
    Sleep    5
    ${location}    Get Current Location
    Close Browser
    Open Site    ${location}    ${BROWSER}
    Sleep    5
    Verify Login Page Displayed

Check Logout - Closing Sisko Must End Session In Other Applications
    [Tags]    regression_x    DIS-106    session
    ###
    # Verify also other applications: WordPress, Webshop, Brightspace
    ###
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    ###@{window_handles_1}    Get Window Handles
    Open New Tab    ${url_wordpress_page}
    Sleep    5
    #Open Site    ${url_wordpress_page}    ${BROWSER}
    Open New Tab    ${url_brightspace_page}
    Open New Tab    ${url_webshop_page}
    #Open New Tab    ${url_ams_page}
    Switch Tabs To Index    0
    Logout Sisko
    Sleep    5
    Switch Tabs To Index    3
    Select School From Dropdown    Pohjois-Haagan Yhteiskoulu
    Verify Login Page Displayed
    Sleep    5
    Switch Tabs To Index    2
    Sleep    5
    Switch Tabs To Index    1
    Close Current Window
    Sleep    5
    Close Current Window
    Sleep    5
    Close Current Window

Check Logout - Closing In WordPresss Must End Session In Sisko
    [Tags]    regression_x    DIS-106    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_wordpress_page}
    Switch Tabs To Index    1
    Logout Wordpress Site
    #Logout Sisko
    Switch Tabs To Index    0
    WPSite
    Verify Login Page Displayed

Check Logout - Closing In Brightspace Must End Session In Sisko
    [Tags]    regression_x    DIS-106    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_wordpress_page}
    Switch Tabs To Index    1
    Select School From Dropdown    Pohjois-Haagan Yhteiskoulu
    Logout Brightspace Site
    Switch Tabs To Index    0
    WPSite
    Verify Login Page Displayed

Check Logout - Closing In Webshop Must End Session In Sisko
    [Tags]    regression_x    DIS-106    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_webshop_page}
    Switch Tabs To Index    1
    Logout Webshop Site
    Switch Tabs To Index    0
    WPSite
    Verify Login Page Displayed

Check Logout - Closing In AMS Must End Session In Sisko
    [Tags]    regression_x    DIS-106    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open New Tab    ${url_ams_page}
    Switch Tabs To Index    1
    Logout AMS Site
    Switch Tabs To Index    0
    WPSite
    Verify Login Page Displayed

Verify Session Expiration When Logged In From Another Device And Log Out
    [Tags]    regression_x   DIS-428    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Verify Session Expiration And Log Out In First Browser Window
    Verify Session Still Active In New Browser Window

Verify Session Expiration When Logged In From Another Device And Log In again
    [Tags]    regression_x    DIS-428    session
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Open Site    ${LOGIN_URL}    ${BROWSER}
    Login To Site    ${USER_NAME_TEACHER_1_SESSION}    ${USER_PASSWORD_TEACHER_1_SESSION}
    Verify Session Expiration And Log In Again In First Browser Window
    Verify Session Not Anymore Active In New Browser Window

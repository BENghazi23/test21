*** Variables ***
${url_main_page}                     https://qa-sisko.sanomapro.fi
${url_wordpress_page}                https://spro-wordpress-qa3.sanomapro.fi/
${url_brightspace_page}              https://oppimisymparistoqa3.sanomapro.fi/d2l/home/
${url_brightspace_domain}            https://oppimisymparistoqa3.sanomapro.fi
${url_ams_page}                      https://spro-ams-qa3.sanomapro.fi/users/
${url_webshop_page}                  https://spro-magento2-qa3.sanomapro.fi
${url_login_page}                    https://spro-atlas-qa3.sanomapro.fi/sso/XUI/#login
${page_header_main}                  Sanoma Pro
${page_header_document}              Document
${school_to_select}                  Pohjois-Haagan Yhteiskoulu


*** Keywords ***
Login Wordpress Site
    [Arguments]   ${user}    ${password}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]
    Wait Until Page Contains Element    username
    Set Focus To Element    username
    Input Text    username    ${user}
    Input Text    password    ${password}
    Click Element On Page    xpath=.//input[@value='Kirjaudu']

Check Login Into Other Sites
    Switch Tabs To Index    1
    Check Login To Brandsite
    Check Login To Brightspace
    Go Back
    Check Login To AMS
    Go Back
    Check Login To Webshop

Check Login Into Sisko
    Switch Tabs To Index    1
    Check Login To Sisko Site

Select School From Dropdown
    [Arguments]   ${school}
    Wait Until Page Contains Element    navbarResponsive
    Wait Until Keyword Succeeds    10 sec    1 sec
    ...    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//div[@class="dropdown-menu material-navigation"]//a[@class="dropdown-item" and contains(text(), "${school}")]

Select Material From Dropdown
    [Arguments]   ${school}
    Wait Until Page Contains Element    navbarResponsive
    Wait Until Keyword Succeeds    10 sec    1 sec
    ...    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//div[@class="dropdown-menu material-navigation"]//a[@class="dropdown-item" and contains(text(), "${school}")]

Select Personal Information From Dropdown
    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//li[@class="nav-item dropdown"]/a
    Wait Until Keyword Succeeds    10 sec    1 sec
    ...    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//a[text()="Omat tiedot"]

Check Login To Brandsite
    ${url}    Get Location
    Should Be Equal As Strings    ${url}    ${url_qa3_page}
    Wait Until Element Is Enabled    xpath=.//div[@id="navbarResponsive"]//ul[@class="nav navbar-nav nav-personal"]//a[@class="nav-link dropdown-toggle" and text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//div[@id="navbarResponsive"]//ul[@class="nav navbar-nav nav-personal"]//a[@class="nav-link dropdown-toggle" and text()="Aineistot"]

Check Login To Brightspace
    Select School From Dropdown    ${school_to_select}
    ${url}    Get Location
    Should Start With    ${url}    ${url_brightspace_page}

Check Login To AMS
    Select Personal Information From Dropdown
    ${url}    Get Location
    Should Start With    ${url}    ${url_ams_page}
    Wait Until Element Is Enabled    xpath=.//div[@class="app-toolbar-top-container"]//div[@class="app-toolbar-top-right"]//span[text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//div[@class="app-toolbar-top-container"]//div[@class="app-toolbar-top-right"]//span[text()="Aineistot"]

Check Login To Webshop
    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//*[@id="menu-paavalikko"]//a[text()="Verkkokauppa"]
    ${url}    Get Location
    Should Start With    ${url}    ${url_webshop_page}
    Wait Until Element Is Enabled    xpath=.//ul[@class="header links"]//div[@class="user-menu-more"]//span[text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//ul[@class="header links"]//div[@class="user-menu-more"]//span[text()="Aineistot"]

Check Login To Sisko Site
    ${url}    Get Location
    Should Start With    ${url}    ${url_main_page}
    Wait Until Element Is Enabled    xpath=.//*[@id="user-info-menu-button"]

Check Not Logged In Another Browser To Sisko
    Wait Until Page Contains Element    username
    ${url}    Get Location
    Should Start With    ${url}    ${url_login_page}

Check Not Logged In Another Browser To Another Site
    Wait Until Page Contains Element    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]
    ${url}    Get Location
    Should Start With    ${url}    ${url_qa3_page}

Logout Wordpress Site
    Scroll Page To Top
    Sleep    2
    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//ul[@class="nav navbar-nav nav-personal"]//li[@class="nav-item dropdown"]/a
    Sleep    2
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element    xpath=.//div[@id="navbarResponsive"]//a[text()="Kirjaudu ulos"]
    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]

Logout Brightspace Site
    Scroll Page To Top
    Sleep    5
    Wait Until Element Is Enabled    xpath=.//span[@class="d2l-navigation-s-personal-menu-text"]
    Click Element On Page    xpath=.//span[@class="d2l-navigation-s-personal-menu-text"]/ancestor::button
    Click Element On Page    xpath=.//a[text()="Kirjaudu ulos"]
    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]

Logout Webshop Site
    Scroll Page To Top
    Sleep    5
    Click Element On Page    xpath=.//*[@class="user-menu-wrapper"]//*[@class="user-menu-more"]
    Click Element On Page    xpath=.//*[@class="user-menu-content"]//*[@class="dropdown"]//a[text()="Kirjaudu ulos"]
    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]

Logout AMS Site
    Scroll Page To Top
    Sleep    5
    ${dialog_visible}    Run Keyword And Return Status        Element Should Be Enabled    xpath=.//*[@class="modal-dialog"]//button[text()="Hyväksyn käyttöehdot"]
    Run Keyword If    "${dialog_visible}"=="True"    Click Element On Page    xpath=.//*[@class="modal-dialog"]//button[text()="Hyväksyn käyttöehdot"]
    Click Element On Page    xpath=.//div[@class="app-toolbar-top-container"]//div[@class="app-toolbar-top-right"]//span[text()[not(.="") and not(.="Aineistot")]]/parent::div//span[@class="Select-arrow-zone"]
    Click Element On Page    xpath=.//*[@class="Select-menu"]//*[text()="Kirjaudu ulos"]
    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]

WPSite
    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]

Verify Login Page Displayed
    Wait Until Element Is Enabled    username

Get Current Location
    ${current_location}    Get Location
    [Return]    ${current_location}

Close Current Window
    Execute JavaScript    open(location, '_self').close();

Login Wordpress Site
    [Arguments]   ${user}    ${password}
    Click Element On Page    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]
    Wait Until Page Contains Element    username
    Set Focus To Element    username
    Input Text    username    ${user}
    Input Text    password    ${password}
    Click Element On Page    xpath=.//input[@value='Kirjaudu']

Check Login Into Other Sites
    Switch Tabs To Index    1
    Check Login To Brandsite
    Check Login To Brightspace
    Go Back
    Check Login To AMS
    Go Back
    Check Login To Webshop

Check Login Into Sisko
    Switch Tabs To Index    1
    Check Login To Sisko Site

Select School From Dropdown
    [Arguments]   ${school}
    Wait Until Page Contains Element    navbarResponsive
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//div[@class="dropdown-menu material-navigation"]//a[@class="dropdown-item" and contains(text(), "${school}")]

Select Material From Dropdown
    [Arguments]   ${school}
    Wait Until Page Contains Element    navbarResponsive
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//div[@class="dropdown-menu material-navigation"]//a[@class="dropdown-item" and contains(text(), "${school}")]

Select Personal Information From Dropdown
    Wait Until Page Contains Element    navbarResponsive
    Click Element    xpath=.//div[@id="navbarResponsive"]//li[@class="nav-item dropdown"]/a
    Wait Until Keyword Succeeds    10 sec    1 sec    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//a[text()="Omat tiedot"]

Check Login To Brandsite
    ${url}    Get Location
    Should Be Equal As Strings    ${url}    ${url_wordpress_page}
    Wait Until Element Is Enabled    xpath=.//div[@id="navbarResponsive"]//ul[@class="nav navbar-nav nav-personal"]//a[@class="nav-link dropdown-toggle" and text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//div[@id="navbarResponsive"]//ul[@class="nav navbar-nav nav-personal"]//a[@class="nav-link dropdown-toggle" and text()="Aineistot"]

Check Login To Brightspace
    Select School From Dropdown    ${school_to_select}
    ${url}    Get Location
    Should Start With    ${url}    ${url_brightspace_page}

Check Login To AMS
    Select Personal Information From Dropdown
    ${url}    Get Location
    Should Start With    ${url}    ${url_ams_page}
    Wait Until Element Is Enabled    xpath=.//div[@class="app-toolbar-top-container"]//div[@class="app-toolbar-top-right"]//span[text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//div[@class="app-toolbar-top-container"]//div[@class="app-toolbar-top-right"]//span[text()="Aineistot"]

Check Login To Webshop
    Click Element On Page    xpath=.//div[@id="navbarResponsive"]//*[@id="menu-paavalikko"]//a[text()="Verkkokauppa"]
    ${url}    Get Location
    Should Start With    ${url}    ${url_webshop_page}
    Wait Until Element Is Enabled    xpath=.//ul[@class="header links"]//div[@class="user-menu-more"]//span[text()[not(.="") and not(.="Aineistot")]]
    Wait Until Element Is Enabled    xpath=.//ul[@class="header links"]//div[@class="user-menu-more"]//span[text()="Aineistot"]

Check Login To Sisko Site
    ${url}    Get Location
    Should Start With    ${url}    ${url_main_page}
    Wait Until Element Is Enabled    xpath=.//*[@id="user-info-menu-button"]

Check Not Logged In Another Browser To Sisko
    Wait Until Page Contains Element    username
    ${url}    Get Location
    Should Start With    ${url}    ${url_login_page}

Check Not Logged In Another Browser To Another Site
    Wait Until Page Contains Element    xpath=.//*[@id="navbarResponsive"]//a/span[contains(normalize-space(text()),"Kirjaudu")]
    ${url}    Get Location
    Should Start With    ${url}    ${url_wordpress_page}

Verify Session Expiration And Log Out In First Browser Window
    Sleep    2
    Switch Browser    1
    ${method_name}    Select Method_2    ${DEFAULT_METHOD}
    Click Element On Page    xpath=.//app-session-ended-dialog//mat-dialog-actions/button[2]
    Wait Until Element Is Enabled    xpath=.//*[@id="navbarResponsive"]//a[@href="https://www.sanomapro.fi/auth/login"]

Verify Session Expiration And Log In Again In First Browser Window
    Sleep    2
    Switch Browser    1
    ${method_name}    Select Method_2    ${DEFAULT_METHOD}
    Click Element On Page    xpath=.//app-session-ended-dialog//mat-dialog-actions/button[1]
    Wait Until Element Is Enabled    xpath=(.//app-method-selection/div[@class="methods-list"]//img[@class="cover-image"])[1]

Verify Session Still Active In New Browser Window
    Switch Browser    2
    ${method_name}    Select Method_2    ${DEFAULT_METHOD}
    Sleep    5
    Scroll Page To Top
    Sleep    2
    Click Element On Page    xpath=(.//mat-list//mat-list-item)[1]//*[@class="mat-list-text"]/p[1]
    Wait Until Element Is Enabled    xpath=.//img[contains(@class, "module-close-button")]

Verify Session Not Anymore Active In New Browser Window
    Switch Browser    2
    ${method_name}    Select Method_2    ${DEFAULT_METHOD}
    Wait Until Element Is Enabled    xpath=.//app-session-ended-dialog//mat-dialog-actions/button[1]

*** Settings ***
Library      SeleniumLibrary    timeout=20.0    run_on_failure=common failure
Library      Collections
Library      Screenshot
Library      DateTime
Library      String


*** Variables ***
${cstool_url_int}                  https://spro-cstool-int3.sanomapro.fi
${cstool_user_int}                 testautomation.csadmin
${cstool_passwd_int}               <INT PASSWORD HERE>
${cstool_url_qa}                   https://spro-cstool-qa3.sanomapro.fi
${cstool_user_qa}                  cs.admin
${cstool_passwd_qa}                <QA PASSWORD HERE>

@{user_name_seed}                  sisko    Sisko
${default_password}                Sisko18

${email_domain}                    mailinator.com

${pupil_class_level}               9A

@{search_criteria_labels_int}      Kunta    Koulu    Rooli
@{search_criteria_labels_qa}       Kunta    Koulu    Rooli
@{school_addition_labels_int}      Valitse kunta    Valitse koulu    Valitse rooli    Valitse ryhmä(t)
@{school_addition_labels_qa}       Valitse kunta    Valitse koulu    Valitse rooli    Valitse ryhmä(t)

@{search_criteria_input_int}       Helsinki    Pohjois-Haagan Yhteiskoulu
@{search_criteria_input_qa}        Helsingin kaupunki    Pohjois-Haagan Yhteiskoulu
@{school_addition_input_int}       Helsinki    Pohjois-Haagan Yhteiskoulu
@{school_addition_input_qa}        Helsingin kaupunki    Pohjois-Haagan Yhteiskoulu

@{user_role_teacher}               Opettaja    Teacher
@{user_role_student}               Opiskelija    Student
@{user_role_pupil}                 Oppilas    Pupil
@{user_role_maintainer}            Ylläpitäjä    Maintainer
@{user_role_private}               Yksityishenkilö    Private


*** Keywords ***
Open Site For User Addition
    [Arguments]   ${site}    ${browser}
    Open Browser    ${cstool_url_${site}}    ${browser}
    Maximize Browser

Login To SiteX
    [Arguments]   ${user}    ${password}
    Wait Until Page Contains Element    username
    Set Focus To Element    username
    Input Text    username    ${user}
    Input Text    password    ${password}
    Click Element    xpath=.//input[@value='Kirjaudu']

Maximize Browser
    Maximize Browser Window

Find The Latest User
    [Arguments]    ${role}
    Wait Until Element Is Enabled    xpath=.//input[@placeholder="Hae etu- tai sukunimellä, käyttäjätunnuksella, sähköpostiosoitteella tai Atlas-ID:llä"]
    Add Search Criteria    ${role}    ${site}
    ${role}    Convert To Lowercase    ${role}
    ${site}    Convert To Lowercase    ${site}
    #${role_text}    Convert To Lowercase    ${user_role_${role}[1]}
    ${role_text}    Convert To Lowercase    ${user_role_pupil[1]}
    ${user_to_search}    Set Variable    ${user_name_seed[0]}${site}.${role_text}*
    Input Text    xpath=.//input[@placeholder="Hae etu- tai sukunimellä, käyttäjätunnuksella, sähköpostiosoitteella tai Atlas-ID:llä"]    ${user_to_search}

    Wait Until Element Is Enabled    xpath=.//div[@role="rowheader"]/span[@title="Käyttäjätunnus"]
    Click Element    xpath=.//div[@role="rowheader"]/span[@title="Käyttäjätunnus"]
    Sleep    5
    Wait Until Element Is Enabled    xpath=.//div[@role="rowheader"]/span[@title="Käyttäjätunnus"]
    Click Element    xpath=.//div[@role="rowheader"]/span[@title="Käyttäjätunnus"]
    Sleep     5
    ${rows}    Get Element Count    xpath=.//div[@class="ReactVirtualized__Table" and @role="grid"]//div[@role="rowgroup"]//div[@role="row"]

    ${highest_user_number}    Run Keyword If    ${rows} > 0    Extract Highest User Number
    ...    ELSE  Set Variable    0

    [Return]    ${highest_user_number}

Extract Highest User Number
    ${user_name}    Get Text    xpath=(.//div[@class="ReactVirtualized__Table" and @role="grid"]//div[@role="rowgroup"]//div[@role="row"])[1]//div[@class="ReactVirtualized__Table__rowColumn"][4]
    ${user_name}    Strip String    ${user_name}
    ${user_index}    Get Substring    ${user_name}    -3
    ${user_index}    Convert To Integer    ${user_index}
    [Return]    ${user_index}

Add Search Criteria
    [Arguments]    ${role}    ${site}
    # Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_${site}[0]}"]
    # Click Element    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_${site}[0]}"]
    #${search_criteria_label}    Catenate    search_criteria_labels_    ${site}
    #${search_criteria_input}    Catenate    search_criteria_input_    ${site}
    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[0]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[0]}"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${search_criteria_input_qa[0]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${search_criteria_input_qa[0]}"]

    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[1]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[1]}"]
    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${search_criteria_input_qa[1]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${search_criteria_input_qa[1]}"]

    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[2]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${search_criteria_labels_qa[2]}"]
    # Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_${role}[0]}"]
    # Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_${role}[0]}"]
    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_pupil[0]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_pupil[0]}"]

Add School
    [Arguments]    ${role}    ${site}
    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[0]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[0]}"]
    Wait Until Keyword Succeeds    10 sec    1 sec    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${school_addition_input_qa[0]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${school_addition_input_qa[0]}"]

    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[1]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[1]}"]
    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${school_addition_input_qa[1]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${school_addition_input_qa[1]}"]

    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[2]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[2]}"]
    # Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_${role}[0]}"]
    # Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_${role}[0]}"]
    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_pupil[0]}"]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${user_role_pupil[0]}"]

    # The below used to work before for pupils. Now groups have changed.
    # ${pupil_lowercase}    Convert To Lowercase    ${user_role_pupil[1]}
    # Run Keyword If    '${role}'=='${pupil_lowercase}'    Add Group


Add Group
    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[3]}"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="${school_addition_labels_qa[3]}"]
    #Run Keyword And Ignore Error    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${pupil_class_level}"]
    #Run Keyword And Ignore Error    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[@role="option" and text()="${pupil_class_level}"]
    Wait Until Element Is Enabled    xpath=.//div[@class="Select-menu-outer"]//div[(@role="option" and text()="${pupil_class_level}") or (@class="Select-noresults" and text()="Ei tuloksia")]
    Click Element    xpath=.//div[@class="Select-menu-outer"]//div[(@role="option" and text()="${pupil_class_level}") or (@class="Select-noresults" and text()="Ei tuloksia")]


Add Users
    [Arguments]    ${role}    ${class_level}    ${highest_index}    ${how_many_to_add}
    ${ending_index}    Evaluate    ${highest_index}+${how_many_to_add}
    FOR    ${index}    IN RANGE     ${highest_index}       ${ending_index}
        Wait Until Element Is Enabled    xpath=.//button[text()="+ Luo uusi käyttäjä"]
        Wait Until Element Is Enabled    xpath=.//input[@placeholder="Hae etu- tai sukunimellä, käyttäjätunnuksella, sähköpostiosoitteella tai Atlas-ID:llä"]
        Click Element    xpath=.//button[text()="+ Luo uusi käyttäjä"]
        ${index}        Evaluate    ${index}+1
        Add New User    ${role}    ${class_level}    ${index}
    END

Add New User
    [Arguments]    ${role}    ${class_level}    ${index}
    ${zero_to_add}    Run Keyword If    ${index} < ${10}    Set Variable    00
    ...    ELSE IF    ${index} >= ${10} or ${index} < ${100}    Set Variable    0
    ...    ELSE IF    ${index} >= ${100} or ${index} < ${1000}    Set Variable    ${EMPTY}
    Wait Until Element Is Enabled    xpath=.//*[@id="Etunimi"]
    Input Text    xpath=.//*[@id="Etunimi"]    ${user_name_seed[1]} ${site}
    ${first_name}    Set Variable    ${user_name_seed[0]}${site}
    Wait Until Element Is Enabled    xpath=.//*[@id="Sukunimi"]
    # Input Text    xpath=.//*[@id="Sukunimi"]    ${user_role_${role}[1]}${index}
    # ${last_name}    Set Variable    ${user_role_${role}[1]}${zero_to_add}${index}
    Input Text    xpath=.//*[@id="Sukunimi"]    ${user_role_pupil[1]}${index}
    ${last_name}    Set Variable    ${user_role_pupil[1]}${zero_to_add}${index}
    ${last_name}    Convert To Lowercase    ${last_name}
    Wait Until Element Is Enabled    xpath=.//*[@id="Sähköposti"]
    Input Text    xpath=.//*[@id="Sähköposti"]    ${first_name}.${last_name}@${email_domain}
    ${pupil_lowercase}    Convert To Lowercase    ${user_role_pupil[1]}
    Run Keyword If    '${role}'=='${pupil_lowercase}'    Add Class Level    ${class_level}
    Wait Until Element Is Enabled    xpath=.//*[@id="Käyttäjätunnus"]
    Input Text    xpath=.//*[@id="Käyttäjätunnus"]    ${first_name}.${last_name}
    Wait Until Element Is Enabled    xpath=.//*[@id="Salasana"]
    Input Text    xpath=.//*[@id="Salasana"]    ${default_password}
    Sleep    2

    Wait Until Element Is Enabled    xpath=.//button[text()="Tallenna"]
    Click Element    xpath=.//button[text()="Tallenna"]

    Sleep    2

    Wait Until Element Is Enabled    xpath=.//a[text()="Koulut ja Ryhmät"]
    Click Element    xpath=.//a[text()="Koulut ja Ryhmät"]

    Add School    ${role}    ${site}

    Wait Until Element Is Enabled    xpath=.//button[text()="Tallenna"]
    Click Element    xpath=.//button[text()="Tallenna"]

    Sleep    2

    Wait Until Element Is Enabled    xpath=.//div[@class="admin-user"]//a[text()="Käyttäjät"]
    Click Element    xpath=.//div[@class="admin-user"]//a[text()="Käyttäjät"]


Add Class Level
    [Arguments]    ${class_level}
    Wait Until Element Is Enabled    xpath=.//*[@class="Select-placeholder" and text()="Ei luokka-astetta"]
    Click Element    xpath=.//*[@class="Select-placeholder" and text()="Ei luokka-astetta"]
    Wait Until Element Is Enabled    xpath=.//*[@class="Select-menu-outer"]//div[@role="option" and text()="${class_level}."]
    Click Element    xpath=.//*[@class="Select-menu-outer"]//div[@role="option" and text()="${class_level}."]

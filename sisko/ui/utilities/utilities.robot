*** Settings ***
Resource          resource/resources.robot
# Suite Teardown    Close All Browsers
# Test Teardown     Close All Browsers

*** Test Cases ***
Add Sisko Users
    [Tags]    utilityX
    Open Site For User Addition    ${site}    ${browser}
    Login To SiteX    ${cstool_user_${site}}    ${cstool_passwd_${site}}
    ${highest_index}    Find The Latest User    ${role}
    Add Users   ${role}    ${class_level}    ${highest_index}    ${how_many_to_add}

*** Variables ***
@{IMAGES}   =  1tqb0ymZq_XBmGxkmq-hue     BR1-o4PlqpT9gbdHFTByJl      2DGhrnQzqTU9NqoDesA0K8      64ybj3oR4jFAEicaikF6Mw       49tcEGXdKZE8LVOFWzCa2R        1A4fyViyK0iADXMEHf9H3y       0NYzflfaKyO8NqOklaplxk      0NYzflfaKyO8NqOklaplxk       0NYzflfaKyO8NqOklaplxk
@{TEXTS}    =  syttyvä      syövyttävä      terveyshaitta      krooninen terveyshaitta      välittömästi myrkyllinen      ympäristölle vaarallinen      hapettava      räjähde      paineen alaiset kaasut
@{TABLE_COLUMN_1}     Jos pH-paperi     punertaa     vihertää      sinertää
@{TABLE_COLUMN_2}     aine on    hapan    neutraali    emäksinen

*** Keywords ***
Turn Flashcard
    Wait Until Element Is Enabled       xpath=.//*[@class="flash-card"]//*[@class="front"]//*[@class="flip-button"]
    Wait Until Keyword Succeeds    10s    1s    Click Element On Page       xpath=.//*[@class="flash-card"]//*[@class="inner"]//*[@class="flip-button"]
    Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled       xpath=.//*[@class="flash-card flipped"]//*[starts-with(@class,"back")]//*[@class="flip-button"]

Turn Back Flashcard
    [Arguments]     ${last}
    Run Keyword If     ${last} != True    Click Element On Page       xpath=.//*[@class="back"]//*[@class="flip-button"]
    ...     ELSE
    ...     Click Element On Page       xpath=.//*[@class="flash-card"]//*[@class="inner"]//*[@class="back"]//*[@class="flip-button"]

Click Next Card Button
    Wait Until Element Is Enabled       xpath=.//*[@class="flash-card"]
    Click Element On Page       xpath=.//*[@class="next"]

Click Previous Card Button
    Click Element On Page       xpath=.//*[@class="previous"]

Click Card Number
    [Arguments]    ${number}
    Wait Until Element Is Enabled       xpath= .//*[@class="flash-card"]
    Click Element On Page       xpath=.//*[@class="ng-star-inserted" and contains(text, ${number})]

Check Image And Card's Number Match
    [Arguments]      ${card_number}
    Wait Until Element Is Enabled       xpath= .//*[@class="image"]
    Sleep       2
    ${scr}    Catenate    SEPARATOR=   https://kampusdev1.sanomapro.fi/content-assets/    ${IMAGES[${card_number}]}      .png
    Page Should Contain Image       ${scr}

Check Text And Card's Number Match
    [Arguments]      ${card_number}
    Run Keyword If      ${card_number} != 9     Wait Until Element Is Enabled       xpath=.//*[@class="flash-card"]
    Wait Until Element Is Enabled       xpath=(.//*[starts-with(@class, "flash-card")])[${card_number}]//*[starts-with(@class, "back")]
    Wait Until Keyword Succeeds    5s    1s
    ...    Element Should Contain      xpath=(.//*[starts-with(@class, "flash-card")])[${card_number}]//*[starts-with(@class, "back")]    ${TEXTS[${card_number}]}

Every Card: Turn, Check Text Contest, Navigate To The Next
    FOR    ${INDEX}    IN RANGE    1    9
        Turn Flashcard
        Check Text And Card's Number Match      ${INDEX}
        Click Next Card Button
    END

Navigate By Previous Button
    FOR    ${INDEX}    IN RANGE    1    9
        Click Previous Card Button
    END

Verify Every Cell Has Correct Contain
    Wait Until Element Is Visible    xpath= .//eb-table
    FOR    ${INDEX}    IN RANGE    1   4
        ${table_index}    Evaluate    ${INDEX}-1
        Table Cell Should Contain      xpath= .//*[@class= "eb-table ng-star-inserted"]      ${INDEX}      1       ${TABLE_COLUMN_1[${table_index}]}
    END
    FOR    ${INDEX}    IN RANGE    1   4
        ${table_index}    Evaluate    ${INDEX}-1
          Table Cell Should Contain       xpath= .//*[@class= "eb-table ng-star-inserted"]     ${INDEX}      2       ${TABLE_COLUMN_2[${table_index}]}
    END
    Sleep    2

Verify Left Column Contains Image
    Wait Until Element Is Visible       xpath= .//*[@class= "contentOrderLeft ng-star-inserted"]
    Element Should Be Visible     xpath= .//*[@class= "contentOrderLeft ng-star-inserted"]//img

Verify Right Column Contains Text
    Wait Until Element Is Visible       xpath= .//*[@class= "contentOrderRight ng-star-inserted"]
    Element Should Contain     xpath= .//*[@class= "contentOrderRight ng-star-inserted"]     What time do you usually go to bed?

Check Math Operations
    Wait Until Element Is Visible     xpath=.//*[@id="MathJax-Element-1-Frame"]
    Wait Until Element Is Visible     xpath=.//*[@id="MathJax-Element-2-Frame"]
    ${math_elements_in_frame1}    Get Element Count    xpath=.//*[@id="MathJax-Element-1-Frame"]//*[starts-with(@id, "MathJax-Span-")]
    ${math_elements_in_frame2}    Get Element Count    xpath=.//*[@id="MathJax-Element-2-Frame"]//*[starts-with(@id, "MathJax-Span-")]
    Should Be True    ${math_elements_in_frame1} > 1
    Should Be True    ${math_elements_in_frame2} > 1

Check Extra Material Content
    Check Page Contains X Extra Elements      1     closed
    Wait Until Element Is Visible     xpath=.//*[@class="mat-line"]
    Click Element     xpath= .//*[@class="splash-extra closed"]//*[@class="arrow"]
    Check Page Contains X Extra Elements      1     opened
    Element Should Contain      xpath=.//*[@class="mat-line"]      EXTRA: Keskeiset käsitteet
    Element Should Be Visible   xpath=.//*[starts-with(@class, "eb-list")]
    Element Should Contain     xpath=.//*[starts-with(@class, "eb-list")]     maailmankatsomus
    Click Element     xpath=.//*[@class="splash-extra opened"]//*[@class="arrow"]
    Wait Until Element Is Not Visible   xpath=.//*[starts-with(@class, "eb-list")]
    Check Page Contains X Extra Elements      1     closed
    Click Element     xpath=.//*[@class="splash-extra closed"]//*[@class="arrow"]
    Element Should Be Visible   xpath=.//*[starts-with(@class, "eb-list")]

Use Quotation Content
    Check Page Contains X Defined Elements      1      quotation
    Element Should Contain      xpath= .//*[@class="splash-quotation"]      Menneisyyden tutkimuksessa on hyödynnetty aina eri tieteenalojen osaamista.
    Element Should Contain      xpath= .//*[@class="splash-quotation"]      QUOTATION: Näkökulma: Laserkeilaus mullistaa arkeologian
    Open Link    xpath= .//*[@class="eb-paragraph ng-star-inserted"]//*[@class="eb-content-block"]//*[@class="eb-content-block ng-star-inserted"]

Use Source Content
    Check Page Contains X Defined Elements      1       source
    Wait Until Element Is Visible    xpath= .//*[@class="mat-line"]
    Element Should Contain      xpath= .//*[@class="mat-line"]      SOURCE: Teksti: Cheddarin mies
    Element Should Contain      xpath= .//*[@class="splash-source"]      Lontoon luonnontieteellisessä museossa
    Element Should Contain      xpath= .//*[@class="splash-source"]      Lue lisää
    Open Link   xpath= .//*[@class="eb-paragraph ng-star-inserted"]//*[@class="eb-content-block"]//*[@href="https://www.hs.fi/sanomapro/art-2000005556741.html"]
    Open Link   xpath= .//*[@class="eb-paragraph ng-star-inserted"]//*[@class="eb-content-block"]//*[@href="https://www.hs.fi/sanomapro/art-2000006073911.html"]

Use Summary Content
    Check Page Contains X Defined Elements      1      summary
    Wait Until Element Is Visible    xpath= .//*[@class="mat-line"]
    Element Should Contain      xpath= .//*[@class="mat-line"]     SUMMARY: Yhteenveto
    Element Should Contain      xpath= .//*[@class="splash-summary"]      • Teknologiset murrokset
    Element Should Contain      xpath= .//*[@class="splash-summary"]      Mittaaminen

Use Infobox Content
    Check Page Contains X Defined Elements      1      infobox
    Wait Until Element Is Visible    xpath= .//*[@class="mat-line"]
    Element Should Contain      xpath= .//*[@class="mat-line"]     INFOBOX: Yleisiä toimintaohjeita kemian tunnille
    Element Should Contain      xpath= .//*[@class="splash-infobox"]      Sammutin, sammutuspeite, hätäsuihku
    Wait Until Element Is Visible    xpath= .//eb-picture

Check Contents Of Theory Combo 1
    Check Page Contains X Extra Elements      1      closed
    Click Element     xpath= .//*[@class="splash-extra closed"]//*[@class="arrow"]
    Check Page Contains X Extra Elements      1      opened
    Check Page Contains X Defined Elements      1      infobox
    Check Page Contains X Defined Elements      1      summary

Check Splash Contains Bolded Text
    ${xpath}    Check Texttype      bold
    Element Should Contain     ${xpath}     1

Check Splash Contains Italicized Text
    ${xpath}    Check Texttype      italic
    Element Should Contain     ${xpath}     item

Check Splash Contains Striketrhoughed Text
    ${xpath}    Check Texttype      strikethrough
    Element Should Contain     ${xpath}     striketrhough

Open Tiff Image And Check Responsiveness
    Wait Until Element Is Visible

Open PSD Image And Check Responsiveness
    Wait Until Element Is Visible

Verify LTI Link
    [Arguments]    ${provider}    ${user}
    ${user}    Catenate    ${USER_FIRST_NAME_${user}}    ${USER_LAST_NAME_${user}}
    Run Keyword If    '${provider}'=='${LTI_LINK_TEAS}'    Verify TEAS    ${user}
    ...    ELSE IF    '${provider}'=='${LTI_LINK_EKOETEHTAVAPANKKI}'    Verify eKoetehtavapankki    ${user}
    ...    ELSE IF    '${provider}'=='${LTI_LINK_VIOPE}'    Verify Viope    ${user}

Verify TEAS
    [Arguments]    ${user}
    Open Content Item     LTI-links    LTI -links    TEAS
    Switch Tabs To Index    1
    Wait Until Element Is Enabled    xpath=.//teacher-dashboard
    Wait Until Element Is Enabled    xpath=.//section[@class="user" and normalize-space(text())="${user}"]
    ${url}    Get Location
    Should Be Equal As Strings    ${url}    https://stg.teas.sanomapro.fi/as/teacher
    Switch Tabs To Index    0

Verify TEAS Fail
    Log    TEAS
    Open Content Item     LTI-links    LTI -links    TEAS
    @{handles}    Get Window Handles
    Log Many    @{handles}
    @{identifiers}    Get Window Identifiers
    Log Many    @{identifiers}
    @{names}    Get Window Names
    Log Many    @{names}
    Wait Until Keyword Succeeds    10s    1s    Alert Should Be Present
    Switch Tabs To Index    0

Verify eKoetehtavapankki
    [Arguments]    ${user}
    Open Content Item     LTI-links    LTI -links    eKoetehtäväpankki
    Switch Tabs To Index    2
    Wait Until Element Is Enabled    xpath=.//*[@id="menu"]
    Wait Until Element Is Enabled    xpath=.//*[@class="uppermenu"]//*[string()="${user}"]
    ${url}    Get Location
    Should Be Equal As Strings    ${url}    https://koetehtavapankki.aws.sanomacloud.net/
    Switch Tabs To Index    0

Verify Viope
    [Arguments]    ${user}
    Open Content Item     LTI-links    LTI -links    Viope
    Switch Tabs To Index    3
    Wait Until Element Is Enabled    xpath=.//*[@id="header"]
    Wait Until Element Is Enabled    xpath=.//*[@id="sidebar"]
    Wait Until Element Is Enabled    xpath=.//*[@id="rightside"]
    Wait Until Element Is Enabled    xpath=.//a[@class="username_title" and not(normalize-space(text())="")]
    ${url}    Get Location
    Should Contain    ${url}    https://vw4.develop.viope.com/teacher/
    Switch Tabs To Index    0

*** Settings ***
Resource          ../common/theoryelement_resources.robot
Resource          ../common/common_resources.robot
Resource          ../common/feed_resources.robot
Resource          resource/resource.robot
Test Teardown     Custom Teardown

*** Test Cases ***
Verify Table Element Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    TEXT+TABLE: 2: Työ 2A Tutkitaan liuosten happamuutta
    Verify Every Cell Has Correct Contain

Verify Flashcard Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    FLASHCARD: Varoitusmerkit
    Every Card: Turn, Check Text Contest, Navigate To The Next
    Navigate By Previous Button

Verify Math Element Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    TEXT+MATH: Esimerkki 1
    Check Math Operations

Verify Extra Element Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    EXTRA: Keskeiset käsitteet
    Check Extra Material Content

Verify Source Material Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    SOURCE: Teksti: Cheddarin mies
    Use Source Content

Verify Quotation Material Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    QUOTATION: Näkökulma: Laserkeilaus mullistaa arkeologian
    Use Quotation Content

Verify Text And Image Content With 2 Column Works Correctly (Non-combi)
    [Tags]    regression   theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    2 COLUMN: Silmäluku 1
    Verify Left Column Contains Image
    Verify Right Column Contains Text

Verify Summary Element Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    SUMMARY: Yhteenveto
    Use Summary Content

Verify Infobox Element Works Correctly (Non-combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    INFOBOX: Yleisiä toimintaohjeita kemian tunnille
    Use Infobox Content

Check Theory Combi Combonents
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Real content    Theory Combi: 1. Geomedia on tietoa alueista
    Check Contents Of Theory Combo 1

Verify Single Meaningful Relationships (single2single, combi2single)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Ref-link testing    ref link single2single
    Check Reflink

Verify Multiple Meaningful Relationships (single2combi, combi2combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Ref-link testing    ref link single2combi
    Check Reflink

Verify Multiple Meaningful Relationships (ref link combi2single)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Ref-link testing    ref link combi2single
    Check Reflink

Verify Multiple Meaningful Relationships (ref link combi2combi)
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    Ref-link testing    ref link combi2combi
    Check Reflink

Verify Teacher Content Relationship
    [Tags]    regression-x    theoryelement
    Create Feed For Theoryelements
    Check Reflink
    Logout Sisko
    Close Browser
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_STUDENT_1_THEORY}    ${USER_PASSWORD_STUDENT_1_THEORY}
    Select Method With Method URL    ${DEFAULT_METHOD_FOR_THEORY}    ${FEED_TYPE_PUBLISHER}
    Verify Student Can Not Check Teacher Reflink

Verify Edubase Stuff Works In Splash
    [Tags]    regression    theoryelement
    Go To Feed Of Theoryelements
    Open Content Item     General and real content    General testing    Edubase 3.0 stuff in Splash (DIS-1497)
    Check Page Contains X Defined Elements      1      infobox
    Check Splash Contains Bolded Text
    Check Splash Contains Italicized Text
    Check Splash Contains Striketrhoughed Text

Verify LTI links
    [Tags]    regression_lti    theoryelement
    Login Kampus    ${USER_NAME_TEACHER_1_THEORY}    ${USER_PASSWORD_TEACHER_1_THEORY}
    Create Feed For LTI links
    Verify LTI Link    ${LTI_LINK_TEAS}    TEACHER_1
    Verify LTI Link    ${LTI_LINK_EKOETEHTAVAPANKKI}    TEACHER_1
    Verify LTI Link    ${LTI_LINK_VIOPE}    TEACHER_1

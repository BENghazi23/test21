*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Suite Setup       Create Feed For Homework
Test Teardown     Custom Teardown

*** Test Cases ***
Check Homework Central View For Teacher
    [Tags]    regression_x    homework_central
    Check Tabs Based On Role    ${ROLE_TEACHER}
    Open Homework Central Tabs    ${ROLE_TEACHER}
    Logout Sisko

Check Homework Central View For Student
    [Tags]    regression_x    homework_central
    Check Tabs Based On Role    ${ROLE_STUDENT}
    Open Homework Central Tabs    ${ROLE_STUDENT}
    Logout Sisko

Check Homework Central View For Pupil
    [Tags]    regression_x    homework_central
    Check Tabs Based On Role    ${ROLE_PUPIL}
    Open Homework Central Tabs    ${ROLE_PUPIL}
    Logout Sisko

Teacher Assigns Homework To Student
    [Tags]    regression_x    homework_central
    Go To Login Page
    Login To Site    ${USER_NAME_TEACHER_1_HOMEWORK}    ${USER_PASSWORD_TEACHER_1_HOMEWORK}
    Select Method With Method URL    ${DEFAULT_METHOD_FOR_HOMEWORK}    ${FEED_TYPE_TEACHER}
    Select Tab    ${TAB_HOMEWORK}
    Open Homework Central Tab Content    ${MENU_HOMEWORK_NEW}
    Add Exercises To Homework    1    2    20
    Remove Added Exercises From Homework
    Add Exercises To Homework    3    13    15
    Fill In Exercise Instructions    some sämble text here
    Fill In Return Date For Exercise    2019    5    30
    Submit Homework
    Remove All Added Exercises
    Logout Sisko

*** Keywords ***
Check Tabs Based On Role
    [Arguments]    ${role}
    ${role_uppercase}    Convert To Uppercase    ${role}
    Go To Login Page
    Login To Site    ${USER_NAME_${role}_1}    ${USER_PASSWORD_${role}_1}
    Join Feed    ${DEFAULT_METHOD_FOR_HOMEWORK}    ${ACCESS_KEY_FOR_HOMEWORK}
    Select Tab    ${TAB_HOMEWORK}
    Check Number Of Tabs In Homework Central    ${role}
    Select Tab    ${TAB_TOC}
    Select Tab    ${TAB_HOMEWORK}

Check Number Of Tabs In Homework Central
    [Arguments]    ${role}
    ${role}    Convert To Lowercase    ${role}
    ${items_in_menu}    Get Element Count    xpath=.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")]
    Run Keyword If    '${role}'=='teacher'    Should Be Equal As Integers    ${items_in_menu}    2
    Run Keyword If    '${role}'!='teacher'    Should Be Equal As Integers    ${items_in_menu}    1

Open Homework Central Menu Item
    [Documentation]    This keyword opens a single menu item in the homework central. The menu item is passed as a parameter.
    [Arguments]    ${menu_item}
    ${accordion_expansion_indicator}    Get Element Attribute    xpath=.//*[@id="${menu_item}"]//span[contains(@class, "mat-expansion-indicator")]    style
    ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Be Equal As Strings    ${accordion_expansion_indicator}    transform: rotate(0deg);
    Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=.//*[@id="${menu_item}"]//span[contains(@class, "mat-expansion-indicator")]

Open Homework Central Tabs
    [Arguments]    ${role}
    ${role}    Convert To Lowercase    ${role}
    ${items_in_menu}    Get Element Count    xpath=.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")]
    FOR    ${index}    IN RANGE     0       ${items_in_menu}
        ${index}        Evaluate    ${index}+1
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")])[${index}]    class
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Not Contain    ${accordion_expansion_indicator}    opened
        Run Keyword If    '${status}'=='PASS'    Click Element On Page    xpath=(.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")])[${index}]
        Wait Until Element Is Enabled    xpath=(.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")])[${index}]
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//app-homework//div[contains(@class, "dropdown-sidebar") and contains(@id, "homework-accordion")])[${index}]    class
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Contain    ${accordion_expansion_indicator}    opened
    END

*** Settings ***
Documentation     Suite holding test cases for Assignment feature in Kampus. Tag to run assignment tests: 'assignment'
Resource          ../common/common_resources.robot
Resource          resources/resources.robot
# Commented out Suite Setups are used for script development
# Suite Setup       Run Keywords    Set Suite Variable    ${ACCESS_KEY_FOR_ASSIGNMENT_FEED}    WMYK
# ...                 AND     Set Suite Variable    ${NAME_FOR_ASSIGNMENT_FEED}    Ben For Scripting På gång 1
Suite Setup       Run Keywords    Create Feed For Assignment    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${ASSIGNMENT_PUBLISHER_FEED}     ${FEED_TYPE_PUBLISHER}    ${DEFAULT_METHOD_FOR_ASSIGNMENT}
...                 AND    Student Joins Feed Using Access Key    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${ACCESS_KEY_FOR_ASSIGNMENT_FEED}
Test Teardown     Custom Teardown
Suite Teardown    Run Keyword And Ignore Error    Cleanup Teacher Feeds    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${DEFAULT_METHOD_FOR_ASSIGNMENT}

*** Test Cases ***
Test Case for Script Development Use - Placeholder
    [Tags]    assignment_XXX
    ${locale1}=    Set Variable    FI
    ${date_next_moth}=    Get Current Date    increment=30 days    result_format=${DUEDATE_FORMAT_${locale1}}
    Log    ${date_next_moth}    console=True
    ${date_month}=    Get Current Date    result_format=%d.%m.
    Log    ${date_month}    console=True

    Set Test Variable    ${LOCALE}    FI
    ${next_month_date}=    Get First Date Of Next Month
    Log    ${next_month_date}    console=True

TC 01: Assignment Feature Not Available In Publisher Feed
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${ASSIGNMENT_PUBLISHER_FEED}    ${FEED_TYPE_PUBLISHER}
    Verify Assignments Panel Not Visible In Content Header
    Capture Page Screenshot
    Close Browser
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${ASSIGNMENT_PUBLISHER_FEED}    ${FEED_TYPE_PUBLISHER}
    Verify Assignments Panel Not Visible In Content Header
    Capture Page Screenshot

TC 02: Student Cannot Create Assignment From Teacher Feed
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Open Assignments Panel
    Verify Create Button Is Not Shown In Assignments Panel
    Capture Page Screenshot

TC 03: Teacher Verifies Due Date Validations In Assignment Form
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Perform Due Date Validations In New Assignment Form
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (Session Continued)

TC 04: Teacher Creates Empty Draft Assignment From Teacher Feed
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Create Empty Draft Assignment
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (Session Continued)

TC 05: Teacher Creates Draft Assignment With Due Date Set
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Create Draft Assignment With Due Date
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (Session Continued)

TC 06: Teacher Creates Draft Assignment With Due Date & Instructions Set
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Create Draft Assignment With Due Date & Instructions
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (Session Continued)

TC 07: Teacher Creates Draft Assignment With Due Date & Instructions & Content Items From 1 Chapter Set
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Create Draft Assignment With Due Date & Instructions & Content Items
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (Session Continued)

TC 08: Teacher Creates Draft Assignment With Due Date & Instructions & Content Items From 2 Chapters Set
    [Documentation]    This test doesn't have a teardown to delete the created assignment as the very same assignment
    ...                 will be used in the following scripts (TC 09, TC 10, TC 11, TC 12 and deleted in TC 13)
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Create Draft Assignment With Due Date & Instructions & Content Items From Multiple Chapters
    Capture Page Screenshot

TC 09: Teacher Edits Existing Draft Assignment By Changing Due Date & Instructions & Content Items
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Edit Draft Assignment With Due Date & Instructions & Content Items
    Capture Page Screenshot

TC 10: Teacher Rearranges Items In Existing Draft Assignment
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Rearrange Content Items In Existing Draft Assignment
    Capture Page Screenshot

TC 11: Student Cannot View Draft Assignments Created By Teacher
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Check Draft Assignments Not Shown In Assignments Panel
    Capture Page Screenshot

TC 12: Teacher Edits Existing Draft Assignment By Emptying Due Date & Instructions & Content Items
    [Tags]    script_ready    funtionality_not_ready (BE Issue - DIS-3053)
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Edit Draft Assignment By Emptying Due Date & Instructions & Content Items

TC 13: Teacher Deletes Draft Assignment From Edit View
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Delete Draft Assignment From Assignment Edit View
    Capture Page Screenshot

TC 14: Teacher Publishes New Assignment Without Due Date
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Publish Assignment Without Due Date
    Capture Page Screenshot

TC 15: Student Views Published Assignment Without Due Date
    [Documentation]    Student is able to view the published assignment but cannot edit or delete it
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    View Published Assignment Without Due Date
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (New Session)    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}

TC 16: Teacher Publishes New Assignment With Due Date Set To Future
    [Documentation]    This test doesn't have a teardown to delete the created assignment as the very same assignment
    ...                 will be used in the following scripts (TC 17 and deleted in TC 18)
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Publish Assignment With Due Date Set To Future
    Capture Page Screenshot

TC 17: Student Views Published Assignment With Due Date Set To Future
    [Documentation]    Student is able to view the published assignment with due date set to future but cannot edit or delete it
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    View Published Assignment With Due Date Set
    Capture Page Screenshot

TC 18: Teacher Deletes Published Assignment From Edit View
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Delete Published Assignment From Assignment Edit View
    Capture Page Screenshot

TC 19: Student Cannot View Deleted Published Assignment
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Check Published Assignment Not Listed In Assignments Panel
    Capture Page Screenshot

TC 20: Teacher Publishes New Assignment With Due Date Set To Today
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Publish Assignment With Due Date Set To Today
    Capture Page Screenshot

TC 21: Student Views Published Assignment With Due Date Set To Today
    [Documentation]    Student is able to view the published assignment with due date set to today but cannot edit or delete it
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    View Published Assignment With Due Date Set
    Capture Page Screenshot
    [Teardown]    Custom Assignment Teardown To Delete Created Assignment (New Session)    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}

TC 22: Teacher Publishes New Assignment With Due Date & Instructions & Content Items From 2 Chapters Set
    [Documentation]    This test doesn't have a teardown to delete the created assignment as the very same assignment
    ...                 will be used in the following scripts (TC 23, TC 24, TC 25, TC 26 and deleted in TC 27)
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Publish Assignment With Content Items From Multiple Chapters
    Capture Page Screenshot

TC 23: Teacher Edits Existing Published Assignment By Changing Due Date & Instructions & Content Items
    [Documentation]    Teacher edits by changing due date, instructions, removing & adding items and rearranging items
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Edit Published Assignment With Due Date & Instructions & Content Items
    Rearrange Content Items In Existing Published Assignment
    Capture Page Screenshot

TC 24: Student Views Edited Published Assignment With Due Date & Instructions & Content Items From 2 Chapters
    [Documentation]    Student is able to view the edited published assignment with content items added from multiple chapters
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    View Published Assignment With Due Date Set
    Capture Page Screenshot

TC 25: Teacher Manually Closes Existing Published Assignment (Mark As Closed)
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Close Published Assignment With Due Date & Instructions & Content Items
    Capture Page Screenshot

TC 26: Student Views Assignment Marked As Closed By Teacher
    [Documentation]    Student is able to view assignment which was manually closed by Teacher
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    View Closed Assignment With Due Date Set
    Capture Page Screenshot

TC 27: Teacher Deletes Closed Assignment From Read View
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Delete Closed Assignment From Assignment Read View
    Capture Page Screenshot

TC 28: Student Cannot View Deleted Closed Assignment
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_STUDENT_1_USERNAME}    ${ASSIGNMENT_STUDENT_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Check Closed Assignment Not Listed In Assignments Panel
    Capture Page Screenshot

TC 29: Student Joining Feed Can View Existing Assignments
    [Documentation]    DIS-3073 - Teacher publishes assignment with due date set to future and Student joining the feed can access this assignment.
    ...                This test doesn't have a teardown to delete the created assignment as the very same assignment
    ...                 will be used in the following scripts (TC  and deleted in TC )
    [Tags]    script_ready    assignment
    Login Kampus And Select Feed    ${ASSIGNMENT_TEACHER_1_USERNAME}    ${ASSIGNMENT_TEACHER_1_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${FEED_TYPE_TEACHER}
    Publish Assignment With Content Items From Multiple Chapters
    Capture Page Screenshot
    Close Browser
    Login Kampus And Join Feed    ${ASSIGNMENT_STUDENT_2_USERNAME}    ${ASSIGNMENT_STUDENT_2_PASSWORD}    ${NAME_FOR_ASSIGNMENT_FEED}    ${ACCESS_KEY_FOR_ASSIGNMENT_FEED}
    View Published Assignment With Due Date Set
    Capture Page Screenshot

# The following are list of To-Do test cases for currently implemented features for Assignments - For reference only

# Teacher opens content items from within published assignment - Not implemented yet DIS-2856
# Student opens content items from within published assignment - Not implemented yet DIS-2856

# Teacher opens content items from within Closed assignment - Not implemented yet DIS-2856
# Student opens content items from within Closed assignment - Not implemented yet DIS-2856

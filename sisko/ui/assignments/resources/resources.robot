*** Keywords ***
Verify Assignments Panel Not Visible In Content Header
    [Documentation]    Verify Assignment icon is not visible in the feed
    Element Should Not Be Visible    xpath=//button[@id="assignments-panel-toggle"]

Verify Assignments Panel Visible In Content Header
    [Documentation]    Verify Assignment icon is not visible in the feed
    Element Should Be Visible    xpath=//button[@id="assignments-panel-toggle"]

Custom Assignment Teardown To Delete Created Assignment (Session Continued)
    [Documentation]    Delete the assignment used in the test case. If test had failed before reaching feed view then skip assignment deletion
    Run Keyword If Test Failed    Common Failure    # To capture screenshot if Test case had failed
    ${curr_page_url}=    Get Location
    Run Keyword If    "content-feed" in """${curr_page_url}"""    Reload Feed And Delete Last Assignment
    Close Browser

Custom Assignment Teardown To Delete Created Assignment (New Session)
    [Documentation]    Open a new browser session with Teacher user and Delete the assignment used in previous test case(s)
    [Arguments]    ${username}    ${userpassword}    ${feed_name}    ${feed_type}
    Run Keyword If Test Failed    Common Failure    # To capture screenshot if Test case had failed
    Close Browser
    Login Kampus And Select Feed    ${username}    ${userpassword}    ${feed_name}    ${feed_type}
    ${curr_page_url}=    Get Location
    Run Keyword If    "content-feed" in """${curr_page_url}"""    Delete Last Assigment
    Close Browser

Reload Feed And Delete Last Assignment
    [Documentation]    Reload current content feed and proced to delete the last assignment if available
    Reload Feed
    Delete Last Assigment

Delete Last Assigment
    [Documentation]    Delete Last Assigment In Assignments List
    ${count}=    Get Count Of Assignments
    Return From Keyword If    ${count}==0
    Open Assignments Panel
    Expand Last Assignment Card
    Delete Assignment

Check Draft Assignments Not Shown In Assignments Panel
    [Documentation]    To verify draft assignments are not visible in Assignments panel
    Open Assignments Panel
    Verify Create Button Is Not Shown In Assignments Panel
    Verify Draft Assignments Not Listed

Proceed To New Assignment Form View
    Open Assignments Panel
    Create New Assignment

Perform Due Date Validations In New Assignment Form
    [Documentation]    Create a new Assignment and verify Due Date validations are in place
    Proceed To New Assignment Form View
    Verify Due Date Validations

Verify Summary, Read View Of Empty Draft Assignment
    Open Assignments Panel
    Verify Card Summary For Empty Draft Assignment
    Expand Last Assignment Card
    Verify Read View Of Draft Assignment (Instructions & Items Not Set)

Create Empty Draft Assignment
    [Documentation]    Create Empty Draft Assignment and Verify created assignment card does not contain any info
    Proceed To New Assignment Form View
    Verify Submit (Publish) Button Is Disabled
    Back To Assignments List From New Assignment View
    Reload Feed
    Verify Summary, Read View Of Empty Draft Assignment

Verify Summary, Read View Of Draft Assignment (Due Date Set)
    Open Assignments Panel
    Verify Card Summary For Draft Assignment (Due Date Set, Items Not Added)
    Expand Last Assignment Card
    Verify Read View Of Draft Assignment (Instructions & Items Not Set)

Create Draft Assignment With Due Date
    [Documentation]    Create Draft Assignment with only Due Date set.
    Proceed To New Assignment Form View
    Add Valid Due Date (Future Date) By Typing In
    Verify Submit (Publish) Button Is Disabled
    Back To Assignments List From New Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date Set)

Verify Summary, Read View Of Draft Assignment (Due Date, Instructions Set)
    Open Assignments Panel
    Verify Card Summary For Draft Assignment (Due Date Set, Items Not Added)
    Expand Last Assignment Card
    Verify Read View Of Draft Assignment (Instructions Set, Items Not Set)

Create Draft Assignment With Due Date & Instructions
    [Documentation]    Create Draft Assignment with Due Date & Instructions set.
    Proceed To New Assignment Form View
    Add Valid Due Date (Future Date) By Typing In
    Add Instructions    Test Instructions for New Draft Assignment\nThis is line 2 of Test Instructions
    Verify Submit (Publish) Button Is Disabled
    Back To Assignments List From New Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions Set)

Add Due Date, Instructions, Content Items In Assignment Form
    [Documentation]    If parameter 'due_date_in_future' is set to True (default), future date will be set to Due Date
    ...                If parameter 'due_date_in_future' is set to False, current date will be set to Due Date
    [Arguments]    ${assignment_type}    ${due_date_in_future}=${TRUE}
    Run Keyword If    ${due_date_in_future}==True    Add Valid Due Date (Future Date) By Typing In
        ...    ELSE    Add Valid Due Date (Current Date) By Typing In
    Add Instructions    Test Instructions for New ${assignment_type} Assignment\nThis is line 2 of Test Instructions
    Add Multiple Content Items To Assignment
    Verify Submit (Publish) Button Is Enabled

Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)
    Open Assignments Panel
    Verify Card Summary For Draft Assignment (Due Date Set, Items Set)
    Expand Last Assignment Card
    Verify Read View Of Draft Assignment (Instructions Set, Items Set)

Create Draft Assignment With Due Date & Instructions & Content Items
    [Documentation]    Create Draft Assignment with Due Date, Instructions and Content items from a chapter set.
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Due Date, Instructions, Content Items In Assignment Form    Draft
    Back To Assignments List From New Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)

Create Draft Assignment With Due Date & Instructions & Content Items From Multiple Chapters
    [Documentation]    Create Draft Assignment with Due Date, Instructions and Content items form multiple chapters set.
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Due Date, Instructions, Content Items In Assignment Form    Draft
    Proceed To Chapter    ${DEFAULT_CHAPTER_2_FOR_ASSIGNMENT}
    Add Multiple Content Items To Assignment
    Verify Submit (Publish) Button Is Enabled
    Back To Assignments List From New Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)

Edit Due Date, Instructions, Content Items In Assignment Form
    [Arguments]    ${assignment_type}
    Edit Due Date
    Edit Instructions    *EDITED* Test Instructions for existing ${assignment_type} Assignment\nThis is line 2 of Test Instructions\nThird line also added
    Edit Multiple Content Items In Assignment
    Verify Submit (Publish) Button Is Enabled

Edit Draft Assignment With Due Date & Instructions & Content Items
    [Documentation]    Edit existing Draft Assignment with Due Date, Instructions and Content items set.
    ...                 Change Due Date, Modify instructions and Remove Content items
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)
    Proceed To Edit View Of Assignment
    Verify Submit (Publish) Button Is Enabled
    Edit Due Date, Instructions, Content Items In Assignment Form    Draft
    Back To Assignments List From Edit Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)

Rearrange Content Items In Existing Draft Assignment
    [Documentation]    Edit existing Draft Assignment with Due Date, Instructions and Content items set:
    ...                 Rearrange Content items
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)
    Proceed To Edit View Of Assignment
    Verify Submit (Publish) Button Is Enabled
    Rearrange Items In Assignment Edit View
    Back To Assignments List From Edit Assignment View
    Reload Feed
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)

Clear Due Date, Instructions, Content Items In Assignment Form
    Clear Due Date
    Clear Instructions
    Clear Content Items In Assignment
    Verify Submit (Publish) Button Is Disabled

Edit Draft Assignment By Emptying Due Date & Instructions & Content Items
    Open Assignments Panel
    Verify Summary, Read View Of Draft Assignment (Due Date, Instructions, Items Set)
    Proceed To Edit View Of Assignment
    Clear Due Date, Instructions, Content Items In Assignment Form
    Back To Assignments List From Edit Assignment View
    Reload Feed
    Verify Summary, Read View Of Empty Draft Assignment

Delete Draft Assignment From Assignment Edit View
    [Documentation]    Delete existing Draft Assignment from Assignment Edit view
    Open Assignments Panel
    Expand Last Assignment Card
    Proceed To Edit View Of Assignment    ignore_count_check=True
    Delete Assignment From Edit View

Add Instructions, Content Items In Assignment Form
    [Arguments]    ${assignment_type}
    Add Instructions    Test Instructions for New ${assignment_type} Assignment\nThis is line 2 of Test Instructions
    Add Multiple Content Items To Assignment
    Verify Submit (Publish) Button Is Enabled

Verify Summary, Read View Of Published Assignment (Instructions, Items Set)
    Open Assignments Panel
    Verify Card Summary For Published Assignment (Due Date Not Set, Items Set)
    Expand Last Assignment Card
    Verify Read View Of Published Assignment (Instructions Set, Items Set)

Publish Assignment Without Due Date
    [Documentation]    Publish new assignment with instructions and content items set but due date not set
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Instructions, Content Items In Assignment Form    Published
    Publish New Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Instructions, Items Set)

View Published Assignment Without Due Date
    [Documentation]    Verify student can view published assignment without due date
    Verify Published Assignments Count Is Not Zero
    Open Assignments Panel
    Verify Card Summary For Published Assignment (Due Date Not Set, Items Set)
    Expand Last Assignment Card
    Student - Verify Read View Of Published Assignment (Instructions Set, Items Set)

Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)
    Open Assignments Panel
    Verify Card Summary For Published Assignment (Due Date Set, Items Set)
    Expand Last Assignment Card
    Verify Read View Of Published Assignment (Instructions Set, Items Set)

Publish Assignment With Due Date Set To Future
    [Documentation]    Publish new assignment with instructions, content items set and due date set to future
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Due Date, Instructions, Content Items In Assignment Form    Published
    Publish New Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)

View Published Assignment With Due Date Set
    [Documentation]    Verify student can view published assignment with due date set to future date or today
    Verify Published Assignments Count Is Not Zero
    Open Assignments Panel
    Verify Card Summary For Published Assignment (Due Date Set, Items Set)
    Expand Last Assignment Card
    Student - Verify Read View Of Published Assignment (Instructions Set, Items Set)

Publish Assignment With Due Date Set To Today
    [Documentation]    Publish new assignment with instructions, content items set and due date set to today
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Due Date, Instructions, Content Items In Assignment Form    Published    ${FALSE}
    Publish New Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)

Publish Assignment With Content Items From Multiple Chapters
    [Documentation]    Publish new Assignment with Due Date, Instructions and Content items form multiple chapters set
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Proceed To New Assignment Form View
    Add Due Date, Instructions, Content Items In Assignment Form    Published
    Proceed To Chapter    ${DEFAULT_CHAPTER_2_FOR_ASSIGNMENT}
    Add Multiple Content Items To Assignment
    Verify Submit (Publish) Button Is Enabled
    Publish New Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)

Edit Published Assignment With Due Date & Instructions & Content Items
    [Documentation]    Edit existing Published Assignment with Due Date, Instructions and Content items set.
    ...                 Change Due Date, Modify instructions and Remove Content items
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)
    Proceed To Edit View Of Assignment
    Verify Submit (Publish) Button Is Enabled
    Edit Due Date, Instructions, Content Items In Assignment Form    Published
    Publish Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)

Rearrange Content Items In Existing Published Assignment
    [Documentation]    Edit existing Published Assignment with Due Date, Instructions and Content items set:
    ...                 Rearrange Content items
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)
    Proceed To Edit View Of Assignment
    Verify Submit (Publish) Button Is Enabled
    Rearrange Items In Assignment Edit View
    Publish Assignment
    Reload Feed
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)

Delete Published Assignment From Assignment Edit View
    [Documentation]    Delete existing Published Assignment from Assignment Edit view
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)
    Open Assignments Panel
    Expand Last Assignment Card
    Proceed To Edit View Of Assignment
    Delete Assignment From Edit View

Check Published Assignment Not Listed In Assignments Panel
    [Documentation]    Student - To verify assignments list does not contain any published assignment
    Open Assignments Panel
    Verify Create Button Is Not Shown In Assignments Panel
    Verify Published Assignments Count Is Zero

Close Published Assignment With Due Date & Instructions & Content Items
    [Documentation]    Edit existing Published Assignment with Due Date, Instructions and Content items set.
    ...                 Change Due Date, Modify instructions and Remove Content items
    Proceed To Chapter    ${DEFAULT_CHAPTER_1_FOR_ASSIGNMENT}
    Verify Summary, Read View Of Published Assignment (Due Date, Instructions, Items Set)
    Mark Assignment As Closed
    Reload Feed
    Verify Summary, Read View Of Closed Assignment (Due Date, Instructions, Items Set)

Verify Summary, Read View Of Closed Assignment (Due Date, Instructions, Items Set)
    Open Assignments Panel
    Verify Closed Assignments Count Is Not Zero
    Verify Card Summary For Closed Assignment (Due Date Set, Items Set)
    Expand Last Assignment Card
    Verify Read View Of Closed Assignment (Instructions Set, Items Set)

View Closed Assignment With Due Date Set
    [Documentation]    Verify student can view closed assignment with due date set
    Open Assignments Panel
    Verify Closed Assignments Count Is Not Zero
    Verify Card Summary For Closed Assignment (Due Date Set, Items Set)
    Expand Last Assignment Card
    Student - Verify Read View Of Closed Assignment (Instructions Set, Items Set)

Delete Closed Assignment From Assignment Read View
    [Documentation]    Delete existing Closed Assignment from Assignment Read view
    Verify Summary, Read View Of Closed Assignment (Due Date, Instructions, Items Set)
    Delete Last Assigment

Check Closed Assignment Not Listed In Assignments Panel
    [Documentation]    Student - To verify assignments list does not contain any closed assignment
    Open Assignments Panel
    Verify Create Button Is Not Shown In Assignments Panel
    Verify Closed Assignments Count Is Zero

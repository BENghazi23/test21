*** Settings ***
Resource          ../common/common_resources.robot
Resource          ../exercises/resource/resources.robot
Resource          ../dashboard/resources/resources.robot
Resource          ../my_page/resource/resources.robot
Suite Setup       Create Feed For Smoke
Test Teardown     Custom Teardown

*** Test Cases ***
Check Dashboard For Smoke
    [Tags]    smoke
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_SMOKE_1}    ${USER_PASSWORD_STUDENT_SMOKE_1}    ${NAME_FOR_SMOKE}   ${ACCESS_KEY_FOR_SMOKE}
    Open Dashboard
    Student View Have To Be Right
    Logout Sisko
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_SMOKE_1}    ${USER_PASSWORD_TEACHER_SMOKE_1}    ${NAME_FOR_SMOKE}    ${ACCESS_KEY_FOR_SMOKE}
    Open Dashboard
    Check Name Appears In Student List   STUDENT_SMOKE_1
    Teacher View Have To Be Right
    Logout Sisko
    Close Browser

Check My Page For Smoke
    [Tags]    smoke
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_SMOKE_1}    ${USER_PASSWORD_TEACHER_SMOKE_1}
    Check Localization
    Check Personal Information For User    TEACHER_SMOKE_1
    Logout Sisko
    Close Browser

Check Exercise For Smoke
    [Tags]    smoke
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_SMOKE_1}    ${USER_PASSWORD_STUDENT_SMOKE_1}    ${NAME_FOR_SMOKE}   ${ACCESS_KEY_FOR_SMOKE}
    Open Content Item    Content testing    Match single response    UI: Match Single response
    Do Exercise UI: Match Single Response
    Close Browser

Check Access Key Functionality For Smoke
    [Tags]    smoke
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_SMOKE_1}    ${USER_PASSWORD_TEACHER_SMOKE_1}    ${NAME_FOR_SMOKE}    ${ACCESS_KEY_FOR_SMOKE}
    ${access_key}    Renew Access Key
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_SMOKE_1}    ${USER_PASSWORD_STUDENT_SMOKE_1}    ${NAME_FOR_SMOKE}    ${access_key}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_SMOKE_1}    ${USER_PASSWORD_TEACHER_SMOKE_1}    ${NAME_FOR_SMOKE}    ${access_key}
    ${access_key}    Create Access Key
    Switch Access Key    off
    Close Browser
    Login Kampus    ${USER_NAME_STUDENT_1}    ${USER_PASSWORD_STUDENT_1}
    Join Feed    ${NAME_FOR_SMOKE}    ${access_key}    ${OPERATION_FAILURE}
    Logout Sisko
    Close Browser

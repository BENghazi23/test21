*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Resource          ../dashboard/resources/resources.robot
Resource          ../my_page/resource/resources.robot
Suite Setup       Create Feed For Feeds     ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}
Test Teardown     Custom Teardown

*** Test Cases ***
# Check Main Page
#     [Tags]    regression    feed
#     Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
#     Close And Reopen Side Menu

Check Content Menu And Sidebar Buttons
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    #Check Content Menu Button Functionality
    #Check Sidebar Button Functionality
    Close And Reopen Side Menu

Check Filter In Exercise And Theory View
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Verify Filters In Exercise And Theory View

Check Bookmark Functionality
    [Tags]    quarantine    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS1}    ${USER_PASSWORD_TEACHER_2_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Verify Bookmark Functionality

Chapter Navigation
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Check Chapter Navigation

Teacher Can See The Content Student Sees
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Turn Student View Filter On
    Check That No Teacher Material Is Visible In Student View Filter
    Check Filters

Student Can See Only Student Content
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS1}    ${USER_PASSWORD_STUDENT_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Check Filters
    Check That No Teacher Material Is Visible

Student Can Browse Content Feed Items
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS1}    ${USER_PASSWORD_STUDENT_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Check Table Of Contents
    #Browse Content Items

Pupil Can See Only Pupil Content
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_PUPIL_1_FEEDS1}    ${USER_PASSWORD_PUPIL_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Check Filters
    Check That No Teacher Material Is Visible

Pupil Can Browse Content Feed Items
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_PUPIL_1_FEEDS1}    ${USER_PASSWORD_PUPIL_1_FEEDS1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Check Table Of Contents
    #Browse Content Items

Verify Link Addition To Feed
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED_MODIFICATION}    ${ACCESS_KEY_FOR_FEEDS_MODIFICATION}
    Open Unit In Chapter    Digiopetusmateriaali (OPS 2016)    Digiopetusmateriaali (OPS 2016)
    Add New Content To Feed    link    menu_after    Digiopetusmateriaali (OPS 2016)    ${CONTENT_ITEM_TYPE_TEACHER_MATERIAL}

Verify File Addition To Feed
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED_MODIFICATION}    ${ACCESS_KEY_FOR_FEEDS_MODIFICATION}
    Open Unit In Chapter    Digiopetusmateriaali (OPS 2016)    Digiopetusmateriaali (OPS 2016)

Verify Content Item Deletion
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}
    ${feed_name}    Add Teacher Feed    ${DEFAULT_METHOD}    date_on    ${FEED_TYPE_PUBLISHER}    deletion    scroll_off
    Delete Content Items    ${CONTENT_ITEM_TYPE_EXERCISE}    File as answer
    Delete Content Items    ${CONTENT_ITEM_TYPE_EXERCISE}    Choice with answer level feedback
    Delete Content Items    ${CONTENT_ITEM_TYPE_CHAPTER}    1 Content testing

Verify Renaming Elements
    [Tags]    regression    feed
    Create Feed For Feed Renaming    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS1}    ${USER_PASSWORD_TEACHER_1_FEEDS1}    ${NAME_FOR_FEED_RENAME}    ${ACCESS_KEY_FOR_FEED_RENAME}
    Open Unit In Chapter    Lämpö    Jakson aluksi
    Edit Content Item Title    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    ${1}
    Edit Chapter Title    chapter
    Edit Chapter Title    subchapter

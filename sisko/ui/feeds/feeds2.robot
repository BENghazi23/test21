*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Resource          ../dashboard/resources/resources.robot
Resource          ../my_page/resource/resources.robot
Suite Setup       Create Feed For Feeds     ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
Test Teardown     Custom Teardown

*** Test Cases ***
Verify Renaming Feeds
    [Tags]    regression    feed
    Create Feed For Feed Renaming    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    Close Access Key Dialog Window
    Go To My Page
    Rename Feed Name    ${NAME_FOR_FEED_RENAME}

Verify Renaming Feeds - Possible For Participant Teacher As Well
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}    ${NAME_FOR_FEED_RENAME_UPDATED}    ${ACCESS_KEY_FOR_FEED_RENAME}
    Check Renaming Allowed For Participant Teacher    ${NAME_FOR_FEED_RENAME_UPDATED}

Verify Renaming Feeds - Check That Feed Name Change Is Reflected To Students As Well
    [Tags]    regression    feed
    Create Feed For Feed Renaming    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${NAME_FOR_FEED_RENAME}    ${ACCESS_KEY_FOR_FEED_RENAME}
    Go To My Page
    Check Feed With Original Name Is Visible    ${NAME_FOR_FEED_RENAME}
    Close Browser
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    Rename Feed Name    ${NAME_FOR_FEED_RENAME}
    Close Browser
    Login Kampus    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}
    Check Feed With Original Name Is Visible    ${NAME_FOR_FEED_RENAME_UPDATED}

Verify Renaming Feeds - Dialogs Show Correct Feed Name
    [Tags]    regression    feed
    Create Feed For Feed Renaming    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    Get Feed Name In Access Key Dialog    feed    ${NAME_FOR_FEED_RENAME}
    Close Access Key Dialog Window
    Go To My Page
    Get Feed Name In Access Key Dialog    mypage    ${NAME_FOR_FEED_RENAME}
    Close Access Key Dialog Window
    Rename Feed Name    ${NAME_FOR_FEED_RENAME}
    Get Feed Name In Access Key Dialog    mypage    ${NAME_FOR_FEED_RENAME_UPDATED}
    Close Access Key Dialog Window
    Join Feed    ${NAME_FOR_FEED_RENAME_UPDATED}    ${ACCESS_KEY_FOR_FEED_RENAME}
    Get Feed Name In Access Key Dialog    feed    ${NAME_FOR_FEED_RENAME_UPDATED}

Verify Hiding Elements
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${feed_name}    Add Teacher Feed    ${DEFAULT_METHOD_FOR_LICENSES}    date_on    ${FEED_TYPE_PUBLISHER}    Element Hiding    scroll_off
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Hide Content Item By Type    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    LT 1_5 Monivalinta
    ${access_key}    Create Access Key
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${feed_name}    ${access_key}
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Check Element Is Hidden    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    TLT 1_5 Monivalinta
    Close Browser
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${method_name}    Select Method With Method URL    ${feed_name}    ${FEED_TYPE_TEACHER}
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Show Content Item By Type    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    LT 1_5 Monivalinta
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${feed_name}    ${access_key}
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Check Element Is Not Hidden    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    LT 1_5 Monivalinta

Verify Element Is Hidden In Copied Feed
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${feed_name}    Add Teacher Feed    ${NAME_FOR_FEED_MODIFICATION}    date_on    ${FEED_TYPE_TEACHER}    Element Hiding    scroll_off
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Hide Content Item By Type    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    LT 1_5 Monivalinta
    Go To My Page
    ${feed_name}    Add Teacher Feed    ${feed_name}    date_on    ${FEED_TYPE_TEACHER}    Element Hiding    scroll_off
    Open Unit In Chapter    Lämpö    Lämpö laajentaa
    Check Element Is Hidden    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    LT 1_5 Monivalinta

Student Joins Feed Using Access Key
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${NAME_FOR_FEED}    ${access_key}

Student Tries To Join Feed With Non-existing Key
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}
    Enter the Non-Existing Key and Get Error Message   XXXX

Student Tries To Join Feed With Old Key
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    ${access_key}    Create Access Key
    ${access_key_renewed}    Renew Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}
    Enter the Non-Existing Key and Get Error Message   ${access_key}

Teacher Joins Other Teacher's Feed Using Access Key
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${method_name}    Select Method With Method URL    ${NAME_FOR_FEED}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}    ${NAME_FOR_FEED}    ${access_key}
    Check Content Feed Is Not Editable

Teacher Creates Access Key
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${method_name}    Select Method With Method URL    ${NAME_FOR_FEED}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key

Student Tries To Join Feed When Access Key Is Not Activated
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${method_name}    Select Method With Method URL    ${NAME_FOR_FEED}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key
    Switch Access Key    off
    Close Browser
    Login Kampus    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}
    Join Feed    ${NAME_FOR_FEED}   ${access_key}    ${OPERATION_FAILURE}

Teacher Joins Other Teacher's Feed And Can Modify Access Key
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${feed_name}    Add Teacher Feed    ${DEFAULT_METHOD}    date_on    ${FEED_TYPE_PUBLISHER}    Access key modification    scroll_off
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}    ${feed_name}    ${access_key}
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${feed_name}    ${access_key}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}    ${feed_name}    ${access_key}
    ${access_key_renewed}    Renew Access Key
    Switch Access Key    off
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_2_FEEDS2}    ${USER_PASSWORD_STUDENT_2_FEEDS2}    ${feed_name}    ${access_key_renewed}    ${OPERATION_FAILURE}
    Close Browser
    Login Kampus    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}
    ${method_name}    Select Method With Method URL    ${feed_name}    ${FEED_TYPE_TEACHER}
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_2_FEEDS2}    ${USER_PASSWORD_STUDENT_2_FEEDS2}    ${feed_name}    ${access_key}

Deleted Feed Disappears Also From Students
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${deletion_message}    Get Confirmation Message For Deletion
    ${feed_name}    Add Teacher Feed    ${DEFAULT_METHOD}    date_on    ${FEED_TYPE_PUBLISHER}    Feed deletion from students as well    scroll_off
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}    ${feed_name}    ${access_key}
    Close Browser
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    Delete Teacher Feed    ${feed_name}    ${deletion_message}
    Close Browser
    Login Kampus    ${USER_NAME_STUDENT_1_FEEDS2}    ${USER_PASSWORD_STUDENT_1_FEEDS2}
    Verify Teacher Feed Not Shown    ${feed_name}
    Join Feed    ${feed_name}    ${access_key}    ${OPERATION_FAILURE}

Participant Teacher Cannot Delete Feed
    [Tags]    regression    feed
    Login Kampus    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${feed_name}    Add Teacher Feed    ${DEFAULT_METHOD}    date_on    ${FEED_TYPE_PUBLISHER}    Feed participant teacher tries to delete    scroll_off
    ${access_key}    Create Access Key
    Switch Access Key    on
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_FEEDS2}    ${USER_PASSWORD_TEACHER_2_FEEDS2}    ${feed_name}    ${access_key}
    Go To My Page
    Deleting Teacher Feed Not Possible    ${feed_name}

###

Test Readspeaker Functionality
    [Tags]    regression_123X    feed
    #Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To Site    ${USER_NAME_TEACHER_1_FEEDS2}    ${USER_PASSWORD_TEACHER_1_FEEDS2}
    ${method_name}    Select Method With Method URL    ${DEFAULT_METHOD}    ${FEED_TYPE_PUBLISHER}
    ###Open Content Item        Automation testing    Automation testing    Unscored: Cloze combi drop
    Open Content Item   Content testing    Text content    Copyright splash
    #Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    #Login To SU Site    sisko.teacher1001    Sisko18
    #${method_name}    Select Method With Method URL    ${DEFAULT_METHOD_SU}    ${FEED_TYPE_PUBLISHER}
    #Open Content Item     Text Templates    TemplateA    Template A
    Wait Until Page Does Not Contain Element    xpath=.//*[@class="loading"]
    Wait Until Keyword Succeeds    20s    1s    Wait Until Page Contains Element    xpath=.//app-module-content-documents
    Wait Until Element Is Enabled    xpath=.//app-readspeaker//a[starts-with(@class, "rsbtn_play") and contains(@href, "readspeaker.com")]
    ${style_before}    Get Element Attribute    xpath=.//app-readspeaker//a[contains(@href, "readspeaker.com")]/parent::*    class
    ${documents}    Get Element Count    xpath=.//app-document
    ${paragraphs}    Get Element Count    xpath=.//app-document//*[contains(@class, "eb-paragraph")]
    Start ReadSpeaker
    Sleep    2
    Pause ReadSpeaker
    @{words_all}    Get All Readable Words In ReadSpeaker
    Close ReadSpeaker
    Sleep    2
    Start ReadSpeaker
    # Log Many    ${words_all}
    # Sleep    2
    # ${style_after}    Get Element Attribute    xpath=.//app-readspeaker//a[starts-with(@class, "rsbtn_play") and contains(@href, "readspeaker.com")]/parent::*    class
    # #Stop ReadSpeaker
    # Sleep    2
    # #Pause ReadSpeaker
    # Sleep    2
    # #----->${selected_word}    Get Selected Word In ReadSpeaker
    # ###--->>>Pause ReadSpeaker
    # ###--->>>Sleep    10
    # ###--->>>Pause ReadSpeaker
    # ${selected_sentance_2}    Get Selected Sentance In ReadSpeaker
    # ${selected_word2}    Get Selected Word In ReadSpeaker
    # ${select_word_in_order}    Get Selected Word Order Number    ${words_all}
    # #Should Be Different
    # ###--->>>Rewind ReadSpeaker
    # ###--->>>Rewind ReadSpeaker
    # ###--->>>Pause ReadSpeaker
    # ${select_word_in_order}    Get Selected Word Order Number    ${words_all}
    # #Sleep    5
    # ${selected_sentance_3}    Get Selected Sentance In ReadSpeaker
    # ###--->>>Forward ReadSpeaker
    # ###--->>>Forward ReadSpeaker
    # ###--->>>Pause ReadSpeaker
    # ${select_word_in_order}    Get Selected Word Order Number    ${words_all}
    # #Sleep    5
    # ###--->>>Pause ReadSpeaker
    # ${selected_sentance_4}    Get Selected Sentance In ReadSpeaker
    # Stop ReadSpeaker
    # Close ReadSpeaker
    #
    ###--->>>Start ReadSpeaker
    Follow Progress In ReadSpeaker    ${words_all}

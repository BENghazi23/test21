*** Keywords ***
Close All Units
    Wait Until Element Is Enabled    xpath=.//mat-sidenav//mat-accordion/mat-expansion-panel
    ${toc_units}    Get Element Count    xpath=.//mat-sidenav//*[starts-with(@id, "mat-expansion-panel-header-")]
    Sleep    10
    FOR    ${index}    IN RANGE     0       ${toc_units}
        ${index}        Evaluate    ${index}+1
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//mat-sidenav//*[@class="mat-content"])[${index}]/following-sibling::span[contains(@class, "mat-expansion-indicator")]    style
        ${status}    ${error_msg}    Run Keyword And Ignore Error    Should Be Equal As Strings    ${accordion_expansion_indicator}    transFORm: rotate(0deg);
        Run Keyword If    '${status}'=='FAIL'    Click Element On Page    xpath=(.//mat-sidenav//*[@class="mat-content"])[${index}]/following-sibling::span[contains(@class, "mat-expansion-indicator")]
        Sleep    2
        ${accordion_expansion_indicator}    Get Element Attribute    xpath=(.//mat-sidenav//*[@class="mat-content"])[${index}]/following-sibling::span[contains(@class, "mat-expansion-indicator")]    style
        Should Be Equal As Strings    ${accordion_expansion_indicator}    transFORm: rotate(0deg);
    END
    [Return]    ${toc_units}

Open Random Unit
    [Arguments]    ${units_in_total}
    ${random_index}    Evaluate    random.randint(1, ${units_in_total})    modules=random
    Click Element    xpath=(.//mat-sidenav//*[@class="mat-content"])[${random_index}]/following-sibling::span[contains(@class, "mat-expansion-indicator")]
    [Return]    ${random_index}

Open Random Chapter
    [Arguments]    ${unit_opened}
    ${index_selected}        Evaluate    ${unit_opened}-1
    Wait Until Element Is Enabled    xpath=.//mat-sidenav//*[@id="cdk-accordion-child-${index_selected}" and @style="visibility: visible;"]
    ${chapters_in_total}    Get Element Count    xpath=.//mat-sidenav//*[@id="cdk-accordion-child-${index_selected}" and @style="visibility: visible;"]//*[@class="toc-content-link"]
    ${random_index}    Evaluate    random.randint(1, ${chapters_in_total})    modules=random
    ${chapters_in_total}    Click Element On Page    xpath=(.//mat-sidenav//*[@id="cdk-accordion-child-${index_selected}" and @style="visibility: visible;"]//*[@class="toc-content-link"])[${random_index}]

Log Out User
    Open User Information View
    Click Element On Page    xpath=.//*[@id="logout-button"]

Check Element Is Hidden
    [Arguments]    ${content_item_type}    ${content_item_text}
    Wait Until Element Is Enabled    xpath=.//app-content-item
    ${element_exists}    Run Keyword And Return Status
    ...    Page Should Contain Element    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div
    ${element_visibility}    Run Keyword If    '${element_exists}'=='True'
    ...    Run Keyword And Return Status    Get Element Style    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div
    ...    ELSE
    ...    Run Keyword And Return Status    Page Should Not Contain Element    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div
    Should Be True    '${element_visibility}'=='True'

Get Element Style
    [Arguments]    ${element}
    ${style}    Get Element Attribute    ${element}    class
    ${status}    Run Keyword And Return Status    Should Contain    ${style}    hidden
    [Return]    ${status}

Check Element Is Not Hidden
    [Arguments]    ${content_item_type}    ${content_item_text}
    Wait Until Element Is Enabled    xpath=.//app-content-item
    ${element_visibility}    Run Keyword And Return Status
    ...    Wait Until Page Contains Element    xpath=.//app-content-item//mat-list-item[@content-type="${content_item_type}"]//*[@class="mat-list-text"]/*[contains(@class, "title") and normalize-space(text())="${content_item_text}"]/ancestor::app-content-item/div
    Should Be True    '${element_visibility}'=='True'

# Check Reflink
#     #Open Content Item By Type    ${CONTENT_ITEM_TYPE_EXERCISE}    Match Matrix
#     Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
#     ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
#     Should Be Equal As Integers    ${split_areas}    1
#     Click Element On Page    xpath=.//*[@id="referral-relations-button" and not(contains(@class, "default-toolbar-extraMaterial-button"))]
#     Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
#     ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
#     Sleep      1
#     Should Be Equal As Integers    ${split_areas}    2
#     ${innerHTML}    Get Element Attribute    xpath=(.//as-split/as-split-area)[2]    innerHTML
#     ${innerHTML_length}    Get Length    ${innerHTML}
#     Should Be True    ${innerHTML_length}>0
#     Click Element On Page    xpath=.//*[@id="referral-relations-button" and not(contains(@class, "default-toolbar-extraMaterial-button"))]
#     Wait Until Element Is Enabled    xpath=.//as-split/as-split-area
#     ${split_areas}    Get Element Count    xpath=.//as-split/as-split-area
#     Sleep       1
#     Should Be Equal As Integers    ${split_areas}    1

Verify Student Can Not Check Teacher Reflink
    Run Keyword And Expect Error    Check Reflink

Check Content Menu Button Functionality
    Wait Until Element Is Enabled    xpath=(.//mat-list//mat-list-item)[1]//*[@class="mat-list-text"]/p[2]
    Scroll Page To Top
    Wait Until Element Is Enabled    xpath=(.//mat-list//mat-list-item)[1]//*[@class="mat-list-text"]/p[2]
    Click Element On Page    xpath=(.//mat-list//mat-list-item)[1]//*[@class="mat-list-text"]/p[2]
    Click Element On Page    xpath=.//*[@id="content-menu-button"]
    Wait Until Keyword Succeeds    10s    50ms    Element Should Be Visible    xpath=.//mat-sidenav
    Click Element On Page    xpath=.//mat-sidenav-container
    Wait Until Keyword Succeeds    10s    50ms    Element Should Not Be Visible    xpath=.//mat-sidenav
    Click Element On Page    xpath=.//*[@id="close-button"]
    Wait Until Keyword Succeeds    10s    50ms    Element Should Be Visible    xpath=.//mat-sidenav

Check Sidebar Button Functionality
    Scroll Page To Top
    Wait Until Element Is Enabled    xpath=.//*[@id="side-nav-mode-toc"]
    Click Element On Page    xpath=.//*[@id="side-nav-mode-toc"]
    Close And Reopen Side Menu

Enter the Non-Existing Key and End Up In Error Page
    [Arguments]     ${access_key}
    Open Access Key Dialog
    Input Text    xpath=.//*[@id="access-key"]    ${access_key}
    Click Element On Page    xpath=.//*[@class="join-with-accesskey"]
    Wait Until Element Is Visible    xpath=.//*[@class="ng-star-inserted"]//*[@class="logo"]
    Wait Until Element Is Visible    xpath=.//*[@class="ng-star-inserted"]//*[@class="content"]

Enter the Non-Existing Key and Get Error Message
    [Arguments]     ${access_key}
    Open Access Key Dialog
    Input Text    xpath=.//*[@id="access-key"]    ${access_key}
    Click Element On Page    xpath=.//*[@class="join-with-accesskey"]
    Wait Until Element Is Enabled    xpath=.//*[@class="ng-star-inserted" and text()[not(.="")]]

Get Feed Name In Access Key Dialog
    [Arguments]    ${page}    ${feed_name_expected}
    Run Keyword If    '${page}'=='feed'    Open Access Key Dialog In Feed
    Run Keyword If    '${page}'=='mypage'    Open Access Key Dialog In My Page    ${feed_name_expected}
    Wait Until Element Is Enabled    xpath=.//mat-dialog-container//*[@class="dialog-title-feed"]
    ${feed_name}    Get Text    xpath=.//mat-dialog-container//*[@class="dialog-title-feed"]
    ${feed_name}    Strip String    ${feed_name}
    Should Be Equal As Strings    ${feed_name}    ${feed_name_expected}

Open Access Key Dialog In Feed
    Wait Until Keyword Succeeds    60s    1s    Wait Until Element Is Enabled    xpath=.//*[@id="accesskey-bar-button"]
    Reload Page
    Wait Until Keyword Succeeds    60s    1s    Wait Until Element Is Enabled    xpath=.//*[@id="accesskey-bar-button"]
    ${status}    Run Keyword And Return Status    Element Should Be Visible    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="accesskey-bar-button"]
    Wait Until Element Is Enabled    xpath=.//mat-dialog-container
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]

Open Access Key Dialog In My Page
    [Arguments]    ${feed_name_expected}
    Wait Until Keyword Succeeds    60s    1s    Wait Until Element Is Enabled    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name_expected}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    ${status}    Run Keyword And Return Status    Element Should Be Visible    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//app-method-feeds//div[@feed-type="teacher"]//*[@class="teacher-feed-title" and normalize-space(text())="${feed_name_expected}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "show-access-key-button")]
    Wait Until Element Is Enabled    xpath=.//mat-dialog-container
    Wait Until Element Is Enabled    xpath=.//*[@id="shared-accesskey" and text()[not(.="")]]

Get Chapter Names
    Open TOC
    Wait Until Element Is Enabled    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[starts-with(@class, "chapter-title")]//*[starts-with(@class, "title-text")]
    ${chapters}    Get Element Count    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[starts-with(@class, "chapter-title")]//*[starts-with(@class, "title-text")]
    @{chapter_names_in_TOC}    Create List
    @{chapter_names_in_page_header}    Create List
    FOR    ${index}    IN RANGE    0    ${chapters}
        ${index}        Evaluate    ${index}+1
        ${chapter_name_in_TOC}    Get Text    xpath=(.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]//*[starts-with(@class, "chapter-title")]//*[starts-with(@class, "title-text")])[${index}]
        ${chapter_name_in_TOC}    Strip String    ${chapter_name_in_TOC}
        ${chapter_name_in_TOC}    Replace String    ${chapter_name_in_TOC}    \n    ${SPACE}
        ${chapter_name_in_page_header}    Replace String    ${chapter_name_in_TOC}    .    ${EMPTY}
        Append To List    ${chapter_names_in_TOC}    ${chapter_name_in_TOC}
        Append To List    ${chapter_names_in_page_header}    ${chapter_name_in_page_header}
    END
    Close TOC From Top Menu
    [Return]    ${chapter_names_in_TOC}    ${chapter_names_in_page_header}

Check Chapter Navigation
    ${chapter_names_in_TOC}    ${chapter_names_in_page_header}    Get Chapter Names
    ${chapter_index}    Set Variable    0
    ${index}    Set Variable    0
    ${chapters_length}    Get Length    ${chapter_names_in_TOC}
    FOR    ${chapter}    IN    @{chapter_names_in_TOC}
        ${chapter_index}    Evaluate    ${chapter_index}+1
        Open Chapter    ${chapter}
        Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="title-zero" and normalize-space(text())="${chapter_names_in_page_header[${index}]}"]
        ${index}    Evaluate    ${index}+1
        Scroll Page To Top
        Run Keyword If    ${chapter_index}==${1}    Check First Chapter Links    ${chapter_names_in_page_header}    ${chapter_index}
        Run Keyword If    ${chapter_index}==${${chapters_length}}    Check Last Chapter Links    ${chapter_names_in_page_header}    ${chapter_index}
        Run Keyword If    ${chapter_index}!=${1} and ${chapter_index}!=${${chapters_length}}    Check Chapter Links    ${chapter_names_in_page_header}    ${chapter_index}
    END
    Open Chapter    ${chapter_names_in_TOC[0]}
    Scroll Page To Top
    Check Navigation With Navigation Links    ${chapter_names_in_page_header}

Check First Chapter Links
    [Arguments]    ${chapter_names}    ${chapter_index}
    Wait Until Element Is Enabled    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    ${chapter_links}    Get Element Count    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    Should Be Equal As Numbers    ${chapter_links}    1
    ${chapter_link_text_found}    Get Text    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])[1]
    ${chapter_link_text_expected}    Get From List    ${chapter_names}    ${chapter_index}
    Should Be Equal As Strings    ${chapter_link_text_found}    ${chapter_link_text_expected}

Check Last Chapter Links
    [Arguments]    ${chapter_names}    ${chapter_index}
    ${chapter_index}    Evaluate    ${chapter_index}-2
    Wait Until Element Is Enabled    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    ${chapter_links}    Get Element Count    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    Should Be Equal As Numbers    ${chapter_links}    1
    ${chapter_link_text_found}    Get Text    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])[1]
    ${chapter_link_text_expected}    Get From List    ${chapter_names}    ${chapter_index}
    Should Be Equal As Strings    ${chapter_link_text_found}    ${chapter_link_text_expected}

Check Chapter Links
    [Arguments]    ${chapter_names}    ${chapter_index}
    ${chapter_index}    Evaluate    ${chapter_index}-1
    Wait Until Element Is Enabled    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    ${chapter_links}    Get Element Count    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])
    Should Be Equal As Numbers    ${chapter_links}    2
    ${chapter_index_previous}    Evaluate    ${chapter_index}-1
    ${chapter_index_next}    Evaluate    ${chapter_index}+1
    ${chapter_link_text_found_1}    Get Text    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])[1]
    ${chapter_link_text_found_2}    Get Text    xpath=((.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[@class="chapter-name" and not(normalize-space(text())="")])[2]
    ${chapter_link_text_expected_1}    Get From List    ${chapter_names}    ${chapter_index_previous}
    ${chapter_link_text_expected_2}    Get From List    ${chapter_names}    ${chapter_index_next}
    Should Be Equal As Strings    ${chapter_link_text_found_1}    ${chapter_link_text_expected_1}
    Should Be Equal As Strings    ${chapter_link_text_found_2}    ${chapter_link_text_expected_2}

Check Navigation With Navigation Links
    [Arguments]    ${chapter_names}
    ${chapter_index}    Set Variable    0
    ${chapters_length}    Get Length    ${chapter_names}
    FOR    ${chapter}    IN    @{chapter_names}
        ${chapter_index}    Evaluate    ${chapter_index}+1
        Run Keyword If    ${chapter_index} < ${chapters_length}    Click Element On Page    xpath=(.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[normalize-space(text())="${chapter_names[${chapter_index}]}"]
        Run Keyword If    ${chapter_index} < ${chapters_length}    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="title-zero" and normalize-space(text())="${chapter_names[${chapter_index}]}"]
    END
    ${chapter_index}    Set Variable    ${chapters_length}
    ${chapters_length}    Evaluate    ${chapters_length}-1
    FOR    ${chapter}    IN    @{chapter_names}
        ${chapter_index}    Evaluate    ${chapter_index}-1
        Run Keyword If    ${chapter_index} < ${chapters_length}    Click Element On Page    xpath=(.//app-chapter-navigation-buttons)[1]//*[starts-with(@class, "nav-button")]//*[normalize-space(text())="${chapter_names[${chapter_index}]}"]
        Run Keyword If    ${chapter_index} < ${chapters_length}    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="title-zero" and normalize-space(text())="${chapter_names[${chapter_index}]}"]
    END

Dashboard TOC Should Be Open In Chapter
    [Arguments]    ${chapter_name}
    Wait Until Element Is Enabled    xpath=.//app-side-nav//*[@class="title-text" and normalize-space(text())="${chapter_name}"]
    ${chapter_style}    Get Element Attribute
    ...    xpath=.//app-side-nav//*[@class="title-text" and normalize-space(text())="${chapter_name}"]/ancestor::*[contains(@class, "dropdown-sidebar")]    class
    Should Contain    ${chapter_style}    opened

Verify Bookmark Functionality
    Open Unit In Chapter    Metadata    Text templates
    ${posi_1}    Get Position
    Sleep    5
    Go To My Page
    Join Feed    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Sleep    5
    ${posi_2}    Get Position
    ${hPos}    Get Horizontal Position
    ...    xpath=(.//*[@class="title-one" and normalize-space(text())="Text templates"]/ancestor::app-chapter-title/following-sibling::app-container//app-content-item)[1]
    ${vPos}    Get Vertical Position
    ...    xpath=(.//*[@class="title-one" and normalize-space(text())="Text templates"]/ancestor::app-chapter-title/following-sibling::app-container//app-content-item)[2]
    Execute JavaScript    window.scrollTo(0, ${vPos});
    Sleep    5
    ${posi_3}    Get Position
    Go To My Page
    Join Feed    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Sleep    5
    ${posi_4}    Get Position
    ${diff_1}    Evaluate    ${posi_1.top}-${posi_2.top}
    ${diff_2}    Evaluate    ${posi_3.top}-${posi_4.top}
    Should Be True    ${diff_1} < 80
    Should Be True    ${diff_2} < 80
    Open Unit In Chapter    Automation testing    Automation testing
    Open Dashboard
    Dashboard TOC Should Be Open In Chapter    Automation testing
    Open Content Feed
    Sleep    5
    ${posi_5}    Get Position
    ${diff_3}    Evaluate    ${posi_4.top}-${posi_5.top}
    Should Be True    ${diff_3} < 80

Verify Filters In Exercise And Theory View
    Open Chapter    Metadata
    ${filters}    Get Filters
    ${student_view_selected}    Is Student View On
    Click Element On Page    xpath=.//*[@id="${filters[1]}-filter"]
    ${content_items}    Get Content Item Visibility    ${filters[1]}
    Check Content Item Visibility    ${content_items}    ${filters[1]}    ${student_view_selected}
    ${status}    Check If Filter Dialog Is Open
    Run Keyword If    '${status}'=='True'    Click Filter Button
    ${status}    Check If Filter Dialog Is Open
    Should Be True    '${status}'=='False'
    Wait Until Element Is Enabled    xpath=.//*[@id="filter-bar-button"]/following-sibling::*[contains(@class, "notification-indicator") and not(contains(@class, "hidden"))]
    ${content_items_visible_begin}    Get Number Of Visible Content Items
    Click Element On Page    xpath=(.//app-content-item[not(contains(@class, "hidden"))])[1]
    Wait Until Element Is Enabled    xpath=.//app-module-content
    Wait Until Element Is Enabled    xpath=.//*[@id="close-button"]
    Element Should Be Visible     xpath=.//*[@id="filter-bar-button"]/following-sibling::*[contains(@class, "notification-indicator") and not(contains(@class, "hidden"))]
    ${next_page_button_visible}    Run Keyword And Return Status    Element Should Be Visible    xpath=.//*[@id="next-button-bg"]
    Run Keyword If    '${next_page_button_visible}'=='True'    Click Element On Page    xpath=.//*[@id="next-button-bg"]
    Element Should Be Visible     xpath=.//*[@id="filter-bar-button"]/following-sibling::*[contains(@class, "notification-indicator") and not(contains(@class, "hidden"))]
    Click Filter Button
    ${status}    Check If Filter Dialog Is Open
    Should Be True    '${status}'=='True'
    ${filters_new}    Get Filters
    ${filters_new_length}    Get Length    ${filters_new}
    Should Be Equal As Integers    ${filters_new_length}    1
    Wait Until Element Is Enabled    xpath=.//*[@id="${filters[1]}-filter"]
    Click Filter Button
    #Close Content Item
    Return To Content Feed From Content Item View
    ${filters}    Get Filters
    ${student_view_selected}    Is Student View On
    ${content_items}    Get Content Item Visibility    ${filters[1]}
    Check Content Item Visibility    ${content_items}    ${filters[1]}    ${student_view_selected}
    Page Should Contain Element    xpath=.//*[@id="${filters[1]}-filter" and contains(@class, "mat-button-toggle-checked")]
    Click Filter Button
    Element Should Be Visible     xpath=.//*[@id="filter-bar-button"]/following-sibling::*[contains(@class, "notification-indicator") and not(contains(@class, "hidden"))]
    ${content_items_visible_end}    Get Number Of Visible Content Items
    Should Be Equal As Integers    ${content_items_visible_begin}    ${content_items_visible_end}


*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Test Teardown     Custom Teardown

*** Test Cases ***
Check User Information For Teacher
    [Tags]    data_security    KEY_HERE
    Login Kampus    ${USER_NAME_TEACHER_1_SECURITY}    ${USER_PASSWORD_TEACHER_1_SECURITY}
    Check Personal Information For User    TEACHER_1
    ${teacher_feed_1}    Check Feeds    teacher
    ${publisher_feed_1}    Check Feeds    publisher
    Logout Sisko
    Go To    ${LOGIN_URL}
    Login To Site    ${USER_NAME_TEACHER_2_SECURITY}    ${USER_PASSWORD_TEACHER_2_SECURITY}
    Check Personal Information For User    TEACHER_2
    ${teacher_feed_2}    Check Feeds    teacher
    ${publisher_feed_2}    Check Feeds    publisher
    Check Feed Contents    ${teacher_feed_1}    ${teacher_feed_2}

Check User Information For Teacher From Portal
    [Tags]    data_security    KEY_HERE
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_SECURITY}    ${USER_PASSWORD_TEACHER_1_SECURITY}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Check Personal Information For User    TEACHER_1
    ${teacher_feed_1}    Check Feeds    teacher
    ${publisher_feed_1}    Check Feeds    publisher
    Logout Sisko
    Go To    ${PORTAL_URL}
    Login Wordpress Site    ${USER_NAME_TEACHER_2_SECURITY}    ${USER_PASSWORD_TEACHER_2_SECURITY}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Check Personal Information For User    TEACHER_2
    ${teacher_feed_2}    Check Feeds    teacher
    ${publisher_feed_2}    Check Feeds    publisher
    Check Feed Contents    ${teacher_feed_1}    ${teacher_feed_2}

Check User Data Changes
    [Tags]    data_security    KEY_HERE
    Change User Data    ${ROLE_TEACHER}
    #Change User Data    ${ROLE_STUDENT}
    #Change User Data    ${ROLE_PUPIL}

Change User Information
    [Tags]    data_security    KEY_HERE
    Login Kampus    ${USER_NAME_TEACHER_DATA}    ${USER_PASSWORD_TEACHER_DATA}
    Change User Data In Kampus
    #Switch Tabs To Index    1
    ${user_name_full}    Change User Name    ${ROLE_TEACHER}
    Close Browser
    Login Kampus    ${USER_NAME_TEACHER_DATA}    ${USER_PASSWORD_TEACHER_DATA}
    Open Personal Information Dialog
    Check User Name    ${user_name_full}

Delete Cookie Problem
    [Tags]    data_security    KEY_HERE    XYZZ
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_SECURITY}    ${USER_PASSWORD_TEACHER_1_SECURITY}
    ${cookies_1}    Get Cookies
    Log Many    ${cookies_1}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Sleep    2
    Go To    ${PORTAL_URL}
    Sleep    5
    Delete Cookie    AMSSO
    ${cookies_2}    Get Cookies
    Log Many    ${cookies_2}
    Go To    ${LOGIN_URL}
    ${location}    Get Location

*** Keywords ***

Check Personal Information For User
    [Arguments]    ${logged_in_user}
    ${user_full_name}    Catenate    ${USER_FIRST_NAME_${logged_in_user}}    ${USER_LAST_NAME_${logged_in_user}}
    ${status}    Run Keyword And Return Status    Element Should Be Enabled    xpath=.//*[@id="logout-button"]
    Run Keyword If    '${status}'=='False'    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Get Text    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Strip String    ${logged_in_user_name_full}
    Should Contain    ${logged_in_user_name_full}    ${user_full_name}
    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]

Check Feeds
    [Arguments]    ${feed_type}
    ${feeds_found}    Create List
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="${feed_type}"]
    ${feeds_in_total}    Get Element Count    xpath=.//app-method-feeds//*[@feed-type="${feed_type}"]
    FOR    ${index}    IN RANGE     0       ${feeds_in_total}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//*[@feed-type="${feed_type}"]//*[@class="${feed_type}-feed-title"])[${index}]
        ${feed_text}        Get Text    xpath=(.//*[@feed-type="${feed_type}"]//*[@class="${feed_type}-feed-title"])[${index}]
        ${feed_text_stripped}        Strip String    ${feed_text}
        Append To List    ${feeds_found}    ${feed_text_stripped}
    END
    [Return]    ${feeds_found}

Check Feed Contents
    [Arguments]    ${feed_1}    ${feed_2}
    Should Not Be Equal    ${feed_1}    ${feed_2}

Change User Data
    [Arguments]    ${role}
    ${role}    Convert To Uppercase    ${role}
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_${role}_DATA}    ${USER_PASSWORD_${role}_DATA}
    ${user_name}    Catenate    ${USER_FIRST_NAME_${role}_DATA}    ${USER_LAST_NAME_${role}_DATA}
    Open User Info    ${user_name}
    Change User Name    ${role}

Change User Name
    [Arguments]    ${role}
    Wait Until Element Is Enabled    xpath=.//div[text()="Profiili"]/button[text()="Muokkaa"]
    Click Element On Page    xpath=.//div[text()="Profiili"]/button[text()="Muokkaa"]
    Wait Until Element Is Enabled    xpath=.//*[text()="Etunimi"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    ${first_name}    Get Value    xpath=.//*[text()="Etunimi"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    ${status}    Run Keyword And Return Status    Should Contain    ${first_name}    Temp
    ${user_first_name}    Run Keyword If    '${status}'=='False'
    ...    Set Variable    Temp User First Name
    ...    ELSE    Set Variable    ${USER_FIRST_NAME_${role}_DATA}
    Input Text    xpath=.//*[text()="Etunimi"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input    ${user_first_name}
    ${first_name_updated}    Get Value    xpath=.//*[text()="Etunimi"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    ${last_name}    Get Value    xpath=.//*[text()="Sukunimi"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    ${new_name}    Catenate    ${first_name_updated}    ${last_name}
    #${email}    Get Value    xpath=.//*[text()="Sähköpostiosoite"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    #${phone}    Get Value    xpath=.//*[text()="Puhelin"]/parent::div[@class="block-row-label"]/following-sibling::div[@class="block-row-value"]//input
    Wait Until Element Is Enabled    xpath=.//button[text()="Tallenna"]
    Click Element On Page    xpath=.//button[text()="Tallenna"]
    [Return]    ${new_name}

Change User Data In Kampus
    Open User Information View
    Click Element On Page    xpath=.//*[@id="pi_link_userprofile"]

Check User Name
    [Arguments]    ${user_full_name}
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Get Text    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Strip String    ${logged_in_user_name_full}
    Should Contain    ${logged_in_user_name_full}    ${user_full_name}
    Click Element On Page    xpath=.//*[@id="user-info-menu-button"]

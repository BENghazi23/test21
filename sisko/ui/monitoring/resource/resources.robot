*** Keywords ***
Check Feeds On My Page
    [Arguments]    ${feed_type}
    ${feeds_found}    Create List
    Wait Until Keyword Succeeds    30s    50ms    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="${feed_type}"]
    ${feeds_in_total}    Get Element Count    xpath=.//app-method-feeds//*[@feed-type="${feed_type}"]
    FOR    ${index}    IN RANGE     0       ${feeds_in_total}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//*[@feed-type="${feed_type}"]//*[@class="${feed_type}-feed-title"])[${index}]
        ${feed_text}        Get Text    xpath=(.//*[@feed-type="${feed_type}"]//*[@class="${feed_type}-feed-title"])[${index}]
        ${feed_text_stripped}        Strip String    ${feed_text}
        Append To List    ${feeds_found}    ${feed_text_stripped}
    END
    [Return]    ${feeds_found}

Check That Feed Is Not Empty
    [Arguments]    ${feed}
    ${feed_length}    Get Length    ${feed}
    Should Be True    ${feed_length} > 0

Check That Action Buttons Have Loaded
    Wait Until Keyword Succeeds    30s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="mypage-button"]
    Wait Until Keyword Succeeds    30s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="dashboard-impl-button"]
    Wait Until Keyword Succeeds    30s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="instructions-button"]
    Wait Until Keyword Succeeds    30s    50ms    Wait Until Element Is Enabled    xpath=.//*[@id="user-info-menu-button"]

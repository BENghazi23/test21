*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Test Teardown     Custom Teardown

*** Test Cases ***
Check Teacher Login
    [Tags]    monitoring
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_MONITORING}    ${USER_PASSWORD_TEACHER_1_MONITORING}
    Open User Materials In School
    Select School From Portal    ${MONITORING_SCHOOL}
    Check That Action Buttons Have Loaded
    ${publisher_feed}    Check Feeds On My Page    ${FEED_TYPE_PUBLISHER}
    Check That Feed Is Not Empty    ${publisher_feed}

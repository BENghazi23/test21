*** Variables ***
@{subchapter_license_on}    Lisätehtävät
@{subchapter_license_off}    Tutkimukset    Tutkimuskysymykset    Tuntitehtävät    Kotitehtävät    Pohdi lisää    Yhteenveto    Arvioi osaamistasi

@{content_items_license_on}    ${CONTENT_ITEM_TYPE_EXTRA_EXERCISE}    ${CONTENT_ITEM_TYPE_EXERCISE}
@{content_items_license_off}    ${CONTENT_ITEM_TYPE_TEXT}    ${CONTENT_ITEM_TYPE_TEACHER_MATERIAL}    ${CONTENT_ITEM_TYPE_CLASSWORK}
...    ${CONTENT_ITEM_TYPE_TEACHERS_GUIDE}    ${CONTENT_ITEM_TYPE_RESEARCH}    ${CONTENT_ITEM_TYPE_MATERIAL}    ${CONTENT_ITEM_TYPE_RESEARCH_QUESTIONS}
...    ${CONTENT_ITEM_TYPE_HOMEWORK}    ${CONTENT_ITEM_TYPE_CHAPTER}    ${CONTENT_ITEM_TYPE_SELF_ASSESSMENT}


*** Keywords ***
Check If Environment Has License On
    Run Keyword If    '${LICENSE_ON}'=='${TRUE}'    Log    Licenses on
    ...    ELSE    Log    Licenses off
    [Return]    ${LICENSE_ON}

Check That Content Items With License Are Shown
    FOR    ${content_item_type}    IN    @{content_items_license_on}
        ${content_items_by_type}    Get Element Count    xpath=.//mat-list-item[starts-with(@content-type, "${content_item_type}")]
        Should Be True    ${content_items_by_type} > 0
    END

Check That Content Items Without License Are Not Shown
    FOR    ${content_item_type}    IN    @{content_items_license_off}
        ${content_items_by_type}    Get Element Count    xpath=.//mat-list-item[starts-with(@content-type, "${content_item_type}")]
        Should Be Equal As Numbers    ${content_items_by_type}    0
    END

Open Chapters In TOC
    ${chapters_in_toc}    Get Element Count    xpath=.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]
    FOR    ${index}    IN RANGE    0    ${chapters_in_toc}
        ${index}    Evaluate    ${index}+1
        ${chapter_text}    Get Text    xpath=(.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]/*[@class="title"])[${index}]
        ${chapter_number}    Get Text    xpath=(.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]/*[@class="title"]//*[starts-with(@class, "title-number")])[${index}]
        ${chapter_text}    Remove String    ${chapter_text}    ${chapter_number}
        ${chapter_text}    Strip String    ${chapter_text}
        Open Chapter    ${chapter_text}
        Check That Content Items With License Are Shown
        Check That Content Items Without License Are Not Shown
    END

Chapter Content Should Be Empty
    [Arguments]    ${chapter}
    Open Chapter    ${chapter}
    Scroll Page to Top
    Wait Until Element Is Enabled    xpath=.//*[@class="title-zero" and text()="${chapter}"]//ancestor::app-chapter-title/following-sibling::app-container
    ${chapter_items}    Get Element Count    xpath=.//*[@class="title-zero" and text()="${chapter}"]//ancestor::app-chapter-title/following-sibling::app-container//app-content-item
    ${items_in_toc}    Get Element Count    xpath=.//app-table-of-contents//*[contains(@class, "dropdown-sidebar")]/*[@class="title" and normalize-space(text())="${chapter}"]/following-sibling::div[@class="content"]/div[starts-with(@class, "item")]
    Should Be Equal As Numbers    ${items_in_toc}    0

Check If Method Found
    [Arguments]    ${method}    ${feed_type}=${FEED_TYPE_PUBLISHER}
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]
    ${methods_found}    Get Element Count    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]//div[contains(@class, "feed-title") and contains(normalize-space(text()),"${method}")]
    ${status}    Run Keyword And Return Status
    ...    Should Be True    ${methods_found} > 0
    [Return]    ${status}

Have License For
    [Arguments]    ${method}    ${feed_type}=${FEED_TYPE_PUBLISHER}
    ${status}    Check If Method Found    ${method}    ${feed_type}
    Should Be True    ${status}==True

Don't Have License For
    [Arguments]    ${method}    ${feed_type}=${FEED_TYPE_PUBLISHER}
    ${status}    Check If Method Found    ${method}    ${feed_type}
    Should Be True    ${status}==False

Check Subchapter Contents
    Wait Until Element Is Enabled    xpath=.//app-chapter-title//*[@class="chapter-title-2"]
    FOR    ${subchapter}    IN    @{subchapter_license_on}
        Check Subchapter Content Items    ${subchapter}    on
    END
    FOR    ${subchapter}    IN    @{subchapter_license_off}
        Check Subchapter Content Items    ${subchapter}    off
    END

Check Subchapter Content Items
    [Arguments]    ${subchapter}    ${is_license_on}
    ${elements}    Get Element Count
    ...    xpath=.//app-chapter-title//*[@class="chapter-title-2"]//*[text()="${subchapter}"]
    FOR    ${index}    IN RANGE    0    ${elements}
        ${index}    Evaluate    ${index}+1
        ${subchapter_contents}    Get Element Count
        ...    xpath=(.//app-chapter-title//*[@class="chapter-title-2"]//*[text()="${subchapter}"])[${index}]/ancestor::div[starts-with(@class, "section")][1]/following-sibling::div[starts-with(@class, "section")][1]//mat-list//app-content-item
        Run Keyword If    '${is_license_on}'=='on'
        ...    Should Be True    ${subchapter_contents} > 0
        ...    ELSE
        ...    Should Be Equal As Numbers    ${subchapter_contents}    0
    END

Get Number Of Feeds
    [Arguments]    ${feed_type}
    ${feeds}    Get Element Count    xpath=.//app-method-feeds//div[@feed-type="${feed_type}"]
    [Return]    ${feeds}

Check If Chapter Found In TOC
    [Arguments]    ${chapter}
    ${status}    Set Variable    False
    ${chapters_in_toc}    Get Element Count    xpath=.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]
    FOR    ${index}    IN RANGE    0    ${chapters_in_toc}
        ${index}    Evaluate    ${index}+1
        ${chapter_text}    Get Text    xpath=(.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]/*[@class="title"])[${index}]
        ${chapter_number}    Get Text    xpath=(.//app-table-of-contents//*[starts-with(@class, "dropdown-sidebar")]/*[@class="title"]//*[starts-with(@class, "title-number")])[${index}]
        ${chapter_text}    Remove String    ${chapter_text}    ${chapter_number}
        ${chapter_text}    Strip String    ${chapter_text}
        ${status}    Run Keyword And Return Status    Should Be Equal As Strings    ${chapter_text}    ${chapter}
    END
    [Return]    ${status}

Check If Chapter Found In Content Feed
    [Arguments]    ${chapter}
    ${status}    Set Variable    False
    ${chapters_in_feed}    Get Element Count    xpath=.//*[@class="content-feed"]//*[@class="title-zero" and text()="${chapter}"]
    ${status}    Run Keyword And Return Status    Should Be True    ${chapters_in_feed} > 0
    [Return]    ${status}

Chapter Content Should Not Be Visible For Students
    [Arguments]    ${chapter}
    ${status}    Check If Chapter Found In TOC    ${chapter}
    Should Be Equal As Strings    ${status}    False
    ${status}    Check If Chapter Found In Content Feed    ${chapter}
    Should Be Equal As Strings    ${status}    False

*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Resource          ../dashboard/resources/resources.robot
Suite Setup       Create Feeds For Licenses
Test Teardown     Close Browser

*** Test Cases ***
Check Only Licensed Content Items Are Shown To User
    [Tags]    regression_DRAFT    license     9e2b46a6-9bd6-11e9-a2a3-2a2ae2dbcce4
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1}    ${USER_PASSWORD_STUDENT_1}    ${NAME_FOR_LICENSES}    ${ACCESS_KEY_FOR_LICENSES}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Chapters In TOC

Check Only Licensed Digibook Is Shown To Teacher (Verso 1)
    [Tags]    regression_DRAFT    license     9e2b46a6-9bd6-11e9-a2a3-2a2ae2dbcce4
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Wordpress Site    ${USER_NAME_TEACHER_LICENSE_1}    ${USER_PASSWORD_TEACHER_LICENSE_1}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open User Materials In School
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select School From Portal    ${DEFAULT_SCHOOL_LICENSE}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Have License For    ${DEFAULT_METHOD_FOR_LICENSES_VERSO}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Don't Have License For    ${DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Don't Have License For    ${DEFAULT_METHOD_FOR_LICENSES_GEOIDI}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_LICENSES_VERSO}    ${FEED_TYPE_PUBLISHER}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Kokeet

Check Only Licensed Content Is Shown To Teacher (On the Go 1, Geoidi, FyKe 7-9)
    [Tags]    regression_DRAFT    license     9e2b46a6-9bd6-11e9-a2a3-2a2ae2dbcce4
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Wordpress Site    ${USER_NAME_TEACHER_LICENSE_1}    ${USER_PASSWORD_TEACHER_LICENSE_1}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open User Materials In School
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select School From Portal    ${DEFAULT_SCHOOL}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Don't Have License For    ${DEFAULT_METHOD_FOR_LICENSES_VERSO}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Have License For    ${DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Have License For    ${DEFAULT_METHOD_FOR_LICENSES_GEOIDI}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_LICENSES_ON_THE_GO}    ${FEED_TYPE_PUBLISHER}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Digiopetusmateriaali
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    etu: Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    etu: Oppilaan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Kokeet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Welcome
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Digikirja
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    REFERENCE SECTION
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Copyright-tiedot
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Go To My Page
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_LICENSES_GEOIDI}    ${FEED_TYPE_PUBLISHER}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Digiopetusmateriaali
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    etu: Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Kokeet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Go To My Page
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select Method With Method URL    ${DEFAULT_METHOD_FOR_LICENSES}    ${FEED_TYPE_PUBLISHER}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Digiopetusmateriaali (OPS 2016)
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    etu: FyKe 7-9 Fysiikka (OPS 2016) Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Kokeet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Digikirja
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Yleistä digikirjasta
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Liitteet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Be Empty    Copyright-tiedot
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Check Subchapter Contents

Check Only Licensed Digibook Is Shown To Student (Verso 1)
    [Tags]    regression_DRAFT    license     9e2b46a6-9bd6-11e9-a2a3-2a2ae2dbcce4
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Wordpress Site    ${USER_NAME_STUDENT_LICENSE_2}    ${USER_PASSWORD_STUDENT_LICENSE_2}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open User Materials In School
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select School From Portal    ${DEFAULT_SCHOOL_LICENSE}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Join Feed    ${NAME_FOR_LICENSES_VERSO}    ${ACCESS_KEY_FOR_LICENSES_VERSO}

Check Only Licensed Content Is Shown To Student (On the Go 1, Geoidi, FyKe 7-9)
    [Tags]    regression_DRAFT    license     9e2b46a6-9bd6-11e9-a2a3-2a2ae2dbcce4
    ${is_license_on}    Check If Environment Has License On
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Login Wordpress Site    ${USER_NAME_STUDENT_LICENSE_1}    ${USER_PASSWORD_STUDENT_LICENSE_1}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Open User Materials In School
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Select School From Portal    ${DEFAULT_SCHOOL}
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Join Feed    ${NAME_FOR_LICENSES}    ${ACCESS_KEY_FOR_LICENSES}
    SeleniumLibrary.Capture Page Screenshot
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Digiopetusmateriaali (OPS 2016)
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    etu: FyKe 7-9 Fysiikka (OPS 2016) Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Kokeet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Go To My Page
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Join Feed    ${NAME_FOR_LICENSES_ON_THE_GO}    ${ACCESS_KEY_FOR_LICENSES_ON_THE_GO}
    SeleniumLibrary.Capture Page Screenshot
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Digiopetusmateriaali
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    etu: Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    etu: Oppilaan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Kokeet
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Go To My Page
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Join Feed    ${NAME_FOR_LICENSES_GEOIDI}    ${ACCESS_KEY_FOR_LICENSES_GEOIDI}
    SeleniumLibrary.Capture Page Screenshot
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Digiopetusmateriaali
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    etu: Opettajan aineisto
    Run Keyword If    '${is_license_on}'=='${TRUE}'
    ...    Chapter Content Should Not Be Visible For Students    Kokeet

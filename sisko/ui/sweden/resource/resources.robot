*** Keywords ***
Login To SU Site
    [Arguments]   ${user}    ${password}
    Wait Until Element Is Enabled    username
    Set Focus To Element    username
    Input Text    username    ${user}
    Wait Until Element Is Enabled    password
    Set Focus To Element    password
    Input Password    password    ${password}
    Click Element On Page    xpath=.//*[@id='loginButton_0']
    Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=.//*[@id="mypage-button"]

Add Teacher Feed SU
    [Arguments]    ${method_to_add}    ${date}=off    ${feed_origin}=${FEED_TYPE_PUBLISHER}    ${new_name}=${EMPTY}    ${add_students_in}=${ADD_STUDENTS_DIALOG}
    Run Keyword If    '${feed_origin}'=='${FEED_TYPE_PUBLISHER}'
    ...    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="${feed_origin}"]//*[@class= "publisher-feed-title" and normalize-space(text())="${method_to_add}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Run Keyword If    '${feed_origin}'=='${FEED_TYPE_TEACHER}'
    ...    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="${feed_origin}"]//*[@class="teacher-feed-title" and normalize-space(text())="${method_to_add}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "create-teacher-feed-button")]
    Wait Until Element Is Enabled    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    Run Keyword If    '${date}'!='off' and '${new_name}'!='${EMPTY}'    Add Date To Feed Name    ${new_name}
    Run Keyword If    '${date}'!='off' and '${new_name}'=='${EMPTY}'    Add Date To Feed Name    ${method_to_add}
    Run Keyword If    '${date}'=='off' and '${new_name}'!='${EMPTY}'    Add New Feed Name    ${new_name}
    ${method_name}    Get Value    xpath=.//app-create-teacher-feed-dialog//*[@id="teacher-feed-form"]//input[@name="title"]
    Click Element On Page    xpath=.//*[@id="teacher-feed-form"]//*[@id="mat-input-1"]
    Click Element On Page    xpath=.//*[@id="teacher-feed-form"]//*[@id="mat-input-1"]//option[@value="${DEFAULT_GRADE}"]
    Click Element On Page    xpath=.//*[@id="teacher-feed-form"]//*[@id="mat-input-2"]
    Click Element On Page    xpath=.//*[@id="teacher-feed-form"]//*[@id="mat-input-2"]//option[@value="${DEFAULT_SCHOOL_YEAR}"]
    Run Keyword If    '${add_students_in}'=='${ADD_STUDENTS_FEED}'    Click Element On Page    xpath=.//*[@id="create-teacher-feed-button"]
    Run Keyword If    '${add_students_in}'=='${ADD_STUDENTS_FEED}'    Wait Until Keyword Succeeds    20s    1s
    ...    Wait Until Element Is Visible
    ...    xpath=.//app-method-feeds//div[@class="item-content"]//*[@class="feed-text-content ng-star-inserted"]//*[@class="teacher-top-content"]//*[@class= "teacher-feed-title" and normalize-space(text())="${method_name}"]
    Run Keyword If    '${add_students_in}'=='${ADD_STUDENTS_FEED}'    Add Students In Feed    ${method_name}
    Run Keyword If    '${add_students_in}'=='${ADD_STUDENTS_DIALOG}'    Click Element On Page    xpath=.//*[@id="create-and-add-button"]
    Run Keyword If    '${add_students_in}'=='${ADD_STUDENTS_DIALOG}'    Add Students In Dialog    ${method_name}
    [Return]    ${method_name}

Add Students In Feed
    [Arguments]    ${method_name}
    Click Element On Page
    ...    xpath=.//app-method-feeds//div[@feed-type="${FEED_TYPE_TEACHER}"]//*[@class="teacher-feed-title" and normalize-space(text())="${method_name}"]/ancestor::div/following-sibling::button[starts-with(@id, "feed-")]
    Click Element On Page    xpath=.//*[@class="mat-menu-content"]//button[starts-with(@id, "feed-") and contains(@id, "modify-feed-button")]
    Wait Until Element Is Enabled    xpath=.//div[contains(@class, "general-info")]//*[text()="${method_name}"]
    Click To Add Students
    Choose License
    Enter Student Information    ${method_name}

Add Students In Dialog
    [Arguments]    ${method_name}
    Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Enabled    xpath=.//div[contains(@class, "general-info")]//*[text()="${method_name}"]
    Click To Add Students
    Choose License
    Enter Student Information    ${method_name}

Create Teacher Feed For Swedish Tests
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_TEACHER_1_SWEDEN}    ${USER_PASSWORD_TEACHER_1_SWEDEN}
    ${feed_name}    Add Teacher Feed SU    ${DEFAULT_METHOD_SU}    date_on    ${FEED_TYPE_PUBLISHER}    Testing country differences    ${ADD_STUDENTS_FEED}
    Set Suite Variable    ${TEACHER_FEED_NAME}    ${feed_name}
    Close Browser

Retry Exercise
    ${status}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="retry-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="retry-button"]

Restart Exercise
    ${status}    Run Keyword And Return Status    Page Should Contain Element    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="restart-button"]
    Run Keyword If    '${status}'=='True'    Click Element On Page    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="restart-button"]

Click To Add Students
    Click Element On Page    xpath=.//*[@class="section-container students"]//div[@class="add-user-button" and normalize-space(string())="Lägg till elev till grupp"]

Click To Add Teachers
    Click Element On Page    xpath=.//*[@class="section-container teachers"]//div[@class="add-user-button" and normalize-space(string())="Lägg till lärare till grupp"]

Close Warning Dialog
    Wait Until Element Is Enabled    xpath=.//app-warning-dialog
    Click Element On Page    xpath=.//app-warning-dialog//button[@class="s-modal__close"]

Click No Button In Warning Dialog
    Wait Until Element Is Enabled    xpath=.//app-warning-dialog
    Click Element On Page    xpath=(.//app-warning-dialog//div[@class="s-modal__buttons"])//button[1]

Click Yes Button In Warning Dialog
    Wait Until Element Is Enabled    xpath=.//app-warning-dialog
    Click Element On Page    xpath=(.//app-warning-dialog//div[@class="s-modal__buttons"])//button[2]

Choose License
    Wait Until Element Is Enabled    xpath=(.//choose-license//table-row)[2]//td[@class="circle"]
    Click Element On Page    xpath=(.//choose-license//table-row)[2]//td[@class="circle"]
    Wait Until Element Is Enabled    xpath=(.//choose-license//table-row)[2]//td[@class="circle"]/div/div/i
    Click Element On Page    xpath=.//add-student-to-group-modal//div[@class="button-holder"]//button[text()="Nästa"]

Enter Student Information
    [Arguments]    ${method_name}
    Wait Until Element Is Enabled    xpath=.//add-student-to-group-modal//h2[@class="modal-title" and contains(text(), "${method_name}")]
    Wait Until Element Is Enabled    xpath=(.//app-input[@label="E_MAIL"]//input)[1]
    Set Focus To Element    xpath=(.//app-input[@label="E_MAIL"]//input)[1]
    Press Keys    xpath=(.//app-input[@label="E_MAIL"]//input)[1]    ${USER_NAME_STUDENT_3}
    Click Element On Page    xpath=(.//button[normalize-space(text())="Sök"])[1]
    Wait Until Element Is Enabled    xpath=.//div[@class="info"]//button[normalize-space(text())="Lägg till"]
    Click Element On Page    xpath=.//div[@class="info"]//button[normalize-space(text())="Lägg till"]
    Wait Until Element Is Not Visible    xpath=.//div[@class="info"]//button[normalize-space(text())="Lägg till"]
    Wait Until Element Is Enabled    xpath=.//div[@class="licenses"]//preview-remove-item/*[@class="user-prev"]
    ${user_info}    Get Text    xpath=.//div[@class="licenses"]//preview-remove-item/*[@class="user-prev"]
    Should Contain    ${user_info}    siskoautomation.student003

Do OQLR Exercise
    ${textarea_visible}    Check If Textarea Visible In OQLR
    Should Be Equal As Strings    ${textarea_visible}    True
    Submit Exercise
    ${textarea_visible}    Check If Textarea Visible In OQLR
    Should Be Equal As Strings    ${textarea_visible}    False

Check If Textarea Visible In OQLR
    ${status}    Run Keyword And Return Status    Wait Until Element Is Enabled    xpath=.//eb-editing//textarea
    [Return]    ${status}

Do Exercise Correctly The First Time
    Wait Until Element Is Enabled    xpath=.//eb-cloze-content-block//eb-cloze-edit/input
    ${answers}    Get Element Count    xpath=.//eb-cloze-content-block//eb-cloze-edit/input
    ${correct_answers}    Create List    Elle    Elle    elle    il    elle
    ${index}    Set Variable    0
    FOR    ${answer}    IN    @{correct_answers}
        ${index}    Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//eb-cloze-content-block//eb-cloze-edit/input)[${index}]
        Input Text    xpath=(.//eb-cloze-content-block//eb-cloze-edit/input)[${index}]    ${answer}
    END
    Submit Exercise
    ${disabled}    Get Element Attribute    xpath=.//*[@id="retry-button"]    disabled
    Should Be Equal As Strings    ${disabled}    true
    FOR    ${index}    IN RANGE    0    ${answers}
        ${index}        Evaluate    ${index}+1
        ${readonly}    Get Element Attribute    xpath=(.//eb-cloze-content-block//eb-cloze-edit/input)[${index}]    readonly
        Should Be Equal As Strings    ${readonly}    true
        ${answer_style}    Get Element Attribute    xpath=(.//eb-cloze-content-block//eb-cloze-edit)[${index}]    class
        Should Contain    ${answer_style}    eb-correct
    END

Check Combi Content Exercise For Teacher
    ${button_visibility_submit}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="submit-button"]
    Run Keyword If    '${button_visibility_submit}'=='False'
    ...    Retry Exercise
    Run Keyword If    '${button_visibility_submit}'=='False'
    ...    Restart Exercise
    Wait Until Element Is Enabled    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="submit-button"]
    ${submit_buttons}    Get Element Count    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="submit-button"]
    FOR    ${index}    IN RANGE     0       ${submit_buttons}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${index}]//*[@id="submit-button"]
        Check Submit Button For Teacher    ${index}
    END

Check Open Question Long Response For Teacher
    ${button_visibility_submit}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled    xpath=.//app-module-content-documents//app-module-content-buttons//*[@id="submit-button"]
    Run Keyword If    '${button_visibility_submit}'=='False'
    ...    Retry Exercise
    Run Keyword If    '${button_visibility_submit}'=='False'
    ...    Restart Exercise

Check Submit Button For Teacher
    [Arguments]    ${button}
    FOR    ${index}    IN RANGE     0       4
        ${index}        Evaluate    ${index}+1
        Run Keyword If    ${index}==1    Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==1    Click Element On Page    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==1    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==1    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
        Run Keyword If    ${index}==2    Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==2    Click Element On Page    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==2    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==2    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
        Run Keyword If    ${index}==3    Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==3    Click Element On Page    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==3    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==3    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
        Run Keyword If    ${index}==4    Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
        Run Keyword If    ${index}==4    Click Element On Page    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
        Run Keyword If    ${index}==4    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==4    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==5    Wait Until Element Is Enabled    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==5    Click Element On Page    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="submit-button"]
        Run Keyword If    ${index}==5    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="retry-button"]
        Run Keyword If    ${index}==5    Page Should Not Contain Element    xpath=(.//app-module-content-documents//app-module-content-buttons)[${button}]//*[@id="restart-button"]
    END

Do Exercise Exercises As Subquestions Turn In Together
    Wait Until Element Is Enabled    xpath=.//app-module-content-documents
    Retry Exercise
    Restart Exercise
    Sleep    2
    Click Element On Page    xpath=(.//eb-checkbox)[1]
    Click Element On Page    xpath=.//*[@id="submit-button"]
    Close Warning Dialog
    Click Element On Page    xpath=.//*[@id="submit-button"]
    Click No Button In Warning Dialog
    Click Element On Page    xpath=.//*[@id="submit-button"]
    Click Yes Button In Warning Dialog
    Retry Exercise
    Restart Exercise

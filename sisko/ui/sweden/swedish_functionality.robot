*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Suite Setup       Create Teacher Feed For Swedish Tests
Test Teardown     Close Browser

*** Test Cases ***
Test Case 1
    [Tags]    sweden    9e2bac68-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_STUDENT_1_SWEDEN}    ${USER_PASSWORD_STUDENT_1_SWEDEN}
    Select Feed    ${TEACHER_FEED_NAME}    ${FEED_TYPE_TEACHER}
    Open Content Item     Exercises    Exercises    4
    Do OQLR Exercise

Test Case 2 (Exercise Done Correctly The First Time Doesn't Give Option To Modify)
    [Tags]    sweden    9e2bac68-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_STUDENT_2_SWEDEN}    ${USER_PASSWORD_STUDENT_2_SWEDEN}
    Select Feed    ${TEACHER_FEED_NAME}    ${FEED_TYPE_TEACHER}
    Open Content Item     Exercises    Extra Close combi exercises    12
    Do Exercise Correctly The First Time

Test Case 3 (Exercise Not OQLR)
    [Tags]    sweden    9e2bac68-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_TEACHER_1_SWEDEN}    ${USER_PASSWORD_TEACHER_1_SWEDEN}
    ${method_name}    Select Method With Method URL    ${DEFAULT_METHOD_SU}    ${FEED_TYPE_PUBLISHER}
    Open Content Item     Exercises    Exercises    Submit per subquestion 1
    Check Combi Content Exercise For Teacher

Test Case 4 (Open Question Long Response - OQLR)
    [Tags]    sweden    9e2bac68-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_TEACHER_1_SWEDEN}    ${USER_PASSWORD_TEACHER_1_SWEDEN}
    ${method_name}    Select Method With Method URL    ${DEFAULT_METHOD_SU}    ${FEED_TYPE_PUBLISHER}
    Open Content Item     Exercises    Exercises    4
    Check Open Question Long Response For Teacher

Test Case 5 (all sent together)
    [Tags]    sweden    9e2bac68-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${LOGIN_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login To SU Site    ${USER_NAME_TEACHER_1_SWEDEN}    ${USER_PASSWORD_TEACHER_1_SWEDEN}
    ${method_name}    Select Method With Method URL    ${DEFAULT_METHOD_SU}    ${FEED_TYPE_PUBLISHER}
    Open Content Item     Exercises    Exercises    Exercises as subquestions turn in together
    Do Exercise Exercises As Subquestions Turn In Together

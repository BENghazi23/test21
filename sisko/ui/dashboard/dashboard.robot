*** Settings ***
Resource          ../common/common_resources.robot
Resource          ../exercises/resource/resources.robot
Resource          resources/resources.robot
Suite Setup       Create Feed For Dashboard
Test Teardown     Custom Teardown

*** Test Cases ***
Teacher Can See The Students Who Are Joined To Feed
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Name Appears In Student List   STUDENT_1_DASHBOARD
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_2_DASHBOARD}    ${USER_PASSWORD_STUDENT_2_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Name Appears In Student List   STUDENT_1_DASHBOARD     STUDENT_2_DASHBOARD

No Teacher Name Should Be Visible In Dashboard
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_2_DASHBOARD}    ${USER_PASSWORD_TEACHER_2_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Name Does Not Appear    TEACHER_2_DASHBOARD
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Name Does Not Appear   TEACHER_1_DASHBOARD     TEACHER_2_DASHBOARD
    Check Name Appears In Student List   STUDENT_1_DASHBOARD

Student and Teacher Enter Correct Dashboard With Correct Information
    [Tags]     regression     dashboard
    Login Kampus     ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}
    Can Enter Only Previous Dashboard From Home Page    ${NAME_FOR_DASHBOARD}    ${USER_NAME_STUDENT_1_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus   ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}
    Can Enter Only Previous Dashboard From Home Page    ${NAME_FOR_DASHBOARD}    ${USER_NAME_STUDENT_1_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}

Check Exercise Pagination
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Pagination
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Pagination

Check Assessment View
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Assessment View

Check Feedback Functionality
    [Tags]    regression    dashboard
    Set Test Variable    ${exercise}    Fill in - Open Question Long Answer matikkaeditorilla
    Set Test Variable    ${exercise_draft}    UI: Open Question Long Response - Explanation Of Solution
    Set Test Variable    @{feedbacks}    1=some text here v1    2=some text here v2    3=some text here v3
    Student Does Exercise For Feedback
    Teacher Gives And Modifies Feedback To Student
    Student Checks That Feedback Exists
    Teacher Saves Feedback As Draft
    Student Cannot See Draft Feedback
    Teacher Submits Draft Feedback
    Student Can See The Saved Feedback

Check Teacher Reassessing An Exercise
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Content Item    Content testing    OQSR and OQLR    UI: Open Question Long Response - Explanation Of Solution
    Do Exercise UI: Open Question Long Response - Explanation Of Solution - For Teacher Assessment
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    ${answer_colors}    Get Answer Colors
    Set Test Variable    ${ANSWER_COLORS}    ${answer_colors}
    Get Exercise Status    UI: Open Question Long Response - Explanation Of Solution
    Exercise Status Should Be Correct In Dashboard    UI: Open Question Long Response - Explanation Of Solution    ${ANSWER_COLORS.auto_assessed}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Exercise Status Should Be Correct In Dashboard    UI: Open Question Long Response - Explanation Of Solution    ${ANSWER_COLORS.auto_assessed}
    Open Assessment For Exercise    UI: Open Question Long Response - Explanation Of Solution
    Select Teacher Assessment    correct
    Close Assessment View
    Exercise Status Should Be Correct In Dashboard    UI: Open Question Long Response - Explanation Of Solution    ${ANSWER_COLORS.correct}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Exercise Status Should Be Correct In Dashboard    UI: Open Question Long Response - Explanation Of Solution    ${ANSWER_COLORS.correct}

Check Dashboard Shows The Latest Attempt Of Exercise
    [Tags]    regression    dashboard
    Login Kampus And Join Feed   ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}   ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Do Exercise Incorrect
    Open Dashboard
    Open Dashboard TOC    Content testing
    ${answer_colors}    Get Answer Colors
    Set Test Variable    ${ANSWER_COLORS}    ${answer_colors}
    Exercise Status Should Be Correct In Dashboard    UI: Multi choice    ${ANSWER_COLORS.incorrect}
    Go Back
    Do Exercise Correct
    Open Dashboard
    Open Dashboard TOC    Content testing
    Exercise Status Should Be Correct In Dashboard    UI: Multi choice    ${ANSWER_COLORS.correct}
    Go Back
    Do Self Assessment Exercise
    Open Dashboard
    Open Dashboard TOC    Content testing
    Exercise Status Should Be Correct In Dashboard    Choice - itsearviointitehtävä    ${ANSWER_COLORS.auto_assessed}
    Go Back
    Start Over Choice - Itsearviointitehtävä
    Open Dashboard
    Open Dashboard TOC    Content testing
    Exercise Status Should Be Correct In Dashboard    Choice - itsearviointitehtävä    ${ANSWER_COLORS.unanswered}

Check Single Student View
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_2_DASHBOARD}    ${USER_PASSWORD_STUDENT_2_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Verify Single Student View

Check Dashboard TOC And Exercise Points
    [Tags]    regression    dashboard
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Close Browser
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}    ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Check Dashboard TOC And Points In Exercises

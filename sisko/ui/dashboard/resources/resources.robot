*** Keywords ***
Teacher View Have To Be Right
    [Arguments]      # @{students_names}=${EMPTY}
    # Check Dashboard TOC
    Check Exercise Pass-level     10
    Check Exercise Pass-level     44
    Check Exercise Pass-level     80
    # Check Name Appears In Student List       @{students_names}

Student View Have To Be Right
    [Arguments]     ${student_name}=${EMPTY}
    #Check Dashboard TOC
    Check Exercise Pass-level     0
    Check Exercise Pass-level     30
    Check Exercise Pass-level     99

Error Message For Dashboard
    [Arguments]        ${visibility}
    Run Keyword If    ${visibility}==True    Wait Until Element Is Visible    xpath=.//*[@class="dialog-content"]
    Run Keyword If    ${visibility}==True    Click Element On Page   xpath=.//app-dashboard-warning-dialog//*[@id="share-accesskey-close"]

Check Exercise Pass-level
    [Arguments]     ${percents}
    Wait Until Element Is Enabled   xpath=.//*[@class="pass-level-slider-percent-container"]
    Handle Slider     ${percents}
    Page Should Contain    ${percents}

Can Enter Only Previous Dashboard From Home Page
    [Arguments]     ${feed_name}     ${student_name}    ${access_key}
    Element Should Not Be Visible    xpath=.//*[@id="dashboard-impl-button"]
    Join Feed       ${feed_name}    ${access_key}
    Open Dashboard
    Element Should Not Be Visible    xpath=.//*[@class="dialog-content"]
    Wait Until Element Is Enabled    xpath=.//app-dashboard-menu//*[starts-with(@class, "dropdown-sidebar")]

Do Exercise Correct
    Open Content Item    Content testing    Choice exercises    UI: Multi choice
    Do Exercise UI: Multi Choice Correctly
    #Close Content Item
    Return To Content Feed From Content Item View

Do Exercise Incorrect
    Open Content Item    Content testing    Choice exercises    UI: Multi choice
    Do Exercise UI: Multi Choice Incorrectly
    #Close Content Item
    Return To Content Feed From Content Item View

Exercise Status Should Be Correct In Dashboard
    [Arguments]    ${exercise_title}    ${expected_status}
    ${status}    ${class}    Get Exercise Status    ${exercise_title}
    Run Keyword If    '${expected_status}'!='${ANSWER_COLORS.unanswered}'    Should Contain    ${status}    ${expected_status}
    ...    ELSE    Should Contain    ${class}    ${expected_status}

Take Info From Teacher Feed TOC
    Wait Until Element Is Enabled    xpath=.//*[@class="dtoc_feed-title"]//*[@class="feed-title"]
    ${toc_header}    Get Text    xpath=.//*[@class="dtoc_feed-title"]//*[@class="feed-title"]
    ${chapters}    Get Element Count    xpath=.//app-dashboard-menu//mat-accordion
    Set Suite Variable    ${CHAPTER_COUNT_TEACHER_FEED}    ${chapters}
    Set Suite Variable    ${TOC_HEADER_TEACHER_FEED}    ${toc_header}

Take Info From Dashboard TOC
    Wait Until Element Is Enabled    [Arguments]
    ${toc_header}    Get Text    xpath=.//*[@class="dtoc_feed"]//*[@class="feed-title"]
    ${chapters}    Get Element Count    xpath=.//app-dashboard-menu//mat-accordion
    Set Suite Variable    ${CHAPTER_COUNT_DASHBOARD}    ${chapters}
    Set Suite Variable    ${TOC_HEADER_TEACHER_DASHBOARD}    ${toc_header}

Take TOC Exercise Percent
    Wait Until Element Is Visible    xpath=.//*[@id="mat-expansion-panel-header-4"]
    ${results}      Get Text     xpath=.//*[@id="mat-expansion-panel-header-4"]//*[@class="mat-content"]//mat-panel-description
    [Return]      ${results}

Take Exercise Circle Color
    Wait Until Element Is Visible    xpath=.//*[@id="mat-expansion-panel-header-4"]
    Click Element On Page       xpath=.//*[@id="mat-expansion-panel-header-4"]
    ${results}      Get Text     xpath=.//*[@id="mat-expansion-panel-header-4"]//*[@class="mat-content"]//mat-panel-description
    [Return]      ${results}

Check Exercise Circle Color
    [Arguments]     ${value}
    ${color}=   Take Exercise Circle Color
    Evaluate        ${color} == ${value}

Verify Scrollbar Works With One Exercise
    Check Exercise Pass-level   10
    Check Exercise Circle Color     green

Check TOC Percent Of Exercise Chapter Is Changed
    [Arguments]     ${level_before}     ${up_or_down}
    ${level_after}     Take TOC Percents
    Run Keyword If      ${up_or_down} == "up"      Verify TOC Percent Has Gotten Bigger     ${level_after}   ${level_before}
    Run Keyword If      ${up_or_down} == "down"     Verify TOC Percent Has Gotten Smaller   ${level_after}   ${level_before}

Verify TOC Percent Has Gotten Bigger
    [Arguments]     ${after}     ${before}
    Evaluate 	${after}  <  ${before}

Verify TOC Percent Has Gotten Smaller
    [Arguments]     ${after}     ${before}
    Evaluate 	${after}  >  ${before}

TOC Percents Should Be Higher After Doing Exercise
    ${level_before}=     Take TOC Exercise Percent
    Go Back
    Open Content Item        Automation testing    Automation testing    Unscored: Choice with many possible answers
    Do Exercise Unscored: Choice With Many Possible Answers
    Open Dashboard
    ${level_now}=     Take TOC Exercise Percent
    Verify TOC Percent Has Gotten Bigger    ${level_before}     ${level_now}
    Go Back

TOC Percents Should Be Smaller After Undoing Exercise
    ${level_before}=     Take TOC Exercise Percent
    Go Back
    Open Content Item        Automation testing    Automation testing    Unscored: Choice with many possible answers
    Undo Exercise Unscored: Choice With Many Possible Answers
    Open Dashboard
    ${level_now}=     Take TOC Exercise Percent
    Verify TOC Percent Has Gotten Smaller    ${level_before}     ${level_now}

Verify Color Of Exercise Circle
    [Arguments]     ${color_should_be}
    ${value}     Get      locator
    ${boolean}     Evaluate Boolean   ${value} == ${boolean}
    [Return]    ${boolean}

Verify Single Student View
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="student-container"]
    ${students_all}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    ${student_name}    Get Text    xpath=(.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")])[1]
    Click Element On Page    xpath=(.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")])[1]
    Wait Until Element Is Enabled    xpath=.//*[@id="select-user-button"]
    Wait Until Element Is Not Visible    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    ${students_visible}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    Should Be Equal As Numbers    ${students_visible}    0
    Wait Until Element Is Enabled    xpath=.//*[@id="select-user-button" and normalize-space(text())="${student_name}"]
    Click Element On Page    xpath=.//*[@id="select-user-button"]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p
    ${student_options_in_dropdown}    Get Element Count    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p
    Click Element On Page    xpath=(.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p)[1]
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    ${students_visible}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    Should Be Equal As Numbers    ${students_visible}    ${students_all}
    #
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="student-container"]
    ${student_name}    Get Text    xpath=(.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")])[1]
    Click Element On Page    xpath=(.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")])[1]
    Wait Until Element Is Enabled    xpath=.//*[@id="select-user-button"]
    Wait Until Element Is Not Visible    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    ${students_visible}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    Should Be Equal As Numbers    ${students_visible}    0
    Wait Until Element Is Enabled    xpath=.//*[@id="select-user-button" and normalize-space(text())="${student_name}"]
    Click Element On Page    xpath=.//*[@id="select-user-button"]
    ${student_name_selected}    Get Text    xpath=(.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p)[${student_options_in_dropdown}]
    Click Element On Page    xpath=(.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p)[${student_options_in_dropdown}]
    ${students_visible}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    Should Be Equal As Numbers    ${students_visible}    0
    #
    Click Element On Page    xpath=.//*[@id="select-user-button"]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p
    ${student_options_in_dropdown}    Get Element Count    xpath=.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p
    Click Element On Page    xpath=(.//*[starts-with(@class, "user-selection-tooltip")]//*[starts-with(@class, "content")]/p)[1]
    Wait Until Element Is Enabled    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    ${students_visible}    Get Element Count    xpath=.//app-student-results//*[@class="student-container"]//*[contains(@class, "student-name")]
    Should Be Equal As Numbers    ${students_visible}    ${students_all}

Student Does Exercise For Feedback
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Content Item    Content testing    Exercises / interactions    Fill in - Open Question Long Answer matikkaeditorilla
    Do Exercise Fill In - Open Question Long Answer Matikkaeditorilla
    Close Browser

Teacher Gives And Modifies Feedback To Student
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise}
    Give Feedback    ${feedbacks}[0]
    ${feedback_status}    Get Feedback Status
    Submit Feedback
    ${feedback_status}    Get Feedback Status
    Close Assessment View
    Exercise Should Have Feedback    ${exercise}
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise}
    Modify Feedback    ${feedbacks}[1]
    Submit Feedback
    Close Browser

Student Checks That Feedback Exists
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Exercise Should Have Feedback    ${exercise}
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise}
    Check Student Feedback    ${feedbacks}[1]
    Close Browser

Teacher Saves Feedback As Draft
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise_draft}
    #Modify Feedback    ${feedbacks}[2]
    Give Feedback    ${feedbacks}[2]
    Save Feedback As Draft
    Exercise Should Not Have Feedback    ${exercise_draft}
    Close Browser

Student Cannot See Draft Feedback
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Exercise Should Not Have Feedback    ${exercise_draft}
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise_draft}
    Check That No Student Feedback Exists
    Close Browser

Teacher Submits Draft Feedback
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1_DASHBOARD}    ${USER_PASSWORD_TEACHER_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise_draft}
    Save Feedback In Feedback Confirmation Window
    Exercise Should Have Feedback    ${exercise_draft}
    Close Browser

Student Can See The Saved Feedback
    Login Kampus And Join Feed    ${USER_NAME_STUDENT_1_DASHBOARD}    ${USER_PASSWORD_STUDENT_1_DASHBOARD}    ${NAME_FOR_DASHBOARD}   ${ACCESS_KEY_FOR_DASHBOARD}
    Open Dashboard
    Open Dashboard TOC    Content testing    OQSR and OQLR
    Exercise Should Have Feedback    ${exercise_draft}
    Wait Until Keyword Succeeds    20s    50ms    Open Exercise In Assessment View    ${exercise_draft}
    Check Student Feedback    ${feedbacks}[2]
    Close Browser

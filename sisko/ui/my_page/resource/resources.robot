*** Variables ***
@{texts_Suomi}             Suomi    Ryhmäni    Kirjasto    Kieli:      Omat tiedot     Ota yhteyttä     Kirjaudu ulos
@{found_texts_Suomi}
@{texts_English}           English    My groups    Library    Language:      User Profile     Contact us      Log out
@{found_texts_English}
@{texts_Svenska}           Svenska    Mina grupper    Mina läromedel    Språk:       Gå till portalen för administration      Kontakta oss      Logga ut
@{found_texts_Svenska}

*** Keywords ***
Check Personal Information For User
    [Arguments]    ${logged_in_user}
    ${user_full_name}    Catenate    ${USER_FIRST_NAME_${logged_in_user}}    ${USER_LAST_NAME_${logged_in_user}}
    Open User Information View
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Get Text    xpath=.//app-user-info-tooltip//*[@class="name-holder"]
    ${logged_in_user_name_full}    Strip String    ${logged_in_user_name_full}
    Should Contain    ${logged_in_user_name_full}    ${user_full_name}
    Open User Information View

Check Localization
    Wait Until Element Is Enabled    xpath=.//app-method-feeds//*[@feed-type="publisher"]
    Open User Information View
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip
    ${language}    Get Text    xpath=.//*[@id="select-language-button"]
    Click Element On Page    xpath=.//*[@id="select-language-button"]
    Wait Until Element Is Enabled    xpath=.//*[@class="lang-select"]//*[starts-with(@id, "language_selection_")]
    ${language_options}    Get Element Count    xpath=.//*[starts-with(@id, "language_selection_")]
    @{language_texts}    Create List
    @{language_option_texts}    Create List
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@id, "language_selection_")]
    FOR    ${index}    IN RANGE     0       ${language_options}
        ${index}        Evaluate    ${index}+1
        Wait Until Element Is Enabled    xpath=(.//*[starts-with(@id, "language_selection_")])[${index}]
        ${language_option_text}        Get Text    xpath=(.//*[starts-with(@id, "language_selection_")])[${index}]
        ${language_option_text}        Strip String    ${language_option_text}
        Click Element On Page    xpath=(.//*[starts-with(@id, "language_selection_")])[${index}]
        Wait Until Element Is Not Visible    xpath=(.//*[starts-with(@id, "language_selection_")])[${index}]
        Sleep    1
        Check Localized Texts    ${language_option_text}
        Click Element On Page    xpath=.//*[@id="select-language-button"]
    END
    Click Element On Page    xpath=(.//*[starts-with(@id, "language_selection_")])[${language_options}]
    Open User Information View

Check Localized Texts
    [Arguments]    ${language_option_text}
    Append To List    ${found_texts_${language_option_text}}    ${language_option_text}
    Wait Until Element Is Enabled    xpath=(.//*[@class="feeds-title"]/*[@class="feed-title"])[1]
    Wait Until Element Is Enabled    xpath=(.//*[@class="feeds-title"]/*[@class="feed-title"])[2]
    Wait Until Element Is Enabled    xpath=.//app-user-info-tooltip//*[@class="lang-select"]/p
    Wait Until Element Is Enabled    xpath=.//*[@id="pi_link_userprofile"]
    Wait Until Element Is Enabled    xpath=.//*[@id="pi_link_feedback"]
    Wait Until Element Is Enabled    xpath=.//*[@id="pi_link_logout"]
    ${feed_teacher}    Get Text    xpath=(.//*[@class="feeds-title"]/*[@class="feed-title"])[1]
    Append To List    ${found_texts_${language_option_text}}    ${feed_teacher}
    ${feed_publisher}    Get Text    xpath=(.//*[@class="feeds-title"]/*[@class="feed-title"])[2]
    Append To List    ${found_texts_${language_option_text}}    ${feed_publisher}
    ${user_language}    Get Text    xpath=.//app-user-info-tooltip//*[@class="lang-select"]/p
    Append To List    ${found_texts_${language_option_text}}    ${user_language}
    ${user_profileinfo}    Get Text    xpath=.//*[@id="pi_link_userprofile"]
    Append To List    ${found_texts_${language_option_text}}    ${user_profileinfo}
    ${user_feedback}    Get Text    xpath=.//*[@id="pi_link_feedback"]
    Append To List    ${found_texts_${language_option_text}}    ${user_feedback}
    ${user_logout}    Get Text    xpath=.//*[@id="pi_link_logout"]
    Append To List    ${found_texts_${language_option_text}}    ${user_logout}
    Lists Should Be Equal    ${found_texts_${language_option_text}}    ${texts_${language_option_text}}

Switch Schools
    FOR    ${index}    IN RANGE    0    2
        Switch School
    END

Switch School
    ${status}    Check If Initial School Selection Appears
    Run Keyword If    '${status}'=='True'    Get Default School Name
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    ${school_before}    Get Text    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name")]
    ${school_before}    Strip String    ${school_before}
    Wait Until Element Is Enabled    xpath=.//*[@class="content-container"]
    ${contents_before}    Get Element Attribute    xpath=.//*[@class="content-container"]    innerHTML
    Click Element On Page    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    Wait Until Element Is Enabled    xpath=.//app-school-switching-dialog
    ${school_selected}    Get Text    xpath=(.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")])[1]
    Click Element On Page    xpath=(.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")])[1]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class, "change-school")]
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name") and text()="${school_selected}"]
    ${school_after}    Get Text    xpath=.//*[starts-with(@class,"school-or-user-detail")]//*[starts-with(@class,"school-name")]
    ${school_after}    Strip String    ${school_after}
    Wait Until Element Is Enabled    xpath=.//*[@class="content-container"]
    Wait Until Element Is Enabled    xpath=(.//*[@class="content-container"]//*[starts-with(@class, "feed-title")])[1]
    Wait Until Element Is Enabled    xpath=(.//*[@class="content-container"]//*[starts-with(@class, "feed-title")])[2]
    Wait For Condition    return document.readyState=="complete"
    #${contents_after}    Get Element Attribute    xpath=.//*[@class="content-container"]    innerHTML
    ###${contents_length_before}    Get Length    ${contents_before}
    ###${contents_length_after}    Get Length    ${contents_after}
    #Should Not Be Equal As Strings    ${contents_before}    ${contents_after}
    Wait Until Keyword Succeeds    10s    1s    Compare School Contents    ${contents_before}


Compare School Contents
    [Arguments]    ${contents_before}
    ${contents_after}    Get Element Attribute    xpath=.//*[@class="content-container"]    innerHTML
    Should Not Be Equal As Strings    ${contents_before}    ${contents_after}

Check If Initial School Selection Appears
    ${status}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled
    ...    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")]/parent::button
    [Return]    ${status}

Get Default School Name
    Wait Until Element Is Enabled
    ...    xpath=(.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")])[1]
    ${default_school_selected}    Get Text    xpath=(.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and not(normalize-space(text())="")])[1]
    Click Element On Page
    ...    xpath=.//app-school-switching-dialog//*[@class="school-selection"]//*[@class="school-selection-name" and normalize-space(text())="${default_school_selected}"]/parent::button

Check Library Button Functionality
    Scroll Page To Top
    Wait Until Element Is Enabled    xpath=.//*[starts-with(@class, "feed-containers")]
    ${status}    Run Keyword And Return Status
    ...    Wait Until Element Is Enabled    xpath=.//*[contains(@class, "show-library")]//*[text()[not(.="")]]
    Run Keyword If    '${status}'=='True'
    ...    Click Element On Page    xpath=.//*[contains(@class, "show-library")]//*[text()[not(.="")]]
    Wait For Condition    return document.readyState=="complete"
    Wait Until Keyword Succeeds    10s    1s    Wait Until Element Is Not Visible    xpath=.//*[contains(@class, "show-library")]//*[text()[not(.="")]]
    Wait Until Element Is Enabled    xpath=.//*[@id="publisherFeeds0"]
    ${element_position}    Execute JavaScript    var position = document.getElementById("publisherFeeds0").getBoundingClientRect(); return position;
    ${window_inner_height}    Execute JavaScript    var innerHeight = window.innerHeight; return innerHeight;
    Should Be True    ${element_position}[top] > 0
    Should Be True    ${element_position}[bottom] < ${window_inner_height}

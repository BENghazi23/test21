*** Settings ***
Resource          ../common/common_resources.robot
Resource          resource/resources.robot
Test Teardown     Custom Teardown

*** Test Cases ***
Check User Information For Teacher
    [Tags]    regression    my_page
    Login Kampus    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Check Personal Information For User    TEACHER_1_MY_PAGE
    Logout Sisko
    Close Browser

Check User Information For Student
    [Tags]    regression    my_page
    Login Kampus    ${USER_NAME_STUDENT_1_MY_PAGE}    ${USER_PASSWORD_STUDENT_1_MY_PAGE}
    Check Personal Information For User    STUDENT_1_MY_PAGE
    Logout Sisko
    Close Browser

Check Localization
    [Tags]    regression    my_page
    Login Kampus    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Check Localization
    Logout Sisko
    Close Browser

Check Switching Schools - logged through Kampus
    [Tags]    regression    my_page
    Login Kampus    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Switch Schools
    Logout Sisko
    Close Browser

Check Switching Schools - logged through portal
    [Tags]    quarantine    my_page    9e2baeb6-9bd6-11e9-a2a3-2a2ae2dbcce4
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Switch Schools
    Logout Sisko
    Close Browser

Check Switching Schools - logged through portal (twice)
    [Tags]    regression_2    my_page_2
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Switch Schools
    Logout Sisko
    Open Site    ${PORTAL_URL}    ${BROWSER}    ${BROWSER_FROM}
    Login Wordpress Site    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Open User Materials In School
    Select School From Portal    ${DEFAULT_SCHOOL}
    Switch Schools
    Logout Sisko
    Close Browser

Check Library Button Functionality
    [Tags]    regression    my_page
    Login Kampus    ${USER_NAME_TEACHER_1_MY_PAGE}    ${USER_PASSWORD_TEACHER_1_MY_PAGE}
    Check Library Button Functionality

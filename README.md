# Kampus Test Automation
Functional tests for Kampus

Tests done in Robot Framework (https://robotframework.org)

## Installation

#### Prerequisites
- Python 3
- PIP
- screen resolution 1920x1080

#### Installing Robot Framework

```
pip install robotframework
pip install robotframework-seleniumlibrary 
```

#### Installing webdrivers (Firefox and Chrome drivers)
Each time a browser is updated also the driver has to be updated.\
NOTE: Windows users change the linkpath according to your environment.\
**The linkpath must be within your system path.**

```
pip install webdrivermanager
webdrivermanager firefox chrome --linkpath /usr/local/bin
```
#### The following directories need to be in the system path
- the directory of Python executable
- the directory the **Scripts** subdirectory under the Python directory
- the directory where the web drivers are installed

## Running the tests

#### The flow of test runs (from top down)
- test script
- Robot Framework
- Robot Framework Selenium library
- Selenium (executable)
- web driver
- browser
- SUT (system under test)

#### Running tests locally (Best for development)

```
cd sisko
sh run_tests_sisko_smoke_local.sh
```

#### Running tests inside Docker (No visible browser)
Further details\
https://github.com/ppodgorsek/docker-robot-framework

```
sh run_docker_tests_sisko_smoke_local.sh
```

### Parameters in the running scripts

```
python -m robot --variable environment:local --variable browser:chrome --variable browser_from:local -i smoke .
```
parameter | meaning
--------- | -------
environment | the environment against which the tests are run (available environmts: local - developer's local environment, prod - production, dev - SP dev's, qa - SP kampusqa, su_dev - SU dev, su_qa - SU QA)
browser | the browser used to run the tests with (depends on the installed web drivers installed, usually Chrome and/ or Firefox)
browser_from | referes to whether the web driver is used from your local computer (local), other options is browserstack but due to the limited licenses Browserstack is reserved for running the test in the Jenkins jobs
-i | sort for include, refers to the tags in the tests that want to be run in the test run (in the example the tests taggged **smoke** are run)
. | refers to the directories from which the taggeed tests are searched (in the example **.** means that all the subdirectories of the current directory are searched for the tests to be run) 

## Example test case

```
Teacher Can See The Content Student Sees
    [Tags]    regression    feed
    Login Kampus And Join Feed    ${USER_NAME_TEACHER_1}    ${USER_PASSWORD_TEACHER_1}    ${NAME_FOR_FEED}    ${ACCESS_KEY_FOR_FEEDS}
    Turn Student View Filter On
    Check That No Teacher Material Is Visible In Student View Filter
    Check Filters
```

- test name: Teacher Can See The Content Student Sees
- test tags: **regression    feed**
- test steps (the keywords to be executed):
  - Login Kampus And Join Feed
  - Turn Student View Filter On
  - Check That No Teacher Material Is Visible In Student View Filter
  - Check Filters

#### Naming conventions
- the test names with camel cases
- the global variables with capitals (e.g.  ${USER_NAME_TEACHER_1} )
- the local variables with small letters (e.g ${element_text} )


## Variables

Variables for each environment can be found in the variable file:\
https://github.com/SanomaPro/sisko-test-automation/blob/master/sisko/common_resources/robot/variablefile.py

**NOTE:** Change the variables only under the **local** environment in order to avoid the sessions to clash with someone else's test users/ test runs.

## Locating elements in HTML

- element IDs (if applicable)
- XPath

## Test results

The tests are executed from the **sisko** directory where also the test results are saved.
- **report.html** (the overall results of the test execution - e.g the number of tests run, the test execution time, the number of passed and failed tests)
- **log.html** (the more specific test results where you can follow the test execution and its success/ failure test step by test step)
- **output.xml** (the XML file that is used as the input for the two above HTML files)

#### Common reasons for test failures
- application not functioning as expected
- test not up to date
- webdriver and browser incompatible (problems, check the versions of both)
- Robot Framework or Selenium library bugs
- elements in SUT not visible or are hidden under another element (overlay, thus cannot be clicked)
- element to which the action is directed is not ready yet, not fully loaded (ElementNotInteractableException)
- BrowserStack (remote browser) failures, e.g. webdriver crashing

## Best practises
Best practises when writing test cases in Robot Framework\
https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst

